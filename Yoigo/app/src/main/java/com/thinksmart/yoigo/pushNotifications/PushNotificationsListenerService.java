package com.thinksmart.yoigo.pushNotifications;

import android.content.Intent;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.thinksmart.app4onesdk.engine.utils.PushNotificationAction;
import com.thinksmart.yoigo.activities.MainActivity;

/**
 * Created by roberto.demiguel on 23/11/2016.
 */
public class PushNotificationsListenerService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Intent resultIntent = new Intent(this, MainActivity.class);
        PushNotificationAction pushNotificationAction = new PushNotificationAction(getApplicationContext(), resultIntent);
        pushNotificationAction.onMessageReceived(remoteMessage);
    }
}
