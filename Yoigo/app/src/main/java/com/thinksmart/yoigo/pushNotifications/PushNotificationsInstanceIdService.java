package com.thinksmart.yoigo.pushNotifications;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.thinksmart.app4onesdk.core.model.constants.UserPreferences;
import com.thinksmart.app4onesdk.engine.utils.preferencesManager.IPreferencesManager;
import com.thinksmart.app4onesdk.engine.utils.preferencesManager.imp.PreferencesManagerImp;

/**
 * Created by roberto.demiguel on 28/11/2016.
 */
public class PushNotificationsInstanceIdService extends FirebaseInstanceIdService {
    private static final String TAG = "PushNInstanceIdService";

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        IPreferencesManager preferencesManager = new PreferencesManagerImp(this);
        preferencesManager.saveStringPreference(UserPreferences.FIREBASE_TOKEN, refreshedToken);
    }
}
