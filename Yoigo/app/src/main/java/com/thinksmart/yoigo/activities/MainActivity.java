package com.thinksmart.yoigo.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.thinksmart.app4onesdk.engine.application.AppCore;
import com.thinksmart.yoigo.modelGenerator.AppConfigModelGenerator;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //start engine
        AppCore.getInstance().startEngine(this, AppConfigModelGenerator.generateConfig());
    }
}
