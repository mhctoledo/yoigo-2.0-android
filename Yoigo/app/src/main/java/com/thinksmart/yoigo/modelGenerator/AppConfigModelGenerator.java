package com.thinksmart.yoigo.modelGenerator;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thinksmart.app4onesdk.core.model.enums.Tab;
import com.thinksmart.app4onesdk.engine.application.appConfigModel.AppConfigModel;
import com.thinksmart.app4onesdk.engine.application.appConfigModel.Event;
import com.thinksmart.app4onesdk.engine.application.appConfigModel.screen.Screen;
import com.thinksmart.app4onesdk.engine.application.appConfigModel.screen.SideMenu;
import com.thinksmart.app4onesdk.engine.application.appConfigModel.screen.SideMenuScreen;
import com.thinksmart.app4onesdk.engine.application.appConfigModel.screen.TabBarScreen;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by roberto.demiguel on 14/10/2016.
 */
public class AppConfigModelGenerator {

    private final static String campaignName = "com.thinksmart.yoigo";
    private final static String bundleCode = "2.0.0";
    private final static String sdkActivityPackage = "com.thinksmart.app4onesdk.engine.views.activities.";
    private final static String sdkFragmentPackage = "com.thinksmart.app4onesdk.engine.views.fragments.";

    public static AppConfigModel generateConfigFromJson(String appModelJson) {
        AppConfigModel appConfigModel = null;
        try {
            ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            appConfigModel = mapper.readValue(appModelJson, AppConfigModel.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return appConfigModel;
    }


    public static AppConfigModel generateConfig() {

        Map<String, Screen> screens = new HashMap<>();

        //Login - Screen
        Map<String, Event> eventsLogin = new HashMap<>();
        addEventToMap(eventsLogin, "goToSelectFromLogin", "SelectProfileActivity");
        addEventToMap(eventsLogin, "goToChangePasswordFromLogin", "ChangePasswordActivity");
        addEventToMap(eventsLogin, "goToRememberPasswordFromLogin", "RememberPasswordActivity");
        addEventToMap(eventsLogin, "goToTabBarFromLogin", "TabBarActivity");
        addEventToMap(eventsLogin, "goToDataAssignmentFromLogin", "DataAssignmentActivity");
        addActivityScreenToMap(screens, "LoginActivity", eventsLogin);

        //Change password - Screen
        Map<String, Event> eventsChangePassword = new HashMap<>();
        addEventToMap(eventsChangePassword, "goToSelectFromChangePassword", "SelectProfileActivity");
        addActivityScreenToMap(screens, "ChangePasswordActivity", eventsChangePassword);

        //Remember password - Screen
        Map<String, Event> rememberPasswordEvents = new HashMap<>();
        addEventToMap(rememberPasswordEvents, "goToLoginFromRememberPassword", "LoginActivity");
        addActivityScreenToMap(screens, "RememberPasswordActivity", rememberPasswordEvents);

        //Select Profile - Screen
        Map<String, Event> selectProfileEvents = new HashMap<>();
        addEventToMap(selectProfileEvents, "goToHomeFromSelectProfile", "TabBarActivity");
        addEventToMap(selectProfileEvents, "goToDataAssignmentFromSelectProfile", "DataAssignmentActivity");
        addActivityScreenToMap(screens, "SelectProfileActivity", selectProfileEvents);

        //Data assignment - Screen
        Map<String, Event> dataAssignmentEvents = new HashMap<>();
        addEventToMap(dataAssignmentEvents, "goToHomeFromDataAssignment", "TabBarActivity");
        addActivityScreenToMap(screens, "DataAssignmentActivity", dataAssignmentEvents);


        //Detail Product - Screen
        Map<String, Event> productDetailsEvents = new HashMap<>();
        addEventToMap(productDetailsEvents, "goToCartFromProductDetails", "CartActivity");
        addActivityScreenToMap(screens, "ProductDetailsActivity", productDetailsEvents);

        //Cart - Screen
        Map<String, Event> cartEvents = new HashMap<>();
        addEventToMap(cartEvents, "goToConfirmShipmentFromCart", "ConfirmShipmentActivity");
        addEventToMap(cartEvents, "goToDetailFromCart", "ProductDetailsActivity");
        addActivityScreenToMap(screens, "CartActivity", cartEvents);

        //ConfirmShipment - Screen
        Map<String, Event> confirmShipmentEvents = new HashMap<>();
        addEventToMap(confirmShipmentEvents, "goToChangeShipmentFromConfirmShipment", "ChangeShipmentActivity");
        addEventToMap(confirmShipmentEvents, "goToFinishPurchaseFromConfirmShipment", "FinishPurchaseActivity");
        addActivityScreenToMap(screens, "ConfirmShipmentActivity", confirmShipmentEvents);

        //Change shipment - Screen
        Map<String, Event> changeShipmentEvents = new HashMap<>();
        addEventToMap(changeShipmentEvents, "goToConfirmShipment", "ConfirmShipmentActivity");
        addActivityScreenToMap(screens, "ChangeShipmentActivity", changeShipmentEvents);


        //FinishPurchase - Screen
        Map<String, Event> finishPurchaseEvents = new HashMap<>();
        addEventToMap(finishPurchaseEvents, "goToOrderOverviewFromFinishPurchase", "OrderOverviewActivity");
        addActivityScreenToMap(screens, "FinishPurchaseActivity", finishPurchaseEvents);


        //OrderOverview
        Map<String, Event> orderOverviewEvents = new HashMap<>();
        addEventToMap(orderOverviewEvents, "goToHomeFromOrderOverview", "TabBarActivity");
        addActivityScreenToMap(screens, "OrderOverviewActivity", orderOverviewEvents);

        //Tabbar - Screen
        List<Tab> tabs = new ArrayList<>();
        tabs.add(Tab.getTabFromResourceName(sdkFragmentPackage + "HomeFragment"));
        tabs.add(Tab.getTabFromResourceName(sdkFragmentPackage + "CatalogFragment"));
        tabs.add(Tab.getTabFromResourceName(sdkFragmentPackage + "PointsFragment"));

        Map<String, Event> eventsTabBar = new HashMap<>();
        addEventToMap(eventsTabBar, "goToProfileInfoFromTabBar", "ProfileInfoActivity");
        addEventToMap(eventsTabBar, "goToMessagesFromTabBar", "MessagesActivity");
        addEventToMap(eventsTabBar, "goToNotificationsFromTabBar", "NotificationsActivity");
        addEventToMap(eventsTabBar, "goToSelectFromTabBar", "SelectProfileActivity");
        addTabBarScreenToMap(screens, "TabBarActivity", eventsTabBar, tabs);

        //home - Screen
        Map<String, Event> homeEvents = new HashMap<>();
        addEventToMap(homeEvents, "goToCatalogFromHome", "TabBarActivity");
        addEventToMap(homeEvents, "goToWebViewFromHome", "WebViewActivity");
        addFragmentScreenToMap(screens, "HomeFragment", homeEvents);

        //WebView - Screen
        addActivityScreenToMap(screens, "WebViewActivity");

        //Profile info - Screen
        Map<String, Event> profileInfoEvents = new HashMap<>();
        addEventToMap(profileInfoEvents, "goToEditProfileFromProfileInfo", "EditProfileActivity");
        addEventToMap(profileInfoEvents, "goToChangeProfilePasswordFromEditProfile", "ProfileChangePasswordActivity");
        addActivityScreenToMap(screens, "ProfileInfoActivity", profileInfoEvents);

        //Profile change password - Screen
        addActivityScreenToMap(screens, "ProfileChangePasswordActivity");

        //Edit Profile - Screen
        addActivityScreenToMap(screens, "EditProfileActivity");

        //Points - Screen
        Map<String, Event> eventsPointsDetail = new HashMap<>();
        addEventToMap(eventsPointsDetail, "goToPointsDetailFromPoints", "PointsDetailActivity");
        addFragmentScreenToMap(screens, "PointsFragment", eventsPointsDetail);

        //Points detail - Screen
        addActivityScreenToMap(screens, "PointsDetailActivity");

        //Catalog - Screen
        Map<String, Event> eventsCatalog = new HashMap<>();
        addEventToMap(eventsCatalog, "goToDetailFromCatalog", "ProductDetailsActivity");
        addEventToMap(eventsCatalog, "goToCartFromCatalog", "CartActivity");
        addEventToMap(eventsCatalog, "goToCategoriesFromCatalog", "CategoriesActivity");
        addFragmentScreenToMap(screens, "CatalogFragment", eventsCatalog);


        //Categories - Screen
        Map<String, Event> categoriesEvents = new HashMap<>();
        addEventToMap(categoriesEvents, "goToCategorySectionsFromCategories", "CategorySectionsActivity");
        addActivityScreenToMap(screens, "CategoriesActivity", categoriesEvents);

        //Section Categories
        Map<String, Event> sectionsEvents = new HashMap<>();
        addEventToMap(sectionsEvents, "goToCatalogFromCategorySections", "TabBarActivity");
        addActivityScreenToMap(screens, "CategorySectionsActivity", sectionsEvents);

        //Messages - Screen
//        SideMenu messageSideMenu = new SideMenu(R.drawable.message_icon, R.string.panel_messages_icon, "goToMessagesFromTabBar");
//        Map<String, Event> messagesEvents = new HashMap<>();
//        addEventToMap(messagesEvents, "goToCreateMessageFromMessages", "CreateMessageActivity");
//        addSideMenuScreenToMap(screens, "MessagesActivity", messagesEvents, messageSideMenu);

        //CreateMessage - Screen
//        addActivityScreenToMap(screens, "CreateMessageActivity");

        //Notifications - Screen
        SideMenu notificationsSideMenu = new SideMenu(com.thinksmart.app4onesdk.R.drawable.info_icon, com.thinksmart.app4onesdk.R.string.panel_notifications_icon, "goToNotificationsFromTabBar");
        addSideMenuScreenToMap(screens, "NotificationsActivity", notificationsSideMenu);

        return new AppConfigModel(campaignName, bundleCode, sdkActivityPackage + "LoginActivity", screens);
    }

    private static void addEventToMap(Map<String, Event> eventMap, String eventName, String screenResourceDestination) {
        eventMap.put(eventName, new Event(eventName, sdkActivityPackage + screenResourceDestination));
    }


    private static void addActivityScreenToMap(Map<String, Screen> screenMap, String screenResource) {
        screenMap.put(sdkActivityPackage + screenResource, new Screen(sdkActivityPackage + screenResource));
    }

    private static void addActivityScreenToMap(Map<String, Screen> screenMap, String screenResource, Map<String, Event> events) {
        screenMap.put(sdkActivityPackage + screenResource, new Screen(sdkActivityPackage + screenResource, events));
    }

    private static void addFragmentScreenToMap(Map<String, Screen> screenMap, String screenResource, Map<String, Event> events) {
        screenMap.put(sdkFragmentPackage + screenResource, new Screen(sdkFragmentPackage + screenResource, events));
    }

    private static void addTabBarScreenToMap(Map<String, Screen> screenMap, String screenResource, Map<String, Event> events, List<Tab> tabs) {
        screenMap.put(sdkActivityPackage + screenResource, new TabBarScreen(sdkActivityPackage + screenResource, events, tabs));
    }

    private static void addSideMenuScreenToMap(Map<String, Screen> screenMap, String screenResource, Map<String, Event> events, SideMenu sideMenu) {
        screenMap.put(sdkActivityPackage + screenResource, new SideMenuScreen(sdkActivityPackage + screenResource, events, sideMenu));
    }


    private static void addSideMenuScreenToMap(Map<String, Screen> screenMap, String screenResource, SideMenu sideMenu) {
        screenMap.put(sdkActivityPackage + screenResource, new SideMenuScreen(sdkActivityPackage + screenResource, sideMenu));
    }


}
