package com.thinksmart.yoigo;

import android.app.Application;
import android.support.v7.app.AppCompatDelegate;

/**
 * Created by roberto.demiguel on 21/11/2016.
 */
public class AppDelegate extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }
}
