package com.thinksmart.app4onesdk;

import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.thinksmart.app4onesdk.core.model.bos.MessageAddressesBO;
import com.thinksmart.app4onesdk.core.model.bos.MessagesParentBO;
import com.thinksmart.app4onesdk.engine.application.AppCore;
import com.thinksmart.app4onesdk.engine.interactors.IMessagesInteractor;
import com.thinksmart.app4onesdk.engine.interactors.IPresenterListener;
import com.thinksmart.app4onesdk.engine.interactors.imp.MessagesInteractorImp;
import com.thinksmart.app4onesdk.engine.net.NetworkManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.fail;

/**
 * Created by roberto.demiguel on 01/12/2016.
 */
@RunWith(AndroidJUnit4.class)
public class MessagesInteractorTest {
    private static final String TAG = "MessagesInteractorTest";

    private static final String token = "0ABC68EE3EB043A48C4AF7A5FE8E8F2C";
    private static final Integer campaignId = 20;
    private static final Integer participantId = 779177;

    private IMessagesInteractor interactor;

    @Before
    public void init() {
        NetworkManager networkManager = new NetworkManager(InstrumentationRegistry.getContext());
        AppCore.getInstance().setNetworkManager(networkManager);
        interactor = new MessagesInteractorImp(new MessagesInteractorListener(), new AddressesInteractorListener(), null);
    }

    @Test
    public void getMessages() throws Exception {
        interactor.getMessages(token, participantId, campaignId);
        Thread.sleep(5000);
    }

    @Test
    public void getAddresses() throws Exception {
        interactor.getAddresses(token, participantId, campaignId);
        Thread.sleep(5000);
    }

    private class MessagesInteractorListener implements IPresenterListener<MessagesParentBO> {

        @Override
        public void onSuccess(MessagesParentBO object) {
            Log.i(TAG, "SUCCESS");
            if (object != null) {
                Log.d(TAG, "Messages result size: " + object.getMessagesList().size());
            } else {
                Log.d(TAG, "Messages result size: 0");
            }
        }

        @Override
        public void onExpiredToken() {
            fail("onExpiredToken");
        }

        @Override
        public void onError(Integer code, String error) {
            fail(error);
        }

        @Override
        public void onServerError(String error) {
            fail(error);
        }
    }

    private class AddressesInteractorListener implements IPresenterListener<MessageAddressesBO> {

        @Override
        public void onSuccess(MessageAddressesBO object) {
            Log.i(TAG, "SUCCESS");
            if (object != null) {
                Log.d(TAG, "Addresses result size: " + object.getAddressList().size());
            } else {
                Log.d(TAG, "Addresses result size: 0");
            }
        }

        @Override
        public void onExpiredToken() {
            fail("onExpiredToken");
        }

        @Override
        public void onError(Integer code, String error) {
            fail(error);
        }

        @Override
        public void onServerError(String error) {
            fail(error);
        }
    }

}
