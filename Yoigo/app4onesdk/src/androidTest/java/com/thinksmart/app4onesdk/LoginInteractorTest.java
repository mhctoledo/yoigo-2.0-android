package com.thinksmart.app4onesdk;

import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.thinksmart.app4onesdk.core.model.bos.UserBO;
import com.thinksmart.app4onesdk.engine.application.AppCore;
import com.thinksmart.app4onesdk.engine.interactors.ILoginInteractor;
import com.thinksmart.app4onesdk.engine.interactors.IPresenterListener;
import com.thinksmart.app4onesdk.engine.interactors.imp.LoginInteractorImp;
import com.thinksmart.app4onesdk.engine.net.NetworkManager;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.fail;

/**
 * Created by roberto.demiguel on 01/12/2016.
 */
@RunWith(AndroidJUnit4.class)
public class LoginInteractorTest {
    private static final String TAG = "LoginInteractorTest";

    private static final String username = "testandroid";
    private static final String password = "demo2016";
    private static final String bundleCode = "com.ThinkSmart.RecompensasBMW";
    private static final String applicationVersion = "1.9.17";

    private ILoginInteractor interactor;

    @Before
    public void init() {
        NetworkManager networkManager = new NetworkManager(InstrumentationRegistry.getContext());
        AppCore.getInstance().setNetworkManager(networkManager);
        interactor = new LoginInteractorImp(new LoginInteractorListener());
    }

    @Test
    public void login() throws Exception {
        interactor.login(username, password, null, bundleCode, applicationVersion, null);
        Thread.sleep(5000);
    }

    private class LoginInteractorListener implements IPresenterListener<UserBO> {

        @Override
        public void onSuccess(UserBO object) {
            Assert.assertNotNull("User shouldn't be null", object);
            Log.i(TAG, "SUCCESS");
            Log.i(TAG, "Token: " + object.getToken());
        }

        @Override
        public void onExpiredToken() {
            fail("onExpiredToken");
        }

        @Override
        public void onError(String error) {
            fail(error);
        }

        @Override
        public void onServerError(String error) {
            fail(error);
        }
    }

}
