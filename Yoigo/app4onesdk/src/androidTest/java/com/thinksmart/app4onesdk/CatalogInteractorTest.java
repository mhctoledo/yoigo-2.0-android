package com.thinksmart.app4onesdk;

import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.thinksmart.app4onesdk.core.model.bos.CatalogBO;
import com.thinksmart.app4onesdk.core.model.enums.FilterType;
import com.thinksmart.app4onesdk.core.vos.CatalogVO;
import com.thinksmart.app4onesdk.engine.application.AppCore;
import com.thinksmart.app4onesdk.engine.interactors.ICatalogInteractor;
import com.thinksmart.app4onesdk.engine.interactors.IPresenterListener;
import com.thinksmart.app4onesdk.engine.interactors.imp.CatalogInteractorImp;
import com.thinksmart.app4onesdk.engine.net.NetworkManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.fail;

/**
 * Created by roberto.demiguel on 01/12/2016.
 */
@RunWith(AndroidJUnit4.class)
public class CatalogInteractorTest {

    private static final String TAG = "CatalogInteractorTest";

    private static final String token = "0ABC68EE3EB043A48C4AF7A5FE8E8F2C";
    private static final Integer campaignId = 20;
    private static final Integer participantId = 779177;

    private ICatalogInteractor interactor;
    private CatalogVO catalogVO;

    @Before
    public void init() {
        NetworkManager networkManager = new NetworkManager(InstrumentationRegistry.getContext());
        AppCore.getInstance().setNetworkManager(networkManager);
        interactor = new CatalogInteractorImp(new CatalogInteractorListener());

        catalogVO = new CatalogVO();
        catalogVO.setCampaignId(campaignId);
        catalogVO.setToken(token);
        catalogVO.setParticipantId(participantId);
    }

    @Test
    public void getCatalog() throws Exception {
        interactor.getCatalog(catalogVO);
        Thread.sleep(5000);
    }

    @Test
    public void searchInCatalog() throws Exception {
        catalogVO.setSearch("test");
        interactor.getCatalog(catalogVO);
        Thread.sleep(5000);
    }

    @Test
    public void filterNewsInCatalog() throws Exception {
        catalogVO.setSearch(FilterType.NEWNESS.getValue());
        interactor.getCatalog(catalogVO);
        Thread.sleep(5000);
    }

    @Test
    public void filterReachableInCatalog() throws Exception {
        catalogVO.setSearch(FilterType.REACHABLE.getValue());
        interactor.getCatalog(catalogVO);
        Thread.sleep(5000);
    }

    private class CatalogInteractorListener implements IPresenterListener<CatalogBO> {

        @Override
        public void onSuccess(CatalogBO object) {
            Log.i(TAG, "SUCCESS");
            if (object != null) {
                Log.d(TAG, "Result size: " + object.getProductList().size());
            } else {
                Log.d(TAG, "Result size: 0");
            }

        }

        @Override
        public void onExpiredToken() {
            fail("onExpiredToken");
        }

        @Override
        public void onError(Integer code, String error) {
            fail(error);
        }


        @Override
        public void onServerError(String error) {
            fail(error);
        }
    }

}
