package com.thinksmart.app4onesdk;

import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.thinksmart.app4onesdk.core.model.bos.ProductBO;
import com.thinksmart.app4onesdk.engine.application.AppCore;
import com.thinksmart.app4onesdk.engine.interactors.IPresenterListener;
import com.thinksmart.app4onesdk.engine.interactors.IProductDetailsInteractor;
import com.thinksmart.app4onesdk.engine.interactors.imp.ProductDetailsInteractorImp;
import com.thinksmart.app4onesdk.engine.net.NetworkManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.fail;

/**
 * Created by roberto.demiguel on 01/12/2016.
 */
@RunWith(AndroidJUnit4.class)
public class ProductDetailsInteractorTest {
    private static final String TAG = "PointsInteractorTest";

    private static final String token = "0ABC68EE3EB043A48C4AF7A5FE8E8F2C";
    private static final Integer campaignId = 20;
    private static final Integer participantId = 779177;
    private static final String productId = "017868";
    private static final Integer isSerie = 0;

    private IProductDetailsInteractor interactor;

    @Before
    public void init() {
        NetworkManager networkManager = new NetworkManager(InstrumentationRegistry.getContext());
        AppCore.getInstance().setNetworkManager(networkManager);
        interactor = new ProductDetailsInteractorImp(new ProductDetailsListener());
    }

    @Test
    public void getNotifications() throws Exception {
        interactor.getProduct(token, participantId, campaignId, productId, isSerie);
        Thread.sleep(5000);
    }

    private class ProductDetailsListener implements IPresenterListener<ProductBO> {

        @Override
        public void onSuccess(ProductBO object) {
            Log.i(TAG, "SUCCESS");
            if (object == null) {
                Log.d(TAG, "Result null");
            } else {
                Log.d(TAG, "Result: " + object.toString());
            }
        }

        @Override
        public void onExpiredToken() {
            fail("onExpiredToken");
        }

        @Override
        public void onError(String error) {
            fail(error);
        }

        @Override
        public void onServerError(String error) {
            fail(error);
        }
    }

}
