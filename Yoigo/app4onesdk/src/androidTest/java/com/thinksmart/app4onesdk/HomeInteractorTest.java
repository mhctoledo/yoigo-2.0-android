package com.thinksmart.app4onesdk;

import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.thinksmart.app4onesdk.core.model.bos.HomeBO;
import com.thinksmart.app4onesdk.engine.application.AppCore;
import com.thinksmart.app4onesdk.engine.interactors.IHomeInteractor;
import com.thinksmart.app4onesdk.engine.interactors.IPresenterListener;
import com.thinksmart.app4onesdk.engine.interactors.imp.HomeInteractorImp;
import com.thinksmart.app4onesdk.engine.net.NetworkManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.HashMap;

import static org.junit.Assert.fail;

/**
 * Created by roberto.demiguel on 01/12/2016.
 */
@RunWith(AndroidJUnit4.class)
public class HomeInteractorTest {
    private static final String TAG = "HomeInteractorTest";

    private static final String token = "0ABC68EE3EB043A48C4AF7A5FE8E8F2C";
    private static final Integer campaignId = 20;
    private static final Integer participantId = 779177;

    private IHomeInteractor interactor;

    @Before
    public void init() {
        NetworkManager networkManager = new NetworkManager(InstrumentationRegistry.getContext());
        AppCore.getInstance().setNetworkManager(networkManager);
        interactor = new HomeInteractorImp(new ConfirmShipmentInteractorListener());
    }

    @Test
    public void getHome() throws Exception {
        interactor.getBanners(token, participantId, campaignId, new HashMap<String, String>());
        Thread.sleep(5000);
    }

    private class ConfirmShipmentInteractorListener implements IPresenterListener<HomeBO> {

        @Override
        public void onSuccess(HomeBO object) {
            Log.i(TAG, "SUCCESS");
            if (object == null) {
                Log.d(TAG, "Result null");
            } else if (object.getSliderList() != null) {
                Log.d(TAG, "Result size: " + object.getSliderList().size());
                Log.d(TAG, "Object: " + object.toString());
            } else if (object.getSliderList() == null) {
                Log.d(TAG, "Result size: 0");
                Log.d(TAG, "Object: " + object.toString());
            }
        }

        @Override
        public void onExpiredToken() {
            fail("onExpiredToken");
        }

        @Override
        public void onError(String error) {
            fail(error);
        }

        @Override
        public void onServerError(String error) {
            fail(error);
        }
    }

}
