package com.thinksmart.app4onesdk.engine.interactors.imp;

import android.support.annotation.NonNull;

import com.thinksmart.app4onesdk.core.api.ApiParameters;
import com.thinksmart.app4onesdk.core.api.ApiResponse;
import com.thinksmart.app4onesdk.core.api.ApiUrls;
import com.thinksmart.app4onesdk.core.model.bos.UserBO;
import com.thinksmart.app4onesdk.core.vos.LoginVO;
import com.thinksmart.app4onesdk.engine.application.AppCore;
import com.thinksmart.app4onesdk.engine.interactors.ILoginInteractor;
import com.thinksmart.app4onesdk.engine.interactors.IPresenterListener;
import com.thinksmart.app4onesdk.engine.net.IRequestCallback;
import com.thinksmart.app4onesdk.engine.net.ResponseMapperUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;


/**
 * Created by roberto.demiguel on 13/10/2016.
 */
public class LoginInteractorImp implements ILoginInteractor {

    public LoginInteractorImp() {
    }

    public void login(LoginVO loginVO, IPresenterListener presenterListener) {
        final JSONObject params = getJsonObject(loginVO);
        AppCore.getInstance().getNetworkManager().postRequest(params, ApiUrls.loginURL
                , new LoginRequestCallback(presenterListener));
    }

    private class LoginRequestCallback implements IRequestCallback {

        private IPresenterListener presenterListener;

        public LoginRequestCallback(IPresenterListener presenterListener) {
            this.presenterListener = presenterListener;
        }

        @Override
        public void onRequestSuccess(JSONObject response) {
            try {
                ResponseMapperUtil.parseResponse(response, this.presenterListener, UserBO.class);
            } catch (JSONException | IOException e) {
                this.presenterListener.onError(ApiResponse.Code.ERROR_PARSING_RESPONSE, ApiResponse.ERROR_PARSING_RESPONSE);
            }
        }

        @Override
        public void onRequestError(String error) {
            presenterListener.onServerError(error);
        }
    }

    @NonNull
    private JSONObject getJsonObject(LoginVO loginVO) {
        final JSONObject params = new JSONObject();
        try {
            params.put(ApiParameters.USERNAME, (loginVO.getUser() != null) ? loginVO.getUser() : "");
            params.put(ApiParameters.PASSWORD, (loginVO.getPassword() != null) ? loginVO.getPassword() : "");
            params.put(ApiParameters.TOKEN, (loginVO.getToken() != null) ? loginVO.getToken() : "");
            params.put(ApiParameters.LANGUAGE, ApiParameters.LANGUAGE_ES);
            params.put(ApiParameters.DEVICE_APP_ID, (loginVO.getFirebaseToken() != null) ? loginVO.getFirebaseToken() : "");
            params.put(ApiParameters.SO, ApiParameters.SO_ANDROID);
            params.put(ApiParameters.DEVICE, "");
            params.put(ApiParameters.SCREEN_SIZE, "");
            params.put(ApiParameters.APPLICATION_VERSION, (loginVO.getApplicationVersion() != null) ? loginVO.getApplicationVersion() : "");
            params.put(ApiParameters.BUNDLE_CODE, (loginVO.getBundleCode() != null) ? loginVO.getBundleCode() : "");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }
}
