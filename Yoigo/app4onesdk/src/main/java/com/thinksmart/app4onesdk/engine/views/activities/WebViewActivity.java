package com.thinksmart.app4onesdk.engine.views.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.engine.views.fragments.WebViewFragment;

import java.util.ArrayList;

public class WebViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);

        initToolbar();

        Intent intent = getIntent();
        ArrayList<String> webViewParams = intent.getStringArrayListExtra("params");

        if (!webViewParams.isEmpty()) {

            String url = webViewParams.get(0);
            WebViewFragment fragment = (WebViewFragment) getSupportFragmentManager().findFragmentById(R.id.web_view_container);
            if (fragment == null) {
                fragment = WebViewFragment.newInstance(url);
                getSupportFragmentManager().beginTransaction().add(R.id.web_view_container, fragment).commit();
            }
        }
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_with_icon);
        toolbar.setTitle(R.string.web_view_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
