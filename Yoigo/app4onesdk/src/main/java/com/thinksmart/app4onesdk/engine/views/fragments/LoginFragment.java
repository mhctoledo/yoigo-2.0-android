package com.thinksmart.app4onesdk.engine.views.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mtramin.rxfingerprint.RxFingerprint;
import com.mtramin.rxfingerprint.data.FingerprintDecryptionResult;
import com.mtramin.rxfingerprint.data.FingerprintEncryptionResult;
import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.core.model.bos.CampaignBO;
import com.thinksmart.app4onesdk.core.model.bos.UserBO;
import com.thinksmart.app4onesdk.core.model.constants.UserPreferences;
import com.thinksmart.app4onesdk.engine.application.AppCore;
import com.thinksmart.app4onesdk.engine.application.utils.NavigationUtil;
import com.thinksmart.app4onesdk.engine.presenters.ILoginPresenter;
import com.thinksmart.app4onesdk.engine.presenters.imp.LoginPresenterImp;
import com.thinksmart.app4onesdk.engine.utils.AlertDialogUtil;
import com.thinksmart.app4onesdk.engine.utils.CustomEditTextView;
import com.thinksmart.app4onesdk.engine.utils.preferencesManager.IPreferencesManager;
import com.thinksmart.app4onesdk.engine.utils.preferencesManager.imp.PreferencesManagerImp;
import com.thinksmart.app4onesdk.engine.views.ILoginView;
import com.thinksmart.app4onesdk.engine.views.activities.LoginActivity;
import com.thinksmart.app4onesdk.engine.views.utils.ViewUtils;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by roberto.demiguel on 13/10/2016.
 */
public class LoginFragment extends Fragment implements ILoginView {

    private ProgressDialog dialog;
    private CustomEditTextView usernameField;
    private CustomEditTextView passwordField;
    private ILoginPresenter presenter;
    private Button loginButton;
    private RelativeLayout forgottenPasswordLink;
    private IPreferencesManager preferencesManager;

    private ImageView fingerprintIcon;
    private TextView fingerprintStatus;

    static final long ERROR_TIMEOUT_MILLIS = 1600;
    static final long SUCCESS_DELAY_MILLIS = 1000;

    private AlertDialog fingerprintDialog;

    public LoginFragment() {
    }

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new LoginPresenterImp(this, getActivity());
        preferencesManager = new PreferencesManagerImp(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        //init ui
        initUIReferences(view);
        initEvents();

        presenter.tryLoginFromToken();

        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onTryLoginFromTokenFailed() {
        if (UserPreferences.FINGERPRINT_SAVED
                .equals(preferencesManager.retrieveStringPreference(UserPreferences.FINGERPRINT))) {
            readFingerprintToGetPassword();
        }
    }

    @Override
    public void setLastUsername() {
        String lastUsername = preferencesManager.retrieveStringPreference(UserPreferences.USER);
        if (lastUsername != null) {
            usernameField.setText(lastUsername);
        }
    }

    private void initEvents() {
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (areMandatoryFieldsFilled()) {
                    presenter.login(usernameField.getText().toString(), passwordField.getText().toString());
                } else {
                    Toast.makeText(getActivity(), R.string.login_fields_are_mandatory, Toast.LENGTH_LONG).show();
                }
            }
        });

        forgottenPasswordLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateToRememberPasswordView();
            }
        });

        initFingerprintDialog();
    }

    private void initFingerprintDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.fingerprint_dialog_content, null);
        fingerprintIcon = (ImageView) dialogView.findViewById(R.id.fingerprint_icon);
        fingerprintStatus = (TextView) dialogView.findViewById(R.id.fingerprint_status);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        fingerprintDialog = dialogBuilder.create();
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    private void initUIReferences(View view) {
        loginButton = (Button) view.findViewById(R.id.login_enter_button);
        forgottenPasswordLink = (RelativeLayout) view.findViewById(R.id.login_forgotten_password_link);
        usernameField = (CustomEditTextView) view.findViewById(R.id.login_user_value);
        passwordField = (CustomEditTextView) view.findViewById(R.id.login_pass_value);
        dialog = new ProgressDialog(getActivity());
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
    }


    @Override
    public void showProgress() {
        ViewUtils.hideKeyboard(getView(), getActivity());
        showProgressDialog();
    }

    private void showProgressDialog() {
        dialog.setMessage(getString(R.string.loading_dialog));
        dialog.show();
    }

    @Override
    public void hideProgress() {
        dialog.dismiss();
    }

    @Override
    public void showCredentialsError(String error) {
        Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void navigateToSelectProfileView() {
        final String eventDestination = "goToSelectFromLogin";
        NavigationUtil.goToNextScreen(LoginActivity.class.getName(), eventDestination);
    }

    @Override
    public void navigateToTabBarsView() {
        final String eventDestination = "goToTabBarFromLogin";
        NavigationUtil.goToNextScreen(LoginActivity.class.getName(), eventDestination);
    }

    @Override
    public void navigateToChangePasswordView() {
        final String eventDestination = "goToChangePasswordFromLogin";
        NavigationUtil.goToNextScreen(LoginActivity.class.getName(), eventDestination);
    }

    public void navigateToRememberPasswordView() {
        final String eventDestination = "goToRememberPasswordFromLogin";
        NavigationUtil.goToNextScreen(LoginActivity.class.getName(), eventDestination);
    }

    private boolean areMandatoryFieldsFilled() {
        boolean correct = true;
        String username = usernameField.getText().toString();
        String password = passwordField.getText().toString();
        if (TextUtils.isEmpty(username) || TextUtils.isEmpty(password)) {
            correct = false;
        }
        return correct;
    }


    @Override
    public void showServerError() {
        AlertDialogUtil.showAlertDialog(getActivity(), R.string.server_error);
    }

    @Override
    public void showOnExpiredTokenError() {
        AlertDialogUtil.showAlertDialogAndCloseSession(getActivity(), R.string.on_confluent_session_error);
    }

    @Override
    public void navigateToDataAssignment() {
        final String eventDestination = "goToDataAssignmentFromLogin";
        NavigationUtil.goToNextScreen(LoginActivity.class.getName(), eventDestination);
    }


    @Override
    public void navigateToNextScreen() {
        preferencesManager.saveStringPreference(UserPreferences.USER, usernameField.getText().toString());
        goToNextScreen();
    }

    @Override
    public void askForFingerPrintAndNavigateToNextScreen() {
        preferencesManager.saveStringPreference(UserPreferences.USER, usernameField.getText().toString());
        boolean fingerprintSaved = UserPreferences.FINGERPRINT_SAVED
                .equals(preferencesManager.retrieveStringPreference(UserPreferences.FINGERPRINT));
        UserBO user = AppCore.getInstance().getSessionManager().getCurrentUser();
        //if change key is true, we have to change password
        if (!user.getChangeKey() && RxFingerprint.isAvailable(getActivity()) && !fingerprintSaved) {
            showAskForFingerprintDialog();
        } else {
            goToNextScreen();
        }
    }


    private void goToNextScreen() {
        UserBO user = AppCore.getInstance().getSessionManager().getCurrentUser();
        //if change key is true, we have to change password
        if (user.getChangeKey()) {
            navigateToChangePasswordView();
        } else {
            //if only one campaign, we should skip profile selector screen
            if (user.getCampaignList().size() == 1) {
                CampaignBO selectedCampaign = user.getCampaignList().get(0);
                user.setSelectedCampaign(selectedCampaign);
                if (user.getFormData() != null && user.getFormData()) {
                    navigateToDataAssignment();
                } else {
                    navigateToTabBarsView();
                }
            } else {
                navigateToSelectProfileView();
            }
        }
    }

    private void readFingerprintToGetPassword() {
        showReadFingerprintDialog();

        String encryptedString = preferencesManager.retrieveStringPreference(UserPreferences.PASSWORD);

        Disposable disposable = RxFingerprint.decrypt(getActivity(), encryptedString)
                .subscribe(new Consumer<FingerprintDecryptionResult>() {
                    @Override
                    public void accept(FingerprintDecryptionResult fingerprintDecryptionResult) throws Exception {
                        switch (fingerprintDecryptionResult.getResult()) {
                            case FAILED:
                                setErrorFingerprintDialogStatus(getString(R.string.fingerprint_not_recognized));
                                break;
                            case HELP:
                                setErrorFingerprintDialogStatus(fingerprintDecryptionResult.getMessage());
                                break;
                            case AUTHENTICATED:
                                final String decryptedPassword = fingerprintDecryptionResult.getDecrypted();
                                final String user = preferencesManager.retrieveStringPreference(UserPreferences.USER);
                                // Do something with decrypted data
                                setSuccessFingerprintDialogStatus();
                                final Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        fingerprintDialog.cancel();
                                        presenter.login(user, decryptedPassword);

                                    }
                                }, SUCCESS_DELAY_MILLIS);
                                break;
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        if (RxFingerprint.keyInvalidated(throwable)) {
                            // The keys you wanted to use are invalidated because the user has turned off his
                            // secure lock screen or changed the fingerprints stored on the device
                            // You have to re-encrypt the data to access it
                        }
                        Log.e("ERROR", "decrypt", throwable);
                    }
                });
    }


    private void readFingerprintToSavePassword() {
        showReadFingerprintDialog();

        Disposable disposable = RxFingerprint.encrypt(getActivity(), passwordField.getText().toString())
                .subscribe(new Consumer<FingerprintEncryptionResult>() {
                    @Override
                    public void accept(FingerprintEncryptionResult fingerprintEncryptionResult) throws Exception {
                        switch (fingerprintEncryptionResult.getResult()) {
                            case FAILED:
                                setErrorFingerprintDialogStatus(getString(R.string.fingerprint_not_recognized));
                                break;
                            case HELP:
                                setErrorFingerprintDialogStatus(fingerprintEncryptionResult.getMessage());

                                break;
                            case AUTHENTICATED:
                                String encrypted = fingerprintEncryptionResult.getEncrypted();
                                Map<String, String> valuesToSave = new HashMap<>();
                                valuesToSave.put(UserPreferences.PASSWORD, encrypted);
                                valuesToSave.put(UserPreferences.FINGERPRINT, UserPreferences.FINGERPRINT_SAVED);
                                preferencesManager.saveStringPreferences(valuesToSave);

                                setSuccessFingerprintDialogStatus();
                                final Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        fingerprintDialog.cancel();
                                        goToNextScreen();
                                    }
                                }, SUCCESS_DELAY_MILLIS);

                                break;
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Log.e("ERROR", "encrypt", throwable);
                    }
                });

        //TODO para que deshabilite en sensor disposable.dispose();

    }

    private void showReadFingerprintDialog() {
        fingerprintDialog.show();
        fingerprintDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(android.R.color.black));
    }


    private void showAskForFingerprintDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        dialogBuilder.setTitle(R.string.ask_for_fingerprint_title);
        dialogBuilder.setMessage(R.string.ask_for_fingerprint_text);

        dialogBuilder.setNegativeButton(R.string.ask_for_fingerprint_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                goToNextScreen();
            }
        });
        dialogBuilder.setPositiveButton(R.string.ask_for_fingerprint_confirm, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                readFingerprintToSavePassword();
            }
        });
        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.positive_button));
        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(android.R.color.black));
    }

    private void setErrorFingerprintDialogStatus(String error) {
        fingerprintIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_fingerprint_error));
        fingerprintStatus.setText(error);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                resetFingerprintDialogStatus();
            }
        }, ERROR_TIMEOUT_MILLIS);
    }

    private void setSuccessFingerprintDialogStatus() {
        fingerprintIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_fingerprint_success));
        fingerprintStatus.setText(R.string.fingerprint_success);
    }


    private void resetFingerprintDialogStatus() {
        fingerprintIcon.setImageDrawable(getResources().getDrawable(R.drawable.fingerprint_ic));
        fingerprintStatus.setText(R.string.fingerprint_hint);
    }


}
