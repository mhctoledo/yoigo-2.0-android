package com.thinksmart.app4onesdk.engine.interactors;

import com.thinksmart.app4onesdk.core.vos.CatalogVO;

/**
 * Created by roberto.demiguel on 25/10/2016.
 */
public interface ICatalogInteractor {

    void getCatalog(CatalogVO catalogVO);
}
