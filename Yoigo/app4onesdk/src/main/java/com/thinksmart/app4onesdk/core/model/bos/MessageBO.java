package com.thinksmart.app4onesdk.core.model.bos;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by roberto.demiguel on 14/11/2016.
 */
public class MessageBO {

    @JsonProperty("message")
    private String message;
    @JsonProperty("sender_id")
    private Integer senderId;
    @JsonProperty("name_sender")
    private String senderName;
    @JsonProperty("photo_sender")
    private String senderPhoto;

    public MessageBO() {
    }

    public MessageBO(String message, Integer senderId, String senderName, String senderPhoto) {
        this.message = message;
        this.senderId = senderId;
        this.senderName = senderName;
        this.senderPhoto = senderPhoto;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getSenderId() {
        return senderId;
    }

    public void setSenderId(Integer senderId) {
        this.senderId = senderId;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getSenderPhoto() {
        return senderPhoto;
    }

    public void setSenderPhoto(String senderPhoto) {
        this.senderPhoto = senderPhoto;
    }
}
