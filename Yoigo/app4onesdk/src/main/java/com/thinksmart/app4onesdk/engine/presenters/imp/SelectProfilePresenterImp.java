package com.thinksmart.app4onesdk.engine.presenters.imp;

import com.thinksmart.app4onesdk.core.model.bos.CampaignBO;
import com.thinksmart.app4onesdk.engine.application.AppCore;
import com.thinksmart.app4onesdk.engine.presenters.ISelectProfilePresenter;
import com.thinksmart.app4onesdk.engine.views.ISelectProfileView;

/**
 * Created by roberto.demiguel on 17/10/2016.
 */

public class SelectProfilePresenterImp implements ISelectProfilePresenter {

    private ISelectProfileView view;

    public SelectProfilePresenterImp(ISelectProfileView selectProfileView) {
        this.view = selectProfileView;
    }

    @Override
    public void selectCampaign(CampaignBO selectedCampaign) {
        AppCore.getInstance().getSessionManager().getCurrentUser().setSelectedCampaign(selectedCampaign);
        view.navigateToTabsView();
    }

    @Override
    public void onDestroy() {
        view = null;
    }
}
