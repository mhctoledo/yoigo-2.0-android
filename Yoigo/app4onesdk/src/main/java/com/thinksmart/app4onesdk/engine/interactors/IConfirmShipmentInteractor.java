package com.thinksmart.app4onesdk.engine.interactors;

/**
 * Created by roberto.demiguel on 04/11/2016.
 */
public interface IConfirmShipmentInteractor {

    /**
     * This method creates request parameters, and sends a post request to get shipment. Listener will receive its result.
     *
     * @param token
     * @param campaignId
     * @param participantId
     */
    void getShipment(String token, Integer campaignId, Integer participantId);
}
