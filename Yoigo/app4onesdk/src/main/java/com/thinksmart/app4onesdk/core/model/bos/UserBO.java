package com.thinksmart.app4onesdk.core.model.bos;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;


/**
 * Created by roberto.demiguel on 13/10/2016.
 */
public class UserBO {

    @JsonProperty("token")
    private String token;
    @JsonProperty("language")
    private String language;
    @JsonProperty("nombreParticipant")
    private String name;
    @JsonProperty("changeKey")
    private Boolean changeKey;
    @JsonProperty("formData")
    private Boolean formData;
    @JsonProperty("campaings")
    private List<CampaignBO> campaignList;
    @JsonProperty("urlAvatar")
    private String urlAvatar;
    private Integer profileId;


    private CampaignBO selectedCampaign;

    //ADD INFO
    private String position;
    private Integer availablePoints;
    private String nameShort;
    private String lastName;
    private String email;
    private String nif;
    private String mobile;

    public UserBO() {
    }

    public String getUrlAvatar() {
        return urlAvatar;
    }

    public void setUrlAvatar(String urlAvatar) {
        this.urlAvatar = urlAvatar;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<CampaignBO> getCampaignList() {
        return campaignList;
    }

    public void setCampaignList(List<CampaignBO> campaignList) {
        this.campaignList = campaignList;
    }

    public Boolean getFormData() {
        return formData;
    }

    public void setFormData(Boolean formData) {
        this.formData = formData;
    }

    public Boolean getChangeKey() {
        return changeKey;
    }

    public void setChangeKey(Boolean changeKey) {
        this.changeKey = changeKey;
    }

    public CampaignBO getSelectedCampaign() {
        return selectedCampaign;
    }

    public void setSelectedCampaign(CampaignBO selectedCampaign) {
        this.selectedCampaign = selectedCampaign;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Integer getAvailablePoints() {
        return availablePoints;
    }

    public void setAvailablePoints(Integer availablePoints) {
        this.availablePoints = availablePoints;
    }

    public String getNameShort() {
        return nameShort;
    }

    public void setNameShort(String nameShort) {
        this.nameShort = nameShort;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Integer getProfileId() {
        return profileId;
    }

    public void setProfileId(Integer profileId) {
        this.profileId = profileId;
    }


}
