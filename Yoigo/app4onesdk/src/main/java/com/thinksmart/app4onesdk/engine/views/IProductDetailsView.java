package com.thinksmart.app4onesdk.engine.views;

import com.thinksmart.app4onesdk.core.model.bos.ProductBO;

/**
 * Created by roberto.demiguel on 26/10/2016.
 */
public interface IProductDetailsView {

    /**
     * Shows progress dialog
     */
    void showProgress();

    /**
     * Hides progress dialog
     */
    void hideProgress();

    /**
     * Displays product in view
     *
     * @param product to display
     */
    void showProduct(ProductBO product);

    /**
     * Shows error toast
     */
    void showRequestError(String error);

    /**
     * Shows server error toast
     */
    void showServerError();

    /**
     * Shows on confluent session error toast
     */
    void showOnExpiredTokenError();
}
