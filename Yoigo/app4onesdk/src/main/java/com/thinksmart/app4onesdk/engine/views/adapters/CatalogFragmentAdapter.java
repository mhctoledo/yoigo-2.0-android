package com.thinksmart.app4onesdk.engine.views.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;
import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.core.model.bos.ProductBO;
import com.thinksmart.app4onesdk.engine.views.fragments.CatalogFragment;

import java.util.List;

/**
 * Created by roberto.demiguel on 17/10/2016.
 */
public class CatalogFragmentAdapter extends RecyclerView.Adapter<CatalogFragmentAdapter.CatalogHolder> {

    private final static String TAG = "CatalogFragmentAdapter";
    private final static int VIEW_TYPE_ITEM = 0;
    private final static int VIEW_TYPE_LOADING = 1;

    private List<ProductBO> productList;
    private Context context;

    public CatalogFragmentAdapter(Context context, List<ProductBO> productList) {
        this.productList = productList;
        this.context = context;
    }


    @Override
    public int getItemViewType(int position) {
        return productList.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public CatalogHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CatalogHolder holder = null;
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_list_item, parent, false);
            holder = new CatalogFragmentAdapter.ProductHolder(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_list_item_loading_footer, parent, false);
            holder = new LoadingViewHolder(view);
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(CatalogHolder holder, int position) {
        if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);

        } else if (holder instanceof ProductHolder) {
            ProductHolder productHolder = (ProductHolder) holder;
            final ProductBO product = productList.get(position);
            productHolder.bind(product, context);
        }

    }


    @Override
    public int getItemCount() {
        return productList.size();
    }

    //HOLDERS
    static abstract class CatalogHolder extends RecyclerView.ViewHolder {
        CatalogHolder(View itemView) {
            super(itemView);
        }
    }

    private static class ProductHolder extends CatalogHolder {
        TextView nameTextView;
        TextView brandTextView;
        TextView pointsTextView;
        ImageView imageView;

        ProductHolder(View itemView) {
            super(itemView);

            nameTextView = (TextView) itemView.findViewById(R.id.sender_name);
            brandTextView = (TextView) itemView.findViewById(R.id.message_body);
            pointsTextView = (TextView) itemView.findViewById(R.id.product_points);
            imageView = (ImageView) itemView.findViewById(R.id.product_image);
        }

        void bind(@NonNull final ProductBO product, final Context context) {
            //name
            String name = Html.fromHtml(Html.fromHtml(product.getShortName()).toString()).toString();
            nameTextView.setText(name);
            //brand
            String brand = Html.fromHtml(Html.fromHtml(product.getBrand()).toString()).toString();
            brandTextView.setText(brand);
            //points
            pointsTextView.setText(product.getPointsFormatted(context));
            //image
            loadImage(product, context);
//            Picasso.with(context).load(product.getImageUrl()).fit().centerCrop().placeholder(R.drawable.progress_animation).into(imageView);

            itemView.setOnClickListener(new CatalogFragment.CatalogProductClickListener(product));
        }

        private void loadImage(@NonNull ProductBO product, Context context) {
            Transformation transformation = new Transformation() {
                @Override
                public Bitmap transform(Bitmap source) {
                    int targetWidth = imageView.getWidth();
                    double aspectRatio = (double) source.getHeight() / (double) source.getWidth();
                    int targetHeight = (int) (targetWidth * aspectRatio);
                    Bitmap result = Bitmap.createScaledBitmap(source, targetWidth, targetHeight, false);
                    if (result != source) {
                        // Same bitmap is returned if sizes are the same
                        source.recycle();
                    }
                    return result;
                }

                @Override
                public String key() {
                    return "transformation" + " desiredWidth";
                }
            };
            String url = product.getImageUrl().replaceAll(" ", "%20");
            Picasso.with(context).load(url).placeholder(R.drawable.progress_animation).transform(transformation).into(imageView);
        }

    }

    private static class LoadingViewHolder extends CatalogHolder {
        ProgressBar progressBar;

        LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.footer_progress_bar);
        }
    }


}
