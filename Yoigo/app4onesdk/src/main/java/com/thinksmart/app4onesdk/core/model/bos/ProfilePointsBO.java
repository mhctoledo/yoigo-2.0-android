package com.thinksmart.app4onesdk.core.model.bos;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.thinksmart.app4onesdk.core.model.enums.PointsType;

import java.util.List;

/**
 * Created by roberto.demiguel on 18/10/2016.
 */
public class ProfilePointsBO {

    @JsonProperty("item")
    private List<PointBO> profilePoints;

    public ProfilePointsBO() {
    }

    public PointBO getAvaiblePoints() {
        return findPointsType(PointsType.AVAILABLE);
    }

    public PointBO getRedeemPoints() {
        return findPointsType(PointsType.REDEEM);
    }

    public PointBO getTotalPoints() {
        return findPointsType(PointsType.TOTAL);
    }

    private PointBO findPointsType(PointsType pointsType) {
        PointBO point = null;
        int count = 0;
        while ((point == null) && (count < profilePoints.size())) {
            if (profilePoints.get(count).getName().equals(pointsType.getName())) {
                point = profilePoints.get(count);
            }
            count++;
        }
        return point;
    }

    public List<PointBO> getProfilePoints() {
        return profilePoints;
    }

    public void setProfilePoints(List<PointBO> profilePoints) {
        this.profilePoints = profilePoints;
    }


}
