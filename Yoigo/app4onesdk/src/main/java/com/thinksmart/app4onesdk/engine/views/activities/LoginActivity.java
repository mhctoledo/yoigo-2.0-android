package com.thinksmart.app4onesdk.engine.views.activities;

import android.os.Bundle;

import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.engine.views.fragments.LoginFragment;

/**
 * Created by roberto.demiguel on 13/10/2016.
 */
public class LoginActivity extends CustomActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        LoginFragment fragment = (LoginFragment) getSupportFragmentManager().findFragmentById(R.id.activity_login);
        if (fragment == null) {
            fragment = LoginFragment.newInstance();
            getSupportFragmentManager().beginTransaction().add(R.id.activity_login, fragment).commit();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
