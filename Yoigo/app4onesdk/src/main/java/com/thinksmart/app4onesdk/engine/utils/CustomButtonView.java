package com.thinksmart.app4onesdk.engine.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;

import com.thinksmart.app4onesdk.R;

/**
 * Created by roberto.demiguel on 20/04/2017.
 */

public class CustomButtonView extends AppCompatButton {

    public CustomButtonView(Context context) {
        super(context);
    }

    public CustomButtonView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initAttrs(context, attrs);
    }

    public CustomButtonView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initAttrs(context, attrs);
    }

    public void setRightDrawable(int resourceIcon){
        setCompoundDrawablesWithIntrinsicBounds(null, null, getResources().getDrawable(resourceIcon), null);
    }

    void initAttrs(Context context, AttributeSet attrs) {
        if (attrs != null) {
            TypedArray attributeArray = context.obtainStyledAttributes(
                    attrs, R.styleable.CustomButtonView);

            Drawable drawableLeft = null;
            Drawable drawableRight = null;
            Drawable drawableBottom = null;
            Drawable drawableTop = null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                drawableLeft = attributeArray.getDrawable(R.styleable.CustomButtonView_drawLeftCompat);
                drawableRight = attributeArray.getDrawable(R.styleable.CustomButtonView_drawRightCompat);
                drawableBottom = attributeArray.getDrawable(R.styleable.CustomButtonView_drawBottomCompat);
                drawableTop = attributeArray.getDrawable(R.styleable.CustomButtonView_drawTopCompat);
            } else {
                final int drawableLeftId = attributeArray.getResourceId(R.styleable.CustomButtonView_drawLeftCompat, -1);
                final int drawableRightId = attributeArray.getResourceId(R.styleable.CustomButtonView_drawRightCompat, -1);
                final int drawableBottomId = attributeArray.getResourceId(R.styleable.CustomButtonView_drawBottomCompat, -1);
                final int drawableTopId = attributeArray.getResourceId(R.styleable.CustomButtonView_drawTopCompat, -1);

                if (drawableLeftId != -1)
                    drawableLeft = AppCompatResources.getDrawable(context, drawableLeftId);
                if (drawableRightId != -1)
                    drawableRight = AppCompatResources.getDrawable(context, drawableRightId);
                if (drawableBottomId != -1)
                    drawableBottom = AppCompatResources.getDrawable(context, drawableBottomId);
                if (drawableTopId != -1)
                    drawableTop = AppCompatResources.getDrawable(context, drawableTopId);
            }
            setCompoundDrawablesWithIntrinsicBounds(drawableLeft, drawableTop, drawableRight, drawableBottom);
            attributeArray.recycle();
        }
    }
}