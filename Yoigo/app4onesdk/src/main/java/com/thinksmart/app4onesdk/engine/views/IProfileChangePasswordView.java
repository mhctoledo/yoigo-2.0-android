package com.thinksmart.app4onesdk.engine.views;

/**
 * Created by roberto.demiguel on 13/10/2016.
 */
public interface IProfileChangePasswordView {

    /**
     * Shows progress dialog
     */
    void showProgress();

    /**
     * Hides progress dialog
     */
    void hideProgress();


    /**
     * Show toast successful message
     */
    void onPasswordSuccessfullyChanged();

    /**
     * Shows  error toast
     */
    void showRequestError(String error);

    /**
     * Shows server error toast
     */
    void showServerError();

    /**
     * Shows on confluent session error toast
     */
    void showOnExpiredTokenError();
}
