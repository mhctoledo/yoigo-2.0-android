package com.thinksmart.app4onesdk.engine.application.appConfigModel;

public class Event {

    private String name;
    private String destination;

    public Event() {
    }

    public Event(String name, String destination) {
        this.name = name;
        this.destination = destination;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
