package com.thinksmart.app4onesdk.engine.interactors;

import java.util.Map;

/**
 * Created by roberto.demiguel on 13/10/2016.
 */

public interface IHomeInteractor {

    /**
     * This method creates request parameters, and sends a post request to get banners. Listener will receive its result.
     *
     * @param token
     * @param participantId
     * @param campaignId
     * @param extraData
     */
    void getBanners(String token, Integer participantId, Integer campaignId, Map<String, String> extraData);
}
