package com.thinksmart.app4onesdk.engine.presenters;

/**
 * Created by roberto.demiguel on 18/10/2016.
 */

public interface IPointsPresenter {
    /**
     * Call interector to get points and display them in view.
     */
    void getPoints();

    /**
     * Destroy view
     */
    void onDestroy();

}
