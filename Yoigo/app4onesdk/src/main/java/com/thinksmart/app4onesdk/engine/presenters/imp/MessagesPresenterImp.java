package com.thinksmart.app4onesdk.engine.presenters.imp;

import android.content.Context;

import com.thinksmart.app4onesdk.core.model.bos.MessagesParentBO;
import com.thinksmart.app4onesdk.engine.application.AppCore;
import com.thinksmart.app4onesdk.engine.interactors.IMessagesInteractor;
import com.thinksmart.app4onesdk.engine.interactors.IPresenterListener;
import com.thinksmart.app4onesdk.engine.interactors.imp.MessagesInteractorImp;
import com.thinksmart.app4onesdk.engine.presenters.IMessagesPresenter;
import com.thinksmart.app4onesdk.engine.views.IMessagesView;
import com.thinksmart.app4onesdk.engine.views.utils.SessionManagerUtil;

/**
 * Created by roberto.demiguel on 14/11/2016.
 */
public class MessagesPresenterImp implements IMessagesPresenter {

    private IMessagesInteractor interactor;
    private IMessagesView view;
    private Context context;

    public MessagesPresenterImp(IMessagesView view, Context context) {
        this.view = view;
        this.interactor = new MessagesInteractorImp(new GetMessagesListener());
        this.context = context;
    }


    @Override
    public void onDestroy() {
        view = null;
    }


    @Override
    public void getMessages() {
        if (view != null) {
            view.showProgress();
        }
        String token = AppCore.getInstance().getSessionManager().getCurrentUser().getToken();
        Integer participantId = AppCore.getInstance().getSessionManager().getCurrentUser().getSelectedCampaign().getParticipantId();
        Integer campaignId = AppCore.getInstance().getSessionManager().getCurrentUser().getSelectedCampaign().getId();
        interactor.getMessages(token, participantId, campaignId);
    }


    private class GetMessagesListener implements IPresenterListener<MessagesParentBO> {

        GetMessagesListener() {
        }

        @Override
        public void onSuccess(MessagesParentBO messages) {
            if (messages == null) {
                messages = new MessagesParentBO();
            }
            if (view != null) {
                view.hideProgress();
                view.showMessages(messages);
            }
        }

        @Override
        public void onExpiredToken() {
            showOnExpiredTokenErrorHelper();
        }

        @Override
        public void onError(Integer code, String error) {
            onErrorHelper(error);
        }

        @Override
        public void onServerError(String error) {
            onServerErrorHelper(error);
        }
    }

    private void showOnExpiredTokenErrorHelper() {
        if (view != null) {
            view.hideProgress();
            view.showOnExpiredTokenError();
        }
    }


    private void onErrorHelper(String error) {
        if (view != null) {
            view.hideProgress();
            view.showRequestError(error);
        }
    }

    private void onServerErrorHelper(String error) {
        if (view != null) {
            view.hideProgress();
            view.showServerError();
        }
    }
}
