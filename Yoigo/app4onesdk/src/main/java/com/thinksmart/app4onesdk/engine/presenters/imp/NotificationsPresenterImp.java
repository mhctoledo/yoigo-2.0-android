package com.thinksmart.app4onesdk.engine.presenters.imp;

import android.content.Context;

import com.thinksmart.app4onesdk.core.model.bos.NotificationsParentBO;
import com.thinksmart.app4onesdk.engine.application.AppCore;
import com.thinksmart.app4onesdk.engine.interactors.INotificationsInteractor;
import com.thinksmart.app4onesdk.engine.interactors.IPresenterListener;
import com.thinksmart.app4onesdk.engine.interactors.imp.NotificationsInteractorImp;
import com.thinksmart.app4onesdk.engine.presenters.INotificationsPresenter;
import com.thinksmart.app4onesdk.engine.views.INotificationsView;
import com.thinksmart.app4onesdk.engine.views.utils.SessionManagerUtil;

/**
 * Created by roberto.demiguel on 14/11/2016.
 */
public class NotificationsPresenterImp implements INotificationsPresenter, IPresenterListener<NotificationsParentBO> {

    private INotificationsInteractor interactor;
    private INotificationsView view;
    private Context context;

    public NotificationsPresenterImp(INotificationsView view, Context context) {
        this.view = view;
        this.interactor = new NotificationsInteractorImp(this);
        this.context = context;
    }

    @Override
    public void getNotifications() {
        if (view != null) {
            view.showProgress();
        }
        String token = AppCore.getInstance().getSessionManager().getCurrentUser().getToken();
        Integer participantId = AppCore.getInstance().getSessionManager().getCurrentUser().getSelectedCampaign().getParticipantId();
        Integer campaignId = AppCore.getInstance().getSessionManager().getCurrentUser().getSelectedCampaign().getId();
        interactor.getNotifications(token, participantId, campaignId);
    }

    @Override
    public void onDestroy() {
        view = null;
    }

    @Override
    public void onSuccess(NotificationsParentBO notificationsParent) {
        if (notificationsParent == null) {
            notificationsParent = new NotificationsParentBO();
        }
        if (view != null) {
            view.hideProgress();
            view.showNotifications(notificationsParent);
        }
    }

    @Override
    public void onExpiredToken() {
        if (view != null) {
            view.hideProgress();
            view.showOnExpiredTokenError();
        }
    }


    @Override
    public void onError(Integer code, String error) {
        if (view != null) {
            view.hideProgress();
            view.showRequestError(error);
        }
    }

    @Override
    public void onServerError(String error) {
        if (view != null) {
            view.hideProgress();
            view.showServerError();
        }
    }
}
