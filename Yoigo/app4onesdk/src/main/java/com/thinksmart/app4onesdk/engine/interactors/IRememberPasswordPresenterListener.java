package com.thinksmart.app4onesdk.engine.interactors;


/**
 * Created by roberto.demiguel on 04/11/2016.
 */
public interface IRememberPasswordPresenterListener {

    /**
     * Callback from Request on success
     */
    void onSuccess();


    /**
     * User is not registered.
     */
    void onNonExistingUserError();


    /**
     * Callback from Request on error
     *
     * @param error from server
     */
    void onError(Integer code, String error);

    /**
     * Callback from Request on server success
     *
     * @param error from volley
     */
    void onServerError(String error);
}
