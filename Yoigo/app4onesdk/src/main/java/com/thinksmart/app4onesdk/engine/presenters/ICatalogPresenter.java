package com.thinksmart.app4onesdk.engine.presenters;

import com.thinksmart.app4onesdk.core.vos.CatalogVO;

/**
 * Created by roberto.demiguel on 24/10/2016.
 */
public interface ICatalogPresenter {

    /**
     * Call interector to get catalog and display them in view.
     *
     * @param catalogVO
     */
    void getCatalog(CatalogVO catalogVO);

    /**
     * Destroy view
     */
    void onDestroy();
}
