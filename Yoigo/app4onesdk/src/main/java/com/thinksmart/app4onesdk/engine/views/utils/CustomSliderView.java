package com.thinksmart.app4onesdk.engine.views.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

/**
 * Created by roberto.demiguel on 22/05/2017.
 */
public class CustomSliderView extends CustomBaseSliderView {

    public CustomSliderView(Context context) {
        super(context);
    }

    @Override
    public View getView() {
        View v = LayoutInflater.from(getContext()).inflate(com.daimajia.slider.library.R.layout.render_type_default, null);
        ImageView target = (ImageView) v.findViewById(com.daimajia.slider.library.R.id.daimajia_slider_image);
        //bindEventAndShow(v, target);
        bindEventAndShow(v, target, true);
        return v;
    }
}
