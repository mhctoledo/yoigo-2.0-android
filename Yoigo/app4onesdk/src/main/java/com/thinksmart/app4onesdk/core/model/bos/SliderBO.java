package com.thinksmart.app4onesdk.core.model.bos;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

/**
 * Created by roberto.demiguel on 17/10/2016.
 */
public class SliderBO implements Serializable {

    @JsonProperty("banner")
    private List<BannerBO> bannerList;

    public SliderBO() {
    }

    public List<BannerBO> getBannerList() {
        return bannerList;
    }

    public void setBannerList(List<BannerBO> bannerList) {
        this.bannerList = bannerList;
    }
}
