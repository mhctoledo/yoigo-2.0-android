package com.thinksmart.app4onesdk.engine.views.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.core.model.bos.NotificationBO;

import java.util.List;

/**
 * Created by roberto.demiguel on 17/10/2016.
 */
public class NotificationsFragmentAdapter extends RecyclerView.Adapter<NotificationsFragmentAdapter.MessageHolder> {

    private List<NotificationBO> notificationList;

    public NotificationsFragmentAdapter(List<NotificationBO> notificationList) {
        this.notificationList = notificationList;
    }


    @Override
    public MessageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_list_item, parent, false);
        return new MessageHolder(view);
    }

    @Override
    public void onBindViewHolder(MessageHolder holder, int position) {
        final NotificationBO notification = notificationList.get(position);
        holder.bind(notification);
    }

    @Override
    public int getItemCount() {
        return notificationList.size();
    }

    static class MessageHolder extends RecyclerView.ViewHolder {
        TextView date;
        TextView message;

        MessageHolder(View itemView) {
            super(itemView);
            date = (TextView) itemView.findViewById(R.id.notification_date);
            message = (TextView) itemView.findViewById(R.id.notification_message);
        }

        void bind(@NonNull final NotificationBO notification) {
            //message
            message.setText(notification.getMessage());
            //date
            date.setText(notification.getCreatedDate());
        }
    }

}
