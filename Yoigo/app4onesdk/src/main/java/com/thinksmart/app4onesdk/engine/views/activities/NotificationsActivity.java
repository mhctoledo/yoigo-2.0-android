package com.thinksmart.app4onesdk.engine.views.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.engine.views.fragments.NotificationsFragment;

public class NotificationsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);

        initToolbar();

        NotificationsFragment fragment = (NotificationsFragment) getSupportFragmentManager().findFragmentById(R.id.notifications_container);
        if (fragment == null) {
            fragment = NotificationsFragment.newInstance();
            getSupportFragmentManager().beginTransaction().add(R.id.notifications_container, fragment).commit();
        }
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_with_icon);
        toolbar.setTitle(R.string.notifications_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
