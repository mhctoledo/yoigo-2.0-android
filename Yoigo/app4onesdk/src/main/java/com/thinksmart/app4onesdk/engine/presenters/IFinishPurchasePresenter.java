package com.thinksmart.app4onesdk.engine.presenters;

import com.thinksmart.app4onesdk.core.model.bos.BasketBO;

/**
 * Created by roberto.demiguel on 04/11/2016.
 */
public interface IFinishPurchasePresenter {

    /**
     * Call interector to obtain points
     */
    void checkAvailablePointsBeforeRedeem();

    /**
     * Call interector to redeem points
     *
     * @param password
     * @param currentBasket
     */
    void redeemPoints(String password, BasketBO currentBasket);

    /**
     * Destroy view
     */
    void onDestroy();
}
