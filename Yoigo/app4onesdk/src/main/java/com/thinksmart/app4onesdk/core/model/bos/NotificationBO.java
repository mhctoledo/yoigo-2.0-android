package com.thinksmart.app4onesdk.core.model.bos;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by roberto.demiguel on 14/11/2016.
 */
public class NotificationBO {

    @JsonProperty("Message")
    private String message;
    @JsonProperty("Created")
    private String createdDate;

    public NotificationBO() {
    }

    public NotificationBO(String message, String createdDate) {
        this.message = message;
        this.createdDate = createdDate;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
