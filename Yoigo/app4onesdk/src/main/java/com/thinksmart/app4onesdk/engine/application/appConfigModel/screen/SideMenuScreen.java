package com.thinksmart.app4onesdk.engine.application.appConfigModel.screen;

import com.thinksmart.app4onesdk.engine.application.appConfigModel.Event;

import java.util.Map;

public class SideMenuScreen extends Screen {

    private SideMenu sideMenu;

    public SideMenuScreen(String resource, Map<String, Event> events) {
        super(resource, events);
    }

    public SideMenuScreen(String resource, Map<String, Event> events, SideMenu sideMenu) {
        super(resource, events);
        this.sideMenu = sideMenu;
    }

    public SideMenuScreen(String resource, SideMenu sideMenu) {
        super(resource);
        this.sideMenu = sideMenu;
    }

    public SideMenu getSideMenu() {
        return sideMenu;
    }

    public void setSideMenu(SideMenu sideMenu) {
        this.sideMenu = sideMenu;
    }
}

