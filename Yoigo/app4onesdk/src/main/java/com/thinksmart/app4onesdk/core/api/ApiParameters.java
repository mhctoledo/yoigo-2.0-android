package com.thinksmart.app4onesdk.core.api;

/**
 * Created by roberto.demiguel on 09/03/2017.
 */

public class ApiParameters {
    public static final String TOKEN = "token";
    public static final String PARTICIPANT_ID = "id_participante";
    public static final String CAMPAIGN_ID = "id_campana";
    public static final String CURRENT_PAGE = "current_page";
    public static final String SEARCH = "search";
    public static final String GROUP_ID = "id_grupo";
    public static final String SETCION_ID = "id_seccion";
    public static final String SUBSETCION_ID = "id_subseccion";
    public static final String NAME = "name";
    public static final String SURNAME = "apellidos";
    public static final String NIF = "nif";
    public static final String EMAIL = "ds_email";
    public static final String PHONE = "movil";
    public static final String DATA_ASSIGNMENT = "cesion_datos";
    public static final String PASSWORD = "password";
    public static final String BASKET = "basket";
    public static final String EXTRA_DATA = "extraData";
    public static final String USERNAME = "username";
    public static final String LANGUAGE = "language";
    public static final String LANGUAGE_ES = "es";
    public static final String DEVICE_APP_ID = "DeviceAppId";
    public static final String SO = "SO";
    public static final String SO_ANDROID = "Android";
    public static final String DEVICE = "Device";
    public static final String SCREEN_SIZE = "ScreenSize";
    public static final String APPLICATION_VERSION = "ApplicationVersion";
    public static final String BUNDLE_CODE = "bundleCode";
    public static final String SENDER_ID = "id_sender";
    public static final String RECEIVER_ID = "id_receiver";
    public static final String MESSAGE = "message";
    public static final String DEVICE_ID = "id_device";
    public static final String CAMPAIGN_NAME = "name_campana";
    public static final String PRODUCT_ID = "cd_producto_s";
    public static final String IS_SERIE = "EsSerie";
    public static final String NEW_PASSWORD = "new_password";
    public static final String OLD_PASSWORD = "old_password";
    public static final String LOGIN = "login";
}
