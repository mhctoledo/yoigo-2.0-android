package com.thinksmart.app4onesdk.engine.views.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.core.model.bos.ShipmentBO;
import com.thinksmart.app4onesdk.engine.application.AppCore;
import com.thinksmart.app4onesdk.engine.views.utils.StringUtils;

/**
 * Created by roberto.demiguel on 31/10/2016.
 */
public class ChangeShipmentFragment extends Fragment {

    private ShipmentBO shipment;

    private EditText cifField;
    private EditText emailField;
    private EditText zipCodeField;
    private EditText companyField;
    private EditText stateField;
    private EditText address1Field;
    private EditText address2Field;
    private EditText cityField;
    private EditText countryField;
    private EditText phoneField;
    private boolean bmwShipmentRules;
    private boolean vfShipmentRules;


    public ChangeShipmentFragment() {
        // Required empty public constructor
    }

    public static ChangeShipmentFragment newInstance() {
        return new ChangeShipmentFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        shipment = AppCore.getInstance().getSessionManager().getCurrentBasket().getShipment();
        bmwShipmentRules = AppCore.getInstance().getAppConfig().isBmwShipmentRules();
        vfShipmentRules = AppCore.getInstance().getAppConfig().isVfShipmentRules();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_change_shipment, container, false);

        initUIReferences(root);
        fillData();

        return root;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.form_menu, menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_save) {
            saveShipment();
        }
        return super.onOptionsItemSelected(item);
    }


    private void initUIReferences(View root) {
        //fields
        cifField = (EditText) root.findViewById(R.id.csh_cif_value);
        emailField = (EditText) root.findViewById(R.id.csh_email_value);
        zipCodeField = (EditText) root.findViewById(R.id.csh_zip_code_value);
        companyField = (EditText) root.findViewById(R.id.csh_company_value);
        stateField = (EditText) root.findViewById(R.id.csh_state_value);
        address1Field = (EditText) root.findViewById(R.id.csh_address1_value);
        address2Field = (EditText) root.findViewById(R.id.csh_address2_value);
        cityField = (EditText) root.findViewById(R.id.csh_city_value);
        countryField = (EditText) root.findViewById(R.id.csh_country_value);
        phoneField = (EditText) root.findViewById(R.id.csh_phone_value);

        if (bmwShipmentRules) {
            setAsNotEditable(cifField);
            setAsNotEditable(emailField);
            hideCompanyField(root);
        } else if (vfShipmentRules) {
            setAsNotEditable(cifField);
            hideCompanyField(root);
            hideAddress2Field(root);
        }

        root.findViewById(R.id.change_shipment_layout).requestFocus();
    }

    private void hideAddress2Field(View root) {
        TextView address2Label = (TextView) root.findViewById(R.id.csh_address2_label);
        address2Label.setVisibility(View.GONE);
        address2Field.setVisibility(View.GONE);
    }

    private void hideCompanyField(View root) {
        TextView companyLabel = (TextView) root.findViewById(R.id.profile_company_label);
        companyLabel.setVisibility(View.GONE);
        companyField.setVisibility(View.GONE);
    }

    private void setAsNotEditable(EditText editText) {
        editText.setTag(editText.getKeyListener());
        editText.setKeyListener(null);
        editText.setBackgroundResource(R.drawable.not_editable_data_field_backgorund);
    }

    private void saveShipment() {
        if (areMandatoryFieldsFilled()) {
            if (shipment == null) {
                shipment = new ShipmentBO();
            }
            shipment.setCif(cifField.getText().toString());
            shipment.setEmail(emailField.getText().toString());
            shipment.setPostal(zipCodeField.getText().toString());
            shipment.setCompany(companyField.getText().toString());
            shipment.setState(stateField.getText().toString());
            shipment.setAddress1(address1Field.getText().toString());
            shipment.setAddress2(address2Field.getText().toString());
            shipment.setCity(cityField.getText().toString());
            shipment.setCountry(countryField.getText().toString());
            shipment.setPhone(phoneField.getText().toString());

            //modified
            shipment.setChangedInSession(true);

            getActivity().finish();
        }
    }


    private boolean areMandatoryFieldsFilled() {
        boolean correct = true;
        String cif = cifField.getText().toString();
        String email = emailField.getText().toString();
        String zipCode = zipCodeField.getText().toString();
        String company = companyField.getText().toString();
        String state = stateField.getText().toString();
        String address1 = address1Field.getText().toString();
        String city = cityField.getText().toString();
        String country = countryField.getText().toString();
        String phone = phoneField.getText().toString();

        boolean bmwRulesMandatoryFieldsFilled = true;
        boolean vfRulesMandatoryFieldsFilled = true;

        String fieldName = "";
        if (TextUtils.isEmpty(cif)) {
            fieldName = getString(R.string.sh_cif_label);
            correct = false;
            bmwRulesMandatoryFieldsFilled = false;
        } else if (TextUtils.isEmpty(email)) {
            fieldName = getString(R.string.sh_email_label);
            correct = false;
            bmwRulesMandatoryFieldsFilled = false;
        } else if (TextUtils.isEmpty(zipCode)) {
            fieldName = getString(R.string.sh_zip_code_label);
            correct = false;
        } else if (!bmwShipmentRules && !vfShipmentRules && TextUtils.isEmpty(company)) {
            fieldName = getString(R.string.sh_company_label);
            correct = false;
        } else if (TextUtils.isEmpty(state)) {
            fieldName = getString(R.string.sh_state_label);
            correct = false;
        } else if (TextUtils.isEmpty(address1)) {
            fieldName = getString(R.string.sh_address_label);
            correct = false;
            vfRulesMandatoryFieldsFilled = false;
        } else if (TextUtils.isEmpty(city)) {
            fieldName = getString(R.string.sh_city_label);
            correct = false;
        } else if (TextUtils.isEmpty(country)) {
            fieldName = getString(R.string.sh_country_label);
            correct = false;
        } else if (TextUtils.isEmpty(phone)) {
            fieldName = getString(R.string.sh_phone_label);
            correct = false;
        }

        if ((bmwShipmentRules && !bmwRulesMandatoryFieldsFilled) || (vfShipmentRules && !vfRulesMandatoryFieldsFilled)) {
            Toast.makeText(getActivity(), String.format(getString(R.string.bmw_shipment_fields_are_mandatory), fieldName), Toast.LENGTH_LONG).show();
        } else if (!correct) {
            Toast.makeText(getActivity(), String.format(getString(R.string.shipment_fields_are_mandatory), fieldName), Toast.LENGTH_LONG).show();
        }

        return correct;
    }


    private void fillData() {
        if (shipment != null) {
            cifField.setText(StringUtils.formatString(shipment.getCif()));
            emailField.setText(StringUtils.formatString(shipment.getEmail()));
            zipCodeField.setText(StringUtils.formatString(shipment.getPostal()));
            companyField.setText(StringUtils.formatString(shipment.getCompany()));
            stateField.setText(StringUtils.formatString(shipment.getState()));
            address1Field.setText(StringUtils.formatString(shipment.getAddress1()));
            address2Field.setText(StringUtils.formatString(shipment.getAddress2()));
            cityField.setText(StringUtils.formatString(shipment.getCity()));
            countryField.setText(StringUtils.formatString(shipment.getCountry()));
            phoneField.setText(StringUtils.formatString(String.valueOf(shipment.getPhone())));
        }
    }

}
