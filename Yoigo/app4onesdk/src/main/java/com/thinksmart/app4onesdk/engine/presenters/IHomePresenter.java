package com.thinksmart.app4onesdk.engine.presenters;

/**
 * Created by roberto.demiguel on 17/10/2016.
 */
public interface IHomePresenter {

    /**
     * Call interector to get banners and display them in view.
     */
    void getBanners();

    /**
     * Destroy view
     */
    void onDestroy();
}
