package com.thinksmart.app4onesdk.engine.views.holders;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.RotateAnimation;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thinksmart.app4onesdk.R;
import com.unnamed.b.atv.model.TreeNode;

/**
 * Created by roberto.demiguel on 28/11/2016.
 */
public class PointsTreeNodeHolder extends TreeNode.BaseNodeViewHolder<PointsTreeNodeHolder.PointsTreeNodeItem> {
    private static final float INITIAL_POSITION = 0.0f;
    private static final float ROTATED_POSITION = 180f;

    private TextView nodeName;
    private RelativeLayout nodeIcon;

    public PointsTreeNodeHolder(Context context) {
        super(context);
    }

    @Override
    public View createNodeView(final TreeNode node, PointsTreeNodeItem value) {
        final LayoutInflater inflater = LayoutInflater.from(context);
        final View view = inflater.inflate(R.layout.layout_points_node, null, false);
        View rowNode = view.findViewById(R.id.points_node_row);

        nodeIcon = (RelativeLayout) view.findViewById(R.id.points_node_icon);
        nodeName = (TextView) view.findViewById(R.id.points_node_name);
        nodeName.setText(value.text);

        if (node.isLeaf()) {
            nodeIcon.setVisibility(View.GONE);
        }

        rowNode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tView.toggleNode(node);
            }
        });

        return view;
    }


    @Override
    public void toggle(boolean active) {
        RotateAnimation rotateAnimation;
        if (!active) { // rotate counterclockwise
            nodeIcon.setRotation(INITIAL_POSITION);
            rotateAnimation = new RotateAnimation(-1 * ROTATED_POSITION,
                    INITIAL_POSITION,
                    RotateAnimation.RELATIVE_TO_SELF, 0.5f,
                    RotateAnimation.RELATIVE_TO_SELF, 0.5f);
        } else { // rotate clockwise
            nodeIcon.setRotation(ROTATED_POSITION);
            rotateAnimation = new RotateAnimation(ROTATED_POSITION,
                    INITIAL_POSITION,
                    RotateAnimation.RELATIVE_TO_SELF, 0.5f,
                    RotateAnimation.RELATIVE_TO_SELF, 0.5f);
        }
        rotateAnimation.setDuration(200);
        rotateAnimation.setFillAfter(true);
        nodeIcon.startAnimation(rotateAnimation);
    }

    public static class PointsTreeNodeItem {
        public String text;

        public PointsTreeNodeItem(String text) {
            this.text = text;
        }
    }
}
