package com.thinksmart.app4onesdk.engine.interactors.imp;

import android.support.annotation.NonNull;

import com.thinksmart.app4onesdk.core.api.ApiParameters;
import com.thinksmart.app4onesdk.core.api.ApiResponse;
import com.thinksmart.app4onesdk.core.api.ApiUrls;
import com.thinksmart.app4onesdk.core.model.bos.RedeemPointsResultBO;
import com.thinksmart.app4onesdk.engine.application.AppCore;
import com.thinksmart.app4onesdk.engine.interactors.IFinishPurchaseInteractor;
import com.thinksmart.app4onesdk.engine.interactors.IPresenterListener;
import com.thinksmart.app4onesdk.engine.net.IRequestCallback;
import com.thinksmart.app4onesdk.engine.net.ResponseMapperUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by roberto.demiguel on 04/11/2016.
 */
public class FinishPurchaseInteractorImp implements IFinishPurchaseInteractor, IRequestCallback {

    private IPresenterListener presenterListener;

    public FinishPurchaseInteractorImp(IPresenterListener presenterListener) {
        this.presenterListener = presenterListener;
    }

    @Override
    public void redeemPoints(String token, Integer participantId, Integer campaignId, String password, String basket) {
        final JSONObject params = getJsonObject(token, participantId, campaignId, password, basket);
        AppCore.getInstance().getNetworkManager().postRequest(params, ApiUrls.redeemURL, this);
    }

    @Override
    public void onRequestSuccess(JSONObject response) {
        try {
            ResponseMapperUtil.parseResponse(response, this.presenterListener, RedeemPointsResultBO.class);
        } catch (JSONException | IOException e) {
            this.presenterListener.onError(ApiResponse.Code.ERROR_PARSING_RESPONSE, ApiResponse.ERROR_PARSING_RESPONSE);
        }
    }

    @Override
    public void onRequestError(String error) {
        presenterListener.onServerError(error);
    }


    @NonNull
    private JSONObject getJsonObject(String token, Integer participantId, Integer campaignId, String password, String basket) {
        final JSONObject params = new JSONObject();
        try {
            params.put(ApiParameters.TOKEN, token);
            params.put(ApiParameters.PARTICIPANT_ID, participantId);
            params.put(ApiParameters.CAMPAIGN_ID, campaignId);
            params.put(ApiParameters.PASSWORD, password);
            params.put(ApiParameters.BASKET, basket);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }
}
