package com.thinksmart.app4onesdk.core.model.bos;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by roberto.demiguel on 17/10/2016.
 */
public class MessagesParentBO {
    @JsonProperty("messages")
    private List<MessageBO> messagesList;

    public MessagesParentBO() {
        messagesList = new ArrayList<>();
    }

    public List<MessageBO> getMessagesList() {
        return messagesList;
    }

    public void setMessagesList(List<MessageBO> messagesList) {
        this.messagesList = messagesList;
    }
}
