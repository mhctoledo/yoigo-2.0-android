package com.thinksmart.app4onesdk.core.model.util;

import android.content.Context;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thinksmart.app4onesdk.core.model.bos.BasketBO;
import com.thinksmart.app4onesdk.core.model.bos.BasketItemBO;
import com.thinksmart.app4onesdk.core.model.constants.UserPreferences;
import com.thinksmart.app4onesdk.engine.utils.preferencesManager.IPreferencesManager;
import com.thinksmart.app4onesdk.engine.utils.preferencesManager.imp.PreferencesManagerImp;

import java.io.IOException;

/**
 * Created by roberto.demiguel on 07/12/2016.
 */

public class BasketUtil {

    public static BasketBO getCurrentBasket(Context context) {
        BasketBO basketObj = new BasketBO();
        try {
            IPreferencesManager preferencesManager = new PreferencesManagerImp(context);
            String basketString = preferencesManager.retrieveStringPreference(UserPreferences.BASKET);
            if (basketString != null) {
                ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                basketObj = mapper.readValue(basketString, BasketBO.class);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return basketObj;
    }

    public static void saveBasket(Context context, BasketBO basket) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            IPreferencesManager preferencesManager = new PreferencesManagerImp(context);
            String jsonBasket = objectMapper.writeValueAsString(basket);
            preferencesManager.saveStringPreference(UserPreferences.BASKET, jsonBasket);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }


    public static String convertToXml(BasketBO basket) {
        //TODO replace in a generic way like that
//        try {
//            ObjectMapper xmlMapper = new XmlMapper();
//            String xml2 = xmlMapper.writeValueAsString(basket);
//
//        } catch (JsonProcessingException e) {
//            e.printStackTrace();
//        }

        String xml = "";

        // Basket
        xml += "<basket>";

        if (basket.getShipment().getCif() != null) {
            xml += "<cif>" + escapeXMLChars(basket.getShipment().getCif()) + "</cif>";
        }
        xml += "<email>" + escapeXMLChars(basket.getShipment().getEmail()) + "</email>";
        xml += "<postal>" + escapeXMLChars(basket.getShipment().getPostal().toString()) + "</postal>";
        if (basket.getShipment().getCompany() != null) {
            xml += "<company>" + escapeXMLChars(basket.getShipment().getCompany()) + "</company>";
        }
        xml += "<address1>" + escapeXMLChars(basket.getShipment().getAddress1()) + "</address1>";
        if (basket.getShipment().getAddress2() != null) {
            xml += "<address2>" + escapeXMLChars(basket.getShipment().getAddress2()) + "</address2>";
        }
        xml += "<fullname>" + escapeXMLChars(basket.getShipment().getFullname()) + "</fullname>";
        xml += "<city>" + escapeXMLChars(basket.getShipment().getCity()) + "</city>";
        xml += "<country>" + escapeXMLChars(basket.getShipment().getCountry()) + "</country>";
        xml += "<state>" + escapeXMLChars(basket.getShipment().getState()) + "</state>";
        xml += "<tel>" + escapeXMLChars(basket.getShipment().getPhone()) + "</tel>";
        xml += "<amount>" + basket.getItems().keySet().size() + "</amount>";

        xml += "<products>";
        for (String itemKey : basket.getItems().keySet()) {
            BasketItemBO item = basket.getItems().get(itemKey);
            xml += "<product>";
            xml += "<idProduct>" + item.getProductId() + "</idProduct>";
            xml += "<idCatalog>" + item.getIdCatalog() + "</idCatalog>";
            xml += "<quantity>" + item.getUnits().toString() + "</quantity>";
            xml += "</product>";
        }

        xml += "</products>";

        xml += "</basket>";

        return xml;
    }


    private static String escapeXMLChars(String s) {
        return s.replaceAll("&", "&amp;")
                .replaceAll("'", "&apos;")
                .replaceAll("\"", "&quot;")
                .replaceAll("<", "&lt;")
                .replaceAll(">", "&gt;");
    }
}
