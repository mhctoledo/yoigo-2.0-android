package com.thinksmart.app4onesdk.engine.interactors.imp;

import android.support.annotation.NonNull;

import com.thinksmart.app4onesdk.core.api.ApiParameters;
import com.thinksmart.app4onesdk.core.api.ApiResponse;
import com.thinksmart.app4onesdk.core.api.ApiUrls;
import com.thinksmart.app4onesdk.core.model.bos.CatalogBO;
import com.thinksmart.app4onesdk.core.vos.CatalogVO;
import com.thinksmart.app4onesdk.engine.application.AppCore;
import com.thinksmart.app4onesdk.engine.interactors.ICatalogInteractor;
import com.thinksmart.app4onesdk.engine.interactors.IPresenterListener;
import com.thinksmart.app4onesdk.engine.net.IRequestCallback;
import com.thinksmart.app4onesdk.engine.net.ResponseMapperUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by roberto.demiguel on 25/10/2016.
 */
public class CatalogInteractorImp implements ICatalogInteractor, IRequestCallback {

    private IPresenterListener presenterListener;

    public CatalogInteractorImp(IPresenterListener listener) {
        this.presenterListener = listener;
    }

    @Override
    public void getCatalog(CatalogVO catalogVO) {
        final JSONObject params = getJsonObject(catalogVO);
        AppCore.getInstance().getNetworkManager().postRequest(params, ApiUrls.productSearchURL, this);
    }

    @Override
    public void onRequestSuccess(JSONObject response) {
        try {
            ResponseMapperUtil.parseResponse(response, this.presenterListener, CatalogBO.class);
        } catch (JSONException | IOException e) {
            this.presenterListener.onError(ApiResponse.Code.ERROR_PARSING_RESPONSE, ApiResponse.ERROR_PARSING_RESPONSE);
        }
    }

    @Override
    public void onRequestError(String error) {
        presenterListener.onServerError(error);

    }

    @NonNull
    private JSONObject getJsonObject(CatalogVO catalogVO) {
        final JSONObject params = new JSONObject();
        try {
            params.put(ApiParameters.TOKEN, catalogVO.getToken());
            params.put(ApiParameters.PARTICIPANT_ID, catalogVO.getParticipantId());
            params.put(ApiParameters.CAMPAIGN_ID, catalogVO.getCampaignId());
            params.put(ApiParameters.CURRENT_PAGE, catalogVO.getPage());
            //optional
            String search = catalogVO.getSearch();
            if (search != null && !search.isEmpty()) {
                params.put(ApiParameters.SEARCH, search);
            }
            //optional
            Integer groupId = catalogVO.getGroupId();
            Integer sectionId = catalogVO.getSectionId();
            Integer subSectionId = catalogVO.getSubSectionId();
            params.put(ApiParameters.GROUP_ID, groupId);
            params.put(ApiParameters.SETCION_ID, sectionId);
            params.put(ApiParameters.SUBSETCION_ID, subSectionId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }
}
