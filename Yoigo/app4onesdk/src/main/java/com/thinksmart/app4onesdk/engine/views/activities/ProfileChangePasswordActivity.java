package com.thinksmart.app4onesdk.engine.views.activities;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.engine.views.fragments.ProfileChangePasswordFragment;

/**
 * Created by roberto.demiguel on 02/11/2016.
 */
public class ProfileChangePasswordActivity extends CustomActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_change_password);

        initToolbar();

        ProfileChangePasswordFragment fragment = (ProfileChangePasswordFragment) getSupportFragmentManager().findFragmentById(R.id.change_password_container);
        if (fragment == null) {
            fragment = ProfileChangePasswordFragment.newInstance();
            getSupportFragmentManager().beginTransaction().add(R.id.change_password_container, fragment).commit();
        }
    }


    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.profile_change_password_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
