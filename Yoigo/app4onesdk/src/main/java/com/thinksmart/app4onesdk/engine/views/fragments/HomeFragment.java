package com.thinksmart.app4onesdk.engine.views.fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.core.model.bos.HomeBO;
import com.thinksmart.app4onesdk.core.model.bos.SliderBO;
import com.thinksmart.app4onesdk.core.model.bos.UserBO;
import com.thinksmart.app4onesdk.core.vos.CatalogVO;
import com.thinksmart.app4onesdk.engine.application.AppCore;
import com.thinksmart.app4onesdk.engine.application.utils.NavigationUtil;
import com.thinksmart.app4onesdk.engine.presenters.IHomePresenter;
import com.thinksmart.app4onesdk.engine.presenters.imp.HomePresenterImp;
import com.thinksmart.app4onesdk.engine.utils.AlertDialogUtil;
import com.thinksmart.app4onesdk.engine.views.IHomeView;
import com.thinksmart.app4onesdk.engine.views.activities.TabBarActivity;
import com.thinksmart.app4onesdk.engine.views.adapters.HomeFragmentAdapter;
import com.thinksmart.app4onesdk.engine.views.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by roberto.demiguel on 17/10/2016.
 */
public class HomeFragment extends Fragment implements IHomeView {

    private static final String HOME_STATE = "hState";

    private ProgressDialog dialog;
    private IHomePresenter presenter;
    private List<SliderBO> sliderList;
    private HomeFragmentAdapter adapter;
    private UserBO user;
    private TextView pointsLabel;
    private TextView deptLabel;
    private TextView userLabel;
    private RelativeLayout userInfoLayout;
    private CircleImageView userImage;
    private SwipeRefreshLayout refreshLayout;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new HomePresenterImp(this, getActivity());
        user = AppCore.getInstance().getSessionManager().getCurrentUser();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        sliderList = new ArrayList<>();
        //init ui
        initUIReferences(view);
        initEvents();

        if(!AppCore.getInstance().getAppConfig().isUserInfoLayoutOnHome()){
            userInfoLayout.setVisibility(View.GONE);
        }

        //restore state after rotation
        if (savedInstanceState != null) {
            HomeBO homeBOSaved = (HomeBO) savedInstanceState.getSerializable(HOME_STATE);
            if (homeBOSaved != null && homeBOSaved.getSliderList() != null) {
                showBanners(homeBOSaved.getSliderList());
            }
        } else {
            presenter.getBanners();
        }

        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        HomeBO homeBO = new HomeBO();
        homeBO.setSliderList(sliderList);
        outState.putSerializable(HOME_STATE, homeBO);
    }


    private void initUIReferences(View root) {
        dialog = new ProgressDialog(getActivity());
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        refreshLayout = (SwipeRefreshLayout) root.findViewById(R.id.slider_list_swiper_refresh);
        ListView sliderListView = (ListView) root.findViewById(R.id.slider_list);
        adapter = new HomeFragmentAdapter(getContext(), this, sliderList);
        sliderListView.setAdapter(adapter);
        sliderListView.setDivider(getActivity().getResources().getDrawable(R.drawable.banner_divider));

        userLabel = (TextView) root.findViewById(R.id.home_profile_user_label);
        pointsLabel = (TextView) root.findViewById(R.id.home_user_points);
        deptLabel = (TextView) root.findViewById(R.id.home_profile_dept_label);
        userImage = (CircleImageView) root.findViewById(R.id.home_profile_user_image);
        userInfoLayout = (RelativeLayout) root.findViewById(R.id.home_user_info_layout);
    }

    private void initEvents() {
        refreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        presenter.getBanners();
                    }
                }
        );

        pointsLabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((TabBarActivity) getActivity()).goToPointsTab();
            }
        });
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void showProgress() {
        showProgressDialog();
    }

    @Override
    public void onResume() {
        super.onResume();
        user = AppCore.getInstance().getSessionManager().getCurrentUser();
        setProfileInfo();
    }

    @Override
    public void setProfileInfo() {
        //set header panel info
        Integer points = user.getAvailablePoints();
        String availablePoints = StringUtils.formatPointsWithUnits(points, getContext());
        pointsLabel.setText(availablePoints);
        deptLabel.setText(StringUtils.formatString(user.getPosition()));
        userLabel.setText(StringUtils.formatString(user.getName()));
        //image
        if (user.getUrlAvatar() != null && !user.getUrlAvatar().isEmpty()) {
            Picasso.with(getActivity()).load(user.getUrlAvatar()).placeholder(getResources().getDrawable(R.drawable.ico_select_user)).fit().centerInside().into(userImage);
        }

        //set navigation drawer info
        NavigationView navigationView = (NavigationView) getActivity().findViewById(R.id.nav_view);
        if (navigationView.getHeaderCount() > 0) {
            View headerLayout = navigationView.getHeaderView(0);
            //user name
            TextView userLabel = (TextView) headerLayout.findViewById(R.id.nav_profile_user_label);
            userLabel.setText(StringUtils.formatString(user.getName()));
            //points
            TextView pointsLabel = (TextView) headerLayout.findViewById(R.id.nav_profile_points_label);
            pointsLabel.setText(availablePoints);
            //position
            TextView deptLabel = (TextView) headerLayout.findViewById(R.id.nav_profile_dept_label);
            deptLabel.setText(StringUtils.formatString(user.getPosition()));
            //image
            if (user.getUrlAvatar() != null && !user.getUrlAvatar().isEmpty()) {
                CircleImageView userImageView = (CircleImageView) headerLayout.findViewById(R.id.panel_edit_profile_image);
                Picasso.with(getActivity()).load(user.getUrlAvatar()).placeholder(getResources().getDrawable(R.drawable.ico_select_user)).fit().centerInside().into(userImageView);
            }
        }
    }

    @Override
    public void hideProgress() {
        dialog.dismiss();
        refreshLayout.setRefreshing(false);
    }

    @Override
    public void showBanners(List<SliderBO> sliderList) {
        this.sliderList.clear();
        if (sliderList != null) {
            this.sliderList.addAll(sliderList);
            //refresh data
            this.adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void showRequestError(String error) {
        AlertDialogUtil.showAlertDialog(getActivity(), error);
    }

    @Override
    public void showServerError() {
        AlertDialogUtil.showAlertDialog(getActivity(), R.string.server_error);
    }

    @Override
    public void showOnExpiredTokenError() {
        AlertDialogUtil.showAlertDialogAndCloseSession(getActivity(), R.string.on_confluent_session_error);
    }

    private void showProgressDialog() {
        dialog.setMessage(getString(R.string.loading_dialog));
        dialog.show();
    }

    @Override
    public void navigateToWebView(String search) {
        List<String> listParams = new ArrayList<>();
        listParams.add(search);

        String eventDestination = "goToWebViewFromHome";
        NavigationUtil.goToNextScreen(HomeFragment.class.getName(), eventDestination, listParams);
    }

    @Override
    public void navigateToCatalog(CatalogVO catalogVO) {
        ((TabBarActivity) getActivity()).goToCatalogTab(catalogVO);
    }

    @Override
    public void navigateToScreen(String destination) {
        NavigationUtil.goToNextScreen(HomeFragment.class.getName(), destination);
    }
}
