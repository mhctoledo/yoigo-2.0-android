package com.thinksmart.app4onesdk.engine.application.utils;


import android.support.v4.app.Fragment;

import com.thinksmart.app4onesdk.engine.application.appConfigModel.screen.Screen;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * Created by roberto.demiguel on 14/10/2016.
 */
public class MakerManager {

    //Methods
    public static Class<?> makeClassFromScreen(Screen screen) {
        Class<?> c = null;
        if (screen.getResource() != null) {
            try {
                c = Class.forName(screen.getResource());
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return c;
    }

    public static Fragment createFragmentFromScreen(Screen screen) {
        try {
            Class<?> c = Class.forName(screen.getResource());
            Constructor<?> constructor = c.getConstructor();
            return (Fragment) constructor.newInstance();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

}
