package com.thinksmart.app4onesdk.engine.views.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;

import com.thinksmart.app4onesdk.engine.application.AppCore;
import com.thinksmart.app4onesdk.engine.application.appConfigModel.screen.Screen;

/**
 * Created by roberto.demiguel on 31/10/2016.
 */
public class CustomActivity extends AppCompatActivity {

    protected Screen screen;
    private Boolean observerIsAdded;
    private Boolean firstTimeHere;

    public CustomActivity() {
        observerIsAdded = false;
        firstTimeHere = false;
    }

    //Lifecycle
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        firstTimeHere = true;

        AppCore.getInstance().getNavigationManager().setCurrentActivity(this);
        Bundle extras = getIntent().getExtras();

        String screenName = extras.getString("screenName");
        screen = AppCore.getInstance().getAppConfig().getScreenFromName(screenName);

        addObservers();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (firstTimeHere) {
            addObservers();
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        addObservers();
    }

    @Override
    protected void onPause() {
        super.onPause();
        removeObservers();
    }

    @Override
    protected void onStop() {
        super.onStop();
        removeObservers();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        removeObservers();
    }

    //Methods
    private void addObservers() {
        if (!observerIsAdded) {
            AppCore.getInstance().getEventManager().addObserver(screen);
            observerIsAdded = true;
        }
    }

    private void removeObservers() {
        if (observerIsAdded) {
            AppCore.getInstance().getEventManager().removeObserver(screen);
            observerIsAdded = false;
        }
    }
}
