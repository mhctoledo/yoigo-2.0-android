package com.thinksmart.app4onesdk.engine.presenters.imp;

import android.content.Context;

import com.thinksmart.app4onesdk.core.model.bos.ProfilePointsBO;
import com.thinksmart.app4onesdk.engine.application.AppCore;
import com.thinksmart.app4onesdk.engine.interactors.IPointsInteractor;
import com.thinksmart.app4onesdk.engine.interactors.IPresenterListener;
import com.thinksmart.app4onesdk.engine.interactors.imp.PointsInteractorImp;
import com.thinksmart.app4onesdk.engine.presenters.IPointsPresenter;
import com.thinksmart.app4onesdk.engine.views.IPointsView;
import com.thinksmart.app4onesdk.engine.views.utils.SessionManagerUtil;

/**
 * Created by roberto.demiguel on 18/10/2016.
 */
public class PointsPresenterImp implements IPointsPresenter, IPresenterListener<ProfilePointsBO> {

    private IPointsInteractor interactor;
    private IPointsView view;
    private Context context;


    public PointsPresenterImp(IPointsView view, Context context) {
        this.view = view;
        this.interactor = new PointsInteractorImp(this);
        this.context = context;
    }

    @Override
    public void getPoints() {
        if (view != null) {
            view.showProgress();
        }
        String token = AppCore.getInstance().getSessionManager().getCurrentUser().getToken();
        Integer participantId = AppCore.getInstance().getSessionManager().getCurrentUser().getSelectedCampaign().getParticipantId();
        Integer campaignId = AppCore.getInstance().getSessionManager().getCurrentUser().getSelectedCampaign().getId();
        interactor.getPoints(token, participantId, campaignId);
    }

    @Override
    public void onDestroy() {
        view = null;
    }

    @Override
    public void onSuccess(ProfilePointsBO profilePoints) {
        Integer availablePoints = profilePoints.getAvaiblePoints().getValue().intValue();
        AppCore.getInstance().getSessionManager().getCurrentUser().setAvailablePoints(availablePoints);

        if (view != null) {
            view.showPoints(profilePoints);
            view.hideProgress();
        }
    }

    @Override
    public void onExpiredToken() {
        if (view != null) {
            view.hideProgress();
            view.showOnExpiredTokenError();
        }
    }

    @Override
    public void onError(Integer code, String error) {
        if (view != null) {
            view.hideProgress();
            view.showRequestError(error);
        }
    }

    @Override
    public void onServerError(String error) {
        if (view != null) {
            view.hideProgress();
            view.showServerError();
        }
    }
}
