package com.thinksmart.app4onesdk.engine.net;


import org.json.JSONObject;

/**
 * Created by roberto.demiguel on 14/10/2016.
 */
public interface IRequestCallback {

    /**
     * Callback from Request on success
     *
     * @param response obtained from request
     */
    void onRequestSuccess(JSONObject response);

    /**
     * Callback from Request on error
     *
     * @param error from server
     */
    void onRequestError(String error);
}