package com.thinksmart.app4onesdk.engine.application.utils;

import java.util.Observable;

/**
 * Created by david.lopez on 21/1/16.
 */

public class CustomObservable extends Observable {

    private String name;

    public CustomObservable(String name) {
        this.name = name;
    }

    // To force notifications to be sent
    public void notifyObservers(Object data) {
        setChanged();
        super.notifyObservers(data);
    }

    public String getName() {
        return name;
    }
}
