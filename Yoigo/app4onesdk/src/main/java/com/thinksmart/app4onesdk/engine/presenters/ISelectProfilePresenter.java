package com.thinksmart.app4onesdk.engine.presenters;

import com.thinksmart.app4onesdk.core.model.bos.CampaignBO;

/**
 * Created by roberto.demiguel on 13/10/2016.
 */

public interface ISelectProfilePresenter {


    /**
     * Sets campaign selected in current session and goes to Tabs view.
     *
     * @param selectedCampaign
     */
    void selectCampaign(CampaignBO selectedCampaign);

    /**
     * Destroy view
     */
    void onDestroy();
}
