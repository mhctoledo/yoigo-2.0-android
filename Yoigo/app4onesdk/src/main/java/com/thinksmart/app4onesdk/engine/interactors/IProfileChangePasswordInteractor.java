package com.thinksmart.app4onesdk.engine.interactors;

/**
 * Created by roberto.demiguel on 02/11/2016.
 */

public interface IProfileChangePasswordInteractor {

    /**
     * This method creates request parameters, and sends a post request to change password. Listener will receive its result.
     *
     * @param token
     * @param newPassword
     * @param oldPassword
     */
    void changePassword(String token, String newPassword, String oldPassword);
}
