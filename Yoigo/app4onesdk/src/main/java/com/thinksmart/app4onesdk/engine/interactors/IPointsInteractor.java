package com.thinksmart.app4onesdk.engine.interactors;

/**
 * Created by roberto.demiguel on 18/10/2016.
 */
public interface IPointsInteractor {

    /**
     * This method creates request parameters, and sends a post request to get points. Listener will receive its result.
     *
     * @param token
     * @param participantId
     * @param campaignId
     */
    void getPoints(String token, Integer participantId, Integer campaignId);
}
