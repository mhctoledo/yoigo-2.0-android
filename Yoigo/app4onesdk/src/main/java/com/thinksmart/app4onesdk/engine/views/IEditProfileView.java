package com.thinksmart.app4onesdk.engine.views;

/**
 * Created by roberto.demiguel on 02/11/2016.
 */
public interface IEditProfileView {

    /**
     * Shows progress dialog
     */
    void showProgress();

    /**
     * Hides progress dialog
     */
    void hideProgress();


    /**
     * Show toast successful message
     */
    void onDataSuccessfullyUpdated();

    /**
     * Shows  error toast
     */
    void showRequestError(String error);

    /**
     * Shows server error toast
     */
    void showServerError();

    /**
     * Shows on confluent session error toast
     */
    void showOnExpiredTokenError();
}
