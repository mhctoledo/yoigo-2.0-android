package com.thinksmart.app4onesdk.engine.net;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

/**
 * Created by roberto.demiguel on 14/10/2016.
 */
public class NetworkManager {

    private static final String TAG = "NetworkManager";

    //for Volley API
    private RequestQueue requestQueue;

    public NetworkManager() {
    }

    public NetworkManager(Context context) {
        requestQueue = Volley.newRequestQueue(context.getApplicationContext());
    }

    public RequestQueue getRequestQueue() {
        return requestQueue;
    }

    public void setRequestQueue(RequestQueue requestQueue) {
        this.requestQueue = requestQueue;
    }

    /**
     * This method sends a post request. Listener will receive its result.
     *
     * @param jsonParams - request params
     * @param url        - url to send request
     * @param listener   - which will get result
     */
    public void postRequest(JSONObject jsonParams, String url, final IRequestCallback listener) {
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                        listener.onRequestSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error.networkResponse != null) {
                            Log.d(TAG, "Error Response code: " + error.networkResponse.statusCode);
                            listener.onRequestError(error.getMessage());
                        }else{
                            Log.d(TAG, error.getLocalizedMessage());
                            listener.onRequestError(null);
                        }
                    }
                });

        request.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(request);
    }
}
