package com.thinksmart.app4onesdk.engine.views.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.core.model.bos.BasketBO;
import com.thinksmart.app4onesdk.core.model.bos.BasketItemBO;
import com.thinksmart.app4onesdk.engine.views.utils.StringUtils;

/**
 * Created by roberto.demiguel on 01/11/2016.
 */
public class FinishPurchaseFragmentAdapter extends RecyclerView.Adapter {

    private BasketBO basket;
    private Context context;

    public FinishPurchaseFragmentAdapter(Context context, BasketBO basket) {
        this.basket = basket;
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return basket.getItems().size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_finish_purchase_item, parent, false);
        return new FinishPurchaseFragmentAdapter.CartItemHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final FinishPurchaseFragmentAdapter.CartItemHolder viewHolder = (FinishPurchaseFragmentAdapter.CartItemHolder) holder;

        final BasketItemBO basketItem = basket.getBasketAsList().get(position);
        if (basketItem != null) {
            //name
            String name = Html.fromHtml(Html.fromHtml(basketItem.getName()).toString()).toString();
            viewHolder.nameTextView.setText(name);
            //brand
            viewHolder.brandTextView.setText(basketItem.getBrand());
            //quantity
            viewHolder.quantityTextView.setText(StringUtils.formatQuantity(basketItem.getUnits(), context));
            //points
            Double points = basketItem.getUnits() * basketItem.getPoints();
            viewHolder.pointsTextView.setText(StringUtils.formatPointsWithUnits(points, context));
            //points per unit
            viewHolder.pointsPerUnit.setText(StringUtils.formatPointsWithoutUnits(points, context));
            //color
            if (basketItem.getColor() != null) {
                viewHolder.colorSelectedLayout.setVisibility(View.VISIBLE);
                viewHolder.cartItemColor.setBackgroundColor(Color.parseColor(basketItem.getColor()));
            }
            //extra selector
            if (basketItem.getExtraTag() != null && basketItem.getExtraValue() != null) {
                viewHolder.extraSelectedLayout.setVisibility(View.VISIBLE);
                String selectedLabel = basketItem.getExtraTag() + ":";
                viewHolder.extraSelectedLabel.setText(selectedLabel);
                viewHolder.extraSelectedValue.setText(basketItem.getExtraValue());
            }
            //image
            String url = basketItem.getImageUrl().replaceAll(" ", "%20");
            Picasso.with(context).load(url).fit().centerInside().placeholder(R.drawable.progress_animation).into(viewHolder.imageView);
        }
    }

    private static class CartItemHolder extends RecyclerView.ViewHolder {
        private TextView nameTextView;
        private TextView brandTextView;
        private TextView pointsTextView;
        private TextView quantityTextView;
        private ImageView imageView;

        private TextView pointsPerUnit;
        private RelativeLayout colorSelectedLayout;
        private RelativeLayout extraSelectedLayout;
        private ImageView cartItemColor;
        private TextView extraSelectedLabel;
        private TextView extraSelectedValue;


        CartItemHolder(View itemView) {
            super(itemView);
            nameTextView = (TextView) itemView.findViewById(R.id.fp_product_name);
            brandTextView = (TextView) itemView.findViewById(R.id.fp_product_brand);
            pointsTextView = (TextView) itemView.findViewById(R.id.fp_product_points);
            quantityTextView = (TextView) itemView.findViewById(R.id.fp_quantity_value);
            imageView = (ImageView) itemView.findViewById(R.id.fp_product_image);

            colorSelectedLayout = (RelativeLayout) itemView.findViewById(R.id.color_selected_layout);
            extraSelectedLayout = (RelativeLayout) itemView.findViewById(R.id.extra_selected_layout);
            cartItemColor = (ImageView) itemView.findViewById(R.id.fp_item_color);
            extraSelectedLabel = (TextView) itemView.findViewById(R.id.extra_selected_label);
            extraSelectedValue = (TextView) itemView.findViewById(R.id.extra_selected_value);
            pointsPerUnit = (TextView) itemView.findViewById(R.id.points_per_unit_value);
        }
    }


}
