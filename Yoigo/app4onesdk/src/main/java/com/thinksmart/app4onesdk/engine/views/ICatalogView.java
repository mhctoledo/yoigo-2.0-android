package com.thinksmart.app4onesdk.engine.views;

import com.thinksmart.app4onesdk.core.model.bos.ProductBO;

import java.util.List;

/**
 * Created by roberto.demiguel on 24/10/2016.
 */

public interface ICatalogView {

    /**
     * Shows progress dialog
     */
    void showProgress();

    /**
     * Sets profile info (points) in header
     */
    void setHeaderInfo();

    /**
     * Hides progress dialog
     */
    void hideProgress();

    /**
     * Displays products in view
     *
     * @param result - to show
     */
    void showCatalog(List<ProductBO> result);

    /**
     * Shows error toast
     */
    void showRequestError(String error);

    /**
     * Shows server error toast
     */
    void showServerError();

    /**
     * Shows on confluent session error toast
     */
    void showOnExpiredTokenError();
}
