package com.thinksmart.app4onesdk.engine.interactors.imp;

import android.support.annotation.NonNull;

import com.thinksmart.app4onesdk.core.api.ApiParameters;
import com.thinksmart.app4onesdk.core.api.ApiResponse;
import com.thinksmart.app4onesdk.core.api.ApiUrls;
import com.thinksmart.app4onesdk.engine.application.AppCore;
import com.thinksmart.app4onesdk.engine.interactors.IRememberPasswordInteractor;
import com.thinksmart.app4onesdk.engine.interactors.IRememberPasswordPresenterListener;
import com.thinksmart.app4onesdk.engine.net.IRequestCallback;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by roberto.demiguel on 02/11/2016.
 */

public class RememberPasswordInteractorImp implements IRememberPasswordInteractor, IRequestCallback {

    private IRememberPasswordPresenterListener presenterListener;

    public RememberPasswordInteractorImp(IRememberPasswordPresenterListener presenterListener) {
        this.presenterListener = presenterListener;
    }

    @Override
    public void resetPassword(String user) {
        final JSONObject params = getJsonObject(user);
        AppCore.getInstance().getNetworkManager().postRequest(params, ApiUrls.forgetPassURL, this);
    }


    @Override
    public void onRequestSuccess(JSONObject response) {
        try {
            parseResponse(response, this.presenterListener);
        } catch (JSONException e) {
            this.presenterListener.onError(ApiResponse.Code.ERROR_PARSING_RESPONSE, ApiResponse.ERROR_PARSING_RESPONSE);
        }
    }


    private void parseResponse(JSONObject response, final IRememberPasswordPresenterListener listener) throws JSONException {
        JSONObject meta = (JSONObject) response.get(ApiResponse.META);
        int code = meta.getInt(ApiResponse.CODE);
        if (code == ApiResponse.Code.SUCCESS) {
            listener.onSuccess();
        } else if (code == ApiResponse.Code.NON_EXISTING_USER) {
            listener.onNonExistingUserError();
        } else {
            String error = meta.getString(ApiResponse.ERROR_DESCRIPTION);
            listener.onError(code, error);
        }
    }

    @Override
    public void onRequestError(String error) {
        presenterListener.onServerError(error);
    }

    @NonNull
    private JSONObject getJsonObject(String user) {
        final JSONObject params = new JSONObject();
        try {
            params.put(ApiParameters.LOGIN, (user != null) ? user : "");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

}
