package com.thinksmart.app4onesdk.engine.views;

import com.thinksmart.app4onesdk.core.model.bos.MessageAddressesBO;

/**
 * Created by roberto.demiguel on 17/10/2016.
 */
public interface ICreateMessageView {

    /**
     * Shows progress dialog
     */
    void showProgress();

    /**
     * Hides progress dialog
     */
    void hideProgress();

    /**
     * Show toast successful message
     */
    void onMessageSuccessfullySent();

    /**
     * Populates addresses in view
     *
     * @param messageAddresses
     */
    void populateAddresses(MessageAddressesBO messageAddresses);

    /**
     * Shows error toast
     */
    void showRequestError(String error);

    /**
     * Shows server error toast
     */
    void showServerError();

    /**
     * Shows on confluent session error toast
     */
    void showOnExpiredTokenError();

}
