package com.thinksmart.app4onesdk.engine.views.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.view.MenuItem;

import com.thinksmart.app4onesdk.R;

/**
 * Created by roberto.demiguel on 17/10/2016.
 */
public class CartUtils {

    public static void setBadgeCount(Context context, MenuItem cartItem, Integer count) {
        if (cartItem != null && count != null) {
            LayerDrawable icon = (LayerDrawable) cartItem.getIcon();

            BadgeDrawable badge;
            // Reuse drawable
            Drawable reuse = icon.findDrawableByLayerId(R.id.ic_badge);
            if (reuse != null && reuse instanceof BadgeDrawable) {
                badge = (BadgeDrawable) reuse;
            } else {
                badge = new BadgeDrawable(context);
            }

            badge.setCount(count);
            icon.mutate();
            icon.setDrawableByLayerId(R.id.ic_badge, badge);
        }
    }
}