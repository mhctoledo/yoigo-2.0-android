package com.thinksmart.app4onesdk.engine.views.activities;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.engine.views.fragments.CartFragment;

/**
 * Created by roberto.demiguel on 31/10/2016.
 */
public class CartActivity extends CustomActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        initToolbar();

        CartFragment fragment = (CartFragment) getSupportFragmentManager().findFragmentById(R.id.cart_container);
        if (fragment == null) {
            fragment = CartFragment.newInstance();
            getSupportFragmentManager().beginTransaction().add(R.id.cart_container, fragment).commit();
        }
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_with_points);
        toolbar.setTitle(R.string.cart_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
