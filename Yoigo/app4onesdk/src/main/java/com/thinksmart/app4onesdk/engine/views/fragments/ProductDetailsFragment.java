package com.thinksmart.app4onesdk.engine.views.fragments;


import android.app.ProgressDialog;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.core.model.bos.BasketBO;
import com.thinksmart.app4onesdk.core.model.bos.BasketItemBO;
import com.thinksmart.app4onesdk.core.model.bos.ProductBO;
import com.thinksmart.app4onesdk.core.model.bos.ProductImageBO;
import com.thinksmart.app4onesdk.engine.application.AppCore;
import com.thinksmart.app4onesdk.engine.application.utils.NavigationUtil;
import com.thinksmart.app4onesdk.engine.presenters.IProductDetailsPresenter;
import com.thinksmart.app4onesdk.engine.presenters.imp.ProductDetailsPresenterImp;
import com.thinksmart.app4onesdk.engine.utils.AlertDialogUtil;
import com.thinksmart.app4onesdk.engine.views.IProductDetailsView;
import com.thinksmart.app4onesdk.engine.views.activities.ProductDetailsActivity;
import com.thinksmart.app4onesdk.engine.views.utils.CartUtils;
import com.thinksmart.app4onesdk.engine.views.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by roberto.demiguel on 26/10/2016.
 */
public class ProductDetailsFragment extends Fragment implements IProductDetailsView {

    private static final String ARG_PARAM = "params";
    private static final String mimeType = "text/html";
    private static final String encoding = "UTF-8";
    private static final String COLOR_PREFFIX = "#";

    private MenuItem cartMenuItem;
    private IProductDetailsPresenter presenter;
    private ProgressDialog dialog;
    private String productId;
    private String isSerie;
    private String selectedColor;
    private String selectedExtraValue;

    //
    private SliderLayout productSlider;
    private WebView productDescription;
    private TextView productNameLabel;
    private TextView productBrandLabel;
    private TextView productPointsLabel;

    private RelativeLayout productDetailSelectorLayout;
    private RelativeLayout colorPickerLayout;
    private RelativeLayout productDetailExtraSelectorLayout;
    private LinearLayout colorPickerContainer;
    private Spinner productDetailExtraSpinner;
    private TextView productDetailExtraLabel;

    private ProductBO initialProduct;
    private ProductBO firstLevelProduct;
    private ProductBO secondLevelProduct;

    private BasketBO currentBasket;
    private FloatingActionButton cartActionButton;
    private boolean comingFromCart = false;

    public ProductDetailsFragment() {
        // Required empty public constructor
    }

    public static ProductDetailsFragment newInstance(ArrayList<String> productParams) {
        ProductDetailsFragment fragment = new ProductDetailsFragment();
        Bundle args = new Bundle();
        args.putStringArrayList(ARG_PARAM, productParams);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            ArrayList<String> productParams = getArguments().getStringArrayList(ARG_PARAM);
            if (productParams != null) {
                if (productParams.size() >= 2) {
                    productId = productParams.get(0);
                    isSerie = productParams.get(1);
                }
                if (productParams.size() == 4) {
                    selectedColor = productParams.get(2);
                    selectedExtraValue = productParams.get(3);
                    comingFromCart = true;
                }
            }
        }
        setHasOptionsMenu(true);
        this.currentBasket = AppCore.getInstance().getSessionManager().getCurrentBasket();
        presenter = new ProductDetailsPresenterImp(this, getActivity());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_product_details, container, false);

        initUIReferences(root);
        presenter.getProduct(productId, isSerie);
        return root;
    }


    @Override
    public void onResume() {
        super.onResume();
        if (initialProduct != null) {
            ProductBO selectedProduct = getCurrentProduct();
            //update cart action button if we are coming back after removing this item in cart view
            setCartActionButton(selectedProduct);
            //update cart count
            CartUtils.setBadgeCount(getContext(), cartMenuItem, currentBasket.getQuantity());

        }
    }

    /**
     * Checks sub levels to get current product
     *
     * @return current product
     */
    private ProductBO getCurrentProduct() {
        ProductBO selectedProduct;
        if (firstLevelProduct == null) {
            selectedProduct = initialProduct;
        } else if (secondLevelProduct == null) {
            selectedProduct = firstLevelProduct;
        } else {
            selectedProduct = secondLevelProduct;
        }
        return selectedProduct;
    }

    private void initUIReferences(View root) {
        dialog = new ProgressDialog(getActivity());
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        cartActionButton = (FloatingActionButton) root.findViewById(R.id.cart_action_button);
        cartActionButton.setOnClickListener(new CartActionButtonListener());
        //product images slider
        productSlider = (SliderLayout) root.findViewById(R.id.product_slider);
        productSlider.setPresetTransformer(SliderLayout.Transformer.Default);
        productSlider.setCustomAnimation(new DescriptionAnimation());
        productSlider.stopAutoCycle();
        productSlider.setCustomIndicator((PagerIndicator) root.findViewById(R.id.product_custom_indicator));
        //labels
        productNameLabel = (TextView) root.findViewById(R.id.product_detail_name);
        productBrandLabel = (TextView) root.findViewById(R.id.product_detail_brand);
        productPointsLabel = (TextView) root.findViewById(R.id.product_detail_points);
        productDescription = (WebView) root.findViewById(R.id.product_detail_description);
        //selector container layout
        productDetailSelectorLayout = (RelativeLayout) root.findViewById(R.id.product_detail_selector);
        //color container view
        colorPickerContainer = (LinearLayout) root.findViewById(R.id.color_list_container);
        colorPickerLayout = (RelativeLayout) root.findViewById(R.id.product_detail_color_selector);
        //extra selector container view
        productDetailExtraSelectorLayout = (RelativeLayout) root.findViewById(R.id.product_detail_extra_selector);
        productDetailExtraSpinner = (Spinner) root.findViewById(R.id.product_detail_extra_spinner);
        productDetailExtraLabel = (TextView) root.findViewById(R.id.product_detail_extra_label);


        //set points label
        TextView productDetailsPointsLabel = (TextView) getActivity().findViewById(R.id.product_details_points_label);

        double points = AppCore.getInstance().getSessionManager().getCurrentUser().getAvailablePoints();
        String availablePoints = StringUtils.formatPointsWithUnits(points, getContext());
        if (availablePoints != null) {
            productDetailsPointsLabel.setText(availablePoints);
        }
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.product_details_menu, menu);
        cartMenuItem = menu.findItem(R.id.pd_action_cart);
        cartMenuItem.setVisible(!comingFromCart);

        CartUtils.setBadgeCount(getContext(), cartMenuItem, currentBasket.getQuantity());
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.pd_action_cart) {
            if (this.currentBasket.getItems().size() > 0) {
                final String eventDestination = "goToCartFromProductDetails";
                NavigationUtil.goToNextScreen(ProductDetailsActivity.class.getName(), eventDestination);
            } else {
                Toast.makeText(getActivity(), R.string.empty_cart, Toast.LENGTH_SHORT).show();
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void showProduct(ProductBO product) {
        initialProduct = product;
        //set common info
        String name = Html.fromHtml(product.getShortName()).toString();
        productNameLabel.setText(name);
        productBrandLabel.setText(product.getBrand());

        if (product.getProdDescription() != null) {
//            String description = Html.fromHtml(product.getProdDescription()).toString();
            String description = product.getProdDescription();
            //add css in order to set font style according to android
            String htmlDescription = "<html><head> <style type=\"text/css\">body{font-family: \"HelveticaNeue-UltraLight\", \"Segoe UI\", \"Roboto Light\", sans-serif;}</style></head><body>"
                    + description + "</body></html>";
            productDescription.loadDataWithBaseURL("", htmlDescription, mimeType, encoding, "");
        }

        //set product info if available
        setSliderImagesFromProductIfAvailable(product);
        setPointsInfoIfAvailable(product);
        ///
        List<ProductBO> subProductList = product.getFirstLevelSelector();
        if (subProductList != null) {
            setSubproductData(subProductList);
        }
        //cart button
        setCartActionButton(getCurrentProduct());
        selectedExtraValue = null;
        selectedColor = null;
    }

    private void setCartActionButton(ProductBO product) {
        //get proper productId
        String productId;
        if (initialProduct.getSerie()) {
            productId = product.getProductId();
        } else {
            productId = product.getId();
        }
        //product id starts with 'Z', we must remove it before requesting product details
        String realProductId = productId.substring(1);
        cartActionButton.setVisibility(View.VISIBLE);
        if (currentBasket.existProductInBasket(realProductId)) {
            setRemoveItemButton();
        } else {
            setAddItemButton();
        }
    }

    private void setAddItemButton() {
        cartActionButton.setImageDrawable(getResources().getDrawable(R.drawable.add_shopping_cart_icon));
        cartActionButton.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.addButtonColor)));
//        cartActionButton.setBackgroundColor(getResources().getColor(R.color.addButtonColor));

    }

    private void setRemoveItemButton() {
        cartActionButton.setImageDrawable(getResources().getDrawable(R.drawable.remove_shopping_cart_icon));
        cartActionButton.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.removeBackroundColor)));
//        cartActionButton.setBackgroundColor(getResources().getColor(R.color.removeButtonColor));
    }

    private void setSubproductData(List<ProductBO> subProductList) {
        //in case we come from cart, to select initial values
        Integer selectedPreviousExtraItem = null;
        List<String> spinnerItems = new ArrayList<>();
        boolean firstItem = true;
        for (int i = 0; i < subProductList.size(); i++) {
            ProductBO subProduct = subProductList.get(i);
            if (subProduct.getValue().startsWith(COLOR_PREFFIX)) {
                LayoutInflater inflater = LayoutInflater.from(getActivity());
                View colorItem = inflater.inflate(R.layout.color_item, null, false);
                colorItem.setTag(subProduct);
                colorItem.findViewById(R.id.color_item).setBackgroundColor(Color.parseColor(subProduct.getValue()));
                colorPickerContainer.addView(colorItem);

                colorItem.setOnClickListener(new ColorSelectorListener());

                //select first option as default value
                if (selectedColor != null && selectedColor.equals(subProduct.getValue())) {
                    setColorSelectorInitialValues(subProduct, colorItem);
                } else if (selectedColor == null && firstItem) {
                    setColorSelectorInitialValues(subProduct, colorItem);
                    firstItem = false;
                }

            } else {
                if (selectedExtraValue != null && selectedExtraValue.equals(subProduct.getValue())) {
                    setExtraSelectorInitialValues(subProduct);

                    selectedPreviousExtraItem = i;
                } else if (selectedExtraValue == null && firstItem) {
                    //set selector container visible
                    setExtraSelectorInitialValues(subProduct);

                    firstItem = false;
                }
                //add items to spinner items array
                spinnerItems.add(subProduct.getValue());
            }

        }

        //if spinnerItems contains values we have to set these items in spinner
        if (!spinnerItems.isEmpty()) {
            ArrayAdapter spinnerAdapter = new ArrayAdapter<>(getActivity(), R.layout.product_spinner_item, spinnerItems);
            spinnerAdapter.setDropDownViewResource(R.layout.product_spinner_dropdown);
            productDetailExtraSpinner.setAdapter(spinnerAdapter);

            productDetailExtraSpinner.setOnItemSelectedListener(new ExtraSelectorListener());
            if (selectedPreviousExtraItem != null) {
                productDetailExtraSpinner.setSelection(selectedPreviousExtraItem);
            }


        }
    }

    private void setExtraSelectorInitialValues(ProductBO subProduct) {
        //set selector container visible
        setItemData(subProduct);
        //set extra picker selector container visible
        productDetailExtraSelectorLayout.setVisibility(View.VISIBLE);
        //set spinner label
        String selectorName = getString(R.string.default_selector_label);
        if (subProduct.getName() != null) {
            selectorName = subProduct.getName();
        }
        productDetailExtraLabel.setText(selectorName);
    }

    private void setColorSelectorInitialValues(ProductBO subProduct, View colorItem) {
        setItemData(subProduct);
        //set color picker selector container visible
        colorPickerLayout.setVisibility(View.VISIBLE);
        //set first item 'selected'
        colorItem.findViewById(R.id.color_selected).setVisibility(View.VISIBLE);
    }

    private class ColorSelectorListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            ProductBO newSelectedProduct = (ProductBO) v.getTag();
            if (firstLevelProduct != null && !newSelectedProduct.getValue().equals(firstLevelProduct.getValue())) {
                //unselect previous color item
                View previousColorItem = colorPickerContainer.findViewWithTag(firstLevelProduct);
                previousColorItem.findViewById(R.id.color_selected).setVisibility(View.INVISIBLE);

                //update current product
                firstLevelProduct = newSelectedProduct;
                //select color item
                View currentColorItem = colorPickerContainer.findViewWithTag(firstLevelProduct);
                currentColorItem.findViewById(R.id.color_selected).setVisibility(View.VISIBLE);
                //set images of the product
                setSliderImagesFromProductIfAvailable(firstLevelProduct);
                //set points label
                setPointsInfoIfAvailable(firstLevelProduct);
                List<ProductBO> subSubProductList = firstLevelProduct.getSecondLevelSelector();
                if (subSubProductList != null) {
                    setSubproductData(subSubProductList);
                } else {
                    //set cart button if we only have one selector
                    setCartActionButton(firstLevelProduct);
                }

            }
        }
    }

    private class ExtraSelectorListener implements AdapterView.OnItemSelectedListener {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            ProductBO selectedProduct;
            if (secondLevelProduct == null) {
                firstLevelProduct = initialProduct.getFirstLevelSelector().get(position);
                selectedProduct = firstLevelProduct;
            } else {
                secondLevelProduct = firstLevelProduct.getSecondLevelSelector().get(position);
                selectedProduct = secondLevelProduct;
            }
            //set images of the product
            setSliderImagesFromProductIfAvailable(selectedProduct);
            //set points label
            setPointsInfoIfAvailable(selectedProduct);
            //cart button
            setCartActionButton(selectedProduct);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    private BasketItemBO getCurrentBasketItem() {
        ProductBO selectedProduct = getCurrentProduct();
        BasketItemBO item = new BasketItemBO();
        item.setSerie(initialProduct.getSerie());
        String realParentProductId = initialProduct.getId().substring(1);
        item.setProductParentId(realParentProductId);

        if (initialProduct.getSerie()) {
            item.setSerie(initialProduct.getSerie());
            //product id starts with 'Z', we must remove it before requesting product details
            String realProductId = selectedProduct.getProductId().substring(1);
            item.setProductId(realProductId);
            item.setIdCatalog(initialProduct.getIdCatalog());
            item.setName(initialProduct.getShortName());
            item.setBrand(initialProduct.getBrand());
            item.setPoints(selectedProduct.getPoints());
            if (selectedProduct.getImages() != null && !selectedProduct.getImages().isEmpty()) {
                String url = selectedProduct.getImages().get(0).getUrl().replaceAll(" ", "%20");
                item.setImageURL(url);
            }

            //if is serie, we set extra info to basket item
            if (firstLevelProduct != null) {
                //color tag
                if (firstLevelProduct.getValue().startsWith(COLOR_PREFFIX)) {
                    item.setColor(firstLevelProduct.getValue());
                } else {
                    //extra selector info
                    item.setExtraTag(firstLevelProduct.getName());
                    item.setExtraValue(firstLevelProduct.getValue());
                }
            }
            if (secondLevelProduct != null) {
                //second level cannot be color
                item.setExtraTag(secondLevelProduct.getName());
                item.setExtraValue(secondLevelProduct.getValue());
            }

        } else {
            String realProductId = selectedProduct.getId().substring(1);
            item.setProductId(realProductId);
            item.setIdCatalog(selectedProduct.getIdCatalog());
            item.setName(selectedProduct.getShortName());
            item.setBrand(selectedProduct.getBrand());
            item.setPoints(selectedProduct.getPoints());
            item.setImageURL(selectedProduct.getImageUrl());
        }
        return item;
    }

    private class CartActionButtonListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            BasketItemBO item = getCurrentBasketItem();
            if (currentBasket.existProductInBasket(item.getProductId())) {
                //this is needed because we need to get item quantity in order to remove it correctly
                item = currentBasket.getItems().get(item.getProductId());
                Toast.makeText(getActivity(), R.string.product_removed_from_cart, Toast.LENGTH_SHORT).show();
                currentBasket.removeItem(getContext(), item);
                setAddItemButton();
            } else {
                Toast.makeText(getActivity(), R.string.product_added_to_cart, Toast.LENGTH_SHORT).show();
                currentBasket.addItem(getContext(), item);
                setRemoveItemButton();
            }

            CartUtils.setBadgeCount(getContext(), cartMenuItem, currentBasket.getQuantity());
        }
    }

    private void setItemData(ProductBO subProduct) {
        if (firstLevelProduct == null) {
            firstLevelProduct = subProduct;
        } else {
            secondLevelProduct = subProduct;
        }
        //set selector container visible
        productDetailSelectorLayout.setVisibility(View.VISIBLE);
        //set images of the product
        setSliderImagesFromProductIfAvailable(subProduct);
        //set points label
        setPointsInfoIfAvailable(subProduct);

        List<ProductBO> subSubProductList = subProduct.getSecondLevelSelector();
        if (subSubProductList != null) {
            setSubproductData(subSubProductList);
        }
    }

    private void setPointsInfoIfAvailable(ProductBO product) {
        if (product.getPoints() != null) {
            productPointsLabel.setText(product.getPointsFormatted(getContext()));
        }
    }

    private void setSliderImagesFromProductIfAvailable(ProductBO product) {
        if (product.getImages() != null) {
            productSlider.removeAllSliders();
            for (ProductImageBO productImage : product.getImages()) {
                DefaultSliderView defaultSliderView = new DefaultSliderView(getContext());
                String url = productImage.getUrl().replaceAll(" ", "%20");
                defaultSliderView.image(url).setScaleType(BaseSliderView.ScaleType.CenterInside);
                productSlider.addSlider(defaultSliderView);
            }
        }
    }

    @Override
    public void showRequestError(String error) {
        AlertDialogUtil.showAlertDialog(getActivity(), error);
    }

    @Override
    public void showServerError() {
        AlertDialogUtil.showAlertDialog(getActivity(), R.string.server_error);
    }

    @Override
    public void showOnExpiredTokenError() {
        AlertDialogUtil.showAlertDialogAndCloseSession(getActivity(), R.string.on_confluent_session_error);
    }

    @Override
    public void showProgress() {
        showProgressDialog();
    }


    private void showProgressDialog() {
        dialog.setMessage(getString(R.string.loading_dialog));
        dialog.show();
    }


    @Override
    public void hideProgress() {
        dialog.dismiss();
    }
}
