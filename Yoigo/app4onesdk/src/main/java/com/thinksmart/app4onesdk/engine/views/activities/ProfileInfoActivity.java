package com.thinksmart.app4onesdk.engine.views.activities;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.widget.Toolbar;

import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.engine.views.fragments.ProfileInfoFragment;

/**
 * Created by roberto.demiguel on 02/11/2016.
 */
public class ProfileInfoActivity extends CustomActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_info);

        initToolbar();

        ProfileInfoFragment fragment = (ProfileInfoFragment) getSupportFragmentManager().findFragmentById(R.id.profile_info_container);
        if (fragment == null) {
            fragment = ProfileInfoFragment.newInstance();
            getSupportFragmentManager().beginTransaction().add(R.id.profile_info_container, fragment).commit();
        }
    }


    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.collapsing_toolbar_with_icon);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        CollapsingToolbarLayout collapsingView = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        collapsingView.setTitle(getString(R.string.profile_info_title));
        collapsingView.setExpandedTitleColor(Color.argb(0, 0, 0, 0));
        collapsingView.setCollapsedTitleTextColor(getResources().getColor(android.R.color.white));
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
