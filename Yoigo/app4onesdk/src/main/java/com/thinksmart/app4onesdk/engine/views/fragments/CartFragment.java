package com.thinksmart.app4onesdk.engine.views.fragments;


import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.core.model.bos.BasketBO;
import com.thinksmart.app4onesdk.engine.application.AppCore;
import com.thinksmart.app4onesdk.engine.application.utils.NavigationUtil;
import com.thinksmart.app4onesdk.engine.views.ICartView;
import com.thinksmart.app4onesdk.engine.views.activities.CartActivity;
import com.thinksmart.app4onesdk.engine.views.adapters.CartFragmentAdapter;
import com.thinksmart.app4onesdk.engine.views.utils.CustomDividerItemDecoration;
import com.thinksmart.app4onesdk.engine.views.utils.StringUtils;

/**
 * Created by roberto.demiguel on 31/10/2016.
 */
public class CartFragment extends Fragment implements ICartView {

    private BasketBO currentBasket;
    private TextView totalPoints;
    private TextView amountOfItems;
    private RelativeLayout finishPurchaseButton;
    private CartFragmentAdapter adapter;

    private RecyclerView cartRecyclerView;

    public CartFragment() {
        // Required empty public constructor
    }

    public static CartFragment newInstance() {
        return new CartFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        currentBasket = AppCore.getInstance().getSessionManager().getCurrentBasket();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_cart, container, false);

        initUIReferences(root);
        initEvents();

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        BasketBO newBasket = AppCore.getInstance().getSessionManager().getCurrentBasket();
        currentBasket.setShipment(newBasket.getShipment());
        currentBasket.setItems(newBasket.getItems());
        currentBasket.setQuantity(newBasket.getQuantity());
        currentBasket.setTotalPoints(newBasket.getTotalPoints());
        adapter.notifyDataSetChanged();

        updateFooterInfo();
        updateToolbarPoints();
    }

    private void updateToolbarPoints() {
        TextView toolbarPoints = (TextView) getActivity().findViewById(R.id.toolbar_app_points);

        Integer points = AppCore.getInstance().getSessionManager().getCurrentUser().getAvailablePoints();
        String availablePoints = StringUtils.formatPointsWithUnits(points, getContext());
        toolbarPoints.setText(availablePoints);
    }

    private void initUIReferences(View root) {
        totalPoints = (TextView) root.findViewById(R.id.cart_total_points);
        amountOfItems = (TextView) root.findViewById(R.id.cart_items_amount);
        finishPurchaseButton = (RelativeLayout) root.findViewById(R.id.cart_finish_purchase);
        //recycler view
        cartRecyclerView = (RecyclerView) root.findViewById(R.id.cart_list);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        cartRecyclerView.setLayoutManager(linearLayoutManager);
        //add list_divider
        //TODO when upgrade project to v25
        //        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(cartRecyclerView.getContext(), linearLayoutManager.getOrientation());
        CustomDividerItemDecoration dividerItemDecoration = new CustomDividerItemDecoration(cartRecyclerView.getContext(), linearLayoutManager.getOrientation());
        cartRecyclerView.addItemDecoration(dividerItemDecoration);
        cartRecyclerView.setHasFixedSize(true);

        adapter = new CartFragmentAdapter(this, getContext(), currentBasket);
        cartRecyclerView.setAdapter(adapter);

        setUpItemTouchHelper();

    }

    private void initEvents() {
        finishPurchaseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentBasket.getItems().isEmpty()) {
                    Toast.makeText(getActivity(), getString(R.string.empty_cart), Toast.LENGTH_SHORT).show();
                } else {
                    final String eventDestination = "goToConfirmShipmentFromCart";
                    NavigationUtil.goToNextScreen(CartActivity.class.getName(), eventDestination);
                }
            }
        });

    }

    @Override
    public void updateFooterInfo() {
        totalPoints.setText(StringUtils.formatPointsWithUnits(currentBasket.getTotalPoints(), getContext()));
        amountOfItems.setText(String.valueOf(currentBasket.getQuantity()));
    }


    private void setUpItemTouchHelper() {
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {

            // we want to cache these and not allocate anything repeatedly in the onChildDraw method
            Drawable background;
            Drawable xMark;
            int xMarkMargin;
            boolean initiated;

            private void init() {
                background = getContext().getResources().getDrawable(R.color.removeBackroundColor);
                xMark = ContextCompat.getDrawable(getContext(), R.drawable.delete_icon);
                xMark.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
                xMarkMargin = (int) getContext().getResources().getDimension(R.dimen.ic_clear_margin);
                initiated = true;
            }

            // not important, we don't want drag & drop
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                return super.getSwipeDirs(recyclerView, viewHolder);
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                int swipedPosition = viewHolder.getAdapterPosition();
                CartFragmentAdapter adapter = (CartFragmentAdapter) cartRecyclerView.getAdapter();
                adapter.remove(swipedPosition);
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                View itemView = viewHolder.itemView;

                // not sure why, but this method get's called for viewholder that are already swiped away
                if (viewHolder.getAdapterPosition() == -1) {
                    // not interested in those
                    return;
                }

                if (!initiated) {
                    init();
                }

                // draw red background
                background.setBounds(itemView.getRight() + (int) dX, itemView.getTop(), itemView.getRight(), itemView.getBottom());
                background.draw(c);

                // draw delete icon mark
                int itemHeight = itemView.getBottom() - itemView.getTop();
                int intrinsicWidth = xMark.getIntrinsicWidth();
                int intrinsicHeight = xMark.getIntrinsicWidth();

                int xMarkLeft = itemView.getRight() - xMarkMargin - intrinsicWidth;
                int xMarkRight = itemView.getRight() - xMarkMargin;
                int xMarkTop = itemView.getTop() + (itemHeight - intrinsicHeight) / 2;
                int xMarkBottom = xMarkTop + intrinsicHeight;
                xMark.setBounds(xMarkLeft, xMarkTop, xMarkRight, xMarkBottom);

                xMark.draw(c);

                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }

        };
        ItemTouchHelper mItemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        mItemTouchHelper.attachToRecyclerView(cartRecyclerView);
    }


}
