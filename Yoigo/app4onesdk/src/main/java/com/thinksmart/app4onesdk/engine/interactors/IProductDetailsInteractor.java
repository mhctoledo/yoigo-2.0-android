package com.thinksmart.app4onesdk.engine.interactors;

/**
 * Created by roberto.demiguel on 26/10/2016.
 */
public interface IProductDetailsInteractor {

    /**
     * This method creates request parameters, and sends a post request to get product. Listener will receive its result.
     */
    void getProduct(String token, Integer participantId, Integer campaignId, String productId, Integer isSerie);
}
