package com.thinksmart.app4onesdk.engine.views.fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.core.model.bos.BasketBO;
import com.thinksmart.app4onesdk.core.model.bos.ShipmentBO;
import com.thinksmart.app4onesdk.engine.application.AppCore;
import com.thinksmart.app4onesdk.engine.application.utils.NavigationUtil;
import com.thinksmart.app4onesdk.engine.presenters.IConfirmShipmentPresenter;
import com.thinksmart.app4onesdk.engine.presenters.imp.ConfirmShipmentPresenterImp;
import com.thinksmart.app4onesdk.engine.utils.AlertDialogUtil;
import com.thinksmart.app4onesdk.engine.views.IConfirmShipmentView;
import com.thinksmart.app4onesdk.engine.views.activities.ConfirmShipmentActivity;
import com.thinksmart.app4onesdk.engine.views.utils.StringUtils;
import com.thinksmart.app4onesdk.engine.views.utils.ViewUtils;

/**
 * Created by roberto.demiguel on 31/10/2016.
 */
public class ConfirmShipmentFragment extends Fragment implements IConfirmShipmentView {

    private ProgressDialog dialog;
    private IConfirmShipmentPresenter presenter;
    private BasketBO currentBasket;

    private TextView cifField;
    private TextView emailField;
    private TextView zipCodeField;
    private TextView companyField;
    private TextView stateField;
    private TextView address1Field;
    private TextView address2Field;
    private TextView cityField;
    private TextView countryField;
    private TextView phoneField;

    private TextView totalPoints;
    private TextView amountOfItems;

    private Button modifyShipmentButton;
    private RelativeLayout finishPurchaseButton;

    private boolean isShipmentEditable;
    private boolean bmwShipmentRules;
    private boolean vfShipmentRules;

    public ConfirmShipmentFragment() {
        // Required empty public constructor
    }

    public static ConfirmShipmentFragment newInstance() {
        return new ConfirmShipmentFragment();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new ConfirmShipmentPresenterImp(this, getActivity());
        currentBasket = AppCore.getInstance().getSessionManager().getCurrentBasket();
        isShipmentEditable = AppCore.getInstance().getAppConfig().isShipmentEditable();
        bmwShipmentRules = AppCore.getInstance().getAppConfig().isBmwShipmentRules();
        vfShipmentRules = AppCore.getInstance().getAppConfig().isVfShipmentRules();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_confirm_shipment, container, false);

        initUIReferences(root);
        initEvents();

        ShipmentBO shipment = AppCore.getInstance().getSessionManager().getCurrentBasket().getShipment();
        if (shipment == null || !shipment.isChangedInSession()) {
            //request shipment
            presenter.getShipment();
        } else {
            //shipment was changed before
            showShipmentData(shipment);
        }

        updateFooterInfo();
        updateToolbarPoints();

        return root;
    }

    private void updateToolbarPoints() {
        TextView toolbarPoints = (TextView) getActivity().findViewById(R.id.toolbar_app_points);
        Integer points = AppCore.getInstance().getSessionManager().getCurrentUser().getAvailablePoints();
        String availablePoints = StringUtils.formatPointsWithUnits(points, getContext());
        toolbarPoints.setText(availablePoints);
    }

    private void initUIReferences(View root) {
        dialog = new ProgressDialog(getActivity());
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        //buttons
        modifyShipmentButton = (Button) root.findViewById(R.id.sh_modify_data_button);
        finishPurchaseButton = (RelativeLayout) root.findViewById(R.id.shipment_finish_purchase);
        //fields
        cifField = (TextView) root.findViewById(R.id.sh_cif_field);
        emailField = (TextView) root.findViewById(R.id.sh_email_field);
        zipCodeField = (TextView) root.findViewById(R.id.sh_zip_code_field);
        companyField = (TextView) root.findViewById(R.id.sh_company_field);
        stateField = (TextView) root.findViewById(R.id.sh_state_field);
        address1Field = (TextView) root.findViewById(R.id.sh_address1_field);
        address2Field = (TextView) root.findViewById(R.id.sh_address2_field);
        cityField = (TextView) root.findViewById(R.id.sh_city_field);
        countryField = (TextView) root.findViewById(R.id.sh_country_field);
        phoneField = (TextView) root.findViewById(R.id.sh_phone_field);
        //
        if (!isShipmentEditable) {
            modifyShipmentButton.setVisibility(View.GONE);
            hideLayout(root, R.id.sh_cif_layout);
            hideLayout(root, R.id.sh_company_layout);
        } else if (bmwShipmentRules) {
            hideLayout(root, R.id.sh_address2_layout);
        } else if (vfShipmentRules) {
            hideLayout(root, R.id.sh_company_layout);
            hideLayout(root, R.id.sh_address2_layout);
        }
        //
        totalPoints = (TextView) root.findViewById(R.id.sh_total_points);
        amountOfItems = (TextView) root.findViewById(R.id.shipment_items_amount);
    }

    private void hideLayout(View root, int layout) {
        RelativeLayout cifLayout = (RelativeLayout) root.findViewById(layout);
        cifLayout.setVisibility(View.GONE);
    }



    @Override
    public void onResume() {
        super.onResume();
        ShipmentBO shipment = AppCore.getInstance().getSessionManager().getCurrentBasket().getShipment();
        if (shipment != null) {
            showShipmentData(shipment);
        }
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    private void initEvents() {
        modifyShipmentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String eventDestination = "goToChangeShipmentFromConfirmShipment";
                NavigationUtil.goToNextScreen(ConfirmShipmentActivity.class.getName(), eventDestination);
            }
        });

        finishPurchaseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (areMandatoryFieldsFilled()) {
                    final String eventDestination = "goToFinishPurchaseFromConfirmShipment";
                    NavigationUtil.goToNextScreen(ConfirmShipmentActivity.class.getName(), eventDestination);
                }
            }
        });
    }

    public void updateFooterInfo() {
        totalPoints.setText(StringUtils.formatPointsWithUnits(currentBasket.getTotalPoints(), getContext()));
        amountOfItems.setText(String.valueOf(currentBasket.getQuantity()));
    }

    private boolean areMandatoryFieldsFilled() {
        boolean correct = true;
        String cif = cifField.getText().toString();
        String email = emailField.getText().toString();
        String zipCode = zipCodeField.getText().toString();
        String company = companyField.getText().toString();
        String state = stateField.getText().toString();
        String address1 = address1Field.getText().toString();
        String city = cityField.getText().toString();
        String country = countryField.getText().toString();
        String phone = phoneField.getText().toString();

        boolean bmwRulesMandatoryFieldsFilled = true;
        boolean vfRulesMandatoryFieldsFilled = true;

        String fieldName = "";
        if (isShipmentEditable && TextUtils.isEmpty(cif)) {
            fieldName = getString(R.string.sh_cif_label);
            correct = false;
            bmwRulesMandatoryFieldsFilled = false;
        } else if (TextUtils.isEmpty(email)) {
            fieldName = getString(R.string.sh_email_label);
            correct = false;
            bmwRulesMandatoryFieldsFilled = false;
        } else if (TextUtils.isEmpty(zipCode)) {
            fieldName = getString(R.string.sh_zip_code_label);
            correct = false;
        } else if (isShipmentEditable && !bmwShipmentRules && !vfShipmentRules && TextUtils.isEmpty(company)) {
            fieldName = getString(R.string.sh_company_label);
            correct = false;
        } else if (TextUtils.isEmpty(state)) {
            fieldName = getString(R.string.sh_state_label);
            correct = false;
        } else if (TextUtils.isEmpty(address1)) {
            fieldName = getString(R.string.sh_address_label);
            correct = false;
            vfRulesMandatoryFieldsFilled = false;
        } else if (TextUtils.isEmpty(city)) {
            fieldName = getString(R.string.sh_city_label);
            correct = false;
        } else if (TextUtils.isEmpty(country)) {
            fieldName = getString(R.string.sh_country_label);
            correct = false;
        } else if (TextUtils.isEmpty(phone)) {
            fieldName = getString(R.string.sh_phone_label);
            correct = false;
        }

        if ((bmwShipmentRules && !bmwRulesMandatoryFieldsFilled) || (vfShipmentRules && !vfRulesMandatoryFieldsFilled)) {
            Toast.makeText(getActivity(), String.format(getString(R.string.bmw_shipment_fields_are_mandatory), fieldName), Toast.LENGTH_LONG).show();
        } else {
            if (!correct) {
                Toast.makeText(getActivity(), String.format(getString(R.string.shipment_fields_are_mandatory), fieldName), Toast.LENGTH_LONG).show();
            }
        }

        return correct;
    }

    @Override
    public void showProgress() {
        ViewUtils.hideKeyboard(getView(), getActivity());
        showProgressDialog();
    }

    private void showProgressDialog() {
        dialog.setMessage(getString(R.string.loading_dialog));
        dialog.show();
    }

    @Override
    public void hideProgress() {
        dialog.dismiss();
    }

    @Override
    public void showShipmentData(ShipmentBO shipment) {
        if (shipment != null) {
            cifField.setText(StringUtils.formatString(shipment.getCif()));
            emailField.setText(StringUtils.formatString(shipment.getEmail()));
            zipCodeField.setText(StringUtils.formatString(shipment.getPostal()));
            companyField.setText(StringUtils.formatString(shipment.getCompany()));
            stateField.setText(StringUtils.formatString(shipment.getState()));
            address1Field.setText(StringUtils.formatString(shipment.getAddress1()));
            address2Field.setText(StringUtils.formatString(shipment.getAddress2()));
            cityField.setText(StringUtils.formatString(shipment.getCity()));
            countryField.setText(StringUtils.formatString(shipment.getCountry()));
            phoneField.setText(StringUtils.formatString(shipment.getPhone()));
        }
    }

    @Override
    public void showRequestError(String error) {
        AlertDialogUtil.showAlertDialog(getActivity(), error);
    }

    @Override
    public void showServerError() {
        AlertDialogUtil.showAlertDialog(getActivity(), R.string.server_error);
    }

    @Override
    public void showOnExpiredTokenError() {
        AlertDialogUtil.showAlertDialogAndCloseSession(getActivity(), R.string.on_confluent_session_error);
    }
}
