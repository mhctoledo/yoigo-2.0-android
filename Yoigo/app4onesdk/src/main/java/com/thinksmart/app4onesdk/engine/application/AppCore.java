package com.thinksmart.app4onesdk.engine.application;

import android.app.Activity;

import com.thinksmart.app4onesdk.engine.application.appConfigModel.AppConfigModel;
import com.thinksmart.app4onesdk.engine.application.utils.ObservingService;
import com.thinksmart.app4onesdk.engine.net.NetworkManager;

/**
 * Created by roberto.demiguel on 14/10/2016.
 */
public class AppCore {

    private static AppCore instance = null;

    private NetworkManager networkManager;
    private AppConfigModel appConfig;
    private NavigationManager navigationManager;
    private EventManager eventManager;
    private ObservingService observingService;
    private AOSessionManager sessionManager;

    public static synchronized AppCore getInstance() {
        if (null == instance)
            instance = new AppCore();
        return instance;
    }

    private AppCore() {
        eventManager = new EventManager();
        observingService = new ObservingService();
        navigationManager = new NavigationManager();
        sessionManager = new AOSessionManager();
    }

    public void startEngine(Activity activity, AppConfigModel appConfigModel) {
        appConfig = appConfigModel;
        networkManager = new NetworkManager(activity.getApplicationContext());
        navigationManager.loadConfig(activity);
    }

    public void setNetworkManager(NetworkManager networkManager) {
        this.networkManager = networkManager;
    }

    public NetworkManager getNetworkManager() {
        return networkManager;
    }

    public AppConfigModel getAppConfig() {
        return appConfig;
    }

    public NavigationManager getNavigationManager() {
        return navigationManager;
    }

    public EventManager getEventManager() {
        return eventManager;
    }

    public ObservingService getObservingService() {
        return observingService;
    }

    public AOSessionManager getSessionManager() {
        return sessionManager;
    }
}
