package com.thinksmart.app4onesdk.engine.views.activities;

import android.os.Bundle;

import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.engine.application.utils.NavigationUtil;
import com.thinksmart.app4onesdk.engine.views.fragments.RememberPasswordFragment;

public class RememberPasswordActivity extends CustomActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remember_password);

        RememberPasswordFragment fragment = (RememberPasswordFragment) getSupportFragmentManager().findFragmentById(R.id.activity_remember_password);
        if (fragment == null) {
            fragment = RememberPasswordFragment.newInstance();
            getSupportFragmentManager().beginTransaction().add(R.id.activity_remember_password, fragment).commit();
        }
    }

    @Override
    public void onBackPressed() {
        final String eventDestination = "goToLoginFromRememberPassword";
        NavigationUtil.goToNextScreen(RememberPasswordActivity.class.getName(), eventDestination);
    }
}
