package com.thinksmart.app4onesdk.engine.interactors.imp;

import android.support.annotation.NonNull;

import com.thinksmart.app4onesdk.core.api.ApiParameters;
import com.thinksmart.app4onesdk.core.api.ApiResponse;
import com.thinksmart.app4onesdk.core.api.ApiUrls;
import com.thinksmart.app4onesdk.core.model.bos.UserBO;
import com.thinksmart.app4onesdk.engine.application.AppCore;
import com.thinksmart.app4onesdk.engine.interactors.IPresenterListener;
import com.thinksmart.app4onesdk.engine.interactors.IProfileChangePasswordInteractor;
import com.thinksmart.app4onesdk.engine.net.IRequestCallback;
import com.thinksmart.app4onesdk.engine.net.ResponseMapperUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by roberto.demiguel on 02/11/2016.
 */

public class ProfileChangePasswordInteractorImp implements IProfileChangePasswordInteractor, IRequestCallback {

    private IPresenterListener presenterListener;

    public ProfileChangePasswordInteractorImp(IPresenterListener presenterListener) {
        this.presenterListener = presenterListener;
    }

    @Override
    public void changePassword(String token, String newPassword, String oldPassword) {
        final JSONObject params = getJsonObject(token, newPassword, oldPassword);
        AppCore.getInstance().getNetworkManager().postRequest(params, ApiUrls.changePassURL, this);
    }

    @Override
    public void onRequestSuccess(JSONObject response) {
        try {
            ResponseMapperUtil.parseResponse(response, this.presenterListener, UserBO.class);
        } catch (JSONException | IOException e) {
            this.presenterListener.onError(ApiResponse.Code.ERROR_PARSING_RESPONSE, ApiResponse.ERROR_PARSING_RESPONSE);
        }
    }

    @Override
    public void onRequestError(String error) {
        presenterListener.onServerError(error);
    }

    @NonNull
    private JSONObject getJsonObject(String token, String newPassword, String oldPassword) {
        final JSONObject params = new JSONObject();
        try {
            params.put(ApiParameters.NEW_PASSWORD, (newPassword != null) ? newPassword : "");
            params.put(ApiParameters.OLD_PASSWORD, (oldPassword != null) ? oldPassword : "");
            params.put(ApiParameters.TOKEN, (token != null) ? token : "");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

}
