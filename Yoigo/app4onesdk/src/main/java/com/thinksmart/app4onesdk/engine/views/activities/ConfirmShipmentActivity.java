package com.thinksmart.app4onesdk.engine.views.activities;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.engine.views.fragments.ConfirmShipmentFragment;

/**
 * Created by roberto.demiguel on 31/10/2016.
 */
public class ConfirmShipmentActivity extends CustomActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_shipment);

        initToolbar();

        ConfirmShipmentFragment fragment = (ConfirmShipmentFragment) getSupportFragmentManager().findFragmentById(R.id.confirm_shipment_container);
        if (fragment == null) {
            fragment = ConfirmShipmentFragment.newInstance();
            getSupportFragmentManager().beginTransaction().add(R.id.confirm_shipment_container, fragment).commit();
        }
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_with_points);
        toolbar.setTitle(R.string.confirm_shipment_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
