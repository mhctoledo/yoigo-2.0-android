package com.thinksmart.app4onesdk.engine.views.fragments;


import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.core.model.bos.BasketBO;
import com.thinksmart.app4onesdk.core.model.bos.ProductBO;
import com.thinksmart.app4onesdk.core.model.enums.FilterType;
import com.thinksmart.app4onesdk.core.vos.CatalogVO;
import com.thinksmart.app4onesdk.engine.application.AppCore;
import com.thinksmart.app4onesdk.engine.application.utils.EndlessRecyclerViewScrollListener;
import com.thinksmart.app4onesdk.engine.application.utils.NavigationUtil;
import com.thinksmart.app4onesdk.engine.presenters.ICatalogPresenter;
import com.thinksmart.app4onesdk.engine.presenters.imp.CatalogPresenterImp;
import com.thinksmart.app4onesdk.engine.utils.AlertDialogUtil;
import com.thinksmart.app4onesdk.engine.views.ICatalogView;
import com.thinksmart.app4onesdk.engine.views.adapters.CatalogFragmentAdapter;
import com.thinksmart.app4onesdk.engine.views.adapters.CatalogSpinnerAdapter;
import com.thinksmart.app4onesdk.engine.views.utils.CartUtils;
import com.thinksmart.app4onesdk.engine.views.utils.CustomDividerItemDecoration;
import com.thinksmart.app4onesdk.engine.views.utils.ReselectableSpinner;
import com.thinksmart.app4onesdk.engine.views.utils.StringUtils;
import com.thinksmart.app4onesdk.engine.views.utils.ViewUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by roberto.demiguel on 17/10/2016.
 */
public class CatalogFragment extends Fragment implements ICatalogView {

    final static String TAG = "CatalogFragment";

    private SwipeRefreshLayout refreshLayout;
    private MenuItem cartMenuItem;
    private SearchView searchView;
    private CatalogFragmentAdapter adapter;
    private RecyclerView catalogRecyclerView;
    private ReselectableSpinner filterSpinner;
    private TextView userPoints;
    private TextView emptyView;

    private ICatalogPresenter presenter;
    private List<ProductBO> productList;

    private Boolean hasMore = true;
    private Boolean isLoading = false;
    private BasketBO currentBasket;

    private CatalogVO currentSearch;

    private boolean firstExecution = true;

    private EndlessRecyclerViewScrollListener scrollListener;

    public CatalogFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        currentSearch = new CatalogVO();
        presenter = new CatalogPresenterImp(this, getActivity());
        currentBasket = AppCore.getInstance().getSessionManager().getCurrentBasket();

        //we are coming from categories
        if (getActivity().getIntent() != null && getActivity().getIntent().getExtras().size() == 2) {
            List<CatalogVO> params = (List<CatalogVO>) getActivity().getIntent().getExtras().get("params");
            if (!params.isEmpty()) {
                currentSearch = params.get(0);
            }
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_catalog, container, false);
        productList = new ArrayList<>();
        initUIReferences(root);
        setHeaderInfo();
        initEvents();

        //we don't need to load catalog due to spinner filter OnItemSelectedListener that is triggered
        getCatalog();

        return root;
    }

    public void setCurrentSearch(CatalogVO currentSearch) {
        this.currentSearch = currentSearch;
        firstExecution = true;
        filterSpinner.setSelection(FilterType.ALL.ordinal());
        getNewCatalog();
    }

    @Override
    public void onResume() {
        updateUserInfo();
        super.onResume();
    }

    private void initEvents() {
        filterSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!firstExecution) {
                    //prepare view if we where searching
                    ViewUtils.hideKeyboard(getView(), getActivity());
                    if (!searchView.isIconified()) {
                        searchView.onActionViewCollapsed();
                    }
                    currentSearch = new CatalogVO();
                    currentSearch.setSearch(FilterType.values()[position].getValue());
                    getNewCatalog();

                } else {
                    firstExecution = false;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        refreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        getNewCatalog();
                    }
                }
        );

    }

    private void initUIReferences(View root) {
        refreshLayout = (SwipeRefreshLayout) root.findViewById(R.id.product_list_swiper_refresh);
        initRecyclerView(root);
        initHeader(root);
    }

    private void initHeader(View root) {
        //header: spinner and points
        filterSpinner = (ReselectableSpinner) root.findViewById(R.id.catalog_filter_spinner);
        int spinnerColor = ContextCompat.getColor(getContext(), android.R.color.white);
        filterSpinner.getBackground().setColorFilter(spinnerColor, PorterDuff.Mode.SRC_IN);
        userPoints = (TextView) root.findViewById(R.id.catalog_user_points);
    }

    private void initRecyclerView(View root) {
        catalogRecyclerView = (RecyclerView) root.findViewById(R.id.product_list);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        catalogRecyclerView.setLayoutManager(linearLayoutManager);
        //add list_divider
        //TODO when upgrade project to v25
        //        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(cartRecyclerView.getContext(), linearLayoutManager.getOrientation());
        CustomDividerItemDecoration dividerItemDecoration = new CustomDividerItemDecoration(getActivity(), linearLayoutManager.getOrientation());
        catalogRecyclerView.addItemDecoration(dividerItemDecoration);
        catalogRecyclerView.setHasFixedSize(true);
        // Retain an instance so that you can call `resetState()` for fresh searches
        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                // Triggered only when new data needs to be appended to the list
                if (hasMore && !isLoading) {
                    currentSearch.incrementPage();
                    getCatalog();
                }
            }
        };
        // Adds the scroll listener to RecyclerView
        catalogRecyclerView.addOnScrollListener(scrollListener);
        // view adapter
        adapter = new CatalogFragmentAdapter(getContext(), productList);
        catalogRecyclerView.setAdapter(adapter);
        //empty view to show when no items
        emptyView = (TextView) root.findViewById(R.id.catalog_empty_view);
    }

    private void getCatalog() {
        isLoading = true;
        presenter.getCatalog(currentSearch);
    }

    private void updateUserInfo() {
        CartUtils.setBadgeCount(getActivity(), cartMenuItem, currentBasket.getQuantity());
        Integer points = AppCore.getInstance().getSessionManager().getCurrentUser().getAvailablePoints();
        String availablePoints = StringUtils.formatPointsWithUnits(points, getContext());
        userPoints.setText(availablePoints);
    }

    private void getNewCatalog() {
        if (!productList.isEmpty()) {
            // 1. First, clear the array of data
            productList.clear();
            // 2. Notify the adapter of the update
            adapter.notifyDataSetChanged();
            // 3. Reset endless scroll listener when performing a new search
            scrollListener.resetState();
        } else {
            //in case we come back from empty view, we should show recycler view again
            catalogRecyclerView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
        }
        getCatalog();
    }

    @Override
    public void showCatalog(List<ProductBO> result) {
        isLoading = false;
        if (result.size() == 21) {
            hasMore = true;
            //remove last item
            result.remove(result.size() - 1);
        } else {
            hasMore = false;
        }

        if (!result.isEmpty()) {
            productList.addAll(result);
            adapter.notifyDataSetChanged();

        } else {
            catalogRecyclerView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
        }

        Log.d(TAG, "PRODUCTS: " + productList.size());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.catalog_menu, menu);
        cartMenuItem = menu.findItem(R.id.action_cart);

        //onCreateOptionsMenu is triggered when catalog is loaded. update user toolbar info then
        updateUserInfo();

        MenuItem searchItem = menu.findItem(R.id.action_search);
        searchItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return false;
            }
        });
        searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setQueryHint(getString(R.string.search_action_hint));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                ViewUtils.hideKeyboard(getView(), getActivity());
                //select all category when we are searching
                if (FilterType.ALL.ordinal() < filterSpinner.getCount()) {
                    firstExecution = true;
                    filterSpinner.setSelection(FilterType.ALL.ordinal());
                }

                currentSearch = new CatalogVO();
                currentSearch.setSearch(query);
                getNewCatalog();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        if (currentSearch.getSearch() != null && !currentSearch.getSearch().startsWith("#")) {
            searchView.setQuery(currentSearch.getSearch(), false);
            searchView.setIconified(false);
            searchView.clearFocus();
        }

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                //only reload catalog if needed
                String searchParam = currentSearch.getSearch();
                if (searchParam != null && !searchParam.isEmpty() && !searchParam.startsWith("#")) {
                    currentSearch = new CatalogVO();
                    getNewCatalog();
                }
                return false;
            }
        });

        cartMenuItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (currentBasket.getItems().size() > 0) {
                    navigateToCart();
                } else {
                    Toast.makeText(getActivity(), R.string.empty_cart, Toast.LENGTH_SHORT).show();
                }

                return true;
            }
        });

        MenuItem categoriesMenuItem = menu.findItem(R.id.action_categories);
        categoriesMenuItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                navigateToCategories();
                return true;
            }

        });

        super.onCreateOptionsMenu(menu, inflater);
    }

    private void navigateToCart() {
        final String eventDestination = "goToCartFromCatalog";
        NavigationUtil.goToNextScreen(CatalogFragment.class.getName(), eventDestination);
    }

    private void navigateToCategories() {
        final String eventDestination = "goToCategoriesFromCatalog";
        NavigationUtil.goToNextScreen(CatalogFragment.class.getName(), eventDestination);
    }


    @Override
    public void showProgress() {
        //disable spinner when loading catalog
        filterSpinner.setEnabled(false);

        catalogRecyclerView.post(new Runnable() {
            public void run() {
                productList.add(null);
                adapter.notifyItemInserted(productList.size() - 1);
            }
        });
    }

    @Override
    public void setHeaderInfo() {
        //fill spinner with filter types
        CatalogSpinnerAdapter spinnerAdapter = new CatalogSpinnerAdapter(getContext());

        filterSpinner.setAdapter(spinnerAdapter);
    }

    @Override
    public void hideProgress() {
        //enable spinner when catalog has loaded
        filterSpinner.setEnabled(true);
        //Remove loading item
        if (!productList.isEmpty()) {
            productList.remove(productList.size() - 1);
            adapter.notifyItemRemoved(productList.size());
        }
        refreshLayout.setRefreshing(false);
    }

    @Override
    public void showRequestError(String error) {
        Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
    }


    @Override
    public void showServerError() {
        AlertDialogUtil.showAlertDialog(getActivity(), R.string.server_error);
    }

    @Override
    public void showOnExpiredTokenError() {
        AlertDialogUtil.showAlertDialogAndCloseSession(getActivity(), R.string.on_confluent_session_error);
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    public static class CatalogProductClickListener implements View.OnClickListener {

        private ProductBO product;

        public CatalogProductClickListener(ProductBO product) {
            this.product = product;
        }

        @Override
        public void onClick(View v) {
            navigateToProductDetailsView(product);
        }

        private void navigateToProductDetailsView(ProductBO product) {
            List<String> listParams = new ArrayList<>();
            //product id starts with 'Z', we must remove it before requesting product details
            String realProductId = product.getId().substring(1);
            listParams.add(realProductId);
            listParams.add(product.getSerie().toString());

            final String eventDestination = "goToDetailFromCatalog";
            NavigationUtil.goToNextScreen(CatalogFragment.class.getName(), eventDestination, listParams);
        }
    }

}
