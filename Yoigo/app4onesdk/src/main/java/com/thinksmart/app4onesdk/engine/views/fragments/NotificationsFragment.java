package com.thinksmart.app4onesdk.engine.views.fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.core.model.bos.NotificationBO;
import com.thinksmart.app4onesdk.core.model.bos.NotificationsParentBO;
import com.thinksmart.app4onesdk.core.model.constants.UserPreferences;
import com.thinksmart.app4onesdk.engine.presenters.INotificationsPresenter;
import com.thinksmart.app4onesdk.engine.presenters.imp.NotificationsPresenterImp;
import com.thinksmart.app4onesdk.engine.utils.AlertDialogUtil;
import com.thinksmart.app4onesdk.engine.utils.preferencesManager.imp.PreferencesManagerImp;
import com.thinksmart.app4onesdk.engine.views.INotificationsView;
import com.thinksmart.app4onesdk.engine.views.adapters.NotificationsFragmentAdapter;
import com.thinksmart.app4onesdk.engine.views.utils.CustomDividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

import me.leolin.shortcutbadger.ShortcutBadger;

/**
 * Created by roberto.demiguel on 10/11/2016.
 */
public class NotificationsFragment extends Fragment implements INotificationsView {

    private ProgressDialog dialog;
    private List<NotificationBO> notificationList;
    private INotificationsPresenter presenter;
    private NotificationsFragmentAdapter adapter;
    private RecyclerView notificationsRecyclerView;
    private TextView emptyView;

    public NotificationsFragment() {
        // Required empty public constructor
    }

    public static NotificationsFragment newInstance() {
        return new NotificationsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new NotificationsPresenterImp(this, getActivity());
        notificationList = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_notifications, container, false);
        dialog = new ProgressDialog(getActivity());
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        initUIReferences(root);
        removePendingNotificationsAlert();
        presenter.getNotifications();

        return root;
    }

    private void removePendingNotificationsAlert() {
        ShortcutBadger.removeCount(getContext());
        new PreferencesManagerImp(getActivity()).saveStringPreference(UserPreferences.PENDING_NOTIFICATIONS, null);
    }


    private void initUIReferences(View root) {
        notificationsRecyclerView = (RecyclerView) root.findViewById(R.id.notifications_list);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        notificationsRecyclerView.setLayoutManager(linearLayoutManager);
        //add list_divider
        //TODO when upgrade project to v25
        //        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(cartRecyclerView.getContext(), linearLayoutManager.getOrientation());
        CustomDividerItemDecoration dividerItemDecoration = new CustomDividerItemDecoration(getActivity(), linearLayoutManager.getOrientation());
        notificationsRecyclerView.addItemDecoration(dividerItemDecoration);
        // view adapter
        adapter = new NotificationsFragmentAdapter(notificationList);
        notificationsRecyclerView.setAdapter(adapter);
        //empty view to show when no items
        emptyView = (TextView) root.findViewById(R.id.notifications_empty_view);
    }


    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void showNotifications(NotificationsParentBO notifications) {
        if (!notifications.getNotificationList().isEmpty()) {
            notificationList.addAll(notifications.getNotificationList());
            adapter.notifyDataSetChanged();
        } else {
            notificationsRecyclerView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void showProgress() {
        showProgressDialog();
    }


    @Override
    public void hideProgress() {
        dialog.dismiss();
    }


    @Override
    public void showRequestError(String error) {
        AlertDialogUtil.showAlertDialog(getActivity(), error);
    }

    @Override
    public void showServerError() {
        AlertDialogUtil.showAlertDialog(getActivity(), R.string.server_error);
    }

    @Override
    public void showOnExpiredTokenError() {
        AlertDialogUtil.showAlertDialogAndCloseSession(getActivity(), R.string.on_confluent_session_error);
    }

    private void showProgressDialog() {
        dialog.setMessage(getString(R.string.loading_dialog));
        dialog.show();
    }
}
