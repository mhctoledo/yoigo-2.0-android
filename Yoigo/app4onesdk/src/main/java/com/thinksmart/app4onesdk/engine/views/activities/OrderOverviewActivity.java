package com.thinksmart.app4onesdk.engine.views.activities;

import android.os.Bundle;

import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.engine.views.fragments.OrderOverviewFragment;

/**
 * Created by roberto.demiguel on 31/10/2016.
 */
public class OrderOverviewActivity extends CustomActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_overview);

        OrderOverviewFragment fragment = (OrderOverviewFragment) getSupportFragmentManager().findFragmentById(R.id.activity_order_overview);
        if (fragment == null) {
            fragment = OrderOverviewFragment.newInstance();
            getSupportFragmentManager().beginTransaction().add(R.id.activity_order_overview, fragment).commit();
        }
    }
}
