package com.thinksmart.app4onesdk.engine.views.fragments;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mtramin.rxfingerprint.RxFingerprint;
import com.mtramin.rxfingerprint.data.FingerprintDecryptionResult;
import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.core.model.bos.BasketBO;
import com.thinksmart.app4onesdk.core.model.constants.UserPreferences;
import com.thinksmart.app4onesdk.engine.application.AppCore;
import com.thinksmart.app4onesdk.engine.application.utils.NavigationUtil;
import com.thinksmart.app4onesdk.engine.presenters.IFinishPurchasePresenter;
import com.thinksmart.app4onesdk.engine.presenters.imp.FinishPurchasePresenterImp;
import com.thinksmart.app4onesdk.engine.utils.AlertDialogUtil;
import com.thinksmart.app4onesdk.engine.utils.preferencesManager.IPreferencesManager;
import com.thinksmart.app4onesdk.engine.utils.preferencesManager.imp.PreferencesManagerImp;
import com.thinksmart.app4onesdk.engine.views.ICartView;
import com.thinksmart.app4onesdk.engine.views.IFinishPurchaseView;
import com.thinksmart.app4onesdk.engine.views.activities.FinishPurchaseActivity;
import com.thinksmart.app4onesdk.engine.views.adapters.FinishPurchaseFragmentAdapter;
import com.thinksmart.app4onesdk.engine.views.utils.CustomDividerItemDecoration;
import com.thinksmart.app4onesdk.engine.views.utils.StringUtils;
import com.thinksmart.app4onesdk.engine.views.utils.ViewUtils;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

import static com.thinksmart.app4onesdk.engine.views.fragments.LoginFragment.ERROR_TIMEOUT_MILLIS;
import static com.thinksmart.app4onesdk.engine.views.fragments.LoginFragment.SUCCESS_DELAY_MILLIS;

/**
 * Created by roberto.demiguel on 31/10/2016.
 */
public class FinishPurchaseFragment extends Fragment implements IFinishPurchaseView, ICartView {

    private IPreferencesManager preferencesManager;
    private IFinishPurchasePresenter presenter;
    private ProgressDialog dialog;
    private ImageView fingerprintIcon;
    private TextView fingerprintStatus;
    private BasketBO currentBasket;
    private TextView totalPoints;
    private TextView amountOfItems;
    private RelativeLayout confirmOrderButton;
    private boolean fingerprintAllowed;

    private AlertDialog passwordRequestDialog;

    public FinishPurchaseFragment() {
        // Required empty public constructor
    }

    public static FinishPurchaseFragment newInstance() {
        return new FinishPurchaseFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        currentBasket = AppCore.getInstance().getSessionManager().getCurrentBasket();
        presenter = new FinishPurchasePresenterImp(this, getActivity());
        preferencesManager = new PreferencesManagerImp(getActivity());
        boolean fingerprintSaved = UserPreferences.FINGERPRINT_SAVED
                .equals(preferencesManager.retrieveStringPreference(UserPreferences.FINGERPRINT));
        fingerprintAllowed = RxFingerprint.isAvailable(getActivity()) && fingerprintSaved;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_finish_purchase, container, false);

        initUIReferences(root);
        initEvents();
        updateFooterInfo();
        updateToolbarPoints();

        return root;
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    private void updateToolbarPoints() {
        TextView toolbarPoints = (TextView) getActivity().findViewById(R.id.toolbar_app_points);
        Integer points = AppCore.getInstance().getSessionManager().getCurrentUser().getAvailablePoints();
        String availablePoints = StringUtils.formatPointsWithUnits(points, getContext());
        toolbarPoints.setText(availablePoints);
    }

    private void initUIReferences(View root) {
        totalPoints = (TextView) root.findViewById(R.id.fp_total_points);
        amountOfItems = (TextView) root.findViewById(R.id.fp_items_amount);
        confirmOrderButton = (RelativeLayout) root.findViewById(R.id.fp_finish_purchase);
        //recycler view
        RecyclerView itemsRecyclerView = (RecyclerView) root.findViewById(R.id.fp_list);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        itemsRecyclerView.setLayoutManager(linearLayoutManager);
        //add list_divider
        //TODO when upgrade project to v25
        //        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(cartRecyclerView.getContext(), linearLayoutManager.getOrientation());
        CustomDividerItemDecoration dividerItemDecoration = new CustomDividerItemDecoration(getActivity(), linearLayoutManager.getOrientation());
        itemsRecyclerView.addItemDecoration(dividerItemDecoration);
        itemsRecyclerView.setHasFixedSize(true);

        FinishPurchaseFragmentAdapter adapter = new FinishPurchaseFragmentAdapter(getContext(), currentBasket);
        itemsRecyclerView.setAdapter(adapter);
    }

    private void initEvents() {
        dialog = new ProgressDialog(getActivity());
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        confirmOrderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.checkAvailablePointsBeforeRedeem();
            }
        });

        initPasswordRequestDialog();
    }

    private void initPasswordRequestDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        dialogBuilder.setTitle(R.string.confirm_pass_to_redeem);

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.input_password, null);
        dialogBuilder.setView(dialogView);
        if (fingerprintAllowed) {
            RelativeLayout fingerprintContainer = (RelativeLayout) dialogView.findViewById(R.id.fingerprint_container);
            fingerprintContainer.setVisibility(View.VISIBLE);
            fingerprintIcon = (ImageView) dialogView.findViewById(R.id.fingerprint_icon);
            fingerprintStatus = (TextView) dialogView.findViewById(R.id.fingerprint_status);
        }

        final EditText input = (EditText) dialogView.findViewById(R.id.input_text_value);
        input.setText("");
        dialogBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        dialogBuilder.setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String password = input.getText().toString();
                presenter.redeemPoints(password, currentBasket);
            }
        });
        passwordRequestDialog = dialogBuilder.create();
    }

    @Override
    public void showProgress() {
        ViewUtils.hideKeyboard(getView(), getActivity());
        showProgressDialog();
    }

    private void showProgressDialog() {
        dialog.setMessage(getString(R.string.loading_dialog));
        dialog.show();
    }

    @Override
    public void hideProgress() {
        dialog.dismiss();
    }


    @Override
    public void showRequestError(String error) {
        AlertDialogUtil.showAlertDialog(getActivity(), error);
    }

    @Override
    public void showServerError() {
        AlertDialogUtil.showAlertDialog(getActivity(), R.string.server_error);
    }

    @Override
    public void showOnExpiredTokenError() {
        AlertDialogUtil.showAlertDialogAndCloseSession(getActivity(), R.string.on_confluent_session_error);
    }

    @Override
    public void showNotEnoughPointsToRedeem() {
        String text = String.format(getActivity().getString(R.string.not_enough_points_to_redeem), StringUtils.getPointsTitle(getActivity()));
        AlertDialogUtil.showAlertDialog(getActivity(), text);
    }

    @Override
    public void askForPasswordBeforeRedeem() {
        passwordRequestDialog.show();
        passwordRequestDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(android.R.color.black));
        passwordRequestDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(android.R.color.black));
        if (fingerprintAllowed) {
            readFingerprintToGetPassword();
        }
    }


    private void readFingerprintToGetPassword() {

        String encryptedString = preferencesManager.retrieveStringPreference(UserPreferences.PASSWORD);

        Disposable disposable = RxFingerprint.decrypt(getActivity(), encryptedString)
                .subscribe(new Consumer<FingerprintDecryptionResult>() {
                    @Override
                    public void accept(FingerprintDecryptionResult fingerprintDecryptionResult) throws Exception {
                        switch (fingerprintDecryptionResult.getResult()) {
                            case FAILED:
                                setErrorFingerprintDialogStatus(getString(R.string.fingerprint_not_recognized));
                                break;
                            case HELP:
                                setErrorFingerprintDialogStatus(fingerprintDecryptionResult.getMessage());
                                break;
                            case AUTHENTICATED:
                                final String decryptedPassword = fingerprintDecryptionResult.getDecrypted();
                                // Do something with decrypted data
                                setSuccessFingerprintDialogStatus();
                                final Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        passwordRequestDialog.cancel();
                                        presenter.redeemPoints(decryptedPassword, currentBasket);

                                    }
                                }, SUCCESS_DELAY_MILLIS);
                                break;
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        if (RxFingerprint.keyInvalidated(throwable)) {
                            // The keys you wanted to use are invalidated because the user has turned off his
                            // secure lock screen or changed the fingerprints stored on the device
                            // You have to re-encrypt the data to access it
                        }
                        Log.e("ERROR", "decrypt", throwable);
                    }
                });
    }

    private void setErrorFingerprintDialogStatus(String error) {
        fingerprintIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_fingerprint_error));
        fingerprintStatus.setText(error);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                resetFingerprintDialogStatus();
            }
        }, ERROR_TIMEOUT_MILLIS);
    }

    private void setSuccessFingerprintDialogStatus() {
        fingerprintIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_fingerprint_success));
        fingerprintStatus.setText(R.string.fingerprint_success);
    }


    private void resetFingerprintDialogStatus() {
        fingerprintIcon.setImageDrawable(getResources().getDrawable(R.drawable.fingerprint_ic));
        fingerprintStatus.setText(R.string.fingerprint_hint);
    }

    @Override
    public void onSuccessfulRedemtion() {
        currentBasket.emptyBasket(getActivity());

        //go to reuslt screen
        final String eventDestination = "goToOrderOverviewFromFinishPurchase";
        NavigationUtil.goToNextScreen(FinishPurchaseActivity.class.getName(), eventDestination);
    }

    @Override
    public void updateFooterInfo() {
        totalPoints.setText(StringUtils.formatPointsWithUnits(currentBasket.getTotalPoints(), getContext()));
        amountOfItems.setText(String.valueOf(currentBasket.getQuantity()));
    }
}
