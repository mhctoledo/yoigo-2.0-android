package com.thinksmart.app4onesdk.engine.application;

import android.app.Activity;

import com.thinksmart.app4onesdk.core.model.bos.BasketBO;
import com.thinksmart.app4onesdk.core.model.bos.UserBO;
import com.thinksmart.app4onesdk.core.model.util.BasketUtil;


/**
 * Created by david.lopez on 11/8/16.
 */
public class AOSessionManager {

    private UserBO currentUSer;
    private BasketBO currentBasket;

    public AOSessionManager() {
    }

    public void restoreBasket(Activity activity) {
        this.currentBasket = BasketUtil.getCurrentBasket(activity);
    }

    public UserBO getCurrentUser() {
        return currentUSer;
    }

    public void setCurrentUser(UserBO currentUSer) {
        this.currentUSer = currentUSer;
    }

    public BasketBO getCurrentBasket() {
        return currentBasket;
    }

    public void setCurrentBasket(BasketBO currentBasket) {
        this.currentBasket = currentBasket;
    }
}
