package com.thinksmart.app4onesdk.engine.application.appConfigModel.screen;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.res.ResourcesCompat;

import com.thinksmart.app4onesdk.engine.application.AppCore;


/**
 * Created by roberto.demiguel on 16/11/2016.
 */
public class SideMenu {
    private int iconResource;
    private int nameResource;
    private String eventName;

    public SideMenu(int iconResource, int nameResource, String eventName) {
        this.iconResource = iconResource;
        this.nameResource = nameResource;
        this.eventName = eventName;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }


    public String getResourceName(Context context) {
        return context.getResources().getString(this.nameResource);
    }

    public Drawable getResourceIcon(Context context) {
        return ResourcesCompat.getDrawable(context.getResources(), this.iconResource, null);
    }

    public boolean isSideMenuShown(){
        return true;
    }

    public int getIconResource() {
        return iconResource;
    }

    public int getNameResource() {
        return nameResource;
    }
}
