package com.thinksmart.app4onesdk.engine.views.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;

import com.thinksmart.app4onesdk.R;

import java.util.List;

import me.srodrigo.androidhintspinner.HintAdapter;

/**
 * Created by roberto.demiguel on 07/11/2016.
 */
public class SelectProfileSpinnerAdapter extends HintAdapter<String> {

    public SelectProfileSpinnerAdapter(Context context, int layoutResource, String hint, List<String> data) {
        super(context, layoutResource, hint, data);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        CheckedTextView textView = (CheckedTextView) View.inflate(getContext(), R.layout.profile_selector_spinner_dropdown, null);
        if (textView != null) {
            textView.setText(getItem(position));
        }
        return textView;
    }

//    @Override
//    public View getDropDownView(int position, View convertView, ViewGroup parent) {
//        CheckedTextView text = (CheckedTextView) convertView;
//        if (text == null) {
//            text = (CheckedTextView) LayoutInflater.from(getContext()).inflate(R.layout.profile_selector_spinner_dropdown, parent, false);
//            text.setText(getItem(position));
//        }
//        return text;
//    }
}