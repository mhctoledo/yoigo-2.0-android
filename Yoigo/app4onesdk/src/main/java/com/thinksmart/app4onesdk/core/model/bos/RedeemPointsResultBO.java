package com.thinksmart.app4onesdk.core.model.bos;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by roberto.demiguel on 13/10/2016.
 */
public class RedeemPointsResultBO implements Serializable {

    @JsonProperty("puntos_disponibles")
    private Integer avaiablePoints;

    public RedeemPointsResultBO() {
    }

    public Integer getAvaiablePoints() {
        return avaiablePoints;
    }

    public void setAvaiablePoints(Integer avaiablePoints) {
        this.avaiablePoints = avaiablePoints;
    }
}
