package com.thinksmart.app4onesdk.engine.views.fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.core.model.bos.UserBO;
import com.thinksmart.app4onesdk.engine.application.AppCore;
import com.thinksmart.app4onesdk.engine.presenters.IEditProfilePresenter;
import com.thinksmart.app4onesdk.engine.presenters.imp.EditProfilePresenterImp;
import com.thinksmart.app4onesdk.engine.utils.AlertDialogUtil;
import com.thinksmart.app4onesdk.engine.views.IEditProfileView;
import com.thinksmart.app4onesdk.engine.views.utils.ViewUtils;

/**
 * Created by roberto.demiguel on 02/11/2016.
 */
public class EditProfileFragment extends Fragment implements IEditProfileView {

    private ProgressDialog dialog;
    private IEditProfilePresenter presenter;

    private EditText nameField;
    private EditText surnameField;
    private EditText nifField;
    private EditText emailField;
    private EditText phoneField;

    public EditProfileFragment() {
        // Required empty public constructor
    }

    public static EditProfileFragment newInstance() {
        return new EditProfileFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        presenter = new EditProfilePresenterImp(this, getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_edit_profile, container, false);

        initUIReferences(view);
        populateInfo();

        return view;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.form_menu, menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_save) {
            sendProfileData();
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    private void initUIReferences(View view) {
        dialog = new ProgressDialog(getActivity());
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        nameField = (EditText) view.findViewById(R.id.profile_name_value);
        surnameField = (EditText) view.findViewById(R.id.profile_surname_value);
        nifField = (EditText) view.findViewById(R.id.profile_nif_value);
        emailField = (EditText) view.findViewById(R.id.profile_mail_value);
        phoneField = (EditText) view.findViewById(R.id.profile_phone_value);


        view.findViewById(R.id.user_profile_data_layout).requestFocus();
    }


    private void sendProfileData() {
        ViewUtils.hideKeyboard(getView(), getActivity());
        if (areMandatoryFieldsFilled()) {
            UserBO transferObject = getUserFromView();
            presenter.updateProfileData(transferObject);
        }
    }


    private boolean areMandatoryFieldsFilled() {
        boolean correct = true;
        String name = nameField.getText().toString();
        String surname = surnameField.getText().toString();
        String email = emailField.getText().toString();
        String nif = nifField.getText().toString();
        String phone = phoneField.getText().toString();

        String fieldName = "";
        if (TextUtils.isEmpty(name)) {
            fieldName = getString(R.string.profile_name);
            correct = false;
        } else if (TextUtils.isEmpty(surname)) {
            fieldName = getString(R.string.profile_surname);
            correct = false;
        } else if (TextUtils.isEmpty(email)) {
            fieldName = getString(R.string.profile_email);
            correct = false;
        } else if (TextUtils.isEmpty(nif)) {
            fieldName = getString(R.string.profile_nif);
            correct = false;
        } else if (TextUtils.isEmpty(phone)) {
            fieldName = getString(R.string.profile_phone);
            correct = false;
        }

        if (!correct) {
            Toast.makeText(getActivity(), String.format(getString(R.string.shipment_fields_are_mandatory), fieldName), Toast.LENGTH_LONG).show();
        }

        return correct;
    }

    @NonNull
    private UserBO getUserFromView() {
        UserBO transferObject = new UserBO();
        transferObject.setNameShort(nameField.getText().toString());
        transferObject.setLastName(surnameField.getText().toString());
        transferObject.setNif(nifField.getText().toString());
        transferObject.setEmail(emailField.getText().toString());
        transferObject.setMobile(phoneField.getText().toString());
        return transferObject;
    }

    private void populateInfo() {
        UserBO user = AppCore.getInstance().getSessionManager().getCurrentUser();

        nameField.setText(user.getNameShort());
        surnameField.setText(user.getLastName());
        nifField.setText(user.getNif());
        emailField.setText(user.getEmail());
        phoneField.setText(user.getMobile());
    }

    @Override
    public void showProgress() {
        ViewUtils.hideKeyboard(getView(), getActivity());
        showProgressDialog();
    }

    private void showProgressDialog() {
        dialog.setMessage(getString(R.string.loading_dialog));
        dialog.show();
    }

    @Override
    public void hideProgress() {
        dialog.dismiss();
    }

    @Override
    public void onDataSuccessfullyUpdated() {
        Toast.makeText(getActivity(), R.string.profile_data_successfully_updated, Toast.LENGTH_LONG).show();
        getActivity().finish();
    }

    @Override
    public void showRequestError(String error) {
        AlertDialogUtil.showAlertDialog(getActivity(), error);
    }

    @Override
    public void showServerError() {
        AlertDialogUtil.showAlertDialog(getActivity(), R.string.server_error);
    }

    @Override
    public void showOnExpiredTokenError() {
        AlertDialogUtil.showAlertDialogAndCloseSession(getActivity(), R.string.on_confluent_session_error);
    }
}
