package com.thinksmart.app4onesdk.engine.presenters.imp;

import com.thinksmart.app4onesdk.engine.interactors.IRememberPasswordInteractor;
import com.thinksmart.app4onesdk.engine.interactors.IRememberPasswordPresenterListener;
import com.thinksmart.app4onesdk.engine.interactors.imp.RememberPasswordInteractorImp;
import com.thinksmart.app4onesdk.engine.presenters.IRememberPasswordPresenter;
import com.thinksmart.app4onesdk.engine.views.IRememberPasswordView;

/**
 * Created by roberto.demiguel on 07/11/2016.
 */

public class RememberPasswordPresenterImp implements IRememberPasswordPresenter, IRememberPasswordPresenterListener {

    private IRememberPasswordView view;
    private IRememberPasswordInteractor interactor;

    public RememberPasswordPresenterImp(IRememberPasswordView view) {
        this.view = view;
        this.interactor = new RememberPasswordInteractorImp(this);
    }

    @Override
    public void requestPasswordReset(String user) {
        if (view != null) {
            view.showProgress();
        }
        interactor.resetPassword(user);
    }

    @Override
    public void onDestroy() {
        view = null;
    }


    @Override
    public void onSuccess() {
        if (view != null) {
            view.hideProgress();
            view.showPasswordResetSuccess();
        }
    }

    @Override
    public void onNonExistingUserError() {
        if (view != null) {
            view.hideProgress();
            view.showNonExistingUserError();
        }
    }

    @Override
    public void onError(Integer code, String error) {
        if (view != null) {
            view.hideProgress();
            view.showRequestError(error);
        }
    }

    @Override
    public void onServerError(String error) {
        if (view != null) {
            view.hideProgress();
            view.showServerError();
        }
    }
}
