package com.thinksmart.app4onesdk.core.model.bos;

import com.bignerdranch.expandablerecyclerview.model.Parent;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

/**
 * Created by roberto.demiguel on 10/11/2016.
 */
public class SectionBO implements Serializable, Parent<SubSectionBO> {

    @JsonProperty("nombre")
    private String name;
    @JsonProperty("productos")
    private Integer productsNum;
    @JsonProperty("id")
    private Integer id;
    @JsonProperty("id_grupo")
    private Integer groupId;
    @JsonProperty("Subsecciones")
    private List<SubSectionBO> subSections;


    public SectionBO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getProductsNum() {
        return productsNum;
    }

    public void setProductsNum(Integer productsNum) {
        this.productsNum = productsNum;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public List<SubSectionBO> getSubSections() {
        return subSections;
    }

    public void setSubSections(List<SubSectionBO> subSections) {
        this.subSections = subSections;
    }

    @Override
    public List<SubSectionBO> getChildList() {
        return subSections;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }
}
