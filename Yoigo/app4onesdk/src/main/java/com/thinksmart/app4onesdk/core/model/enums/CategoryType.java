package com.thinksmart.app4onesdk.core.model.enums;

import android.content.Context;
import android.graphics.drawable.Drawable;

import com.thinksmart.app4onesdk.R;

/**
 * Created by roberto.demiguel on 07/12/2016.
 */
public enum CategoryType {

    PRODUCTS(1, R.string.categories_products_name, R.string.categories_products_description, R.drawable.categories_products_background, R.drawable.categories_products_picture),
    TRIPS(2, R.string.categories_trips_name, R.string.categories_trips_description, R.drawable.categories_trips_background, R.drawable.categories_trips_background),
    EXPERIENCES(3, R.string.categories_experiences_name, R.string.categories_experiences_description, R.drawable.categories_experiences_background, R.drawable.categories_experiences_picture);

    private int id;
    private int resourceName;
    private int resourceDescription;
    private int resourceBackground;
    private int resourcePicture;

    CategoryType(int id, int resourceName, int resourceDescription, int resourceBackground, int resourcePicture) {
        this.id = id;
        this.resourceName = resourceName;
        this.resourceDescription = resourceDescription;
        this.resourceBackground = resourceBackground;
        this.resourcePicture = resourcePicture;
    }

    public int getId() {
        return id;
    }

    public String getName(Context context) {
        return context.getResources().getString(this.resourceName);
    }

    public String getDescription(Context context) {
        return context.getResources().getString(this.resourceDescription);
    }

    public Drawable getResourceBackground(Context context) {
        return context.getResources().getDrawable(this.resourceBackground);
    }

    public int getResourceBackground() {
        return resourceBackground;
    }

    public Drawable getResourcePicture(Context context) {
        return context.getResources().getDrawable(this.resourcePicture);
    }

    public static CategoryType getCategoryFromId(int id) {
        CategoryType result = null;
        for (CategoryType categoryType : CategoryType.values()) {
            if (categoryType.getId() == id) {
                result = categoryType;
                break;
            }
        }

        return result;
    }

}
