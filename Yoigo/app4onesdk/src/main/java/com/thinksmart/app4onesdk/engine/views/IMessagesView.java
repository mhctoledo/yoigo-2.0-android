package com.thinksmart.app4onesdk.engine.views;

import com.thinksmart.app4onesdk.core.model.bos.MessagesParentBO;

/**
 * Created by roberto.demiguel on 17/10/2016.
 */
public interface IMessagesView {

    /**
     * Shows progress dialog
     */
    void showProgress();

    /**
     * Hides progress dialog
     */
    void hideProgress();

    /**
     * Displays messages in view
     *
     * @param messages - to show
     */
    void showMessages(MessagesParentBO messages);

    /**
     * Shows error toast
     */
    void showRequestError(String error);

    /**
     * Shows server error toast
     */
    void showServerError();

    /**
     * Shows on confluent session error toast
     */
    void showOnExpiredTokenError();
}
