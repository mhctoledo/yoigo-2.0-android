package com.thinksmart.app4onesdk.engine.views.fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.core.model.bos.CategoriesBO;
import com.thinksmart.app4onesdk.core.model.bos.GroupBO;
import com.thinksmart.app4onesdk.engine.presenters.ICategoriesPresenter;
import com.thinksmart.app4onesdk.engine.presenters.imp.CategoriesPresenterImp;
import com.thinksmart.app4onesdk.engine.utils.AlertDialogUtil;
import com.thinksmart.app4onesdk.engine.views.ICategoriesView;
import com.thinksmart.app4onesdk.engine.views.adapters.CategoriesFragmentAdapter;
import com.thinksmart.app4onesdk.engine.views.utils.CustomDividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by roberto.demiguel on 10/11/2016.
 */
public class CategoriesFragment extends Fragment implements ICategoriesView {

    private ProgressDialog dialog;
    private List<GroupBO> categoryList;
    private ICategoriesPresenter presenter;
    private CategoriesFragmentAdapter adapter;

    public CategoriesFragment() {
        // Required empty public constructor
    }

    public static CategoriesFragment newInstance() {
        return new CategoriesFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new CategoriesPresenterImp(this, getActivity());
        categoryList = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_categories, container, false);
        dialog = new ProgressDialog(getActivity());

        initUIReferences(root);
        presenter.getCategories();

        return root;
    }


    private void initUIReferences(View root) {
        RecyclerView categoriesRecyclerView = (RecyclerView) root.findViewById(R.id.categories_list);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        categoriesRecyclerView.setLayoutManager(linearLayoutManager);
        //TODO when upgrade project to v25
        CustomDividerItemDecoration dividerItemDecoration = new CustomDividerItemDecoration(getActivity(), linearLayoutManager.getOrientation());
        categoriesRecyclerView.addItemDecoration(dividerItemDecoration);
        categoriesRecyclerView.setHasFixedSize(true);
        // view adapter
        adapter = new CategoriesFragmentAdapter(getContext(), categoryList);
        categoriesRecyclerView.setAdapter(adapter);
    }


    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void showCategories(CategoriesBO categories) {
        if (!categoryList.isEmpty()) {
            categoryList.clear();
        }
        categoryList.addAll(categories.getGroups());
        adapter.notifyDataSetChanged();
    }

    @Override
    public void showProgress() {
        showProgressDialog();
    }


    @Override
    public void hideProgress() {
        dialog.dismiss();
    }


    @Override
    public void showRequestError(String error) {
        Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showServerError() {
        AlertDialogUtil.showAlertDialog(getActivity(), R.string.server_error);
    }

    private void showProgressDialog() {
        dialog.setMessage(getString(R.string.loading_dialog));
        dialog.show();
    }

    @Override
    public void showOnExpiredTokenError() {
        AlertDialogUtil.showAlertDialogAndCloseSession(getActivity(), R.string.on_confluent_session_error);
    }
}
