package com.thinksmart.app4onesdk.engine.views.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.core.model.bos.CampaignBO;
import com.thinksmart.app4onesdk.core.model.bos.UserBO;
import com.thinksmart.app4onesdk.engine.application.AppCore;
import com.thinksmart.app4onesdk.engine.application.utils.NavigationUtil;
import com.thinksmart.app4onesdk.engine.presenters.ISelectProfilePresenter;
import com.thinksmart.app4onesdk.engine.presenters.imp.SelectProfilePresenterImp;
import com.thinksmart.app4onesdk.engine.views.ISelectProfileView;
import com.thinksmart.app4onesdk.engine.views.activities.SelectProfileActivity;
import com.thinksmart.app4onesdk.engine.views.adapters.SelectProfileSpinnerAdapter;
import com.thinksmart.app4onesdk.engine.views.utils.StringUtils;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import me.srodrigo.androidhintspinner.HintAdapter;
import me.srodrigo.androidhintspinner.HintSpinner;


/**
 * Created by roberto.demiguel on 17/10/2016.
 */
public class SelectProfileFragment extends Fragment implements ISelectProfileView {

    private ISelectProfilePresenter presenter;
    private UserBO user;
    private ArrayList<String> campaignNameList;
    private HintSpinner<String> hintSpinner;

    public SelectProfileFragment() {
    }

    public static SelectProfileFragment newInstance() {
        return new SelectProfileFragment();
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new SelectProfilePresenterImp(this);
        user = AppCore.getInstance().getSessionManager().getCurrentUser();
        initCampaignList();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_select_profile, container, false);

        initUIReferences(root);
        return root;
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        hintSpinner.init();
    }

    private void initUIReferences(View root) {
        Spinner profilesSpinner = (Spinner) root.findViewById(R.id.profilesSpinner);
        String hint;
        if (!campaignNameList.isEmpty()) {
            hint = getString(R.string.select_profile);
        } else {
            hint = getString(R.string.no_selected_profiles);
        }

        HintAdapter<String> adapter = new SelectProfileSpinnerAdapter(getActivity(), R.layout.profile_selector_spinner_item, hint, campaignNameList);
        adapter.setDropDownViewResource(R.layout.profile_selector_spinner_dropdown);
        hintSpinner = new HintSpinner<>(profilesSpinner, adapter, new HintSpinner.Callback<String>() {
            @Override
            public void onItemSelected(int position, String itemAtPosition) {
                CampaignBO selectedCampaign = user.getCampaignList().get(position);
                presenter.selectCampaign(selectedCampaign);

            }
        });
        hintSpinner.init();


        if (user.getUrlAvatar() != null && !user.getUrlAvatar().isEmpty()) {
            CircleImageView imgUser = (CircleImageView) root.findViewById(R.id.select_profile_user_image);
            Picasso.with(getActivity()).load(user.getUrlAvatar()).placeholder(getResources().getDrawable(R.drawable.ico_select_user)).fit().centerInside().into(imgUser);
        }

        TextView userLabel = (TextView) root.findViewById(R.id.select_profile_user_label);
        userLabel.setText(StringUtils.formatString(user.getName()));
//        TextView deptLabel = (TextView) root.findViewById(R.id.select_profile_dept_label);
//        deptLabel.setText(StringUtils.formatString(user.getPosition()));

    }


    private void initCampaignList() {
        campaignNameList = new ArrayList<>();
        if (user.getCampaignList() != null) {
            for (CampaignBO item : user.getCampaignList()) {
                campaignNameList.add(item.getName());
            }
        }
    }

    @Override
    public void navigateToTabsView() {
        if (user.getFormData() != null && user.getFormData()) {
            navigateToDataAssignment();
        } else {
            final String eventDestination = "goToHomeFromSelectProfile";
            NavigationUtil.goToNextScreen(SelectProfileActivity.class.getName(), eventDestination);
        }
    }

    private void navigateToDataAssignment() {
        final String eventDestination = "goToDataAssignmentFromSelectProfile";
        NavigationUtil.goToNextScreen(SelectProfileActivity.class.getName(), eventDestination);
    }
}
