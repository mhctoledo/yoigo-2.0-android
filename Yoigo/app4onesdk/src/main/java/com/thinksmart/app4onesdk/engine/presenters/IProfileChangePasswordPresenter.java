package com.thinksmart.app4onesdk.engine.presenters;

/**
 * Created by roberto.demiguel on 02/11/2016.
 */
public interface IProfileChangePasswordPresenter {

    /**
     * Call interector to change password
     *
     * @param newPassword
     * @param oldPassword
     */
    void changePassword(String newPassword, String oldPassword);

    /**
     * Destroy view
     */
    void onDestroy();
}
