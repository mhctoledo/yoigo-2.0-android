package com.thinksmart.app4onesdk.engine.views.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.core.model.bos.AddressBO;

import java.util.List;

/**
 * Created by roberto.demiguel on 07/11/2016.
 */
public class AddressesSpinnerAdapter extends ArrayAdapter {

    public AddressesSpinnerAdapter(Context context, List<AddressBO> addressBOList) {
        super(context, 0, addressBOList);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView text = (TextView) convertView;
        if (text == null) {
            text = (TextView) LayoutInflater.from(getContext()).inflate(R.layout.product_spinner_item, parent, false);
        }
        AddressBO item = (AddressBO) getItem(position);
        if (item != null) {
            text.setText(item.getName());
        }
        return text;
    }

    //not working in current library
//    @Override
//    public View getDropDownView(int position, View convertView, ViewGroup parent) {
//        RelativeLayout container = (RelativeLayout) convertView;
//
//        if (container == null) {
//            container = (RelativeLayout) LayoutInflater.from(getContext()).inflate(R.layout.address_spinner_dropdown, parent, false);
//        }
//
//        CheckedTextView name = (CheckedTextView) container.findViewById(R.id.address_name);
//        ImageView image = (ImageView) container.findViewById(R.id.address_photo);
//        AddressBO address = (AddressBO) getItem(position);
//        if (address != null) {
//            name.setText(address.getName());
//            Picasso.with(getContext()).load(address.getPhoto()).fit().centerInside().placeholder(R.drawable.progress_animation).into(image);
//
//        }
//        return image;
//    }
}
