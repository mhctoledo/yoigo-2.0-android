package com.thinksmart.app4onesdk.engine.views.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.BaseAdapter;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.core.model.bos.BannerBO;
import com.thinksmart.app4onesdk.core.model.bos.SliderBO;
import com.thinksmart.app4onesdk.core.vos.CatalogVO;
import com.thinksmart.app4onesdk.engine.views.IHomeView;
import com.thinksmart.app4onesdk.engine.views.utils.CustomSliderView;

import java.util.List;

/**
 * Created by roberto.demiguel on 17/10/2016.
 */
public class HomeFragmentAdapter extends BaseAdapter {

    private static final String TAG = "HomeFragmentAdapter";

    private LayoutInflater mInflater;
    private List<SliderBO> sliderList;
    private Context context;
    private IHomeView view;

    public HomeFragmentAdapter(Context context, IHomeView view, List<SliderBO> sliderList) {
        this.sliderList = sliderList;
        this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
        this.view = view;
    }

    @Override
    public int getCount() {
        return sliderList.size();
    }

    @Override
    public SliderBO getItem(int position) {
        return sliderList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        SliderBO aoSlider = getItem(position);
        if (aoSlider != null) {
            convertView = mInflater.inflate(R.layout.banner_item, parent, false);
            SliderLayout slider = (SliderLayout) convertView.findViewById(R.id.banner_slider);

            for (BannerBO banner : aoSlider.getBannerList()) {
                CustomSliderView defaultSliderView = new CustomSliderView(this.context);

                CatalogVO catalogVO = new CatalogVO();
                catalogVO.setSearch(banner.getSearch());
                catalogVO.setGroupId(banner.getGroupId());
                catalogVO.setSectionId(banner.getSectionId());
                catalogVO.setSubSectionId(banner.getSubSectionId());

                // initialize a SliderLayout
                defaultSliderView
                        .image(banner.getLogo())
                        .setScaleType(BaseSliderView.ScaleType.CenterCrop)
                        .setOnSliderClickListener(new BannerOnClickListener(catalogVO));

                slider.addSlider(defaultSliderView);
            }
            //set image transition animation
            slider.setPresetTransformer(SliderLayout.Transformer.Default);
            slider.setCustomAnimation(new DescriptionAnimation());
            //without transitions
            slider.stopAutoCycle();
            slider.setCustomIndicator((PagerIndicator) convertView.findViewById(R.id.banner_custom_indicator));
            //we setCurrentPosition to workaround non selected first item in slider
            // in case setCurrentPosition throws exception, we should do nothing
            try {
                if (slider.getChildCount() > 0) {
                    slider.setCurrentPosition(0);
                }
            } catch (IllegalStateException ise) {
                Log.d(TAG, ise.getMessage());
            }
        }
        return convertView;
    }


    private class BannerOnClickListener implements BaseSliderView.OnSliderClickListener {

        private CatalogVO catalogVO;

        BannerOnClickListener(CatalogVO catalogVO) {
            this.catalogVO = catalogVO;
        }

        @Override
        public void onSliderClick(BaseSliderView slider) {
            if (catalogVO != null) {
                if (catalogVO.getSearch() != null &&URLUtil.isValidUrl(catalogVO.getSearch())) {
                    view.navigateToWebView(catalogVO.getSearch());
                } else if (catalogVO.getSearch() != null && catalogVO.getSearch().startsWith("#")) {
                    view.navigateToScreen(catalogVO.getSearch().substring(1));
                } else {
                    view.navigateToCatalog(catalogVO);
                }
            }
        }
    }

}
