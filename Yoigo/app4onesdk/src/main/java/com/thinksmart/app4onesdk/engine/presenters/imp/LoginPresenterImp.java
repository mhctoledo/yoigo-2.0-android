package com.thinksmart.app4onesdk.engine.presenters.imp;

import android.content.Context;

import com.thinksmart.app4onesdk.core.model.bos.UserBO;
import com.thinksmart.app4onesdk.core.model.constants.UserPreferences;
import com.thinksmart.app4onesdk.core.vos.LoginVO;
import com.thinksmart.app4onesdk.engine.application.AppCore;
import com.thinksmart.app4onesdk.engine.interactors.ILoginInteractor;
import com.thinksmart.app4onesdk.engine.interactors.IPresenterListener;
import com.thinksmart.app4onesdk.engine.interactors.imp.LoginInteractorImp;
import com.thinksmart.app4onesdk.engine.presenters.ILoginPresenter;
import com.thinksmart.app4onesdk.engine.utils.LanguageUtil;
import com.thinksmart.app4onesdk.engine.utils.preferencesManager.IPreferencesManager;
import com.thinksmart.app4onesdk.engine.utils.preferencesManager.imp.PreferencesManagerImp;
import com.thinksmart.app4onesdk.engine.views.ILoginView;
import com.thinksmart.app4onesdk.engine.views.utils.SessionManagerUtil;

/**
 * Created by roberto.demiguel on 13/10/2016.
 */
public class LoginPresenterImp implements ILoginPresenter {

    private ILoginInteractor interactor;
    private ILoginView view;
    private IPreferencesManager preferencesManager;
    private Context context;

    public LoginPresenterImp(ILoginView view, Context context) {
        this.interactor = new LoginInteractorImp();
        this.view = view;
        this.context = context;
        this.preferencesManager = new PreferencesManagerImp(context);
    }

    @Override
    public void tryLoginFromToken() {
        String token = this.preferencesManager.retrieveStringPreference(UserPreferences.TOKEN);
        if (token != null) {
            if (view != null) {
                view.showProgress();
            }
            LoginVO loginVO = new LoginVO();
            loginVO.setToken(token);
            loginAux(loginVO, new LoginFromTokenPresenterListener());
        } else {
            String fingerprintSaved = this.preferencesManager.retrieveStringPreference(UserPreferences.FINGERPRINT);
            if (fingerprintSaved != null && fingerprintSaved.equals(UserPreferences.FINGERPRINT_SAVED)) {
                view.onTryLoginFromTokenFailed();
            }
            view.setLastUsername();
        }
    }

    @Override
    public void login(String user, String password) {
        if (view != null) {
            view.showProgress();
        }
        LoginVO loginVO = new LoginVO();
        loginVO.setPassword(password);
        loginVO.setUser(user);

        loginAux(loginVO, new LoginFromPasswordPresenterListener());
    }

    private void loginAux(LoginVO loginVO, IPresenterListener<UserBO> loginPresenterListener) {
        loginVO.setApplicationVersion(AppCore.getInstance().getAppConfig().getApplicationVersion());
        loginVO.setBundleCode(AppCore.getInstance().getAppConfig().getBundleCode());
        loginVO.setFirebaseToken(preferencesManager.retrieveStringPreference(UserPreferences.FIREBASE_TOKEN));

        interactor.login(loginVO, loginPresenterListener);
    }

    private class LoginFromTokenPresenterListener implements IPresenterListener<UserBO> {

        public LoginFromTokenPresenterListener() {
        }

        @Override
        public void onSuccess(UserBO user) {
            onListenerSuccessAux(user);
            if (view != null) {
                view.hideProgress();
                view.navigateToNextScreen();
            }
        }

        @Override
        public void onExpiredToken() {
            onListenerExpiredToken();
            if (view != null) {
                view.onTryLoginFromTokenFailed();
            }
        }

        @Override
        public void onError(Integer code, String error) {
            onListenerError(error);
            if (view != null) {
                view.onTryLoginFromTokenFailed();
                view.setLastUsername();
            }
        }

        @Override
        public void onServerError(String error) {
            onListenerServerErrorAux();
        }
    }

    private class LoginFromPasswordPresenterListener implements IPresenterListener<UserBO> {

        public LoginFromPasswordPresenterListener() {
        }

        @Override
        public void onSuccess(UserBO user) {
            onListenerSuccessAux(user);
            if (view != null) {
                view.hideProgress();
                view.askForFingerPrintAndNavigateToNextScreen();
            }
        }

        @Override
        public void onExpiredToken() {
            onListenerExpiredToken();
        }

        @Override
        public void onError(Integer code, String error) {
            SessionManagerUtil.resetFingerprintPreferences(context);
            onListenerError(error);
        }

        @Override
        public void onServerError(String error) {
            onListenerServerErrorAux();
        }
    }

    private void onListenerExpiredToken() {
        if (view != null) {
            view.hideProgress();
            view.showOnExpiredTokenError();
        }
    }

    private void onListenerServerErrorAux() {
        if (view != null) {
            view.hideProgress();
            view.showServerError();
        }
    }


    private void onListenerError(String error) {
        SessionManagerUtil.resetToken(context);
        if (view != null) {
            view.hideProgress();
            view.showCredentialsError(error);
        }
    }


    private void onListenerSuccessAux(UserBO user) {
        //Set session on singleton instance after login successful request
        AppCore.getInstance().getSessionManager().setCurrentUser(user);
        //set language
        if (user.getLanguage() != null) {
            LanguageUtil.setLanguage(context, user.getLanguage());
        }
        //Save token in shared prefs
        this.preferencesManager.saveStringPreference(UserPreferences.TOKEN, user.getToken());
    }

    @Override
    public void onDestroy() {
        view = null;
    }


}
