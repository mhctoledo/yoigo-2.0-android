package com.thinksmart.app4onesdk.engine.views.activities;

import android.os.Bundle;

import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.engine.views.fragments.ChangePasswordFragment;

/**
 * Created by roberto.demiguel on 31/10/2016.
 */
public class ChangePasswordActivity extends CustomActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        ChangePasswordFragment fragment = (ChangePasswordFragment) getSupportFragmentManager().findFragmentById(R.id.activity_change_password);
        if (fragment == null) {
            fragment = ChangePasswordFragment.newInstance();
            getSupportFragmentManager().beginTransaction().add(R.id.activity_change_password, fragment).commit();
        }
    }
}

