package com.thinksmart.app4onesdk.engine.views;

/**
 * Created by roberto.demiguel on 03/11/2016.
 */
public interface ICartView {

    /**
     * Updates footer info with points and total amount of items
     */
    void updateFooterInfo();
}
