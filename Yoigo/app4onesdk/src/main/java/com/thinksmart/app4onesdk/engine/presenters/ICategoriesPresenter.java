package com.thinksmart.app4onesdk.engine.presenters;

/**
 * Created by roberto.demiguel on 10/11/2016.
 */
public interface ICategoriesPresenter {

    /**
     * Call interector to get categories and display them in view.
     */
    void getCategories();

    /**
     * Destroy view
     */
    void onDestroy();

}
