package com.thinksmart.app4onesdk.core.model.bos;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by roberto.demiguel on 17/10/2016.
 */
public class NotificationsParentBO {
    @JsonProperty("messages")
    private List<NotificationBO> notificationList;

    public NotificationsParentBO() {
        notificationList = new ArrayList<>();
    }

    public List<NotificationBO> getNotificationList() {
        return notificationList;
    }

    public void setNotificationList(List<NotificationBO> notificationList) {
        this.notificationList = notificationList;
    }
}
