package com.thinksmart.app4onesdk.core.model.bos;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by roberto.demiguel on 14/10/2016.
 */
public class ProductImageBO {

    @JsonProperty("url")
    private String url;

    public ProductImageBO() {
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "ProductImageBO{" +
                "url='" + url + '\'' +
                '}';
    }
}
