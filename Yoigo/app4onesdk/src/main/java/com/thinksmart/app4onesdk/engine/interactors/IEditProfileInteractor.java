package com.thinksmart.app4onesdk.engine.interactors;

import com.thinksmart.app4onesdk.core.model.bos.UserBO;

/**
 * Created by roberto.demiguel on 02/11/2016.
 */
public interface IEditProfileInteractor {

    /**
     * This method creates request parameters, and sends a post request to udpate profile data. Listener will receive its result.
     *
     * @param token         of current user
     * @param participantId of current user
     * @param campaignId    of current user
     * @param transferUser  - which contains user data to update
     */
    void updateProfileData(String token, Integer participantId, Integer campaignId, UserBO transferUser);
}
