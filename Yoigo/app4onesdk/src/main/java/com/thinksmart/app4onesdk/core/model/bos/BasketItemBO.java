package com.thinksmart.app4onesdk.core.model.bos;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by roberto.demiguel on 13/10/2016.
 */
public class BasketItemBO {

    private String productParentId;
    @JsonProperty("productId")
    private String productId;
    @JsonProperty("name")
    private String name;
    @JsonProperty("points")
    private Double points;
    @JsonProperty("idCatalog")
    private String idCatalog;
    @JsonProperty("imageURL")
    private String imageURL;
    @JsonProperty("brand")
    private String brand;
    @JsonProperty("color")
    private String color;
    @JsonProperty("extraTag")
    private String extraTag;
    @JsonProperty("extraValue")
    private String extraValue;
    @JsonProperty("units")
    private Integer units;
    @JsonProperty("isSerie")
    private Boolean isSerie;

    public BasketItemBO() {
        this.units = 1;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getIdCatalog() {
        return idCatalog;
    }

    public void setIdCatalog(String idCatalog) {
        this.idCatalog = idCatalog;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPoints() {
        return points;
    }

    public void setPoints(Double points) {
        this.points = points;
    }

    public String getImageUrl() {
        return imageURL;
    }

    public void setImageURL(String imageUrl) {
        this.imageURL = imageUrl;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getExtraTag() {
        return extraTag;
    }

    public void setExtraTag(String extraTag) {
        this.extraTag = extraTag;
    }

    public String getExtraValue() {
        return extraValue;
    }

    public void setExtraValue(String extraValue) {
        this.extraValue = extraValue;
    }

    public Integer getUnits() {
        return units;
    }

    public void setUnits(Integer units) {
        this.units = units;
    }

    public Double getTotalPoints() {
        return points * units;
    }

    public String getImageURL() {
        return imageURL;
    }

    public Boolean getSerie() {
        return isSerie;
    }

    public void setSerie(Boolean serie) {
        isSerie = serie;
    }

    public String getProductParentId() {
        return productParentId;
    }

    public void setProductParentId(String productParentId) {
        this.productParentId = productParentId;
    }
}
