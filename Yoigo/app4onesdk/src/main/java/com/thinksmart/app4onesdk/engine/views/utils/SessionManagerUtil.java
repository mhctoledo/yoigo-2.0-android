package com.thinksmart.app4onesdk.engine.views.utils;

import android.content.Context;
import android.content.Intent;

import com.thinksmart.app4onesdk.core.model.constants.UserPreferences;
import com.thinksmart.app4onesdk.engine.utils.preferencesManager.IPreferencesManager;
import com.thinksmart.app4onesdk.engine.utils.preferencesManager.imp.PreferencesManagerImp;
import com.thinksmart.app4onesdk.engine.views.activities.LoginActivity;

import java.util.HashMap;
import java.util.Map;

import me.leolin.shortcutbadger.ShortcutBadger;

/**
 * Created by roberto.demiguel on 27/10/2016.
 */
public class SessionManagerUtil {

    public static void closeSession(Context context) {
        ShortcutBadger.removeCount(context);
        resetUserPreferences(context);
        resetFingerprintPreferences(context);
        goToLoginScreen(context);
    }

    public static void closeSessionOnExpiredToken(Context context) {
        resetToken(context);
        goToLoginScreen(context);
    }

    public static void resetToken(Context context) {
        IPreferencesManager preferencesManager = new PreferencesManagerImp(context);
        preferencesManager.saveStringPreference(UserPreferences.TOKEN, null);
    }

    private static void goToLoginScreen(Context context) {
        Intent intent = new Intent(context, LoginActivity.class);
        intent.putExtra("screenName", LoginActivity.class.getName());
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static void resetFingerprintPreferences(Context context) {
        IPreferencesManager preferencesManager = new PreferencesManagerImp(context);
        Map<String, String> preferencesToSave = new HashMap<>();
        preferencesToSave.put(UserPreferences.PASSWORD, null);
        preferencesToSave.put(UserPreferences.USER, null);
        preferencesToSave.put(UserPreferences.FINGERPRINT, null);
        preferencesManager.saveStringPreferences(preferencesToSave);

    }

    public static void resetUserPreferences(Context context) {
        IPreferencesManager preferencesManager = new PreferencesManagerImp(context);
        Map<String, String> preferencesToSave = new HashMap<>();
        preferencesToSave.put(UserPreferences.TOKEN, null);
        preferencesToSave.put(UserPreferences.BASKET, null);
        preferencesManager.saveStringPreferences(preferencesToSave);
    }

}
