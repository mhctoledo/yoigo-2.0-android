package com.thinksmart.app4onesdk.engine.views.activities;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.engine.views.fragments.FinishPurchaseFragment;

/**
 * Created by roberto.demiguel on 31/10/2016.
 */
public class FinishPurchaseActivity extends CustomActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finish_purchase);

        initToolbar();

        FinishPurchaseFragment fragment = (FinishPurchaseFragment) getSupportFragmentManager().findFragmentById(R.id.finish_purchase_container);
        if (fragment == null) {
            fragment = FinishPurchaseFragment.newInstance();
            getSupportFragmentManager().beginTransaction().add(R.id.finish_purchase_container, fragment).commit();
        }
    }


    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_with_points);
        toolbar.setTitle(R.string.finish_purchase_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
