package com.thinksmart.app4onesdk.core.model.bos;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by roberto.demiguel on 13/10/2016.
 */
public class ShipmentBO {

    @JsonProperty("fullname")
    private String fullname;
    @JsonProperty("address1")
    private String address1;
    @JsonProperty("address2")
    private String address2;
    @JsonProperty("country")
    private String country;
    @JsonProperty("state")
    private String state;
    @JsonProperty("city")
    private String city;
    @JsonProperty("postal")
    private String postal;
    @JsonProperty("email")
    private String email;
    @JsonProperty("cif")
    private String cif;
    @JsonProperty("company")
    private String company;
    @JsonProperty("tel")
    private String phone;

    private boolean isChangedInSession = false;

    public ShipmentBO() {
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostal() {
        return postal;
    }

    public void setPostal(String postal) {
        this.postal = postal;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public Boolean isChangedInSession() {
        return isChangedInSession;
    }

    public void setChangedInSession(Boolean changedInSession) {
        isChangedInSession = changedInSession;
    }

    @Override
    public String toString() {
        return "ShipmentBO{" +
                "fullname='" + fullname + '\'' +
                ", address1='" + address1 + '\'' +
                ", country='" + country + '\'' +
                ", state='" + state + '\'' +
                ", city='" + city + '\'' +
                ", company='" + company + '\'' +
                ", postal=" + postal +
                ", email='" + email + '\'' +
                ", cif='" + cif + '\'' +
                ", phone=" + phone +
                '}';
    }
}
