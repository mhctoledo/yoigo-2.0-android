package com.thinksmart.app4onesdk.engine.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.engine.views.utils.SessionManagerUtil;

/**
 * Created by roberto.demiguel on 31/05/2017.
 */

public class AlertDialogUtil {

    public static void showAlertDialog(Context context, String messageResource){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        dialogBuilder.setTitle(R.string.error_title_dialog);
        dialogBuilder.setMessage(messageResource);
        dialogBuilder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }

    public static void showAlertDialog(Context context, int messageResource){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        dialogBuilder.setTitle(R.string.error_title_dialog);
        dialogBuilder.setMessage(messageResource);
        dialogBuilder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }

    public static void showAlertDialogAndCloseSession(final Context context, int messageResource){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        dialogBuilder.setTitle(R.string.error_title_dialog);
        dialogBuilder.setMessage(messageResource);
        dialogBuilder.setCancelable(false);
        dialogBuilder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                SessionManagerUtil.closeSessionOnExpiredToken(context);
            }
        });
        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }
}
