package com.thinksmart.app4onesdk.engine.presenters.imp;

import android.content.Context;

import com.thinksmart.app4onesdk.core.model.bos.ProductBO;
import com.thinksmart.app4onesdk.engine.application.AppCore;
import com.thinksmart.app4onesdk.engine.interactors.IPresenterListener;
import com.thinksmart.app4onesdk.engine.interactors.IProductDetailsInteractor;
import com.thinksmart.app4onesdk.engine.interactors.imp.ProductDetailsInteractorImp;
import com.thinksmart.app4onesdk.engine.presenters.IProductDetailsPresenter;
import com.thinksmart.app4onesdk.engine.views.IProductDetailsView;
import com.thinksmart.app4onesdk.engine.views.utils.SessionManagerUtil;

/**
 * Created by roberto.demiguel on 26/10/2016.
 */

public class ProductDetailsPresenterImp implements IProductDetailsPresenter, IPresenterListener<ProductBO> {

    private IProductDetailsInteractor interactor;
    private IProductDetailsView view;
    private Context context;

    public ProductDetailsPresenterImp(IProductDetailsView view, Context context) {
        this.view = view;
        this.interactor = new ProductDetailsInteractorImp(this);
        this.context = context;
    }

    @Override
    public void getProduct(String productId, String isSerieStr) {
        if (view != null) {
            view.showProgress();
        }
        String token = AppCore.getInstance().getSessionManager().getCurrentUser().getToken();
        Integer participantId = AppCore.getInstance().getSessionManager().getCurrentUser().getSelectedCampaign().getParticipantId();
        Integer campaignId = AppCore.getInstance().getSessionManager().getCurrentUser().getSelectedCampaign().getId();
        Integer isSerie = Boolean.valueOf(isSerieStr) ? 1 : 0;

        interactor.getProduct(token, participantId, campaignId, productId, isSerie);
    }

    @Override
    public void onDestroy() {
        view = null;
    }

    @Override
    public void onSuccess(ProductBO product) {
        if (view != null) {
            view.hideProgress();
            if (product != null) {
                view.showProduct(product);
            }
        }
    }

    @Override
    public void onExpiredToken() {
        if (view != null) {
            view.hideProgress();
            view.showOnExpiredTokenError();
        }
    }

    @Override
    public void onError(Integer code, String error) {
        if (view != null) {
            view.hideProgress();
            view.showRequestError(error);
        }
    }

    @Override
    public void onServerError(String error) {
        if (view != null) {
            view.hideProgress();
            view.showServerError();
        }
    }
}
