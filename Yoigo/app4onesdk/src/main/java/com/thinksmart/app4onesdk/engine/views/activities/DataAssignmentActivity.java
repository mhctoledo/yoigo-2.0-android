package com.thinksmart.app4onesdk.engine.views.activities;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.engine.views.fragments.DataAssignmentFragment;

public class DataAssignmentActivity extends CustomActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_assignment);

        initToolbar();


        DataAssignmentFragment fragment = (DataAssignmentFragment) getSupportFragmentManager().findFragmentById(R.id.data_assignment_container);
        if (fragment == null) {
            fragment = DataAssignmentFragment.newInstance();
            getSupportFragmentManager().beginTransaction().add(R.id.data_assignment_container, fragment).commit();
        }
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.data_assignment_title);
        setSupportActionBar(toolbar);
    }

}
