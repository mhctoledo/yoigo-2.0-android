package com.thinksmart.app4onesdk.core.model.enums;

import android.content.Context;
import android.graphics.drawable.Drawable;

import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.engine.views.utils.StringUtils;

/**
 * Created by roberto.demiguel on 07/12/2016.
 */
public enum Tab {

    HOME("com.thinksmart.app4onesdk.engine.views.fragments.HomeFragment", R.drawable.home_icon, R.string.home_title),
    CATALOG("com.thinksmart.app4onesdk.engine.views.fragments.CatalogFragment", R.drawable.catalog_icon, R.string.catalog_title),
    POINTS("com.thinksmart.app4onesdk.engine.views.fragments.PointsFragment", R.drawable.points_icon, R.string.points_title);

    private static String sdkFragmentPackage = "com.thinksmart.app4onesdk.engine.views.fragments.";

    private String resourceName;
    private int iconResource;
    private int nameResource;

    Tab(String resourceName, int iconResource, int nameResource) {
        this.resourceName = resourceName;
        this.iconResource = iconResource;
        this.nameResource = nameResource;
    }

    public static Tab getTabFromResourceName(String resourceName) {
        Tab result = null;
        for (Tab categoryType : Tab.values()) {
            if (categoryType.getResourceName().equals(resourceName)) {
                result = categoryType;
                break;
            }
        }

        return result;
    }

    public String getResourceName() {
        return resourceName;
    }

    public int getIconResource() {
        return iconResource;
    }

    public Drawable getResourceIcon(Context context) {
        return context.getResources().getDrawable(this.iconResource);
    }

    public String getNameResource(Context context) {
        String result;
        if (this == POINTS) {
            result = StringUtils.changeFirstCharToUpperCase(StringUtils.getPointsTitle(context));
        } else {
            result = context.getResources().getString(this.nameResource);
        }
        return result;

    }
}
