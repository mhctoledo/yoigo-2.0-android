package com.thinksmart.app4onesdk.engine.views;

import com.thinksmart.app4onesdk.core.model.bos.ShipmentBO;

/**
 * Created by roberto.demiguel on 13/10/2016.
 */
public interface IConfirmShipmentView {

    /**
     * Shows progress dialog
     */
    void showProgress();

    /**
     * Hides progress dialog
     */
    void hideProgress();


    /**
     * Show obtained shipment on UI
     */
    void showShipmentData(ShipmentBO shipment);

    /**
     * Shows  error toast
     */
    void showRequestError(String error);

    /**
     * Shows server error toast
     */
    void showServerError();

    /**
     * Shows on confluent session error toast
     */
    void showOnExpiredTokenError();
}
