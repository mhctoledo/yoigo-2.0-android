package com.thinksmart.app4onesdk.engine.views.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.UiThread;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bignerdranch.expandablerecyclerview.ExpandableRecyclerAdapter;
import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.core.model.bos.SectionBO;
import com.thinksmart.app4onesdk.core.model.bos.SubSectionBO;
import com.thinksmart.app4onesdk.engine.views.holders.SectionViewHolder;
import com.thinksmart.app4onesdk.engine.views.holders.SubSectionViewHolder;

import java.util.List;

/**
 * Created by roberto.demiguel on 17/10/2016.
 */
public class CategorySectionsFragmentAdapter extends ExpandableRecyclerAdapter<SectionBO, SubSectionBO, SectionViewHolder, SubSectionViewHolder> {

    private LayoutInflater mInflater;

    public CategorySectionsFragmentAdapter(Context context, @NonNull List<SectionBO> sectionList) {
        super(sectionList);
        mInflater = LayoutInflater.from(context);
    }

    @UiThread
    @NonNull
    @Override
    public SectionViewHolder onCreateParentViewHolder(@NonNull ViewGroup parentViewGroup, int viewType) {
        View sectionView = mInflater.inflate(R.layout.category_section_item, parentViewGroup, false);
        return new SectionViewHolder(sectionView);
    }

    @UiThread
    @NonNull
    @Override
    public SubSectionViewHolder onCreateChildViewHolder(@NonNull ViewGroup childViewGroup, int viewType) {
        View subSectionView = mInflater.inflate(R.layout.category_subsection_item, childViewGroup, false);
        return new SubSectionViewHolder(subSectionView);
    }

    @UiThread
    @Override
    public void onBindParentViewHolder(@NonNull SectionViewHolder sectionViewHolder, int parentPosition, @NonNull SectionBO section) {
        sectionViewHolder.bind(section);
    }

    @UiThread
    @Override
    public void onBindChildViewHolder(@NonNull SubSectionViewHolder subSectionViewHolder, int parentPosition, int childPosition, @NonNull SubSectionBO subSection) {
        subSectionViewHolder.bind(subSection);
    }
}
