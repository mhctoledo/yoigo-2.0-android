package com.thinksmart.app4onesdk.core.model.enums;

/**
 * Created by roberto.demiguel on 07/12/2016.
 */
public enum PointsType {

    AVAILABLE("available"), TOTAL("total"), REDEEM("redeem");

    private String name;

    PointsType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
