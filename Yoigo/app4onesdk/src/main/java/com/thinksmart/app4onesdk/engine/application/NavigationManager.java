package com.thinksmart.app4onesdk.engine.application;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import com.thinksmart.app4onesdk.engine.application.appConfigModel.screen.Screen;
import com.thinksmart.app4onesdk.engine.application.utils.MakerManager;
import com.thinksmart.app4onesdk.engine.views.activities.SelectProfileActivity;
import com.thinksmart.app4onesdk.engine.views.activities.TabBarActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by roberto.demiguel on 14/10/2016.
 */
public class NavigationManager {

    final static private String TAG = "NavigationManager";

    //Properties
    private Activity currentActivity;

    //Constructor
    public NavigationManager() {

    }

    //Methods
    public void loadConfig(Activity activity) {

        String screenName = AppCore.getInstance().getAppConfig().getInitialScreen();
        Screen screen = AppCore.getInstance().getAppConfig().getScreenFromName(screenName);


        Intent intent = new Intent(activity, MakerManager.makeClassFromScreen(screen));
        intent.putExtra("screenName", screen.getResource());

        activity.startActivity(intent);
        activity.finish();

    }

    public void changeRootActivity(Screen screen, List<String> params) {
        Log.d(TAG, "cerrando sesion");

        Class<?> customClass = MakerManager.makeClassFromScreen(screen);
        Intent intent = new Intent(getCurrentActivity(), customClass);

        intent.putExtra("screenName", screen.getResource());

        if (params != null) {
            intent.putStringArrayListExtra("params", (ArrayList<String>) params);
        }

        //in case:
        //      we are returning to TabBar from Categories
        //      we are returning to SelectProfile from TabBar (this transition is also an Intent)
        // , stack must be cleared
        if (screen.getResource().equals(TabBarActivity.class.getName())
                || screen.getResource().equals(SelectProfileActivity.class.getName())) {
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        }

        getCurrentActivity().startActivity(intent);
        AppCore.getInstance().getEventManager().addObserver(screen);

//        getCurrentActivity().finish();
    }

    public Activity getCurrentActivity() {
        return currentActivity;
    }

    public void setCurrentActivity(Activity currentActivity) {
        this.currentActivity = currentActivity;
    }
}
