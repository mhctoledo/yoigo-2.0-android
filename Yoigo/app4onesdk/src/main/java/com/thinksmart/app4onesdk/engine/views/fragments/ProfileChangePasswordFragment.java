package com.thinksmart.app4onesdk.engine.views.fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.engine.presenters.IProfileChangePasswordPresenter;
import com.thinksmart.app4onesdk.engine.presenters.imp.ProfileChangePasswordPresenterImp;
import com.thinksmart.app4onesdk.engine.utils.AlertDialogUtil;
import com.thinksmart.app4onesdk.engine.views.IProfileChangePasswordView;
import com.thinksmart.app4onesdk.engine.views.utils.SessionManagerUtil;
import com.thinksmart.app4onesdk.engine.views.utils.ViewUtils;

/**
 * Created by roberto.demiguel on 31/10/2016.
 */
public class ProfileChangePasswordFragment extends Fragment implements IProfileChangePasswordView {

    private ProgressDialog dialog;

    private IProfileChangePasswordPresenter presenter;

    private EditText currentPasswordField;
    private EditText newPasswordField;
    private EditText repeatedPasswordField;

    public ProfileChangePasswordFragment() {
        // Required empty public constructor
    }

    public static ProfileChangePasswordFragment newInstance() {
        return new ProfileChangePasswordFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        presenter = new ProfileChangePasswordPresenterImp(this, getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile_change_password, container, false);

        initUIReferences(view);

        return view;
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.form_menu, menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_save) {
            updatePassword();
        }
        return super.onOptionsItemSelected(item);
    }

    private void initUIReferences(View view) {
        dialog = new ProgressDialog(getActivity());
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        currentPasswordField = (EditText) view.findViewById(R.id.profile_cs_current_pass_value);
        newPasswordField = (EditText) view.findViewById(R.id.profile_cs_new_pass_value);
        repeatedPasswordField = (EditText) view.findViewById(R.id.profile_cs_repeat_pass_value);
    }

    private void updatePassword() {
        ViewUtils.hideKeyboard(getView(), getActivity());
        String newPassword = newPasswordField.getText().toString();
        String currentPassword = currentPasswordField.getText().toString();
        if (arePasswordsOk(newPassword, currentPassword)) {
            presenter.changePassword(newPassword, currentPassword);
        }
    }

    private boolean arePasswordsOk(String newPassword, String currentPassword) {
        boolean ok = true;
        String repeatedPassword = repeatedPasswordField.getText().toString();
        if (newPassword.isEmpty() || currentPassword.isEmpty() || repeatedPassword.isEmpty()) {
            Toast.makeText(getActivity(), R.string.all_fields_are_mandatory, Toast.LENGTH_LONG).show();
            ok = false;
        } else if (newPassword.equals(currentPassword)) {
            Toast.makeText(getActivity(), R.string.current_and_new_password_are_equals, Toast.LENGTH_LONG).show();
            ok = false;
        } else if (!repeatedPassword.equals(newPassword)) {
            Toast.makeText(getActivity(), R.string.new_password_and_repeated_pass_not_equals, Toast.LENGTH_LONG).show();
            ok = false;
        } else if (!newPasswordContainsLowerChars(newPassword)) {
            Toast.makeText(getActivity(), R.string.new_password_doesnt_contains_lowercase_chars, Toast.LENGTH_LONG).show();
            ok = false;
        } else if (!newPasswordContainsEnoughChars(newPassword)) {
            Toast.makeText(getActivity(), R.string.new_password_doesnt_contains_enough_chars, Toast.LENGTH_LONG).show();
            ok = false;
        }

        return ok;
    }

    private boolean newPasswordContainsLowerChars(String newPassword) {
        boolean result = false;
        int lowerCase = 0;
        for (int k = 0; k < newPassword.length(); k++) {
            // Check for lowercase letters.
            if (Character.isLowerCase(newPassword.charAt(k))) lowerCase++;
        }
        if (lowerCase >= 4) {
            result = true;
        }
        return result;
    }

    private boolean newPasswordContainsEnoughChars(String newPassword) {
        return newPassword.length() >= 6;
    }


    @Override
    public void showProgress() {
        ViewUtils.hideKeyboard(getView(), getActivity());
        showProgressDialog();
    }

    private void showProgressDialog() {
        dialog.setMessage(getString(R.string.loading_dialog));
        dialog.show();
    }

    @Override
    public void hideProgress() {
        dialog.dismiss();
    }

    @Override
    public void onPasswordSuccessfullyChanged() {
        SessionManagerUtil.resetFingerprintPreferences(getActivity());
        Toast.makeText(getActivity(), R.string.profile_password_successfully_changed, Toast.LENGTH_LONG).show();
        getActivity().finish();
    }

    @Override
    public void showRequestError(String error) {
        AlertDialogUtil.showAlertDialog(getActivity(), error);
    }

    @Override
    public void showServerError() {
        AlertDialogUtil.showAlertDialog(getActivity(), R.string.server_error);
    }

    @Override
    public void showOnExpiredTokenError() {
        AlertDialogUtil.showAlertDialogAndCloseSession(getActivity(), R.string.on_confluent_session_error);
    }
}
