package com.thinksmart.app4onesdk.core.model.bos;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

/**
 * Created by roberto.demiguel on 13/10/2016.
 */
public class PointBO implements Serializable {

    @JsonProperty("name")
    private String name;
    @JsonProperty("value")
    private Double value;
    @JsonProperty("item")
    private List<PointBO> points;

    public PointBO() {
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public List<PointBO> getPoints() {
        return points;
    }

    public void setPoints(List<PointBO> points) {
        this.points = points;
    }

    @Override
    public String toString() {
        return "PointBO{" +
                "name='" + name + '\'' +
                ", value=" + value +
                ", points=" + points +
                '}';
    }
}
