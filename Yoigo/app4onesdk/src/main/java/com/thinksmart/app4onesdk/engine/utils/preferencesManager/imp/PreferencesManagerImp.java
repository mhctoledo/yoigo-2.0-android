package com.thinksmart.app4onesdk.engine.utils.preferencesManager.imp;

import android.content.Context;
import android.content.SharedPreferences;

import com.thinksmart.app4onesdk.engine.utils.preferencesManager.IPreferencesManager;

import java.util.Map;

/**
 * Created by roberto.demiguel on 14/10/2016.
 */

public class PreferencesManagerImp implements IPreferencesManager {

    private final SharedPreferences prefs;

    public PreferencesManagerImp(Context context) {
        prefs = context.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);
    }

    @Override
    public void saveStringPreference(String key, String value) {
        prefs.edit().putString(key, value).apply();
    }

    @Override
    public void saveIntPreference(String key, int value) {
        prefs.edit().putInt(key, value).apply();

    }

    @Override
    public void saveStringPreferences(Map<String, String> preferences) {
        SharedPreferences.Editor editor = prefs.edit();
        for (Map.Entry<String, String> entry : preferences.entrySet()) {
            editor.putString(entry.getKey(), entry.getValue());
        }
        editor.apply();
    }

    @Override
    public String retrieveStringPreference(String key) {
        return prefs.getString(key, null);
    }

    @Override
    public int retrieveIntPreference(String key) {
        return prefs.getInt(key, 0);
    }

}
