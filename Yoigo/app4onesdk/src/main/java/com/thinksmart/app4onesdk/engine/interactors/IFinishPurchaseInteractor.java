package com.thinksmart.app4onesdk.engine.interactors;

/**
 * Created by roberto.demiguel on 04/11/2016.
 */
public interface IFinishPurchaseInteractor {

    /**
     * REDEEM basket
     *
     * @param token
     * @param participantId
     * @param campaignId
     * @param password
     * @param basket
     */
    void redeemPoints(String token, Integer participantId, Integer campaignId, String password, String basket);

}
