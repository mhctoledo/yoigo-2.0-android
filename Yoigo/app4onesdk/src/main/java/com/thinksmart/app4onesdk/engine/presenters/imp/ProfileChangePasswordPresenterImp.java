package com.thinksmart.app4onesdk.engine.presenters.imp;

import android.content.Context;

import com.thinksmart.app4onesdk.core.model.bos.UserBO;
import com.thinksmart.app4onesdk.core.model.constants.UserPreferences;
import com.thinksmart.app4onesdk.engine.application.AppCore;
import com.thinksmart.app4onesdk.engine.interactors.IPresenterListener;
import com.thinksmart.app4onesdk.engine.interactors.IProfileChangePasswordInteractor;
import com.thinksmart.app4onesdk.engine.interactors.imp.ProfileChangePasswordInteractorImp;
import com.thinksmart.app4onesdk.engine.presenters.IProfileChangePasswordPresenter;
import com.thinksmart.app4onesdk.engine.utils.preferencesManager.imp.PreferencesManagerImp;
import com.thinksmart.app4onesdk.engine.views.IProfileChangePasswordView;
import com.thinksmart.app4onesdk.engine.views.utils.SessionManagerUtil;

/**
 * Created by roberto.demiguel on 02/11/2016.
 */

public class ProfileChangePasswordPresenterImp implements IProfileChangePasswordPresenter, IPresenterListener<UserBO> {

    private IProfileChangePasswordInteractor interactor;
    private IProfileChangePasswordView view;
    private Context context;


    public ProfileChangePasswordPresenterImp(IProfileChangePasswordView view, Context context) {
        this.interactor = new ProfileChangePasswordInteractorImp(this);
        this.view = view;
        this.context = context;
    }

    @Override
    public void changePassword(String newPassword, String oldPassword) {
        if(view != null){
            view.showProgress();
        }
        String token = AppCore.getInstance().getSessionManager().getCurrentUser().getToken();
        interactor.changePassword(token, newPassword, oldPassword);
    }

    @Override
    public void onDestroy() {
        view = null;
    }

    @Override
    public void onSuccess(UserBO user) {
        //gets and save new user with new password and new token
        updateCurrentUserData(user);

        if (view != null) {
            view.hideProgress();
            view.onPasswordSuccessfullyChanged();
        }
    }

    private void updateCurrentUserData(UserBO user) {
        UserBO currentUser = AppCore.getInstance().getSessionManager().getCurrentUser();
        currentUser.setToken(user.getToken());
        AppCore.getInstance().getSessionManager().setCurrentUser(currentUser);
        new PreferencesManagerImp(context).saveStringPreference(UserPreferences.TOKEN, user.getToken());
    }

    @Override
    public void onExpiredToken() {
        if (view != null) {
            view.hideProgress();
            view.showOnExpiredTokenError();
        }
    }

    @Override
    public void onError(Integer code, String error) {
        if (view != null) {
            view.hideProgress();
            view.showRequestError(error);
        }
    }

    @Override
    public void onServerError(String error) {
        if (view != null) {
            view.hideProgress();
            view.showServerError();
        }
    }
}
