package com.thinksmart.app4onesdk.engine.interactors;

import com.thinksmart.app4onesdk.core.vos.MessageVO;

/**
 * Created by roberto.demiguel on 13/10/2016.
 */

public interface IMessagesInteractor {

    /**
     * This method creates request parameters, and sends a post request to get messages. Listener will receive its result.
     */
    void getMessages(String token, Integer participantId, Integer campaignId);

    /**
     * This method creates request parameters, and sends a post request to get addresses. Listener will receive its result.
     */
    void getAddresses(String token, Integer participantId, Integer campaignId);

    /**
     * This method creates request parameters, and sends a post request to create a new message. Listener will receive its result.
     */
    void sendMessage(MessageVO messageVO);
}
