package com.thinksmart.app4onesdk.engine.views.fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.core.model.bos.PointBO;
import com.thinksmart.app4onesdk.core.model.bos.ProfilePointsBO;
import com.thinksmart.app4onesdk.engine.application.utils.NavigationUtil;
import com.thinksmart.app4onesdk.engine.presenters.IPointsPresenter;
import com.thinksmart.app4onesdk.engine.presenters.imp.PointsPresenterImp;
import com.thinksmart.app4onesdk.engine.utils.AlertDialogUtil;
import com.thinksmart.app4onesdk.engine.views.IPointsView;
import com.thinksmart.app4onesdk.engine.views.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by roberto.demiguel on 17/10/2016.
 */
public class PointsFragment extends Fragment implements IPointsView {

    private ProgressDialog dialog;
    private IPointsPresenter presenter;
    private SwipeRefreshLayout refreshLayout;

    private ProfilePointsBO profilePoints;

    private CircleImageView availablePointsView;
    private CircleImageView totalPointsView;
    private CircleImageView redeemPointsView;

    private TextView availableTextView;
    private TextView totalTextView;
    private TextView redeemTextView;

    public PointsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new PointsPresenterImp(this, getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_points, container, false);
        //init ui
        initUIReferences(view);
        initEvents();
        presenter.getPoints();

        return view;
    }

    private void initUIReferences(View view) {
        refreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.points_swiper_refresh);
        dialog = new ProgressDialog(getActivity());
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        availablePointsView = (CircleImageView) view.findViewById(R.id.availablePointsView);
        totalPointsView = (CircleImageView) view.findViewById(R.id.totalPointsView);
        redeemPointsView = (CircleImageView) view.findViewById(R.id.redeemPointsView);
        availableTextView = (TextView) view.findViewById(R.id.availableTextView);
        totalTextView = (TextView) view.findViewById(R.id.totalTextView);
        redeemTextView = (TextView) view.findViewById(R.id.redeemTextView);
    }

    private void initEvents() {
        availablePointsView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (profilePoints != null) {
                    goToPointsDetailView(profilePoints.getAvaiblePoints());
                }
            }
        });

        totalPointsView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (profilePoints != null) {
                    goToPointsDetailView(profilePoints.getTotalPoints());
                }
            }
        });

        redeemPointsView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (profilePoints != null) {
                    goToPointsDetailView(profilePoints.getRedeemPoints());
                }
            }
        });

        refreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        presenter.getPoints();
                    }
                }
        );
    }

    private void goToPointsDetailView(PointBO points) {
        if (points.getPoints() == null) {
            Toast.makeText(getActivity(), R.string.no_more_points_detail, Toast.LENGTH_SHORT).show();
        } else {
            List<PointBO> pointsParam = new ArrayList<>();
            pointsParam.add(points);

            final String eventDestination = "goToPointsDetailFromPoints";
            NavigationUtil.goToNextScreen(PointsFragment.class.getName(), eventDestination, pointsParam);
        }
    }


    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void showProgress() {
        showProgressDialog();
    }

    @Override
    public void hideProgress() {
        dialog.dismiss();
        refreshLayout.setRefreshing(false);
    }

    @Override
    public void showPoints(ProfilePointsBO profilePoints) {
        this.profilePoints = profilePoints;

        String avaiblePoints = StringUtils.formatPointsWithoutUnits(this.profilePoints.getAvaiblePoints().getValue(), getActivity());
        String redeemPoints = StringUtils.formatPointsWithoutUnits(this.profilePoints.getRedeemPoints().getValue(), getActivity());
        String totalPoints = StringUtils.formatPointsWithoutUnits(this.profilePoints.getTotalPoints().getValue(), getActivity());

        availableTextView.setText(avaiblePoints);
        totalTextView.setText(totalPoints);
        redeemTextView.setText(redeemPoints);
    }


    @Override
    public void showRequestError(String error) {
        AlertDialogUtil.showAlertDialog(getActivity(), error);
    }

    @Override
    public void showServerError() {
        AlertDialogUtil.showAlertDialog(getActivity(), R.string.server_error);
    }

    @Override
    public void showOnExpiredTokenError() {
        AlertDialogUtil.showAlertDialogAndCloseSession(getActivity(), R.string.on_confluent_session_error);
    }

    private void showProgressDialog() {
        dialog.setMessage(getString(R.string.loading_dialog));
        dialog.show();
    }
}
