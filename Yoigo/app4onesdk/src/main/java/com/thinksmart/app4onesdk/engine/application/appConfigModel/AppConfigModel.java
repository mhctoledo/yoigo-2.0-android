package com.thinksmart.app4onesdk.engine.application.appConfigModel;

import com.thinksmart.app4onesdk.engine.application.appConfigModel.screen.Screen;

import java.util.Map;

/**
 * Created by david.lopez on 20/1/16.
 */
public class AppConfigModel {

    private String bundleCode;
    private String applicationVersion;
    private String initialScreen;
    private boolean shipmentEditable;
    private boolean bmwShipmentRules;
    private boolean vfShipmentRules;
    private boolean userInfoLayoutOnHome;
    private Map<String, Screen> screens;

    public AppConfigModel() {
    }

    public AppConfigModel(String bundleCode, String applicationVersion, String initialScreen, Map<String, Screen> screens) {
        this.bundleCode = bundleCode;
        this.applicationVersion = applicationVersion;
        this.initialScreen = initialScreen;
        this.screens = screens;
        this.shipmentEditable = true;
        this.bmwShipmentRules = false;
        this.vfShipmentRules = false;
        this.userInfoLayoutOnHome = true;
    }

    public boolean isShipmentEditable() {
        return shipmentEditable;
    }

    public void setShipmentEditable(boolean shipmentEditable) {
        this.shipmentEditable = shipmentEditable;
    }

    public String getBundleCode() {
        return bundleCode;
    }

    public void setBundleCode(String bundleCode) {
        this.bundleCode = bundleCode;
    }

    public String getApplicationVersion() {
        return applicationVersion;
    }

    public void setApplicationVersion(String applicationVersion) {
        this.applicationVersion = applicationVersion;
    }

    public String getInitialScreen() {
        return initialScreen;
    }

    public void setInitialScreen(String initialScreen) {
        this.initialScreen = initialScreen;
    }

    public Map<String, Screen> getScreens() {
        return screens;
    }

    public void setScreens(Map<String, Screen> screens) {
        this.screens = screens;
    }

    public Screen getScreenFromName(String name) {
        return getScreens().get(name);
    }

    public Event eventOfScreen(String name, String eventName) {
        if (getScreenFromName(name) != null) {
            return getScreenFromName(name).eventFromName(eventName);
        }

        return null;
    }

    public boolean isUserInfoLayoutOnHome() {
        return userInfoLayoutOnHome;
    }

    public void setUserInfoLayoutOnHome(boolean userInfoLayoutOnHome) {
        this.userInfoLayoutOnHome = userInfoLayoutOnHome;
    }

    public boolean isBmwShipmentRules() {
        return bmwShipmentRules;
    }

    public void setBmwShipmentRules(boolean bmwShipmentRules) {
        this.bmwShipmentRules = bmwShipmentRules;
    }

    public boolean isVfShipmentRules() {
        return vfShipmentRules;
    }

    public void setVfShipmentRules(boolean vfShipmentRules) {
        this.vfShipmentRules = vfShipmentRules;
    }
}

