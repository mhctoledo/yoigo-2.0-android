package com.thinksmart.app4onesdk.core.model.constants;

/**
 * Created by roberto.demiguel on 09/03/2017.
 */

public class UserPreferences {

    public static final String BASKET = "basket";
    public static final String FINGERPRINT = "fingerprint";
    public static final String FINGERPRINT_SAVED = "fingerprintSaved";
    public static final String PENDING_NOTIFICATIONS = "pendingNotifications";
    public static final String TOKEN = "token";
    public static final String PASSWORD = "password";
    public static final String USER = "user";
    public static final String FIREBASE_TOKEN = "firebaseToken";

}
