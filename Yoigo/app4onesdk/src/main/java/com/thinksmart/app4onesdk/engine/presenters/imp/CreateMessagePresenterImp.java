package com.thinksmart.app4onesdk.engine.presenters.imp;

import android.support.v4.app.FragmentActivity;

import com.thinksmart.app4onesdk.core.model.bos.MessageAddressesBO;
import com.thinksmart.app4onesdk.core.vos.MessageVO;
import com.thinksmart.app4onesdk.engine.application.AppCore;
import com.thinksmart.app4onesdk.engine.interactors.IMessagesInteractor;
import com.thinksmart.app4onesdk.engine.interactors.IPresenterListener;
import com.thinksmart.app4onesdk.engine.interactors.imp.MessagesInteractorImp;
import com.thinksmart.app4onesdk.engine.presenters.ICreateMessagePresenter;
import com.thinksmart.app4onesdk.engine.views.ICreateMessageView;
import com.thinksmart.app4onesdk.engine.views.utils.SessionManagerUtil;

/**
 * Created by roberto.demiguel on 14/11/2016.
 */
public class CreateMessagePresenterImp implements ICreateMessagePresenter {

    private IMessagesInteractor interactor;
    private ICreateMessageView view;
    private FragmentActivity context;

    public CreateMessagePresenterImp(ICreateMessageView view, FragmentActivity context) {
        this.view = view;
        this.interactor = new MessagesInteractorImp(new GetAddressesListener(), new SendMessageListener());
        this.context = context;
    }


    @Override
    public void onDestroy() {
        view = null;
    }


    @Override
    public void getAddresses() {
        if (view != null) {
            view.showProgress();
        }
        String token = AppCore.getInstance().getSessionManager().getCurrentUser().getToken();
        Integer participantId = AppCore.getInstance().getSessionManager().getCurrentUser().getSelectedCampaign().getParticipantId();
        Integer campaignId = AppCore.getInstance().getSessionManager().getCurrentUser().getSelectedCampaign().getId();
        interactor.getAddresses(token, participantId, campaignId);
    }

    @Override
    public void sendMessage(MessageVO messageVO) {
        if (view != null) {
            view.showProgress();
        }
        String token = AppCore.getInstance().getSessionManager().getCurrentUser().getToken();
        Integer participantId = AppCore.getInstance().getSessionManager().getCurrentUser().getSelectedCampaign().getParticipantId();
        Integer campaignId = AppCore.getInstance().getSessionManager().getCurrentUser().getSelectedCampaign().getId();

        messageVO.setToken(token);
        messageVO.setSenderId(participantId);
        messageVO.setCampaignId(campaignId);

        interactor.sendMessage(messageVO);
    }


    private class GetAddressesListener implements IPresenterListener<MessageAddressesBO> {

        GetAddressesListener() {
        }

        @Override
        public void onSuccess(MessageAddressesBO messageAddresses) {
            if (messageAddresses == null) {
                messageAddresses = new MessageAddressesBO();
            }
            if (view != null) {
                view.hideProgress();
                view.populateAddresses(messageAddresses);
            }
        }

        @Override
        public void onExpiredToken() {
            showOnExpiredTokenErrorHelper();
        }

        @Override
        public void onError(Integer code, String error) {
            onErrorHelper(code, error);
        }

        @Override
        public void onServerError(String error) {
            onServerErrorHelper(error);
        }
    }

    private class SendMessageListener implements IPresenterListener {

        SendMessageListener() {
        }

        @Override
        public void onSuccess(Object object) {
            if (view != null) {
                view.hideProgress();
                view.onMessageSuccessfullySent();
            }
        }

        @Override
        public void onExpiredToken() {
            showOnExpiredTokenErrorHelper();
        }

        @Override
        public void onError(Integer code, String error) {
            onErrorHelper(code, error);
        }

        @Override
        public void onServerError(String error) {
            onServerErrorHelper(error);
        }
    }


    private void showOnExpiredTokenErrorHelper() {
        if (view != null) {
            view.hideProgress();
            view.showOnExpiredTokenError();
        }
    }


    private void onErrorHelper(Integer code, String error) {
        if (view != null) {
            view.hideProgress();
            view.showRequestError(error);
        }
    }

    private void onServerErrorHelper(String error) {
        if (view != null) {
            view.hideProgress();
            view.showServerError();
        }
    }
}
