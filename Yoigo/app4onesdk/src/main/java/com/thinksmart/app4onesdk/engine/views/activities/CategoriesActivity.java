package com.thinksmart.app4onesdk.engine.views.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.engine.views.fragments.CategoriesFragment;

/**
 * Created by roberto.demiguel on 10/11/2016.
 */
public class CategoriesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);

        initToolbar();

        CategoriesFragment fragment = (CategoriesFragment) getSupportFragmentManager().findFragmentById(R.id.categories_container);
        if (fragment == null) {
            fragment = CategoriesFragment.newInstance();
            getSupportFragmentManager().beginTransaction().add(R.id.categories_container, fragment).commit();
        }

    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_with_icon);
        toolbar.setTitle(R.string.categories_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
