package com.thinksmart.app4onesdk.core.api;

/**
 * Created by roberto.demiguel on 13/10/2016.
 */
public class ApiUrls {
    public static final String baseURL = "https://app4onedev.thinksmart.es";
    //    public static final String baseURL           = "http://app4one.thinksmart.es";
    public static final String version = "v2";

    public static final String accesTokenURL = "";
    public static final String loginURL = baseURL + "/" + version + "/login";
    public static final String eventURL = baseURL + "/" + version + "/diary";
    public static final String checkinURL = baseURL + "/" + version + "/checkin";
    public static final String checkoutURL = baseURL + "/" + version + "/checkout";
    public static final String cancelURL = baseURL + "/" + version + "/cancel";
    public static final String statisticURL = baseURL + "/" + version + "/statistics";
    public static final String changePassURL = baseURL + "/" + version + "/changePassword";
    public static final String forgetPassURL = baseURL + "/" + version + "/forgetPassword";
    public static final String saveUserDataURL = baseURL + "/" + version + "/saveUserData";
    public static final String homeParametersURL = baseURL + "/" + version + "/homeParametersV2";
    public static final String filterURL = baseURL + "/" + version + "/getFilters";
    public static final String productSearchURL = baseURL + "/" + version + "/productSearch";
    public static final String productDetailURL = baseURL + "/" + version + "/productDetail";
    public static final String redeemURL = baseURL + "/" + version + "/redeemShopping";
    public static final String shipmentURL = baseURL + "/" + version + "/shipmentAdress";
    public static final String pointsURL = baseURL + "/" + version + "/pointsV2";
    public static final String notificationsURL = baseURL + "/" + version + "/notifications";
    public static final String devicesMessageURL = baseURL + "/" + version + "/devicesMessages";
    public static final String messagesURL = baseURL + "/" + version + "/messages";
    public static final String sendMessageURL = baseURL + "/" + version + "/sendMessage";
}
