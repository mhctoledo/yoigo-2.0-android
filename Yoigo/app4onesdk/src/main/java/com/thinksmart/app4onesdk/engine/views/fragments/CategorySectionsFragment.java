package com.thinksmart.app4onesdk.engine.views.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.core.model.bos.GroupBO;
import com.thinksmart.app4onesdk.core.model.bos.SubSectionBO;
import com.thinksmart.app4onesdk.core.vos.CatalogVO;
import com.thinksmart.app4onesdk.engine.application.utils.NavigationUtil;
import com.thinksmart.app4onesdk.engine.views.activities.CategorySectionsActivity;
import com.thinksmart.app4onesdk.engine.views.adapters.CategorySectionsFragmentAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by roberto.demiguel on 11/11/2016.
 */
public class CategorySectionsFragment extends Fragment {

    private static final String ARG_PARAM = "params";

    private GroupBO group;

    public CategorySectionsFragment() {
        // Required empty public constructor
    }

    public static CategorySectionsFragment newInstance(GroupBO group) {
        CategorySectionsFragment pointsDetailFragment = new CategorySectionsFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM, group);
        pointsDetailFragment.setArguments(args);
        return pointsDetailFragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            group = (GroupBO) getArguments().getSerializable(ARG_PARAM);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.fragment_category_sections, container, false);

        initUIReferences(rootView);

        return rootView;
    }

    private void initUIReferences(View root) {
        RecyclerView categoriesRecyclerView = (RecyclerView) root.findViewById(R.id.category_sections_list);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        categoriesRecyclerView.setLayoutManager(linearLayoutManager);
        // view adapter
        CategorySectionsFragmentAdapter adapter = new CategorySectionsFragmentAdapter(getContext(), group.getSections());
        categoriesRecyclerView.setAdapter(adapter);
    }


    public static class CategorySectionsOnClickListener implements View.OnClickListener {

        private SubSectionBO subSection;

        public CategorySectionsOnClickListener(SubSectionBO subSection) {
            this.subSection = subSection;
        }

        @Override
        public void onClick(View v) {
            CatalogVO catalogVO = new CatalogVO();
            catalogVO.setSubSectionId(subSection.getId());
            catalogVO.setGroupId(subSection.getGroupId());
            catalogVO.setSectionId(subSection.getSectionId());
            catalogVO.setPage(1);
            catalogVO.setSearch(null);

            List<CatalogVO> pointsParam = new ArrayList<>();
            pointsParam.add(catalogVO);

            final String eventDestination = "goToCatalogFromCategorySections";
            NavigationUtil.goToNextScreen(CategorySectionsActivity.class.getName(), eventDestination, pointsParam);
        }
    }


}
