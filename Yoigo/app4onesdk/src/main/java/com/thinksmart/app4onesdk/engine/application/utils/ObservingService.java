package com.thinksmart.app4onesdk.engine.application.utils;

import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by david.lopez on 21/1/16.
 */
public class ObservingService {

    private HashMap<String, CustomObservable> observables;

    public ObservingService() {
        observables = new HashMap<>();
    }

    public void addObserver(String notification, Observer observer) {
        CustomObservable observable = observables.get(notification);
        if (observable == null) {
            observable = new CustomObservable(notification);
            observables.put(notification, observable);
        }
        observable.addObserver(observer);
    }

    public void removeObserver(String notification, Observer observer) {
        Observable observable = observables.get(notification);
        if (observable != null) {
            observable.deleteObserver(observer);
        }
    }

    public void postNotification(String notification, Object object) {
        CustomObservable observable = observables.get(notification);
        if (observable != null) {
            observable.notifyObservers(object);
        }
    }

    public HashMap<String, CustomObservable> getObservables() {
        return observables;
    }
}

