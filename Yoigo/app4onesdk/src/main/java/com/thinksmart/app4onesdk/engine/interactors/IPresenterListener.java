package com.thinksmart.app4onesdk.engine.interactors;


/**
 * Created by roberto.demiguel on 14/10/2016.
 */
public interface IPresenterListener<T> {

    /**
     * Callback from Request on success
     *
     * @param object obtained from request
     */
    void onSuccess(T object);


    /**
     * Another device has connected with same credentials.
     */
    void onExpiredToken();


    /**
     * Callback from Request on error
     *
     * @param error from server
     */
    void onError(Integer code, String error);

    /**
     * Callback from Request on server success
     *
     * @param error from volley
     */
    void onServerError(String error);
}
