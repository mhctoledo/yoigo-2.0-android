package com.thinksmart.app4onesdk.core.model.bos;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

/**
 * Created by roberto.demiguel on 13/10/2016.
 */
public class CampaignBO {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("nameLong")
    private String name;
    @JsonProperty("idParticipant")
    private Integer participantId;
    @JsonProperty("pointsAlias")
    private String pointsAlias;
    @JsonProperty("extraData")
    private Map<String, String> extraData;

    public CampaignBO() {
    }

    public Map<String, String> getExtraData() {
        return extraData;
    }

    public void setExtraData(Map<String, String> extraData) {
        this.extraData = extraData;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getParticipantId() {
        return participantId;
    }

    public void setParticipantId(Integer participantId) {
        this.participantId = participantId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPointsAlias() {
        return pointsAlias;
    }

    public void setPointsAlias(String pointsAlias) {
        this.pointsAlias = pointsAlias;
    }

    @Override
    public String toString() {
        return "CampaignBO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", participantId=" + participantId +
                ", pointsAlias='" + pointsAlias + '\'' +
                '}';
    }
}
