package com.thinksmart.app4onesdk.engine.presenters.imp;

import android.content.Context;

import com.thinksmart.app4onesdk.core.model.bos.BasketBO;
import com.thinksmart.app4onesdk.core.model.bos.ProfilePointsBO;
import com.thinksmart.app4onesdk.core.model.bos.RedeemPointsResultBO;
import com.thinksmart.app4onesdk.core.model.util.BasketUtil;
import com.thinksmart.app4onesdk.engine.application.AppCore;
import com.thinksmart.app4onesdk.engine.interactors.IFinishPurchaseInteractor;
import com.thinksmart.app4onesdk.engine.interactors.IPointsInteractor;
import com.thinksmart.app4onesdk.engine.interactors.IPresenterListener;
import com.thinksmart.app4onesdk.engine.interactors.imp.FinishPurchaseInteractorImp;
import com.thinksmart.app4onesdk.engine.interactors.imp.PointsInteractorImp;
import com.thinksmart.app4onesdk.engine.presenters.IFinishPurchasePresenter;
import com.thinksmart.app4onesdk.engine.views.IFinishPurchaseView;
import com.thinksmart.app4onesdk.engine.views.utils.SessionManagerUtil;

/**
 * Created by roberto.demiguel on 04/11/2016.
 */

public class FinishPurchasePresenterImp implements IFinishPurchasePresenter {

    private IFinishPurchaseInteractor redeemInteractor;
    private IPointsInteractor pointsInteractor;
    private IFinishPurchaseView view;
    private Context context;


    public FinishPurchasePresenterImp(IFinishPurchaseView view, Context context) {
        this.redeemInteractor = new FinishPurchaseInteractorImp(new RedeemPointsListener());
        this.pointsInteractor = new PointsInteractorImp(new CheckAvailablePointsListener());
        this.view = view;
        this.context = context;
    }

    @Override
    public void checkAvailablePointsBeforeRedeem() {
        if (view != null) {
            view.showProgress();
        }
        String token = AppCore.getInstance().getSessionManager().getCurrentUser().getToken();
        Integer participantId = AppCore.getInstance().getSessionManager().getCurrentUser().getSelectedCampaign().getParticipantId();
        Integer campaignId = AppCore.getInstance().getSessionManager().getCurrentUser().getSelectedCampaign().getId();
        pointsInteractor.getPoints(token, participantId, campaignId);
    }

    @Override
    public void redeemPoints(String password, BasketBO currentBasket) {
        if (view != null) {
            view.showProgress();
        }
        String token = AppCore.getInstance().getSessionManager().getCurrentUser().getToken();
        Integer participantId = AppCore.getInstance().getSessionManager().getCurrentUser().getSelectedCampaign().getParticipantId();
        Integer campaignId = AppCore.getInstance().getSessionManager().getCurrentUser().getSelectedCampaign().getId();

        String basketString = BasketUtil.convertToXml(currentBasket);
        redeemInteractor.redeemPoints(token, participantId, campaignId, password, basketString);
    }

    @Override
    public void onDestroy() {
        view = null;
    }


    private class CheckAvailablePointsListener implements IPresenterListener<ProfilePointsBO> {

        @Override
        public void onSuccess(ProfilePointsBO profilePoints) {
            Double basketPoints = AppCore.getInstance().getSessionManager().getCurrentBasket().getTotalPoints();
            Double availablePoints = profilePoints.getAvaiblePoints().getValue();

            view.hideProgress();
            if (basketPoints <= availablePoints) {
                view.askForPasswordBeforeRedeem();
            } else if (view != null) {
                view.showNotEnoughPointsToRedeem();
            }
        }

        @Override
        public void onError(Integer code, String error) {
            onErrorHelper(error);

        }

        @Override
        public void onExpiredToken() {
            showOnExpiredTokenErrorHelper();
        }


        @Override
        public void onServerError(String error) {
            onServerErrorHelper(error);
        }
    }


    private class RedeemPointsListener implements IPresenterListener<RedeemPointsResultBO> {

        @Override
        public void onSuccess(RedeemPointsResultBO redeemPointsResultBO) {
            if (redeemPointsResultBO != null && redeemPointsResultBO.getAvaiablePoints() != null) {
                AppCore.getInstance().getSessionManager().getCurrentUser().setAvailablePoints(redeemPointsResultBO.getAvaiablePoints());
            }
            if (view != null) {
                view.hideProgress();
                view.onSuccessfulRedemtion();
            }
        }

        @Override
        public void onExpiredToken() {
            showOnExpiredTokenErrorHelper();
        }

        @Override
        public void onError(Integer code, String error) {
            onErrorHelper(error);
        }

        @Override
        public void onServerError(String error) {
            onServerErrorHelper(error);
        }
    }


    private void showOnExpiredTokenErrorHelper() {
        if (view != null) {
            view.hideProgress();
            view.showOnExpiredTokenError();
        }
    }

    private void onErrorHelper(String error) {
        if (view != null) {
            view.hideProgress();
            view.showRequestError(error);
        }
    }

    private void onServerErrorHelper(String error) {
        if (view != null) {
            view.hideProgress();
            view.showServerError();
        }
    }


}
