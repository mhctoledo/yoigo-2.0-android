package com.thinksmart.app4onesdk.engine.views.activities;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.engine.views.fragments.ChangeShipmentFragment;

/**
 * Created by roberto.demiguel on 02/11/2016.
 */
public class ChangeShipmentActivity extends CustomActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_shipment);

        initToolbar();

        ChangeShipmentFragment fragment = (ChangeShipmentFragment) getSupportFragmentManager().findFragmentById(R.id.change_shipment_container);
        if (fragment == null) {
            fragment = ChangeShipmentFragment.newInstance();
            getSupportFragmentManager().beginTransaction().add(R.id.change_shipment_container, fragment).commit();
        }
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.change_shipment_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
