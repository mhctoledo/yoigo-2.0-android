package com.thinksmart.app4onesdk.core.model.bos;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by roberto.demiguel on 25/10/2016.
 */

public class CatalogBO {

    @JsonProperty("productos")
    private List<ProductBO> productList;

    public CatalogBO() {
    }

    public List<ProductBO> getProductList() {
        return productList;
    }

    public void setProductList(List<ProductBO> productList) {
        this.productList = productList;
    }

    @Override
    public String toString() {
        return "CatalogBO{" +
                "productList=" + productList.toString() +
                '}';
    }

}
