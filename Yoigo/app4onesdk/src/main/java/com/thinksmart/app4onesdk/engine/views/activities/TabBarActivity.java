package com.thinksmart.app4onesdk.engine.views.activities;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.core.model.enums.Tab;
import com.thinksmart.app4onesdk.core.vos.CatalogVO;
import com.thinksmart.app4onesdk.engine.application.AppCore;
import com.thinksmart.app4onesdk.engine.application.appConfigModel.screen.Screen;
import com.thinksmart.app4onesdk.engine.application.appConfigModel.screen.SideMenu;
import com.thinksmart.app4onesdk.engine.application.appConfigModel.screen.SideMenuScreen;
import com.thinksmart.app4onesdk.engine.application.appConfigModel.screen.TabBarScreen;
import com.thinksmart.app4onesdk.engine.application.utils.NavigationUtil;
import com.thinksmart.app4onesdk.engine.views.adapters.DrawerMenuAdapter;
import com.thinksmart.app4onesdk.engine.views.adapters.TabBarFragmentPagerAdapter;
import com.thinksmart.app4onesdk.engine.views.fragments.CatalogFragment;
import com.thinksmart.app4onesdk.engine.views.fragments.PointsFragment;
import com.thinksmart.app4onesdk.engine.views.utils.CustomDividerItemDecoration;
import com.thinksmart.app4onesdk.engine.views.utils.SessionManagerUtil;
import com.thinksmart.app4onesdk.engine.views.utils.ViewUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by roberto.demiguel on 17/10/2016.
 */
public class TabBarActivity extends CustomActivity {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private DrawerLayout drawer;
    private View appIcon;
    private Button closeSession;
    private Integer catalogViewPagePostion = null;
    private Integer pointsViewPagePostion = null;
    private boolean doubleBackToExitPressedOnce = false;
    private DrawerMenuAdapter drawerMenuAdapter;
    private TabBarFragmentPagerAdapter tabBarFragmentPagerAdapter;

    private TabBarScreen tabBarScreen;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab_bar);

        tabBarScreen = (TabBarScreen) screen;

        initUIReferences();
        initEvents();
        //init basket
        AppCore.getInstance().getSessionManager().restoreBasket(this);


        //get catalog params from intent if we are coming from Categories
        List<CatalogVO> catalogParams = (List<CatalogVO>) getIntent().getSerializableExtra("params");
        if (catalogViewPagePostion != null && catalogParams != null && !catalogParams.isEmpty()) {
            viewPager.setCurrentItem(catalogViewPagePostion);
        }

    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            //back goes to Select profile if we have several campaigns, else exit app
            if (AppCore.getInstance().getSessionManager().getCurrentUser().getCampaignList().size() == 1) {
                if (doubleBackToExitPressedOnce) {
                    super.onBackPressed();
                } else {
                    this.doubleBackToExitPressedOnce = true;
                    Toast.makeText(this, getString(R.string.press_back_again), Toast.LENGTH_SHORT).show();

                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            doubleBackToExitPressedOnce = false;
                        }
                    }, 2000);
                }
            } else {
                final String eventDestination = "goToSelectFromTabBar";
                NavigationUtil.goToNextScreen(TabBarActivity.class.getName(), eventDestination);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (drawerMenuAdapter != null) {
            drawerMenuAdapter.notifyDataSetChanged();
        }
    }

    private void initUIReferences() {
        appIcon = findViewById(R.id.toolbar_app_icon);

        initFirstTab();
        initTabLayout();
        initViewPager();
        initNavigationDrawer();
    }

    private void initNavigationDrawer() {
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setItemIconTintList(null);

        //set navigation drawer info
        initNavigationDrawerInfo(navigationView);

        initDrawerMenu();

        closeSession = (Button) findViewById(R.id.close_session_button);


    }

    private void initNavigationDrawerInfo(NavigationView navigationView) {
        if (navigationView.getHeaderCount() > 0) {
            View headerLayout = navigationView.getHeaderView(0);
            RelativeLayout profileLayout = (RelativeLayout) headerLayout.findViewById(R.id.profile);
            TextView pointsLabel = (TextView) headerLayout.findViewById(R.id.nav_profile_points_label);

            profileLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final String eventDestination = "goToProfileInfoFromTabBar";
                    NavigationUtil.goToNextScreen(TabBarActivity.class.getName(), eventDestination);
                }
            });
            pointsLabel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    goToPointsTab();
                }
            });

        }
    }

    public void goToCatalogTab(CatalogVO catalogVO) {
        if (catalogViewPagePostion != null) {
            catalogVO.setPage(1);
            viewPager.setCurrentItem(catalogViewPagePostion);
            ((CatalogFragment) tabBarFragmentPagerAdapter.getRegisteredFragment(catalogViewPagePostion)).setCurrentSearch(catalogVO);
        }

    }

    public void goToPointsTab() {
        if (pointsViewPagePostion != null) {
            drawer.closeDrawer(GravityCompat.START);
            viewPager.setCurrentItem(pointsViewPagePostion);
        }
    }

    private void initDrawerMenu() {
        //menu recycler view
        RecyclerView drawerMenuRecyclerView = (RecyclerView) findViewById(R.id.drawer_menu_recycler_view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        drawerMenuRecyclerView.setLayoutManager(linearLayoutManager);
        //add list_divider
        //TODO when upgrade project to v25
        //        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(cartRecyclerView.getContext(), linearLayoutManager.getOrientation());
        CustomDividerItemDecoration dividerItemDecoration = new CustomDividerItemDecoration(this, linearLayoutManager.getOrientation(), 40);
        drawerMenuRecyclerView.addItemDecoration(dividerItemDecoration);
        drawerMenuRecyclerView.setHasFixedSize(true);

        List<SideMenu> sideMenuList = new ArrayList<>();

        Map<String, Screen> screens = AppCore.getInstance().getAppConfig().getScreens();
        for (Map.Entry<String, Screen> screenEntry : screens.entrySet()) {
            //if screen type is SIDE_MENU we have to add it in navigation drawer
            if (screenEntry.getValue() instanceof SideMenuScreen) {
                SideMenu menu = ((SideMenuScreen) screenEntry.getValue()).getSideMenu();
                //if screen name is available
                if (menu != null && menu.isSideMenuShown()) {
                    sideMenuList.add(menu);
                }
            }
        }
        drawerMenuAdapter = new DrawerMenuAdapter(this, sideMenuList);
        drawerMenuRecyclerView.setAdapter(drawerMenuAdapter);
    }

    private void initFirstTab() {
        if (!tabBarScreen.getTabs().isEmpty()) {
            Tab tab = tabBarScreen.getTabs().get(0);
            //set toolbar title
            toolbar = (Toolbar) findViewById(R.id.tabs_toolbar);
            if (toolbar != null) {
                toolbar.setTitle(tab.getNameResource(getApplicationContext()));
            }
            //add events of first tab
            AppCore.getInstance().getEventManager().addObserver(AppCore.getInstance().getAppConfig().getScreenFromName(tab.getResourceName()));
            setSupportActionBar(toolbar);
        }
    }

    private void initTabLayout() {
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.setSelectedTabIndicatorColor(Color.WHITE);
//        boolean isFirstTab = true;
        for (Tab tabItem : tabBarScreen.getTabs()) {
            TabLayout.Tab tab = tabLayout.newTab();
            tab.setTag(tabItem.getResourceName());
            tab.setIcon(tabItem.getIconResource());

            //save catalog tab position
            if (tabItem.getResourceName().equals(CatalogFragment.class.getName())) {
                catalogViewPagePostion = tabLayout.getTabCount();
            } else if (tabItem.getResourceName().equals(PointsFragment.class.getName())) {
                pointsViewPagePostion = tabLayout.getTabCount();
            }
            tabLayout.addTab(tab);
        }
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
    }


    private void initViewPager() {
        viewPager = (ViewPager) findViewById(R.id.pager);
        tabBarFragmentPagerAdapter = new TabBarFragmentPagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount(), tabBarScreen);
        viewPager.setAdapter(tabBarFragmentPagerAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        //to avoid onCreateView each time you enter a fragment
        viewPager.setOffscreenPageLimit(tabBarScreen.getTabs().size());
    }

    private void initEvents() {
        closeSession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), R.string.cloasing_session, Toast.LENGTH_SHORT).show();
                SessionManagerUtil.closeSession(getBaseContext());
            }
        });

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                String tabName = tabBarScreen.getTabs().get(tab.getPosition()).getNameResource(getApplicationContext());
                toolbar.setTitle(tabName);
                viewPager.setCurrentItem(tab.getPosition());
                showAppIconInTab(tab);
                Tab order = tabBarScreen.getTabs().get(tab.getPosition());
                AppCore.getInstance().getEventManager().addObserver(AppCore.getInstance().getAppConfig().getScreenFromName(order.getResourceName()));
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                ViewUtils.hideKeyboard(findViewById(android.R.id.content), getApplicationContext());
                Tab order = tabBarScreen.getTabs().get(tab.getPosition());
                AppCore.getInstance().getEventManager().removeObserver(AppCore.getInstance().getAppConfig().getScreenFromName(order.getResourceName()));
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    /**
     * Show or Hide app icon in toolbar
     *
     * @param tab - current Tab
     */
    private void showAppIconInTab(TabLayout.Tab tab) {
        //only hide in catalog
        if ((tab.getTag() != null) && tab.getTag().equals(CatalogFragment.class.getName())) {
            toolbar.removeView(appIcon);

        } else if (toolbar.findViewById(R.id.toolbar_app_icon) == null) {
            toolbar.addView(appIcon);
        }
    }

    /**
     * Changes color of tab. Used when tab is selected or unselected.
     *
     * @param tab   - current tab
     * @param color to tint
     */
    private void changeTabColor(TabLayout.Tab tab, int color) {
        if (tab.getIcon() != null) {
            int tabIconColor = ContextCompat.getColor(getApplicationContext(), color);
            tab.getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
        }
    }

}
