package com.thinksmart.app4onesdk.engine.presenters.imp;

import android.support.v4.app.FragmentActivity;

import com.thinksmart.app4onesdk.core.model.bos.CatalogBO;
import com.thinksmart.app4onesdk.core.model.bos.ProductBO;
import com.thinksmart.app4onesdk.core.vos.CatalogVO;
import com.thinksmart.app4onesdk.engine.application.AppCore;
import com.thinksmart.app4onesdk.engine.interactors.ICatalogInteractor;
import com.thinksmart.app4onesdk.engine.interactors.IPresenterListener;
import com.thinksmart.app4onesdk.engine.interactors.imp.CatalogInteractorImp;
import com.thinksmart.app4onesdk.engine.presenters.ICatalogPresenter;
import com.thinksmart.app4onesdk.engine.views.ICatalogView;
import com.thinksmart.app4onesdk.engine.views.utils.SessionManagerUtil;

import java.util.ArrayList;


/**
 * Created by roberto.demiguel on 25/10/2016.
 */
public class CatalogPresenterImp implements ICatalogPresenter, IPresenterListener<CatalogBO> {

    private ICatalogInteractor interactor;
    private ICatalogView view;
    private FragmentActivity context;

    public CatalogPresenterImp(ICatalogView view, FragmentActivity context) {
        this.view = view;
        this.interactor = new CatalogInteractorImp(this);
        this.context = context;
    }

    @Override
    public void getCatalog(CatalogVO catalogVO) {
        if (view != null) {
            view.showProgress();
        }
        String token = AppCore.getInstance().getSessionManager().getCurrentUser().getToken();
        Integer participantId = AppCore.getInstance().getSessionManager().getCurrentUser().getSelectedCampaign().getParticipantId();
        Integer campaignId = AppCore.getInstance().getSessionManager().getCurrentUser().getSelectedCampaign().getId();

        catalogVO.setToken(token);
        catalogVO.setParticipantId(participantId);
        catalogVO.setCampaignId(campaignId);

        interactor.getCatalog(catalogVO);
    }

    @Override
    public void onDestroy() {
        view = null;
    }

    @Override
    public void onSuccess(CatalogBO catalog) {
        if (view != null) {
            view.hideProgress();
            if (catalog != null && catalog.getProductList() != null) {
                view.showCatalog(catalog.getProductList());
            } else {
                view.showCatalog(new ArrayList<ProductBO>());
            }
        }
    }

    @Override
    public void onExpiredToken() {
        if (view != null) {
            view.hideProgress();
            view.showOnExpiredTokenError();
        }
    }

    @Override
    public void onError(Integer code, String error) {
        if (view != null) {
            view.hideProgress();
            view.showRequestError(error);
        }
    }

    @Override
    public void onServerError(String error) {
        if (view != null) {
            view.hideProgress();
            view.showServerError();
        }
    }
}
