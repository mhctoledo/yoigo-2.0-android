package com.thinksmart.app4onesdk.engine.interactors;

/**
 * Created by roberto.demiguel on 13/10/2016.
 */

public interface ICategoriesInteractor {

    /**
     * This method creates request parameters, and sends a post request to get categories. Listener will receive its result.
     */
    void getCategories(String token, Integer participantId, Integer campaignId);
}
