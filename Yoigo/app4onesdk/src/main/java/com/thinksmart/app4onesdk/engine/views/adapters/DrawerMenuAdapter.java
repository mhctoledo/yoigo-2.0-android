package com.thinksmart.app4onesdk.engine.views.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.core.model.constants.UserPreferences;
import com.thinksmart.app4onesdk.engine.application.appConfigModel.screen.SideMenu;
import com.thinksmart.app4onesdk.engine.application.utils.NavigationUtil;
import com.thinksmart.app4onesdk.engine.utils.preferencesManager.imp.PreferencesManagerImp;
import com.thinksmart.app4onesdk.engine.views.activities.TabBarActivity;

import java.util.List;

/**
 * Created by roberto.demiguel on 17/10/2016.
 */
public class DrawerMenuAdapter extends RecyclerView.Adapter<DrawerMenuAdapter.SideMenuHolder> {

    private List<SideMenu> sideMenuList;
    private Context context;

    public DrawerMenuAdapter(Context context, List<SideMenu> productList) {
        this.sideMenuList = productList;
        this.context = context;
    }


    @Override
    public SideMenuHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.side_menu_item, parent, false);
        return new SideMenuHolder(view);
    }

    @Override
    public void onBindViewHolder(SideMenuHolder holder, int position) {
        final SideMenu sideMenu = sideMenuList.get(position);
        holder.bind(sideMenu, context);
    }

    @Override
    public int getItemCount() {
        return sideMenuList.size();
    }

    static class SideMenuHolder extends RecyclerView.ViewHolder {
        ImageView sideMenuIcon;
        TextView sideMenuName;
        TextView sideMenuAlert;

        SideMenuHolder(View itemView) {
            super(itemView);

            sideMenuName = (TextView) itemView.findViewById(R.id.menu_name);
            sideMenuIcon = (ImageView) itemView.findViewById(R.id.menu_icon);
            sideMenuAlert = (TextView) itemView.findViewById(R.id.menu_item_alert);
        }

        void bind(@NonNull final SideMenu sideMenu, Context context) {
            //name
            sideMenuName.setText(sideMenu.getResourceName(context));
            //icon
            sideMenuIcon.setBackgroundDrawable(sideMenu.getResourceIcon(context));

            if (sideMenu.getNameResource() == R.string.panel_notifications_icon) {
                int pendingNotifications = new PreferencesManagerImp(context).retrieveIntPreference(UserPreferences.PENDING_NOTIFICATIONS);
                if(pendingNotifications > 0){
                    sideMenuAlert.setText(String.valueOf(pendingNotifications));
                    sideMenuAlert.setVisibility(View.VISIBLE);
                } else {
                    sideMenuAlert.setVisibility(View.GONE);
                }
            }


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    navigateToCategorySectionView();
                }

                private void navigateToCategorySectionView() {
                    NavigationUtil.goToNextScreen(TabBarActivity.class.getName(), sideMenu.getEventName());
                }
            });
        }
    }

}
