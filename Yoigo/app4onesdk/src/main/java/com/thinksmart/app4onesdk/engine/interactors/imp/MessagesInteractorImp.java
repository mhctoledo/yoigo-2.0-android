package com.thinksmart.app4onesdk.engine.interactors.imp;

import android.support.annotation.NonNull;

import com.thinksmart.app4onesdk.core.api.ApiParameters;
import com.thinksmart.app4onesdk.core.api.ApiResponse;
import com.thinksmart.app4onesdk.core.api.ApiUrls;
import com.thinksmart.app4onesdk.core.model.bos.MessageAddressesBO;
import com.thinksmart.app4onesdk.core.model.bos.MessagesParentBO;
import com.thinksmart.app4onesdk.core.vos.MessageVO;
import com.thinksmart.app4onesdk.engine.application.AppCore;
import com.thinksmart.app4onesdk.engine.interactors.IMessagesInteractor;
import com.thinksmart.app4onesdk.engine.interactors.IPresenterListener;
import com.thinksmart.app4onesdk.engine.net.IRequestCallback;
import com.thinksmart.app4onesdk.engine.net.ResponseMapperUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by roberto.demiguel on 17/10/2016.
 */
public class MessagesInteractorImp implements IMessagesInteractor {

    private IPresenterListener getMessagesListener;
    private IPresenterListener listAddressesListener;
    private IPresenterListener sendMessageListener;

    public MessagesInteractorImp(IPresenterListener getMessagesListener) {
        this.getMessagesListener = getMessagesListener;
        this.listAddressesListener = null;
        this.sendMessageListener = null;
    }

    public MessagesInteractorImp(IPresenterListener listAddressesListener, IPresenterListener sendMessageListener) {
        this.getMessagesListener = null;
        this.listAddressesListener = listAddressesListener;
        this.sendMessageListener = sendMessageListener;
    }

    public MessagesInteractorImp(IPresenterListener getMessagesListener, IPresenterListener listAddressesListener, IPresenterListener sendMessageListener) {
        this.getMessagesListener = getMessagesListener;
        this.listAddressesListener = listAddressesListener;
        this.sendMessageListener = sendMessageListener;
    }

    @Override
    public void getMessages(String token, Integer participantId, Integer campaignId) {
        final JSONObject params = getJsonObject(token, participantId, campaignId);
        AppCore.getInstance().getNetworkManager().postRequest(params, ApiUrls.messagesURL, new IRequestCallback() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                try {
                    ResponseMapperUtil.parseResponse(response, getMessagesListener, MessagesParentBO.class);
                } catch (JSONException | IOException e) {
                    getMessagesListener.onError(ApiResponse.Code.ERROR_PARSING_RESPONSE, ApiResponse.ERROR_PARSING_RESPONSE);
                }
            }

            @Override
            public void onRequestError(String error) {
                getMessagesListener.onServerError(error);
            }
        });
    }

    @Override
    public void getAddresses(String token, Integer participantId, Integer campaignId) {
        final JSONObject params = getJsonObject(token, participantId, campaignId);
        AppCore.getInstance().getNetworkManager().postRequest(params, ApiUrls.devicesMessageURL, new IRequestCallback() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                try {
                    ResponseMapperUtil.parseResponse(response, listAddressesListener, MessageAddressesBO.class);
                } catch (JSONException | IOException e) {
                    listAddressesListener.onError(ApiResponse.Code.ERROR_PARSING_RESPONSE, ApiResponse.ERROR_PARSING_RESPONSE);
                }
            }

            @Override
            public void onRequestError(String error) {
                listAddressesListener.onServerError(error);
            }
        });
    }

    @Override
    public void sendMessage(MessageVO messageVO) {
        final JSONObject params = getJsonObject(messageVO);
        AppCore.getInstance().getNetworkManager().postRequest(params, ApiUrls.sendMessageURL, new IRequestCallback() {
            @Override
            public void onRequestSuccess(JSONObject response) {
                parseSendMessageResponse(response, sendMessageListener);
            }

            @Override
            public void onRequestError(String error) {
                sendMessageListener.onServerError(error);
            }
        });

    }

    private void parseSendMessageResponse(JSONObject response, final IPresenterListener listener) {
        try {
            JSONObject saved = (JSONObject) response.get(ApiResponse.SAVED);
            JSONObject responseTag = (JSONObject) saved.get(ApiResponse.RESPONSE);
            JSONObject meta = (JSONObject) responseTag.get(ApiResponse.META);
            int code = meta.getInt(ApiResponse.CODE);
            if (code == ApiResponse.Code.SUCCESS) {
                listener.onSuccess(null);
            } else {
                String error = meta.getString(ApiResponse.ERROR_DESCRIPTION);
                listener.onError(code, error);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @NonNull
    private JSONObject getJsonObject(String token, Integer participantId, Integer campaignId) {
        final JSONObject params = new JSONObject();
        try {
            params.put(ApiParameters.TOKEN, token);
            params.put(ApiParameters.PARTICIPANT_ID, participantId);
            params.put(ApiParameters.CAMPAIGN_ID, campaignId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    @NonNull
    private JSONObject getJsonObject(MessageVO messageVO) {
        final JSONObject params = new JSONObject();
        try {
            params.put(ApiParameters.TOKEN, messageVO.getToken());
            params.put(ApiParameters.CAMPAIGN_ID, messageVO.getCampaignId());
            params.put(ApiParameters.SENDER_ID, messageVO.getSenderId());
            params.put(ApiParameters.RECEIVER_ID, messageVO.getReceiverId());
            params.put(ApiParameters.MESSAGE, messageVO.getMessage());
            params.put(ApiParameters.DEVICE_ID, messageVO.getDeviceId());
            params.put(ApiParameters.CAMPAIGN_NAME, messageVO.getCampaignName());
            params.put(ApiParameters.SO, messageVO.getSo());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }
}
