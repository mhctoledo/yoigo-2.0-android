package com.thinksmart.app4onesdk.engine.views.fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.core.model.bos.MessageBO;
import com.thinksmart.app4onesdk.core.model.bos.MessagesParentBO;
import com.thinksmart.app4onesdk.engine.application.utils.NavigationUtil;
import com.thinksmart.app4onesdk.engine.presenters.IMessagesPresenter;
import com.thinksmart.app4onesdk.engine.presenters.imp.MessagesPresenterImp;
import com.thinksmart.app4onesdk.engine.utils.AlertDialogUtil;
import com.thinksmart.app4onesdk.engine.views.IMessagesView;
import com.thinksmart.app4onesdk.engine.views.activities.MessagesActivity;
import com.thinksmart.app4onesdk.engine.views.adapters.MessagesFragmentAdapter;
import com.thinksmart.app4onesdk.engine.views.utils.CustomDividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by roberto.demiguel on 10/11/2016.
 */
public class MessagesFragment extends Fragment implements IMessagesView {

    private ProgressDialog dialog;
    private List<MessageBO> messageList;
    private IMessagesPresenter presenter;
    private MessagesFragmentAdapter adapter;
    private RecyclerView messagesRecyclerView;
    private TextView emptyView;
    private FloatingActionButton createMessageButton;

    public MessagesFragment() {
        // Required empty public constructor
    }

    public static MessagesFragment newInstance() {
        return new MessagesFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new MessagesPresenterImp(this, getActivity());
        messageList = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_messages, container, false);
        dialog = new ProgressDialog(getActivity());
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        initUIReferences(root);
        initEvents();

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.getMessages();
    }

    private void initEvents() {
        createMessageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String eventDestination = "goToCreateMessageFromMessages";
                NavigationUtil.goToNextScreen(MessagesActivity.class.getName(), eventDestination);
            }
        });
    }


    private void initUIReferences(View root) {
        createMessageButton = (FloatingActionButton) root.findViewById(R.id.create_message_button);
        messagesRecyclerView = (RecyclerView) root.findViewById(R.id.messages_list);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        messagesRecyclerView.setLayoutManager(linearLayoutManager);
        //add list_divider
        //TODO when upgrade project to v25
        //        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(cartRecyclerView.getContext(), linearLayoutManager.getOrientation());
        CustomDividerItemDecoration dividerItemDecoration = new CustomDividerItemDecoration(getActivity(), linearLayoutManager.getOrientation());
        messagesRecyclerView.addItemDecoration(dividerItemDecoration);
        // view adapter
        adapter = new MessagesFragmentAdapter(getContext(), messageList);
        messagesRecyclerView.setAdapter(adapter);
        //empty view to show when no items
        emptyView = (TextView) root.findViewById(R.id.messages_empty_view);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public void showMessages(MessagesParentBO messages) {
        if (!messages.getMessagesList().isEmpty()) {
            messageList.addAll(messages.getMessagesList());
            adapter.notifyDataSetChanged();
        } else {
            messagesRecyclerView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void showProgress() {
        showProgressDialog();
    }


    @Override
    public void hideProgress() {
        dialog.dismiss();
    }


    @Override
    public void showRequestError(String error) {
        AlertDialogUtil.showAlertDialog(getActivity(), error);
    }

    @Override
    public void showServerError() {
        AlertDialogUtil.showAlertDialog(getActivity(), R.string.server_error);
    }

    @Override
    public void showOnExpiredTokenError() {
        AlertDialogUtil.showAlertDialogAndCloseSession(getActivity(), R.string.on_confluent_session_error);
    }

    private void showProgressDialog() {
        dialog.setMessage(getString(R.string.loading_dialog));
        dialog.show();
    }
}
