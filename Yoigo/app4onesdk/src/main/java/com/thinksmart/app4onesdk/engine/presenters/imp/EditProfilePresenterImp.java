package com.thinksmart.app4onesdk.engine.presenters.imp;

import android.content.Context;

import com.thinksmart.app4onesdk.core.model.bos.UserBO;
import com.thinksmart.app4onesdk.engine.application.AppCore;
import com.thinksmart.app4onesdk.engine.interactors.IEditProfileInteractor;
import com.thinksmart.app4onesdk.engine.interactors.IPresenterListener;
import com.thinksmart.app4onesdk.engine.interactors.imp.EditProfileInteractorImp;
import com.thinksmart.app4onesdk.engine.presenters.IEditProfilePresenter;
import com.thinksmart.app4onesdk.engine.views.IEditProfileView;
import com.thinksmart.app4onesdk.engine.views.utils.SessionManagerUtil;

/**
 * Created by roberto.demiguel on 03/11/2016.
 */
public class EditProfilePresenterImp implements IEditProfilePresenter, IPresenterListener {

    private IEditProfileInteractor interactor;
    private IEditProfileView view;
    private UserBO userToUpdate;
    private Context context;

    public EditProfilePresenterImp(IEditProfileView view, Context context) {
        this.view = view;
        this.context = context;
        this.interactor = new EditProfileInteractorImp(this);
    }

    @Override
    public void updateProfileData(UserBO transferUser) {
        if (view != null) {
            view.showProgress();
        }

        userToUpdate = transferUser;

        UserBO currentUser = AppCore.getInstance().getSessionManager().getCurrentUser();
        String token = currentUser.getToken();
        Integer participantId = currentUser.getSelectedCampaign().getParticipantId();
        Integer campaignId = currentUser.getSelectedCampaign().getId();
        interactor.updateProfileData(token, participantId, campaignId, transferUser);
    }


    @Override
    public void onDestroy() {
        view = null;
    }

    @Override
    public void onSuccess(Object object) {
        //gets and save new user with new password and new token
        updateCurrentUserData(userToUpdate);

        if (view != null) {
            view.hideProgress();
            view.onDataSuccessfullyUpdated();
        }
    }

    private void updateCurrentUserData(UserBO user) {
        UserBO currentUser = AppCore.getInstance().getSessionManager().getCurrentUser();
        currentUser.setEmail(user.getEmail());
        currentUser.setNif(user.getNif());
        currentUser.setNameShort(user.getNameShort());
        currentUser.setLastName(user.getLastName());
        currentUser.setMobile(user.getMobile());
        currentUser.setName(user.getNameShort() + " " + user.getLastName());
        AppCore.getInstance().getSessionManager().setCurrentUser(currentUser);
    }

    @Override
    public void onExpiredToken() {
        if (view != null) {
            view.hideProgress();
            view.showOnExpiredTokenError();
        }
    }

    @Override
    public void onError(Integer code, String error) {
        if (view != null) {
            view.hideProgress();
            view.showRequestError(error);
        }
    }

    @Override
    public void onServerError(String error) {
        if (view != null) {
            view.hideProgress();
            view.showServerError();
        }
    }
}
