package com.thinksmart.app4onesdk.engine.views.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.engine.application.AppCore;
import com.thinksmart.app4onesdk.engine.application.utils.NavigationUtil;
import com.thinksmart.app4onesdk.engine.views.activities.OrderOverviewActivity;
import com.thinksmart.app4onesdk.engine.views.utils.StringUtils;

/**
 * Created by roberto.demiguel on 31/10/2016.
 */
public class OrderOverviewFragment extends Fragment {

    private RelativeLayout goBackToCatalogButton;
    private TextView overviewPointsValue;
    private TextView overviewPointsLabel;

    public OrderOverviewFragment() {
        // Required empty public constructor
    }

    public static OrderOverviewFragment newInstance() {
        return new OrderOverviewFragment();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_order_overview, container, false);

        initUIReferences(root);
        initEvents();
        populateData();

        return root;
    }

    private void initUIReferences(View root) {
        goBackToCatalogButton = (RelativeLayout) root.findViewById(R.id.overview_go_to_catalog_button);
        overviewPointsValue = (TextView) root.findViewById(R.id.overview_points_value);
        overviewPointsLabel = (TextView) root.findViewById(R.id.overview_points_label_2);
    }

    private void initEvents() {
        goBackToCatalogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String eventDestination = "goToHomeFromOrderOverview";
                NavigationUtil.goToNextScreen(OrderOverviewActivity.class.getName(), eventDestination);
            }
        });
    }

    private void populateData() {
        Integer points = AppCore.getInstance().getSessionManager().getCurrentUser().getAvailablePoints();
        String avaiblePoints = StringUtils.formatPointsWithoutUnits(points, getActivity());
        overviewPointsValue.setText(avaiblePoints);
        String pointAlias = StringUtils.getPointUnits(getContext());
        overviewPointsLabel.setText(pointAlias);
    }


}
