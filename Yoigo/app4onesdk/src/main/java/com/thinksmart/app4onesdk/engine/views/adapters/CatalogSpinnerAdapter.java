package com.thinksmart.app4onesdk.engine.views.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.TextView;

import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.core.model.enums.FilterType;

/**
 * Created by roberto.demiguel on 07/11/2016.
 */
public class CatalogSpinnerAdapter extends ArrayAdapter<FilterType> {

    public CatalogSpinnerAdapter(Context context) {
        super(context, 0, FilterType.values());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView text = (TextView) convertView;
        if (text == null) {
            text = (TextView) LayoutInflater.from(getContext()).inflate(R.layout.catalog_spinner_item, parent, false);
        }
        FilterType item = getItem(position);
        if (item != null) {
            text.setText(item.getLabel(getContext()));
        }
        return text;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        CheckedTextView text = (CheckedTextView) convertView;
        if (text == null) {
            text = (CheckedTextView) LayoutInflater.from(getContext()).inflate(R.layout.catalog_spinner_dropdown, parent, false);
        }
        FilterType item = getItem(position);
        if (item != null) {
            text.setText(item.getLabel(getContext()));
        }
        return text;
    }
}
