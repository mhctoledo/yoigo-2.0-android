package com.thinksmart.app4onesdk.engine.presenters;

import com.thinksmart.app4onesdk.core.vos.MessageVO;

/**
 * Created by roberto.demiguel on 14/11/2016.
 */
public interface ICreateMessagePresenter {

    /**
     * Call interector to get addresses and display them in view.
     */
    void getAddresses();

    /**
     * Call interector to send message and display its result in view.
     *
     * @param messageVO - value object to send
     */
    void sendMessage(MessageVO messageVO);

    /**
     * Destroy view
     */
    void onDestroy();
}
