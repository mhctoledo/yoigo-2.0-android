package com.thinksmart.app4onesdk.engine.views.fragments;

import android.support.v4.app.Fragment;

/**
 * Created by roberto.demiguel on 15/11/2016.
 */

public abstract class CustomFragment extends Fragment {

    public abstract CustomFragment newInstance();
}
