package com.thinksmart.app4onesdk.engine.views.utils;

import android.content.Context;

import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.engine.application.AppCore;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by roberto.demiguel on 21/10/2016.
 */

public class StringUtils {

    public static String formatString(String text) {
        return (text == null) ? "" : text;
    }

    public static String formatString(Long number) {
        return number == null ? null : String.valueOf(number);
    }

    public static String formatString(Integer number) {
        return number == null ? null : String.valueOf(number);
    }


    public static String formatPointsWithUnits(Integer productPoints, Context context) {
        Double productPointsDouble = null;
        if (productPoints != null) {
            productPointsDouble = Double.parseDouble(String.valueOf(productPoints));
        }
        return formatPointsWithUnits(productPointsDouble, context);
    }


    public static String formatPointsWithUnits(Double productPoints, Context context) {
        String points = "0";
        if (productPoints != null) {
            DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
            DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
            String separator = context.getString(R.string.default_points_separator);
            char separatorChar = '.';
            if (separator.length() > 0) {
                separatorChar = separator.charAt(0);
            }
            symbols.setGroupingSeparator(separatorChar);
            formatter.setDecimalFormatSymbols(symbols);
            points = formatter.format(productPoints);
        }
        String pointUnits = getPointUnits(context);
        return points + " " + pointUnits;
    }

    public static String getPointUnits(Context context) {
        String pointUnits = context.getString(R.string.default_points_suffix);
        String pointsAlias = AppCore.getInstance().getSessionManager().getCurrentUser().getSelectedCampaign().getPointsAlias();
        if (pointsAlias != null) {
            String[] pointAliasSplit = pointsAlias.split(" | ");
            if (pointAliasSplit.length > 0) {
                pointUnits = pointAliasSplit[0].toLowerCase();
            }
        }
        return pointUnits;
    }

    public static String getPointsTitle(Context context) {
        String result;
        if (AppCore.getInstance().getSessionManager().getCurrentUser().getLanguage().equals("eu")) {
            result = context.getString(R.string.points_title);
        } else {
            result = StringUtils.getPointUnits(context);
        }
        return result;
    }

    public static String changeFirstCharToUpperCase(String text) {
        String result;
        if (text.length() > 1) {
            String firstChar = text.substring(0, 1).toUpperCase();
            String restText = text.substring(1, text.length()).toLowerCase();
            result = firstChar + restText;
        } else {
            result = text;
        }
        return result;
    }


    public static String formatPointsWithoutUnits(Integer productPoints, Context context) {
        Double productPointsDouble = null;
        if (productPoints != null) {
            productPointsDouble = Double.parseDouble(String.valueOf(productPoints));
        }
        return formatPointsWithoutUnits(productPointsDouble, context);
    }


    public static String formatPointsWithoutUnits(Double productPoints, Context context) {
        String points = "0";
        if (productPoints != null) {
            DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
            DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
            String separator = context.getString(R.string.default_points_separator);
            char separatorChar = '.';
            if (separator.length() > 0) {
                separatorChar = separator.charAt(0);
            }
            symbols.setGroupingSeparator(separatorChar);
            formatter.setDecimalFormatSymbols(symbols);
            points = formatter.format(productPoints);
        }
        return points;
    }

    public static String formatQuantity(Integer productQuantity, Context context) {
        String suffix;
        if (productQuantity == 1) {
            suffix = context.getString(R.string.default_quantity_singular_suffix);
        } else {
            suffix = context.getString(R.string.default_quantity_plural_suffix);
        }
        return productQuantity + " " + suffix;
    }

    public static String formatProductNumber(Integer productNumber) {
        String result = "";
        if (productNumber != null) {
            result = "(" + String.valueOf(productNumber) + ")";
        }
        return result;
    }
}
