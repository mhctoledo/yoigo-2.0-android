package com.thinksmart.app4onesdk.engine.views.fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.core.model.bos.AddressBO;
import com.thinksmart.app4onesdk.core.model.bos.MessageAddressesBO;
import com.thinksmart.app4onesdk.core.vos.MessageVO;
import com.thinksmart.app4onesdk.engine.presenters.ICreateMessagePresenter;
import com.thinksmart.app4onesdk.engine.presenters.imp.CreateMessagePresenterImp;
import com.thinksmart.app4onesdk.engine.utils.AlertDialogUtil;
import com.thinksmart.app4onesdk.engine.views.ICreateMessageView;
import com.thinksmart.app4onesdk.engine.views.adapters.AddressesSpinnerAdapter;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

/**
 * Created by roberto.demiguel on 10/11/2016.
 */
public class CreateMessageFragment extends Fragment implements ICreateMessageView {

    private ProgressDialog dialog;
    private MessageAddressesBO messageAddresses;
    private ICreateMessagePresenter presenter;

    private AddressesSpinnerAdapter spinnerAdapter;
    private SearchableSpinner spinner;
    private EditText messageField;

    public CreateMessageFragment() {
        // Required empty public constructor
    }

    public static CreateMessageFragment newInstance() {
        return new CreateMessageFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        presenter = new CreateMessagePresenterImp(this, getActivity());
        messageAddresses = new MessageAddressesBO();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_create_message, container, false);
        dialog = new ProgressDialog(getActivity());
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        initUIReferences(root);
        presenter.getAddresses();

        return root;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.send_message_menu, menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_send) {
            sendMessage();
        }
        return super.onOptionsItemSelected(item);
    }

    private void sendMessage() {
        String message = messageField.getText().toString();
        AddressBO selectedAddress = (AddressBO) spinner.getSelectedItem();
        if (isMessageOk(message, selectedAddress)) {
            MessageVO messageVO = new MessageVO();

            messageVO.setMessage(message);
            messageVO.setSo(selectedAddress.getSo());
            messageVO.setDeviceId(selectedAddress.getDeviceAppId());
            messageVO.setReceiverId(selectedAddress.getParticipantId());
            if (messageAddresses.getCampaignName() != null) {
                messageVO.setCampaignName(messageAddresses.getCampaignName());
            }

            presenter.sendMessage(messageVO);
        }
    }

    private boolean isMessageOk(String message, AddressBO selectedAddress) {
        boolean ok = true;
        if (selectedAddress == null) {
            Toast.makeText(getActivity(), R.string.message_address_empty_error, Toast.LENGTH_LONG).show();
            ok = false;
        } else if (message.isEmpty()) {
            Toast.makeText(getActivity(), R.string.message_body_empty_error, Toast.LENGTH_LONG).show();
            ok = false;
        }
        return ok;
    }


    private void initUIReferences(View root) {
        messageField = (EditText) root.findViewById(R.id.cm_text_value);
        spinner = (SearchableSpinner) root.findViewById(R.id.cm_addresses_spinner);
        spinner.setPositiveButton(getString(R.string.address_spinner_close));
        spinner.setTitle(getString(R.string.address_spinner_title));
        spinnerAdapter = new AddressesSpinnerAdapter(getContext(), messageAddresses.getAddressList());
        spinner.setAdapter(spinnerAdapter);
    }


    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void showProgress() {
        showProgressDialog();
    }


    @Override
    public void hideProgress() {
        dialog.dismiss();
    }

    @Override
    public void onMessageSuccessfullySent() {
        Toast.makeText(getActivity(), R.string.message_successfully_sent, Toast.LENGTH_LONG).show();
        getActivity().finish();
    }

    @Override
    public void populateAddresses(MessageAddressesBO messageAddresses) {
        if (!messageAddresses.getAddressList().isEmpty()) {
            this.messageAddresses.getAddressList().addAll(messageAddresses.getAddressList());
            this.messageAddresses.setCampaignName(messageAddresses.getCampaignName());
            this.messageAddresses.setCampaignId(messageAddresses.getCampaignId());
            spinnerAdapter.notifyDataSetChanged();
        }
    }


    @Override
    public void showRequestError(String error) {
        AlertDialogUtil.showAlertDialog(getActivity(), error);
    }

    @Override
    public void showServerError() {
        AlertDialogUtil.showAlertDialog(getActivity(), R.string.server_error);
    }

    private void showProgressDialog() {
        dialog.setMessage(getString(R.string.loading_dialog));
        dialog.show();
    }

    @Override
    public void showOnExpiredTokenError() {
        AlertDialogUtil.showAlertDialogAndCloseSession(getActivity(), R.string.on_confluent_session_error);
    }
}
