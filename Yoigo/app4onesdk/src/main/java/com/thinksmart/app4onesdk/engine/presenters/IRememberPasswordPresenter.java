package com.thinksmart.app4onesdk.engine.presenters;

/**
 * Created by roberto.demiguel on 07/11/2016.
 */

public interface IRememberPasswordPresenter {

    /**
     * Request password reset
     *
     * @param user
     */
    void requestPasswordReset(String user);

    /**
     * Destroy view
     */
    void onDestroy();

}
