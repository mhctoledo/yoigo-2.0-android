package com.thinksmart.app4onesdk.engine.presenters;

/**
 * Created by roberto.demiguel on 26/10/2016.
 */
public interface IProductDetailsPresenter {

    /**
     * Call interector to get product and display it in view.
     *
     * @param productId
     * @param isSerie
     */
    void getProduct(String productId, String isSerie);

    /**
     * Destroy view
     */
    void onDestroy();

}
