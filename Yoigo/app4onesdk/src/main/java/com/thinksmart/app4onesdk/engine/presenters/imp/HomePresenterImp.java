package com.thinksmart.app4onesdk.engine.presenters.imp;

import android.content.Context;

import com.thinksmart.app4onesdk.core.model.bos.HomeBO;
import com.thinksmart.app4onesdk.core.model.bos.UserBO;
import com.thinksmart.app4onesdk.engine.application.AppCore;
import com.thinksmart.app4onesdk.engine.interactors.IHomeInteractor;
import com.thinksmart.app4onesdk.engine.interactors.IPresenterListener;
import com.thinksmart.app4onesdk.engine.interactors.imp.HomeInteractorImp;
import com.thinksmart.app4onesdk.engine.presenters.IHomePresenter;
import com.thinksmart.app4onesdk.engine.views.IHomeView;
import com.thinksmart.app4onesdk.engine.views.utils.SessionManagerUtil;

import java.util.Map;

/**
 * Created by roberto.demiguel on 17/10/2016.
 */
public class HomePresenterImp implements IHomePresenter, IPresenterListener<HomeBO> {

    private IHomeInteractor interactor;
    private IHomeView view;
    private Context context;


    public HomePresenterImp(IHomeView view, Context context) {
        this.view = view;
        this.interactor = new HomeInteractorImp(this);
        this.context = context;
    }

    @Override
    public void getBanners() {
        if (view != null) {
            view.showProgress();
        }
        UserBO currentUser = AppCore.getInstance().getSessionManager().getCurrentUser();
        String token = currentUser.getToken();
        Integer participantId = currentUser.getSelectedCampaign().getParticipantId();
        Integer campaignId = currentUser.getSelectedCampaign().getId();
        Map<String, String> extraData = currentUser.getSelectedCampaign().getExtraData();

        interactor.getBanners(token, participantId, campaignId, extraData);
    }

    @Override
    public void onDestroy() {
        view = null;
    }

    @Override
    public void onSuccess(HomeBO homeBO) {
        //Set some attributes on singleton instance after successful request
        updateCurrentUserData(homeBO);

        if (view != null) {
            view.hideProgress();
            view.showBanners(homeBO.getSliderList());
            view.setProfileInfo();
        }
    }

    @Override
    public void onExpiredToken() {
        if (view != null) {
            view.hideProgress();
            view.showOnExpiredTokenError();
        }
    }

    private void updateCurrentUserData(HomeBO homeBO) {
        AppCore.getInstance().getSessionManager().getCurrentUser().setAvailablePoints(homeBO.getAvailablePoints());
        AppCore.getInstance().getSessionManager().getCurrentUser().setName(homeBO.getName());
        AppCore.getInstance().getSessionManager().getCurrentUser().setPosition(homeBO.getPosition());
        AppCore.getInstance().getSessionManager().getCurrentUser().setNameShort(homeBO.getNameShort());
        AppCore.getInstance().getSessionManager().getCurrentUser().setLastName(homeBO.getLastName());
        AppCore.getInstance().getSessionManager().getCurrentUser().setEmail(homeBO.getEmail());
        AppCore.getInstance().getSessionManager().getCurrentUser().setNif(homeBO.getNif());
        AppCore.getInstance().getSessionManager().getCurrentUser().setMobile(homeBO.getMobile());
        AppCore.getInstance().getSessionManager().getCurrentUser().setProfileId(homeBO.getProfileId());
    }

    @Override
    public void onError(Integer code, String error) {
        if (view != null) {
            view.hideProgress();
            view.showRequestError(error);
        }
    }

    @Override
    public void onServerError(String error) {
        if (view != null) {
            view.hideProgress();
            view.showServerError();
        }
    }
}
