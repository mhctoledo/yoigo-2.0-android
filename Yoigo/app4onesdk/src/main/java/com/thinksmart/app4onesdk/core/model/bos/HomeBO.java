package com.thinksmart.app4onesdk.core.model.bos;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

/**
 * Created by roberto.demiguel on 17/10/2016.
 */
public class HomeBO implements Serializable {

    @JsonProperty("puntos_disponibles")
    private Integer availablePoints;
    @JsonProperty("nombre")
    private String name;
    @JsonProperty("ds_cargo")
    private String position;
    @JsonProperty("name")
    private String nameShort;
    @JsonProperty("last_name")
    private String lastName;
    @JsonProperty("email")
    private String email;
    @JsonProperty("nif")
    private String nif;
    @JsonProperty("mobile_number")
    private String mobile;
    @JsonProperty("id_perfil")
    private Integer profileId;
    @JsonProperty("slider")
    private List<SliderBO> sliderList;

    public HomeBO() {
    }

    public List<SliderBO> getSliderList() {
        return sliderList;
    }

    public void setSliderList(List<SliderBO> sliderList) {
        this.sliderList = sliderList;
    }

    public Integer getAvailablePoints() {
        return availablePoints;
    }

    public void setAvailablePoints(Integer availablePoints) {
        this.availablePoints = availablePoints;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getNameShort() {
        return nameShort;
    }

    public void setNameShort(String nameShort) {
        this.nameShort = nameShort;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Integer getProfileId() {
        return profileId;
    }

    public void setProfileId(Integer profileId) {
        this.profileId = profileId;
    }
}
