package com.thinksmart.app4onesdk.core.api;

/**
 * Created by roberto.demiguel on 09/03/2017.
 */

public class ApiResponse {
    public static final String ERROR_DESCRIPTION = "errorDescription";
    public static final String CODE = "code";
    public static final String META = "meta";
    public static final String RESPONSE = "response";
    public static final String SAVED = "saved";
    public static final String DATA = "data";
    public static final String ERROR_PARSING_RESPONSE = "Error parsing response from server";
    public static final String ERROR_NO_META_FOUND= "No 'meta' found on response.";


    public static class Code {
        public static final int SUCCESS = 200;
        public static final int HALCASH_UNACTIVE = 501;
        public static final int ERROR_PARSING_RESPONSE = 0;
        public static final int ERROR_NO_META_FOUND = 1;
        public static final int NON_EXISTING_USER = 11;
        public static final int EXPIRED_TOKEN = 6;
    }

}
