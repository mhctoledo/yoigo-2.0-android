package com.thinksmart.app4onesdk.engine.views.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.core.model.bos.PointBO;
import com.thinksmart.app4onesdk.engine.views.fragments.PointsDetailFragment;
import com.thinksmart.app4onesdk.engine.views.utils.StringUtils;

import java.util.List;

/**
 * Created by roberto.demiguel on 22/10/2016.
 */
public class PointsDetailActivity extends CustomActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_points_detail);

        initToolbar();

        Intent intent = getIntent();
        List<PointBO> paramPoints = (List<PointBO>) intent.getSerializableExtra("params");
        if (paramPoints != null && !paramPoints.isEmpty()) {

            PointsDetailFragment fragment = (PointsDetailFragment) getSupportFragmentManager().findFragmentById(R.id.points_detail_container);
            if (fragment == null) {
                fragment = PointsDetailFragment.newInstance(paramPoints.get(0));
                getSupportFragmentManager().beginTransaction().add(R.id.points_detail_container, fragment).commit();
            }
        }
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_with_icon);
        toolbar.setTitle(String.format(getString(R.string.points_detail_title), StringUtils.getPointsTitle(getApplicationContext())));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
