package com.thinksmart.app4onesdk.core.model.bos;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by roberto.demiguel on 13/10/2016.
 */
public class BannerBO implements Serializable {

    private Integer id;
    @JsonProperty("logo")
    private String logo;
    @JsonProperty("search")
    private String search;
    @JsonProperty("id_grupo")
    private Integer groupId;
    @JsonProperty("id_seccion")
    private Integer sectionId;
    @JsonProperty("id_subseccion")
    private Integer subSectionId;

    public BannerBO() {
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public Integer getSectionId() {
        return sectionId;
    }

    public void setSectionId(Integer sectionId) {
        this.sectionId = sectionId;
    }

    public Integer getSubSectionId() {
        return subSectionId;
    }

    public void setSubSectionId(Integer subSectionId) {
        this.subSectionId = subSectionId;
    }
}
