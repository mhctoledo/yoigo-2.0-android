package com.thinksmart.app4onesdk.engine.utils;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.RemoteMessage;
import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.core.model.constants.UserPreferences;
import com.thinksmart.app4onesdk.engine.utils.preferencesManager.IPreferencesManager;
import com.thinksmart.app4onesdk.engine.utils.preferencesManager.imp.PreferencesManagerImp;

import me.leolin.shortcutbadger.ShortcutBadger;

/**
 * Created by roberto.demiguel on 29/05/2017.
 */

public class PushNotificationAction {

    private static final String TAG = "PushNListenerService";

    private Context context;
    private Intent resultIntent;

    public PushNotificationAction(Context context, Intent resultIntent) {
        this.context = context;
        this.resultIntent = resultIntent;
    }

    public void onMessageReceived(RemoteMessage remoteMessage) {
        int pendingNotifications = setPendingNotificationsAlert();
        ShortcutBadger.applyCount(context, pendingNotifications);
        createNotification(remoteMessage, pendingNotifications);
    }

    private int setPendingNotificationsAlert() {
        IPreferencesManager preferencesManager = new PreferencesManagerImp(context);
        int pendingNotifications = preferencesManager.retrieveIntPreference(UserPreferences.PENDING_NOTIFICATIONS);
        preferencesManager.saveIntPreference(UserPreferences.PENDING_NOTIFICATIONS, ++pendingNotifications);
        return pendingNotifications;
    }

    private void createNotification(RemoteMessage remoteMessage, int pendingNotifications) {
        Notification notification = getNotification(remoteMessage);
        Log.d(TAG, notification.getBody());
        showNotification(pendingNotifications, notification);
    }

    private Notification getNotification(RemoteMessage remoteMessage) {
        Notification notification = new Notification();
        notification.setBody(getNotificationBody(remoteMessage));
        notification.setTitle(getNotificationTitle(remoteMessage));

        return notification;
    }

    private String getNotificationBody(RemoteMessage remoteMessage) {
        String notificationText = "";
        if (remoteMessage.getData() != null && remoteMessage.getData().containsKey("body")) {
            notificationText = remoteMessage.getData().get("body");
        } else if (remoteMessage.getNotification() != null && remoteMessage.getNotification().getBody() != null) {
            notificationText = remoteMessage.getNotification().getBody();
        }
        return notificationText;
    }

    private String getNotificationTitle(RemoteMessage remoteMessage) {
        String notificationTitle;
        if (remoteMessage.getData() != null && remoteMessage.getData().containsKey("title")) {
            notificationTitle = remoteMessage.getData().get("title");
        }else{
            notificationTitle = context.getString(R.string.app_name);
        }
        return notificationTitle;
    }

    private void showNotification(int pendingNotifications, Notification notification) {
        //Building a Notification
        NotificationCompat.Builder mBuilder =
                (NotificationCompat.Builder) new NotificationCompat.Builder(context)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setAutoCancel(true)
                        .setContentTitle(notification.getTitle())
                        .setContentText(notification.getBody());


        PendingIntent resultPendingIntent = PendingIntent.getActivity(context, pendingNotifications, resultIntent, PendingIntent.FLAG_ONE_SHOT);
        mBuilder.setContentIntent(resultPendingIntent);

        // Gets an instance of the NotificationManager service
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        // Builds the notification and issues it.
        notificationManager.notify(pendingNotifications, mBuilder.build());
    }
}
