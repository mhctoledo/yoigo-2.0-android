package com.thinksmart.app4onesdk.engine.views.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.thinksmart.app4onesdk.engine.application.AppCore;
import com.thinksmart.app4onesdk.engine.application.appConfigModel.screen.Screen;
import com.thinksmart.app4onesdk.engine.application.appConfigModel.screen.TabBarScreen;
import com.thinksmart.app4onesdk.engine.application.utils.MakerManager;


/**
 * Created by david.lopez on 22/1/16.
 */
public class TabBarFragmentPagerAdapter extends FragmentStatePagerAdapter {
    private int mNumOfTabs;
    private TabBarScreen screen;
    private SparseArray<Fragment> registeredFragments = new SparseArray<>();


    public TabBarFragmentPagerAdapter(FragmentManager fm, int NumOfTabs, TabBarScreen screen) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
        this.screen = screen;
    }

    @Override
    public Fragment getItem(int position) {
        String screenName = screen.getTabs().get(position).getResourceName();
        Screen screenToGo = AppCore.getInstance().getAppConfig().getScreenFromName(screenName);

        return MakerManager.createFragmentFromScreen(screenToGo);
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        registeredFragments.remove(position);
        super.destroyItem(container, position, object);
    }

    public Fragment getRegisteredFragment(int position) {
        return registeredFragments.get(position);
    }
}