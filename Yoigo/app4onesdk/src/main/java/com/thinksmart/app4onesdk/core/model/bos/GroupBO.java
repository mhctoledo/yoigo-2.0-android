package com.thinksmart.app4onesdk.core.model.bos;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

/**
 * Created by roberto.demiguel on 10/11/2016.
 */
public class GroupBO implements Serializable {

    @JsonProperty("nombre")
    private String name;
    @JsonProperty("id")
    private Integer id;
    @JsonProperty("ico")
    private Integer ico;
    @JsonProperty("Secciones")
    private List<SectionBO> sections;


    public GroupBO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getIco() {
        return ico;
    }

    public void setIco(Integer ico) {
        this.ico = ico;
    }

    public List<SectionBO> getSections() {
        return sections;
    }

    public void setSections(List<SectionBO> sections) {
        this.sections = sections;
    }
}
