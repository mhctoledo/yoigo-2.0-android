package com.thinksmart.app4onesdk.engine.interactors.imp;

import android.support.annotation.NonNull;

import com.thinksmart.app4onesdk.core.api.ApiParameters;
import com.thinksmart.app4onesdk.core.api.ApiResponse;
import com.thinksmart.app4onesdk.core.api.ApiUrls;
import com.thinksmart.app4onesdk.core.model.bos.UserBO;
import com.thinksmart.app4onesdk.engine.application.AppCore;
import com.thinksmart.app4onesdk.engine.interactors.IEditProfileInteractor;
import com.thinksmart.app4onesdk.engine.interactors.IPresenterListener;
import com.thinksmart.app4onesdk.engine.net.IRequestCallback;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by roberto.demiguel on 02/11/2016.
 */

public class EditProfileInteractorImp implements IEditProfileInteractor, IRequestCallback {

    private IPresenterListener presenterListener;

    public EditProfileInteractorImp(IPresenterListener presenterListener) {
        this.presenterListener = presenterListener;
    }

    @Override
    public void updateProfileData(String token, Integer participantId, Integer campaignId, UserBO transferUser) {
        final JSONObject params = getJsonObject(token, participantId, campaignId, transferUser);
        AppCore.getInstance().getNetworkManager().postRequest(params, ApiUrls.saveUserDataURL, this);
    }


    @Override
    public void onRequestSuccess(JSONObject response) {
        try {
            parseResponse(response, this.presenterListener);
        } catch (JSONException e) {
            this.presenterListener.onError(ApiResponse.Code.ERROR_PARSING_RESPONSE, ApiResponse.ERROR_PARSING_RESPONSE);
        }
    }

    @Override
    public void onRequestError(String error) {
        presenterListener.onServerError(error);
    }


    private void parseResponse(JSONObject response, final IPresenterListener listener) throws JSONException {
        JSONObject meta = (JSONObject) response.get(ApiResponse.META);
        int code = meta.getInt(ApiResponse.CODE);
        if (code == ApiResponse.Code.SUCCESS) {
            listener.onSuccess(null);
        } else {
            String error = meta.getString(ApiResponse.ERROR_DESCRIPTION);
            listener.onError(code, error);
        }
    }

    @NonNull
    private JSONObject getJsonObject(String token, Integer participantId, Integer campaignId, UserBO transferUser) {
        final JSONObject params = new JSONObject();
        try {
            params.put(ApiParameters.TOKEN, (token != null) ? token : "");
            params.put(ApiParameters.PARTICIPANT_ID, (participantId != null) ? participantId : "");
            params.put(ApiParameters.CAMPAIGN_ID, (campaignId != null) ? campaignId : "");
            params.put(ApiParameters.NAME, (transferUser.getNameShort() != null) ? transferUser.getNameShort() : "");
            params.put(ApiParameters.SURNAME, (transferUser.getLastName() != null) ? transferUser.getLastName() : "");
            params.put(ApiParameters.NIF, (transferUser.getNif() != null) ? transferUser.getNif() : "");
            params.put(ApiParameters.EMAIL, (transferUser.getEmail() != null) ? transferUser.getEmail() : "");
            params.put(ApiParameters.PHONE, (transferUser.getMobile() != null) ? transferUser.getMobile() : "");
            params.put(ApiParameters.DATA_ASSIGNMENT, 1);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

}
