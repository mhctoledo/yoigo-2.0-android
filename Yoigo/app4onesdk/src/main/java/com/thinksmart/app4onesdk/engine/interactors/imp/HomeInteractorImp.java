package com.thinksmart.app4onesdk.engine.interactors.imp;

import android.support.annotation.NonNull;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thinksmart.app4onesdk.core.api.ApiParameters;
import com.thinksmart.app4onesdk.core.api.ApiResponse;
import com.thinksmart.app4onesdk.core.api.ApiUrls;
import com.thinksmart.app4onesdk.core.model.bos.HomeBO;
import com.thinksmart.app4onesdk.engine.application.AppCore;
import com.thinksmart.app4onesdk.engine.interactors.IHomeInteractor;
import com.thinksmart.app4onesdk.engine.interactors.IPresenterListener;
import com.thinksmart.app4onesdk.engine.net.IRequestCallback;
import com.thinksmart.app4onesdk.engine.net.ResponseMapperUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Map;

/**
 * Created by roberto.demiguel on 17/10/2016.
 */
public class HomeInteractorImp implements IHomeInteractor, IRequestCallback {

    private IPresenterListener presenterListener;

    public HomeInteractorImp(IPresenterListener listener) {
        this.presenterListener = listener;
    }

    @Override
    public void getBanners(String token, Integer participantId, Integer campaignId, Map<String, String> extraData) {
        final JSONObject params = getJsonObject(token, participantId, campaignId, extraData);
        AppCore.getInstance().getNetworkManager().postRequest(params, ApiUrls.homeParametersURL, this);
    }

    @Override
    public void onRequestSuccess(JSONObject response) {
        try {
            ResponseMapperUtil.parseResponse(response, this.presenterListener, HomeBO.class);
        } catch (JSONException | IOException e) {
            this.presenterListener.onError(ApiResponse.Code.ERROR_PARSING_RESPONSE, ApiResponse.ERROR_PARSING_RESPONSE);
        }
    }

    @Override
    public void onRequestError(String error) {
        presenterListener.onServerError(error);
    }

    @NonNull
    private JSONObject getJsonObject(String token, Integer participantId, Integer campaignId, Map<String, String> extraData) {
        final JSONObject params = new JSONObject();
        try {
            params.put(ApiParameters.TOKEN, token);
            params.put(ApiParameters.PARTICIPANT_ID, participantId);
            params.put(ApiParameters.CAMPAIGN_ID, campaignId);
            try {
                ObjectMapper objectMapper = new ObjectMapper();
                String jsonExtraData = objectMapper.writeValueAsString(extraData);
                params.put(ApiParameters.EXTRA_DATA, jsonExtraData);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }
}
