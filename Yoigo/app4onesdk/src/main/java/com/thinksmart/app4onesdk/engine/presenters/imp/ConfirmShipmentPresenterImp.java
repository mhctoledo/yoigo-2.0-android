package com.thinksmart.app4onesdk.engine.presenters.imp;

import android.content.Context;

import com.thinksmart.app4onesdk.core.model.bos.BasketBO;
import com.thinksmart.app4onesdk.core.model.bos.ShipmentBO;
import com.thinksmart.app4onesdk.core.model.bos.UserBO;
import com.thinksmart.app4onesdk.engine.application.AppCore;
import com.thinksmart.app4onesdk.engine.interactors.IConfirmShipmentInteractor;
import com.thinksmart.app4onesdk.engine.interactors.IPresenterListener;
import com.thinksmart.app4onesdk.engine.interactors.imp.ConfirmShipmentInteractorImp;
import com.thinksmart.app4onesdk.engine.presenters.IConfirmShipmentPresenter;
import com.thinksmart.app4onesdk.engine.views.IConfirmShipmentView;
import com.thinksmart.app4onesdk.engine.views.utils.SessionManagerUtil;

/**
 * Created by roberto.demiguel on 02/11/2016.
 */

public class ConfirmShipmentPresenterImp implements IConfirmShipmentPresenter, IPresenterListener<BasketBO> {

    private IConfirmShipmentInteractor interactor;
    private IConfirmShipmentView view;
    private Context context;


    public ConfirmShipmentPresenterImp(IConfirmShipmentView view, Context context) {
        this.interactor = new ConfirmShipmentInteractorImp(this);
        this.view = view;
        this.context = context;
    }

    @Override
    public void getShipment() {
        UserBO user = AppCore.getInstance().getSessionManager().getCurrentUser();
        String token = user.getToken();
        Integer campaignId = user.getSelectedCampaign().getId();
        Integer participant = user.getSelectedCampaign().getParticipantId();
        interactor.getShipment(token, campaignId, participant);
    }

    @Override
    public void onDestroy() {
        view = null;
    }

    @Override
    public void onSuccess(BasketBO basket) {
        ShipmentBO shipment = basket.getShipment();
        AppCore.getInstance().getSessionManager().getCurrentBasket().setShipment(shipment);
        if (view != null) {
            view.hideProgress();
            view.showShipmentData(shipment);
        }
    }


    @Override
    public void onExpiredToken() {
        if (view != null) {
            view.hideProgress();
            view.showOnExpiredTokenError();
        }
    }

    @Override
    public void onError(Integer code, String error) {
        if (view != null) {
            view.hideProgress();
            view.showRequestError(error);
        }
    }

    @Override
    public void onServerError(String error) {
        if (view != null) {
            view.hideProgress();
            view.showServerError();
        }
    }
}
