package com.thinksmart.app4onesdk.engine.presenters.imp;

import android.content.Context;

import com.thinksmart.app4onesdk.core.model.bos.CategoriesBO;
import com.thinksmart.app4onesdk.engine.application.AppCore;
import com.thinksmart.app4onesdk.engine.interactors.ICategoriesInteractor;
import com.thinksmart.app4onesdk.engine.interactors.IPresenterListener;
import com.thinksmart.app4onesdk.engine.interactors.imp.CategoriesInteractorImp;
import com.thinksmart.app4onesdk.engine.presenters.ICategoriesPresenter;
import com.thinksmart.app4onesdk.engine.views.ICategoriesView;
import com.thinksmart.app4onesdk.engine.views.utils.SessionManagerUtil;

/**
 * Created by roberto.demiguel on 17/10/2016.
 */
public class CategoriesPresenterImp implements ICategoriesPresenter, IPresenterListener<CategoriesBO> {

    private ICategoriesInteractor interactor;
    private ICategoriesView view;
    private Context context;

    public CategoriesPresenterImp(ICategoriesView view, Context context) {
        this.view = view;
        this.interactor = new CategoriesInteractorImp(this);
        this.context = context;
    }

    @Override
    public void getCategories() {
        if (view != null) {
            view.showProgress();
        }
        String token = AppCore.getInstance().getSessionManager().getCurrentUser().getToken();
        Integer participantId = AppCore.getInstance().getSessionManager().getCurrentUser().getSelectedCampaign().getParticipantId();
        Integer campaignId = AppCore.getInstance().getSessionManager().getCurrentUser().getSelectedCampaign().getId();
        interactor.getCategories(token, participantId, campaignId);
    }

    @Override
    public void onDestroy() {
        view = null;
    }

    @Override
    public void onSuccess(CategoriesBO aoCategories) {

        if (view != null) {
            view.hideProgress();
            view.showCategories(aoCategories);
        }
    }

    @Override
    public void onExpiredToken() {
        if (view != null) {
            view.hideProgress();
            view.showOnExpiredTokenError();
        }
    }


    @Override
    public void onError(Integer code, String error) {
        if (view != null) {
            view.hideProgress();
            view.showRequestError(error);
        }
    }

    @Override
    public void onServerError(String error) {
        if (view != null) {
            view.hideProgress();
            view.showServerError();
        }
    }
}
