package com.thinksmart.app4onesdk.engine.views.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.core.model.bos.BasketBO;
import com.thinksmart.app4onesdk.core.model.bos.BasketItemBO;
import com.thinksmart.app4onesdk.engine.application.utils.NavigationUtil;
import com.thinksmart.app4onesdk.engine.views.ICartView;
import com.thinksmart.app4onesdk.engine.views.activities.CartActivity;
import com.thinksmart.app4onesdk.engine.views.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

import static com.thinksmart.app4onesdk.core.model.bos.BasketBO.MAX_AMOUNT_PER_PRODUCT;


/**
 * Created by roberto.demiguel on 17/10/2016.
 */
public class CartFragmentAdapter extends RecyclerView.Adapter {

    private BasketBO basket;
    private ICartView view;
    private Context context;

    public CartFragmentAdapter(ICartView view, Context context, BasketBO basket) {
        this.view = view;
        this.basket = basket;
        this.context = context;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_cart_item, parent, false);
        return new CartItemHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final CartItemHolder viewHolder = (CartItemHolder) holder;

        final BasketItemBO basketItem = basket.getBasketAsList().get(position);
        if (basketItem != null) {
            viewHolder.bind(basketItem, context, basket, view);
        }
    }

    @Override
    public int getItemCount() {
        return basket.getItems().size();
    }


    public void remove(int position) {
        BasketItemBO basketItem = basket.getBasketAsList().get(position);
        basket.removeItem(context, basketItem);
        //update main view
        view.updateFooterInfo();
        Toast.makeText(context, R.string.cart_item_successfully_removed, Toast.LENGTH_SHORT).show();
        notifyItemRemoved(position);
    }


    private static class CartItemHolder extends RecyclerView.ViewHolder {
        private TextView nameTextView;
        private TextView brandTextView;
        private TextView pointsTextView;
        private TextView quantityTextView;

        private ImageView imageView;
        private ImageView lessQuantity;
        private ImageView addQuantity;

        private TextView pointsPerUnit;
        private RelativeLayout colorSelectedLayout;
        private RelativeLayout extraSelectedLayout;
        private ImageView cartItemColor;
        private TextView extraSelectedLabel;
        private TextView extraSelectedValue;


        CartItemHolder(View itemView) {
            super(itemView);
            nameTextView = (TextView) itemView.findViewById(R.id.cart_product_name);
            brandTextView = (TextView) itemView.findViewById(R.id.cart_product_brand);
            pointsTextView = (TextView) itemView.findViewById(R.id.cart_product_points);
            quantityTextView = (TextView) itemView.findViewById(R.id.cart_quantity_value);
            imageView = (ImageView) itemView.findViewById(R.id.cart_product_image);
            lessQuantity = (ImageView) itemView.findViewById(R.id.cart_remove_quantity);
            addQuantity = (ImageView) itemView.findViewById(R.id.cart_add_quantity);
            colorSelectedLayout = (RelativeLayout) itemView.findViewById(R.id.color_selected_layout);
            extraSelectedLayout = (RelativeLayout) itemView.findViewById(R.id.extra_selected_layout);
            cartItemColor = (ImageView) itemView.findViewById(R.id.cart_item_color);
            extraSelectedLabel = (TextView) itemView.findViewById(R.id.extra_selected_label);
            extraSelectedValue = (TextView) itemView.findViewById(R.id.extra_selected_value);
            pointsPerUnit = (TextView) itemView.findViewById(R.id.points_per_unit_value);

        }

        void bind(@NonNull final BasketItemBO basketItem, final Context context, final BasketBO basket, final ICartView view) {
            populateData(basketItem, context);

            initEvents(basketItem, context, basket, view);

        }

        private void initEvents(@NonNull final BasketItemBO basketItem, final Context context, final BasketBO basket, final ICartView view) {

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    List<String> listParams = new ArrayList<>();
                    listParams.add(basketItem.getProductParentId());
                    listParams.add(basketItem.getSerie().toString());
                    listParams.add(basketItem.getColor());
                    listParams.add(basketItem.getExtraValue());

                    final String eventDestination = "goToDetailFromCart";
                    NavigationUtil.goToNextScreen(CartActivity.class.getName(), eventDestination, listParams);
                }
            });

            addQuantity.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //TODO replace with stock
                    Integer currrentUnits = basketItem.getUnits();
//                    if(currrentUnits < product.getStock()){
                    if (currrentUnits < MAX_AMOUNT_PER_PRODUCT) {
                        basket.addItemUnit(context, basketItem);

                        //update item view
                        currrentUnits++;
                        Double points = currrentUnits * basketItem.getPoints();
                        quantityTextView.setText(String.valueOf(currrentUnits));
                        pointsTextView.setText(StringUtils.formatPointsWithUnits(points, context));
                        //update main view
                        view.updateFooterInfo();

//                        addQuantity(currrentUnits, basket, context, basketItem, view);
                    }
                }
            });

            lessQuantity.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Integer currrentUnits = basketItem.getUnits();
                    if (currrentUnits > 1) {
                        basket.removeItemUnit(context, basketItem);

                        //update item view
                        currrentUnits--;
                        Double points = currrentUnits * basketItem.getPoints();
                        quantityTextView.setText(String.valueOf(currrentUnits));
                        pointsTextView.setText(StringUtils.formatPointsWithUnits(points, context));
                        //update main view
                        view.updateFooterInfo();
//                        lessQuantity(currrentUnits, basket, context, basketItem, view);
                    } else {
                        showDeleteInfoPopup(context);
                    }
                }
            });
        }

        private void lessQuantity(Integer currrentUnits, BasketBO basket, Context context, @NonNull BasketItemBO basketItem, ICartView view) {
            basket.removeItemUnit(context, basketItem);

            //update item view
            currrentUnits--;
            Double points = currrentUnits * basketItem.getPoints();
            quantityTextView.setText(String.valueOf(currrentUnits));
            pointsTextView.setText(StringUtils.formatPointsWithUnits(points, context));
            //update main view
            view.updateFooterInfo();
        }

        private void addQuantity(Integer currrentUnits, BasketBO basket, Context context, @NonNull BasketItemBO basketItem, ICartView view) {
            basket.addItemUnit(context, basketItem);

            //update item view
            currrentUnits++;
            Double points = currrentUnits * basketItem.getPoints();
            quantityTextView.setText(String.valueOf(currrentUnits));
            pointsTextView.setText(StringUtils.formatPointsWithUnits(points, context));
            //update main view
            view.updateFooterInfo();
        }


        private void showDeleteInfoPopup(Context context) {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
            dialogBuilder.setTitle(R.string.info);
            dialogBuilder.setMessage(R.string.delete_cart_item_info);

            dialogBuilder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            dialogBuilder.create().show();
        }

        private void populateData(@NonNull BasketItemBO basketItem, Context context) {
            //name
            String name = Html.fromHtml(Html.fromHtml(basketItem.getName()).toString()).toString();
            nameTextView.setText(name);
            //brand
            brandTextView.setText(basketItem.getBrand());
            //quantity
            quantityTextView.setText(String.valueOf(basketItem.getUnits()));
            //points
            Double points = basketItem.getUnits() * basketItem.getPoints();
            pointsTextView.setText(StringUtils.formatPointsWithUnits(points, context));
            //points per unit
            pointsPerUnit.setText(StringUtils.formatPointsWithoutUnits(basketItem.getPoints(), context));
            //color
            if (basketItem.getColor() != null) {
                colorSelectedLayout.setVisibility(View.VISIBLE);
                cartItemColor.setBackgroundColor(Color.parseColor(basketItem.getColor()));
            }
            //extra selector
            if (basketItem.getExtraTag() != null && basketItem.getExtraValue() != null) {
                extraSelectedLayout.setVisibility(View.VISIBLE);
                String selectedLabel = basketItem.getExtraTag() + ":";
                extraSelectedLabel.setText(selectedLabel);
                extraSelectedValue.setText(basketItem.getExtraValue());
            }

            //image
            String url = basketItem.getImageUrl().replaceAll(" ", "%20");
            Picasso.with(context).load(url).fit().centerInside().placeholder(R.drawable.progress_animation).into(imageView);
        }
    }


}