package com.thinksmart.app4onesdk.engine.views.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.core.model.bos.GroupBO;
import com.thinksmart.app4onesdk.core.model.enums.CategoryType;
import com.thinksmart.app4onesdk.engine.application.utils.NavigationUtil;
import com.thinksmart.app4onesdk.engine.views.activities.CategoriesActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by roberto.demiguel on 17/10/2016.
 */
public class CategoriesFragmentAdapter extends RecyclerView.Adapter<CategoriesFragmentAdapter.CategoryHolder> {

    private List<GroupBO> categoryList;
    private Context context;

    public CategoriesFragmentAdapter(Context context, List<GroupBO> productList) {
        this.categoryList = productList;
        this.context = context;
    }


    @Override
    public CategoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_group_item, parent, false);
        return new CategoryHolder(view);
    }

    @Override
    public void onBindViewHolder(CategoryHolder holder, int position) {
        final GroupBO category = categoryList.get(position);
        holder.bind(category, context);
    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    static class CategoryHolder extends RecyclerView.ViewHolder {
        RelativeLayout categoriesProductsPicture;
        RelativeLayout categoriesProductsBackground;
        TextView categoryName;
        TextView categoryDescription;

        CategoryHolder(View itemView) {
            super(itemView);

            categoriesProductsPicture = (RelativeLayout) itemView.findViewById(R.id.category_picture);
            categoriesProductsBackground = (RelativeLayout) itemView.findViewById(R.id.category_background);
            categoryName = (TextView) itemView.findViewById(R.id.category_name);
            categoryDescription = (TextView) itemView.findViewById(R.id.category_description);
        }

        void bind(@NonNull final GroupBO category, Context context) {
            //name
            String name = category.getName();
            categoryName.setText(name);

            CategoryType categoryType = CategoryType.getCategoryFromId(category.getId());
            if (categoryType != null) {
                //description
                categoryDescription.setText(categoryType.getDescription(context));
                //picture
                categoriesProductsPicture.setBackgroundDrawable(categoryType.getResourcePicture(context));
                //background
                categoriesProductsBackground.setBackgroundDrawable(categoryType.getResourceBackground(context));
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    navigateToCategorySectionView();
                }

                private void navigateToCategorySectionView() {
                    final String eventDestination = "goToCategorySectionsFromCategories";
                    List<GroupBO> categoryParam = new ArrayList<>();
                    categoryParam.add(category);
                    NavigationUtil.goToNextScreen(CategoriesActivity.class.getName(), eventDestination, categoryParam);
                }
            });
        }
    }

}
