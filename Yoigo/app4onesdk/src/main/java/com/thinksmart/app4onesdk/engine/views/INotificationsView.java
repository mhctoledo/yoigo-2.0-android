package com.thinksmart.app4onesdk.engine.views;

import com.thinksmart.app4onesdk.core.model.bos.NotificationsParentBO;

/**
 * Created by roberto.demiguel on 14/11/2016.
 */
public interface INotificationsView {

    /**
     * Shows progress dialog
     */
    void showProgress();

    /**
     * Hides progress dialog
     */
    void hideProgress();

    /**
     * Displays messages in view
     *
     * @param notifications - to show
     */
    void showNotifications(NotificationsParentBO notifications);

    /**
     * Shows error toast
     */
    void showRequestError(String error);

    /**
     * Shows server error toast
     */
    void showServerError();

    /**
     * Shows on confluent session error toast
     */
    void showOnExpiredTokenError();
}
