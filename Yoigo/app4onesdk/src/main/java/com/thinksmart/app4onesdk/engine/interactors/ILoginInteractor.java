package com.thinksmart.app4onesdk.engine.interactors;

import com.thinksmart.app4onesdk.core.vos.LoginVO;

/**
 * Created by roberto.demiguel on 13/10/2016.
 */

public interface ILoginInteractor {

    void login(LoginVO loginVO, IPresenterListener presenterListener);
}
