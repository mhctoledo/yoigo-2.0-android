package com.thinksmart.app4onesdk.engine.views;

/**
 * Created by roberto.demiguel on 13/10/2016.
 */
public interface IFinishPurchaseView {

    /**
     * Shows progress dialog
     */
    void showProgress();

    /**
     * Hides progress dialog
     */
    void hideProgress();

    /**
     * Shows error toast
     */
    void showRequestError(String error);

    /**
     * Shows server error toast
     */
    void showServerError();

    /**
     * Shows on confluent session error toast
     */
    void showOnExpiredTokenError();

    /**
     * Shows error toast
     */
    void showNotEnoughPointsToRedeem();

    /**
     * Ask user for its password before redemption
     */
    void askForPasswordBeforeRedeem();


    /**
     * Shows message and clean basket
     */
    void onSuccessfulRedemtion();
}
