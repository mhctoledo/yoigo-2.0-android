package com.thinksmart.app4onesdk.engine.views.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.engine.views.fragments.ProductDetailsFragment;

import java.util.ArrayList;

/**
 * Created by roberto.demiguel on 26/10/2016.
 */
public class ProductDetailsActivity extends CustomActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);

        initToolbar();

        Intent intent = getIntent();
        ArrayList<String> productParams = intent.getStringArrayListExtra("params");

        if (!productParams.isEmpty()) {
            ProductDetailsFragment fragment = (ProductDetailsFragment) getSupportFragmentManager().findFragmentById(R.id.product_details_container);
            if (fragment == null) {
                fragment = ProductDetailsFragment.newInstance(productParams);
                getSupportFragmentManager().beginTransaction().add(R.id.product_details_container, fragment).commit();
            }
        }
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.product_details_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
