package com.thinksmart.app4onesdk.engine.application.appConfigModel.screen;

import com.thinksmart.app4onesdk.core.model.enums.Tab;
import com.thinksmart.app4onesdk.engine.application.appConfigModel.Event;

import java.util.List;
import java.util.Map;

public class TabBarScreen extends Screen {

    private List<Tab> tabs;

    public TabBarScreen(String resource, Map<String, Event> events, List<Tab> tabs) {
        super(resource, events);
        this.tabs = tabs;
    }

    public List<Tab> getTabs() {
        return tabs;
    }

    public void setTabs(List<Tab> tabs) {
        this.tabs = tabs;
    }
}

