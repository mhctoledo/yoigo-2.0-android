package com.thinksmart.app4onesdk.engine.presenters;

/**
 * Created by roberto.demiguel on 04/11/2016.
 */
public interface IConfirmShipmentPresenter {

    /**
     * Call interector to obtain shipment
     */
    void getShipment();

    /**
     * Destroy view
     */
    void onDestroy();
}
