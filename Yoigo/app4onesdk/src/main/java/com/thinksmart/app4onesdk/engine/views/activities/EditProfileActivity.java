package com.thinksmart.app4onesdk.engine.views.activities;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.engine.views.fragments.EditProfileFragment;

/**
 * Created by roberto.demiguel on 18/11/2016.
 */
public class EditProfileActivity extends CustomActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        initToolbar();

        EditProfileFragment fragment = (EditProfileFragment) getSupportFragmentManager().findFragmentById(R.id.edit_profile_container);
        if (fragment == null) {
            fragment = EditProfileFragment.newInstance();
            getSupportFragmentManager().beginTransaction().add(R.id.edit_profile_container, fragment).commit();
        }
    }


    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.edit_profile_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
