package com.thinksmart.app4onesdk.engine.views;

/**
 * Created by roberto.demiguel on 17/10/2016.
 */
public interface ISelectProfileView {

    /**
     * Goes to Tabs view
     */
    void navigateToTabsView();

}
