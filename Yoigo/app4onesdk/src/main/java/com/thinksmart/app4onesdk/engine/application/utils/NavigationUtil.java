package com.thinksmart.app4onesdk.engine.application.utils;

import com.thinksmart.app4onesdk.engine.application.AppCore;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by roberto.demiguel on 07/12/2016.
 */

public class NavigationUtil {

    public static void goToNextScreen(String currentScreen, String eventDestination) {
        Map<String, Object> data = new HashMap<>();
        data.put("Screen", currentScreen);
        AppCore.getInstance().getObservingService().postNotification(eventDestination, data);
    }

    public static void goToNextScreen(String currentScreen, String eventDestination, Object extraParam) {
        Map<String, Object> data = new HashMap<>();
        data.put("Screen", currentScreen);
        data.put("params", extraParam);
        AppCore.getInstance().getObservingService().postNotification(eventDestination, data);
    }


}
