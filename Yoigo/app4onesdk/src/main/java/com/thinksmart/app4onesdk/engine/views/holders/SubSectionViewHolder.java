package com.thinksmart.app4onesdk.engine.views.holders;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ChildViewHolder;
import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.core.model.bos.SubSectionBO;
import com.thinksmart.app4onesdk.engine.views.fragments.CategorySectionsFragment;
import com.thinksmart.app4onesdk.engine.views.utils.StringUtils;

public class SubSectionViewHolder extends ChildViewHolder {

    private TextView subsectionName;
    private TextView subsectionProductNumber;

    public SubSectionViewHolder(@NonNull View itemView) {
        super(itemView);
        subsectionName = (TextView) itemView.findViewById(R.id.subsection_name);
        subsectionProductNumber = (TextView) itemView.findViewById(R.id.subsection_products_number);
    }

    public void bind(@NonNull final SubSectionBO subSection) {
        subsectionName.setText(subSection.getName());
        subsectionProductNumber.setText(StringUtils.formatProductNumber(subSection.getProductsNum()));

        this.itemView.setOnClickListener(new CategorySectionsFragment.CategorySectionsOnClickListener(subSection));
    }

}
