package com.thinksmart.app4onesdk.core.model.enums;

import android.content.Context;

import com.thinksmart.app4onesdk.R;

/**
 * Created by roberto.demiguel on 07/12/2016.
 */
public enum FilterType {

    ALL(R.string.catalog_spinner_all, ""), NEWNESS(R.string.catalog_spinner_news, "#novedades"), REACHABLE(R.string.catalog_spinner_reachable, "#alcance");

    private int resourceName;
    private String value;

    FilterType(int resourceName, String value) {
        this.resourceName = resourceName;
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public String getLabel(Context context) {
        return context.getResources().getString(this.resourceName);
    }
}
