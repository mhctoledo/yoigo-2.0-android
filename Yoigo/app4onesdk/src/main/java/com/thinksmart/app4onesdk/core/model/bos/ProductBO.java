package com.thinksmart.app4onesdk.core.model.bos;

import android.content.Context;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.thinksmart.app4onesdk.engine.views.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by roberto.demiguel on 13/10/2016.
 */
public class ProductBO {

    @JsonProperty("id")
    private String id;
    @JsonProperty("shortName")
    private String shortName;
    @JsonProperty("brand")
    private String brand;
    @JsonProperty("isSerie")
    private Boolean isSerie;
    @JsonProperty("idCatalog")
    private String idCatalog;
    @JsonProperty("image")
    private String imageUrl;
    @JsonProperty("img")
    private ArrayList<ProductImageBO> images;
    @JsonProperty("points")
    private Double points;
    @JsonProperty("stars")
    private Integer stars;
    @JsonProperty("stock")
    private Integer stock;
    @JsonProperty("new")
    private Boolean newProduct;
    @JsonProperty("lastUnits")
    private Integer lastUnits;
    @JsonProperty("description")
    private String prodDescription;
    @JsonProperty("value")
    private String value;
    @JsonProperty("idProduct")
    private String productId;
    @JsonProperty("name")
    private String name;
    @JsonProperty("selector_level_1")
    private List<ProductBO> firstLevelSelector;
    @JsonProperty("selector_level_2")
    private List<ProductBO> secondLevelSelector;

    public ProductBO() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Boolean getSerie() {
        return isSerie;
    }

    public void setSerie(Boolean serie) {
        isSerie = serie;
    }

    public String getIdCatalog() {
        return idCatalog;
    }

    public void setIdCatalog(String idCatalog) {
        this.idCatalog = idCatalog;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public ArrayList<ProductImageBO> getImages() {
        return images;
    }

    public void setImages(ArrayList<ProductImageBO> images) {
        this.images = images;
    }

    public Double getPoints() {
        return points;
    }

    public String getPointsFormatted(Context context) {
        return StringUtils.formatPointsWithUnits(this.points, context);
    }

    public void setPoints(Double points) {
        this.points = points;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStars() {
        return stars;
    }

    public void setStars(Integer stars) {
        this.stars = stars;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public Boolean getNewProduct() {
        return newProduct;
    }

    public void setNewProduct(Boolean newProduct) {
        this.newProduct = newProduct;
    }

    public Integer getLastUnits() {
        return lastUnits;
    }

    public void setLastUnits(Integer lastUnits) {
        this.lastUnits = lastUnits;
    }

    public String getProdDescription() {
        return prodDescription;
    }

    public void setProdDescription(String prodDescription) {
        this.prodDescription = prodDescription;
    }

    public List<ProductBO> getFirstLevelSelector() {
        return firstLevelSelector;
    }

    public void setFirstLevelSelector(List<ProductBO> firstLevelSelector) {
        this.firstLevelSelector = firstLevelSelector;
    }

    public List<ProductBO> getSecondLevelSelector() {
        return secondLevelSelector;
    }

    public void setSecondLevelSelector(List<ProductBO> secondLevelSelector) {
        this.secondLevelSelector = secondLevelSelector;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    @Override
    public String toString() {
        return "ProductBO{" +
                "id='" + id + '\'' +
                ", shortName='" + shortName + '\'' +
                ", brand='" + brand + '\'' +
                ", isSerie=" + isSerie +
                ", idCatalog='" + idCatalog + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", images=" + images +
                ", points=" + points +
                ", stars=" + stars +
                ", stock=" + stock +
                ", newProduct=" + newProduct +
                ", lastUnits=" + lastUnits +
                ", prodDescription='" + prodDescription + '\'' +
                ", value='" + value + '\'' +
                ", productId='" + productId + '\'' +
                ", name='" + name + '\'' +
                ", firstLevelSelector=" + firstLevelSelector +
                ", secondLevelSelector=" + secondLevelSelector +
                '}';
    }
}
