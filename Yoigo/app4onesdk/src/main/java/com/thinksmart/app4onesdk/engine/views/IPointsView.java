package com.thinksmart.app4onesdk.engine.views;

import com.thinksmart.app4onesdk.core.model.bos.ProfilePointsBO;

/**
 * Created by roberto.demiguel on 18/10/2016.
 */
public interface IPointsView {

    /**
     * Shows progress dialog
     */
    void showProgress();

    /**
     * Hides progress dialog
     */
    void hideProgress();

    /**
     * Displays points in view
     *
     * @param profilePoints to display
     */
    void showPoints(ProfilePointsBO profilePoints);


    /**
     * Shows error toast
     */
    void showRequestError(String error);

    /**
     * Shows server error toast
     */
    void showServerError();

    /**
     * Shows on confluent session error toast
     */
    void showOnExpiredTokenError();
}
