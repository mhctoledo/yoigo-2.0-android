package com.thinksmart.app4onesdk.engine.application.appConfigModel.screen;

import com.thinksmart.app4onesdk.engine.application.appConfigModel.Event;

import java.util.Map;

public class Screen {

    protected String resource;
    protected Map<String, Event> events;

    public Screen() {
    }

    public Screen(String resource, Map<String, Event> events) {
        this.resource = resource;
        this.events = events;
    }

    public Screen(String resource) {
        this.resource = resource;
        this.events = null;
    }

    public Event eventFromName(String name) {
        if (getEvents() != null) {

            return getEvents().get(name);
        }
        return null;
    }

    public String getResource() {
        return resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }

    public Map<String, Event> getEvents() {
        return events;
    }

    public void setEvents(Map<String, Event> events) {
        this.events = events;
    }

}

