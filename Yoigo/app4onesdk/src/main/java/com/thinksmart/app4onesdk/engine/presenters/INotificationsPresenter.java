package com.thinksmart.app4onesdk.engine.presenters;

/**
 * Created by roberto.demiguel on 14/11/2016.
 */
public interface INotificationsPresenter {

    /**
     * Call interector to get notifications and display them in view.
     */
    void getNotifications();

    /**
     * Destroy view
     */
    void onDestroy();
}
