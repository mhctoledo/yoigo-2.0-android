package com.thinksmart.app4onesdk.engine.interactors;

/**
 * Created by roberto.demiguel on 07/11/2016.
 */
public interface IRememberPasswordInteractor {

    /**
     * This method creates request parameters, and sends a post request to reset user password. Listener will receive its result.
     *
     * @param user
     */
    void resetPassword(String user);
}
