package com.thinksmart.app4onesdk.engine.presenters;

import com.thinksmart.app4onesdk.core.model.bos.UserBO;

/**
 * Created by roberto.demiguel on 03/11/2016.
 */

public interface IEditProfilePresenter {

    /**
     * Call interector to udpate profile data. Listener will receive its result.
     *
     * @param transferUser
     */
    void updateProfileData(UserBO transferUser);

    /**
     * Destroy view
     */
    void onDestroy();

}
