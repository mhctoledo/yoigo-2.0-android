package com.thinksmart.app4onesdk.engine.views.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.core.model.bos.MessageBO;

import java.util.List;

/**
 * Created by roberto.demiguel on 17/10/2016.
 */
public class MessagesFragmentAdapter extends RecyclerView.Adapter<MessagesFragmentAdapter.MessageHolder> {

    private List<MessageBO> messageList;
    private Context context;

    public MessagesFragmentAdapter(Context context, List<MessageBO> messageList) {
        this.messageList = messageList;
        this.context = context;
    }


    @Override
    public MessageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.message_list_item, parent, false);
        return new MessageHolder(view);
    }

    @Override
    public void onBindViewHolder(MessageHolder holder, int position) {
        final MessageBO message = messageList.get(position);
        holder.bind(message, context);
    }

    @Override
    public int getItemCount() {
        return messageList.size();
    }

    static class MessageHolder extends RecyclerView.ViewHolder {
        ImageView senderPhoto;
        TextView senderName;
        TextView messageBody;

        MessageHolder(View itemView) {
            super(itemView);

            senderPhoto = (ImageView) itemView.findViewById(R.id.sender_photo);
            senderName = (TextView) itemView.findViewById(R.id.sender_name);
            messageBody = (TextView) itemView.findViewById(R.id.message_body);
        }

        void bind(@NonNull final MessageBO message, Context context) {
            //sender photo
            Picasso.with(context).load(message.getSenderPhoto()).fit().centerInside().placeholder(R.drawable.progress_animation).into(senderPhoto);
            //sender name
            senderName.setText(message.getSenderName());
            //message
            messageBody.setText(message.getMessage());
        }
    }

}
