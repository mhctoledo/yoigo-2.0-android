package com.thinksmart.app4onesdk.engine.presenters;

/**
 * Created by roberto.demiguel on 13/10/2016.
 */

public interface ILoginPresenter {

    /**
     * Call interector to login
     *
     * @param user
     * @param password
     */
    void login(String user, String password);

    /**
     * Call interector to login if token is available
     */
    void tryLoginFromToken();

    /**
     * Destroy view
     */
    void onDestroy();
}
