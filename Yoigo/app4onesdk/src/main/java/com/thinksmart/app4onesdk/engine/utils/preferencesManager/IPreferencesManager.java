package com.thinksmart.app4onesdk.engine.utils.preferencesManager;

import java.util.Map;

/**
 * Created by roberto.demiguel on 14/10/2016.
 */

public interface IPreferencesManager {

    /**
     * Save string in Android SharedPreferences with name 'MyPreferences' and private context
     *
     * @param key   of value to save
     * @param value to save
     */
    void saveStringPreference(String key, String value);

    void saveIntPreference(String key, int value);


    /**
     * Save several string entries in Android SharedPreferences with name 'MyPreferences' and private context
     *
     * @param preferences - map of entries to save
     */
    void saveStringPreferences(Map<String, String> preferences);

    /**
     * Get string from Android SharedPreferences with name 'MyPreferences' and private context
     *
     * @param key of value to get
     * @return value, null otherwise
     */
    String retrieveStringPreference(String key);

    int retrieveIntPreference(String key);
}
