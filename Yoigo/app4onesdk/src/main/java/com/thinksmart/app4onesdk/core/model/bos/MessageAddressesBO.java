package com.thinksmart.app4onesdk.core.model.bos;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by roberto.demiguel on 17/10/2016.
 */
public class MessageAddressesBO {

    @JsonProperty("users")
    private List<AddressBO> addressList;
    @JsonProperty("Campaing")
    private String campaignName;
    @JsonProperty("id_campana")
    private Integer campaignId;

    public MessageAddressesBO() {
        addressList = new ArrayList<>();
    }

    public List<AddressBO> getAddressList() {
        return addressList;
    }

    public void setAddressList(List<AddressBO> addressList) {
        this.addressList = addressList;
    }

    public String getCampaignName() {
        return campaignName;
    }

    public void setCampaignName(String campaignName) {
        this.campaignName = campaignName;
    }

    public Integer getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(Integer campaignId) {
        this.campaignId = campaignId;
    }
}
