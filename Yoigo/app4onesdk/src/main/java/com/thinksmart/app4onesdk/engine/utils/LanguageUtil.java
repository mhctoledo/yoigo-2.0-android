package com.thinksmart.app4onesdk.engine.utils;

import android.content.Context;
import android.content.res.Configuration;

import java.util.Locale;

/**
 * Created by roberto.demiguel on 08/06/2017.
 */

public class LanguageUtil {

    @SuppressWarnings("deprecation")
    public static void setLanguage(Context context, String languageCode) {
        Locale locale = new Locale(languageCode);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        context.getResources().updateConfiguration(config, null);
    }

}
