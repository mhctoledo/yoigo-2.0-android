package com.thinksmart.app4onesdk.core.vos;

import java.io.Serializable;

/**
 * Created by roberto.demiguel on 14/11/2016.
 */
public class CatalogVO implements Serializable {

    private String token;
    private Integer participantId;
    private Integer campaignId;
    private String search;
    private Integer page;
    private Integer groupId;
    private Integer sectionId;
    private Integer subSectionId;

    public CatalogVO() {
        page = 1;
    }

    public CatalogVO(String search, Integer page) {
        this.search = search;
        this.page = page;
    }

    public CatalogVO(Integer subSectionId, Integer sectionId, Integer groupId) {
        this.subSectionId = subSectionId;
        this.sectionId = sectionId;
        this.groupId = groupId;
    }

    public CatalogVO(String search, Integer page, Integer groupId, Integer sectionId, Integer subSectionId) {
        this.search = search;
        this.page = page;
        this.groupId = groupId;
        this.sectionId = sectionId;
        this.subSectionId = subSectionId;
    }

    public void incrementPage() {
        if (this.page != null) {
            this.page++;
        }
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Integer getParticipantId() {
        return participantId;
    }

    public void setParticipantId(Integer participantId) {
        this.participantId = participantId;
    }

    public Integer getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(Integer campaignId) {
        this.campaignId = campaignId;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public Integer getSectionId() {
        return sectionId;
    }

    public void setSectionId(Integer sectionId) {
        this.sectionId = sectionId;
    }

    public Integer getSubSectionId() {
        return subSectionId;
    }

    public void setSubSectionId(Integer subSectionId) {
        this.subSectionId = subSectionId;
    }
}
