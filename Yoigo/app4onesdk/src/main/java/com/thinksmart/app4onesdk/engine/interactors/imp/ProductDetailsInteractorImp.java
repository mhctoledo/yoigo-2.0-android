package com.thinksmart.app4onesdk.engine.interactors.imp;

import android.support.annotation.NonNull;

import com.thinksmart.app4onesdk.core.api.ApiParameters;
import com.thinksmart.app4onesdk.core.api.ApiResponse;
import com.thinksmart.app4onesdk.core.api.ApiUrls;
import com.thinksmart.app4onesdk.core.model.bos.ProductBO;
import com.thinksmart.app4onesdk.engine.application.AppCore;
import com.thinksmart.app4onesdk.engine.interactors.IPresenterListener;
import com.thinksmart.app4onesdk.engine.interactors.IProductDetailsInteractor;
import com.thinksmart.app4onesdk.engine.net.IRequestCallback;
import com.thinksmart.app4onesdk.engine.net.ResponseMapperUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by roberto.demiguel on 26/10/2016.
 */
public class ProductDetailsInteractorImp implements IProductDetailsInteractor, IRequestCallback {

    private IPresenterListener presenterListener;

    public ProductDetailsInteractorImp(IPresenterListener listener) {
        this.presenterListener = listener;
    }

    @Override
    public void getProduct(String token, Integer participantId, Integer campaignId, String productId, Integer isSerie) {
        final JSONObject params = getJsonObject(token, participantId, campaignId, productId, isSerie);
        AppCore.getInstance().getNetworkManager().postRequest(params, ApiUrls.productDetailURL, this);
    }

    @Override
    public void onRequestSuccess(JSONObject response) {
        try {
            ResponseMapperUtil.parseResponse(response, this.presenterListener, ProductBO.class);
        } catch (JSONException | IOException e) {
            this.presenterListener.onError(ApiResponse.Code.ERROR_PARSING_RESPONSE, ApiResponse.ERROR_PARSING_RESPONSE);
        }
    }

    @Override
    public void onRequestError(String error) {
        presenterListener.onServerError(error);
    }

    @NonNull
    private JSONObject getJsonObject(String token, Integer participantId, Integer campaignId, String productId, Integer isSerie) {
        final JSONObject params = new JSONObject();
        try {
            params.put(ApiParameters.TOKEN, token);
            params.put(ApiParameters.PARTICIPANT_ID, participantId);
            params.put(ApiParameters.CAMPAIGN_ID, campaignId);
            params.put(ApiParameters.PRODUCT_ID, productId);
            params.put(ApiParameters.IS_SERIE, isSerie);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }
}
