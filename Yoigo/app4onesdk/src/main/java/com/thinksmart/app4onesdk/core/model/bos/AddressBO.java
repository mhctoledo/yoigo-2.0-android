package com.thinksmart.app4onesdk.core.model.bos;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by roberto.demiguel on 14/11/2016.
 */
public class AddressBO {

    @JsonProperty("id_participante")
    private Integer participantId;
    @JsonProperty("name")
    private String name;
    @JsonProperty("photo")
    private String photo;
    @JsonProperty("SO")
    private String so;
    @JsonProperty("DeviceAppId")
    private String deviceAppId;

    public AddressBO() {
    }

    public Integer getParticipantId() {
        return participantId;
    }

    public void setParticipantId(Integer participantId) {
        this.participantId = participantId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getSo() {
        return so;
    }

    public void setSo(String so) {
        this.so = so;
    }

    public String getDeviceAppId() {
        return deviceAppId;
    }

    public void setDeviceAppId(String deviceAppId) {
        this.deviceAppId = deviceAppId;
    }

    @Override
    public String toString() {
        return name;
    }
}
