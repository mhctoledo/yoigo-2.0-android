package com.thinksmart.app4onesdk.engine.application;

import android.util.Log;

import com.thinksmart.app4onesdk.engine.application.appConfigModel.Event;
import com.thinksmart.app4onesdk.engine.application.appConfigModel.screen.Screen;
import com.thinksmart.app4onesdk.engine.application.utils.CustomObservable;

import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by david.lopez on 20/1/16.
 */
public class EventManager implements Observer {

    final static private String TAG = "EventManager";

    //Constructor
    public EventManager() {
    }

    //Methods
    public void addObserver(Screen screen) {
        Log.d(TAG, "screen" + screen);
        if (screen.getEvents() != null) {
            for (Event event : screen.getEvents().values()) {
                AppCore.getInstance().getObservingService().addObserver(event.getName(), this);
            }
        }
    }

    public void removeObserver(Screen screen) {
        if (screen.getEvents() != null) {
            for (Event event : screen.getEvents().values()) {
                AppCore.getInstance().getObservingService().removeObserver(event.getName(), this);
            }
        }
    }

    @Override
    public void update(Observable observable, Object data) {
        String screenName = (String) ((Map<String, Object>) data).get("Screen");
        List<String> params = (List<String>) ((Map<String, Object>) data).get("params");

        CustomObservable customObservable = (CustomObservable) observable;
        String eventName = customObservable.getName();

        Log.d(TAG, "Screen name: " + screenName + ", Event Name: " + eventName);

        Event event = AppCore.getInstance().getAppConfig().eventOfScreen(screenName, eventName);
        Log.d(TAG, "Event: " + event);

        //Change Activity
        Screen screenToGo = AppCore.getInstance().getAppConfig().getScreenFromName(event.getDestination());
        if (screenToGo != null) {
            AppCore.getInstance().getNavigationManager().changeRootActivity(screenToGo, params);
        }
    }
}
