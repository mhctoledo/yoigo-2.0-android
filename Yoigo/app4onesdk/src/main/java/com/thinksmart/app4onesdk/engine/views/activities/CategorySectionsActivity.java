package com.thinksmart.app4onesdk.engine.views.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.core.model.bos.GroupBO;
import com.thinksmart.app4onesdk.engine.views.fragments.CategorySectionsFragment;

import java.util.List;

/**
 * Created by roberto.demiguel on 11/11/2016.
 */
public class CategorySectionsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_sections);


        Intent intent = getIntent();
        List<GroupBO> categoryParam = (List<GroupBO>) intent.getSerializableExtra("params");
        if (!categoryParam.isEmpty()) {
            GroupBO currentCategory = categoryParam.get(0);
            //init toolbar with category name
            initToolbar(currentCategory.getName());

            CategorySectionsFragment fragment = (CategorySectionsFragment) getSupportFragmentManager().findFragmentById(R.id.category_sections_container);
            if (fragment == null) {
                fragment = CategorySectionsFragment.newInstance(currentCategory);
                getSupportFragmentManager().beginTransaction().add(R.id.category_sections_container, fragment).commit();
            }
        }

    }


    private void initToolbar(String categoryName) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_with_icon);
        toolbar.setTitle(categoryName);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
