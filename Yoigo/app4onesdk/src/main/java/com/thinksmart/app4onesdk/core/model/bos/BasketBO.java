package com.thinksmart.app4onesdk.core.model.bos;

import android.content.Context;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.thinksmart.app4onesdk.core.model.util.BasketUtil;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by roberto.demiguel on 13/10/2016.
 */
public class BasketBO {

    public static int MAX_AMOUNT_PER_PRODUCT = 10;

    @JsonProperty("totalPoints")
    private Double totalPoints;
    @JsonProperty("quantity")
    private Integer quantity;
    @JsonProperty("shipment")
    private ShipmentBO shipment;
    @JsonProperty("items")
    private HashMap<String, BasketItemBO> items;

    public BasketBO(HashMap<String, BasketItemBO> items) {
        this.items = items;
        this.totalPoints = 0.0;
        this.quantity = 0;
        for (String key : items.keySet()) {

            BasketItemBO item = items.get(key);
            this.quantity += item.getUnits();
            this.totalPoints += item.getTotalPoints();
        }
    }

    public BasketBO() {
        this.items = new HashMap<>();
        this.totalPoints = 0.0;
        this.quantity = 0;
    }


    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Double getTotalPoints() {
        return totalPoints;
    }

    public void setTotalPoints(Double totalPoints) {
        this.totalPoints = totalPoints;
    }

    public void deductQuantity() {
        this.quantity--;
    }

    public void deductPoints(Double totalPoints) {
        this.totalPoints -= totalPoints;
    }

    public void addPoints(Double totalPoints) {
        this.totalPoints += totalPoints;
    }

    public void addQuantity() {
        this.quantity++;
    }

    public HashMap<String, BasketItemBO> getItems() {
        return items;
    }

    public void setItems(HashMap<String, BasketItemBO> items) {
        this.items = items;
    }

    public void addItem(Context context, BasketItemBO item) {
        this.items.put(item.getProductId(), item);
        this.quantity += item.getUnits();
        this.totalPoints += item.getTotalPoints();

        BasketUtil.saveBasket(context, this);
    }

    public void addItemUnit(Context context, BasketItemBO basketItem) {
        Integer currrentUnits = basketItem.getUnits() + 1;
        this.items.get(basketItem.getProductId()).setUnits(currrentUnits);
        addPoints(basketItem.getPoints());
        addQuantity();

        BasketUtil.saveBasket(context, this);

    }

    public void removeItemUnit(Context context, BasketItemBO basketItem) {
        Integer currrentUnits = basketItem.getUnits() - 1;
        this.items.get(basketItem.getProductId()).setUnits(currrentUnits);
        deductPoints(basketItem.getPoints());
        deductQuantity();

        BasketUtil.saveBasket(context, this);
    }


    public void removeItem(Context context, BasketItemBO item) {
        this.items.remove(item.getProductId());
        this.totalPoints -= item.getTotalPoints();
        this.quantity -= item.getUnits();

        BasketUtil.saveBasket(context, this);

    }

    public Boolean existProductInBasket(String productID) {
        return items.containsKey(productID);
    }

    public void emptyBasket(Context context) {
        this.items.clear();
        this.totalPoints = 0.0;
        this.quantity = 0;

        BasketUtil.saveBasket(context, this);
    }


    public ShipmentBO getShipment() {
        return shipment;
    }

    public void setShipment(ShipmentBO shipment) {
        this.shipment = shipment;
    }


    public ArrayList<BasketItemBO> getBasketAsList() {

        ArrayList<BasketItemBO> items = new ArrayList<>();

        for (String itemKey : this.items.keySet()) {
            BasketItemBO item = this.items.get(itemKey);
            items.add(item);
        }

        return items;
    }

    @Override
    public String toString() {
        return "BasketBO{" +
                "totalPoints=" + totalPoints +
                ", shipment=" + shipment +
                ", items=" + items +
                '}';
    }
}