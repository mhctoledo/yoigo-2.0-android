package com.thinksmart.app4onesdk.engine.views.activities;

import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.engine.views.fragments.SelectProfileFragment;

/**
 * Created by roberto.demiguel on 31/10/2016.
 */
public class SelectProfileActivity extends CustomActivity {

    private boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_profile);

        SelectProfileFragment fragment = (SelectProfileFragment) getSupportFragmentManager().findFragmentById(R.id.activity_select_profile);
        if (fragment == null) {
            fragment = SelectProfileFragment.newInstance();
            getSupportFragmentManager().beginTransaction().add(R.id.activity_select_profile, fragment).commit();
        }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
        } else {
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, getString(R.string.press_back_again), Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }
    }
}
