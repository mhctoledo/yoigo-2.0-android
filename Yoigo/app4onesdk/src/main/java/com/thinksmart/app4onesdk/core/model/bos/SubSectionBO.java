package com.thinksmart.app4onesdk.core.model.bos;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by roberto.demiguel on 10/11/2016.
 */
public class SubSectionBO implements Serializable {

    @JsonProperty("nombre")
    private String name;
    @JsonProperty("productos")
    private Integer productsNum;
    @JsonProperty("id")
    private Integer id;
    @JsonProperty("id_grupo")
    private Integer groupId;
    @JsonProperty("id_seccion")
    private Integer sectionId;

    public SubSectionBO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getProductsNum() {
        return productsNum;
    }

    public void setProductsNum(Integer productsNum) {
        this.productsNum = productsNum;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public Integer getSectionId() {
        return sectionId;
    }

    public void setSectionId(Integer sectionId) {
        this.sectionId = sectionId;
    }
}
