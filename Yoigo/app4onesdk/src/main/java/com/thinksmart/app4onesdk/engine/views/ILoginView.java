package com.thinksmart.app4onesdk.engine.views;

/**
 * Created by roberto.demiguel on 13/10/2016.
 */
public interface ILoginView {

    void setLastUsername();

    /**
     * Shows progress dialog
     */
    void showProgress();

    /**
     * Hides progress dialog
     */
    void hideProgress();

    /**
     * Shows credentials error toast
     */
    void showCredentialsError(String error);

    /**
     * Goes to Tabs view
     */
    void navigateToSelectProfileView();

    /**
     * Goes to Tabs view
     */
    void navigateToTabBarsView();

    /**
     * Goes to forgotten password view
     */
    void navigateToChangePasswordView();

    /**
     * Shows server error toast
     */
    void showServerError();

    /**
     * Shows on confluent session error toast
     */
    void showOnExpiredTokenError();

    void navigateToDataAssignment();

    void onTryLoginFromTokenFailed();

    void navigateToNextScreen();

    void askForFingerPrintAndNavigateToNextScreen();
}
