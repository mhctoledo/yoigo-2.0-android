package com.thinksmart.app4onesdk.engine.views.fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.core.model.bos.UserBO;
import com.thinksmart.app4onesdk.engine.application.AppCore;
import com.thinksmart.app4onesdk.engine.application.utils.NavigationUtil;
import com.thinksmart.app4onesdk.engine.presenters.IEditProfilePresenter;
import com.thinksmart.app4onesdk.engine.presenters.imp.EditProfilePresenterImp;
import com.thinksmart.app4onesdk.engine.utils.AlertDialogUtil;
import com.thinksmart.app4onesdk.engine.views.IEditProfileView;
import com.thinksmart.app4onesdk.engine.views.activities.DataAssignmentActivity;
import com.thinksmart.app4onesdk.engine.views.utils.StringUtils;
import com.thinksmart.app4onesdk.engine.views.utils.ViewUtils;

import de.hdodenhof.circleimageview.CircleImageView;

public class DataAssignmentFragment extends Fragment implements IEditProfileView {

    private ProgressDialog dialog;

    private IEditProfilePresenter presenter;
    private TextView nameField;
    private TextView surnameField;
    private TextView nifField;
    private TextView emailField;
    private TextView phoneField;
    private CircleImageView userImage;


    public DataAssignmentFragment() {
    }

    public static DataAssignmentFragment newInstance() {
        return new DataAssignmentFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        presenter = new EditProfilePresenterImp(this, getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_data_assignment, container, false);

        initUIReferences(root);
        populateInfo();

        return root;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.form_menu, menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_save) {
            sendData();
        }
        return super.onOptionsItemSelected(item);
    }


    private void initUIReferences(View view) {
        dialog = new ProgressDialog(getActivity());
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        userImage = (CircleImageView) view.findViewById(R.id.data_assignment_img_user);
        nameField = (TextView) view.findViewById(R.id.profile_name_value);
        surnameField = (TextView) view.findViewById(R.id.profile_surname_value);
        nifField = (TextView) view.findViewById(R.id.profile_nif_value);
        emailField = (TextView) view.findViewById(R.id.profile_mail_value);
        phoneField = (TextView) view.findViewById(R.id.profile_phone_value);
    }

    private void sendData() {
        if (areMandatoryFieldsFilled()) {
            ViewUtils.hideKeyboard(getView(), getActivity());
            UserBO transferObject = getUserFromView();
            presenter.updateProfileData(transferObject);
        }
    }


    private boolean areMandatoryFieldsFilled() {
        boolean correct = true;
        String name = nameField.getText().toString();
        String surname = surnameField.getText().toString();
        String email = emailField.getText().toString();
        String nif = nifField.getText().toString();
        String phone = phoneField.getText().toString();

        String fieldName = "";
        if (TextUtils.isEmpty(name)) {
            fieldName = getString(R.string.profile_name);
            correct = false;
        } else if (TextUtils.isEmpty(surname)) {
            fieldName = getString(R.string.profile_surname);
            correct = false;
        } else if (TextUtils.isEmpty(email)) {
            fieldName = getString(R.string.profile_email);
            correct = false;
        } else if (TextUtils.isEmpty(nif)) {
            fieldName = getString(R.string.profile_nif);
            correct = false;
        } else if (TextUtils.isEmpty(phone)) {
            fieldName = getString(R.string.profile_phone);
            correct = false;
        }

        if (!correct) {
            Toast.makeText(getActivity(), String.format(getString(R.string.shipment_fields_are_mandatory), fieldName), Toast.LENGTH_LONG).show();
        }

        return correct;
    }


    private void populateInfo() {
        UserBO user = AppCore.getInstance().getSessionManager().getCurrentUser();
        //image
        if (user.getUrlAvatar() != null && !user.getUrlAvatar().isEmpty()) {
            Picasso.with(getActivity()).load(user.getUrlAvatar()).placeholder(getResources().getDrawable(R.drawable.ico_select_user)).fit().centerInside().into(userImage);
        }
        nameField.setText(StringUtils.formatString(user.getNameShort()));
        surnameField.setText(StringUtils.formatString(user.getLastName()));
        nifField.setText(StringUtils.formatString(user.getNif()));
        emailField.setText(StringUtils.formatString(user.getEmail()));
        phoneField.setText(StringUtils.formatString(user.getMobile()));
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    @NonNull
    private UserBO getUserFromView() {
        UserBO transferObject = new UserBO();
        transferObject.setNameShort(nameField.getText().toString());
        transferObject.setLastName(surnameField.getText().toString());
        transferObject.setNif(nifField.getText().toString());
        transferObject.setEmail(emailField.getText().toString());
        transferObject.setMobile(phoneField.getText().toString());
        return transferObject;
    }

    @Override
    public void showProgress() {
        ViewUtils.hideKeyboard(getView(), getActivity());
        showProgressDialog();
    }

    private void showProgressDialog() {
        dialog.setMessage(getString(R.string.loading_dialog));
        dialog.show();
    }

    @Override
    public void hideProgress() {
        dialog.dismiss();
    }

    @Override
    public void onDataSuccessfullyUpdated() {
        Toast.makeText(getActivity(), R.string.profile_data_successfully_updated, Toast.LENGTH_LONG).show();
        navigateToTabsView();
    }

    private void navigateToTabsView() {
        final String eventDestination = "goToHomeFromDataAssignment";
        NavigationUtil.goToNextScreen(DataAssignmentActivity.class.getName(), eventDestination);
    }

    @Override
    public void showRequestError(String error) {
        AlertDialogUtil.showAlertDialog(getActivity(), error);
    }

    @Override
    public void showServerError() {
        AlertDialogUtil.showAlertDialog(getActivity(), R.string.server_error);
    }

    @Override
    public void showOnExpiredTokenError() {
        AlertDialogUtil.showAlertDialogAndCloseSession(getActivity(), R.string.on_confluent_session_error);
    }

}
