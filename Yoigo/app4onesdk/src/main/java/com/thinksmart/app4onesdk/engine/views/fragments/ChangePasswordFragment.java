package com.thinksmart.app4onesdk.engine.views.fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.engine.application.utils.NavigationUtil;
import com.thinksmart.app4onesdk.engine.presenters.IProfileChangePasswordPresenter;
import com.thinksmart.app4onesdk.engine.presenters.imp.ProfileChangePasswordPresenterImp;
import com.thinksmart.app4onesdk.engine.utils.AlertDialogUtil;
import com.thinksmart.app4onesdk.engine.utils.CustomEditTextView;
import com.thinksmart.app4onesdk.engine.views.IProfileChangePasswordView;
import com.thinksmart.app4onesdk.engine.views.activities.ChangePasswordActivity;
import com.thinksmart.app4onesdk.engine.views.utils.SessionManagerUtil;
import com.thinksmart.app4onesdk.engine.views.utils.ViewUtils;

public class ChangePasswordFragment extends Fragment implements IProfileChangePasswordView {

    private ProgressDialog dialog;

    private IProfileChangePasswordPresenter presenter;

    private CustomEditTextView currentPasswordField;
    private CustomEditTextView newPasswordField;
    private CustomEditTextView repeatedPasswordField;
    private Button changePasswordButton;
    private Button goBackButton;

    public ChangePasswordFragment() {
    }

    public static ChangePasswordFragment newInstance() {
        return new ChangePasswordFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new ProfileChangePasswordPresenterImp(this, getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_change_password, container, false);

        initUIReferences(root);
        initEvents();

        return root;
    }

    private void initUIReferences(View root) {
        dialog = new ProgressDialog(getActivity());
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        currentPasswordField = (CustomEditTextView) root.findViewById(R.id.login_cp_current_password_value);
        newPasswordField = (CustomEditTextView) root.findViewById(R.id.login_cp_new_password_value);
        repeatedPasswordField = (CustomEditTextView) root.findViewById(R.id.login_cp_confirm_password_value);
        changePasswordButton = (Button) root.findViewById(R.id.login_cp_change_password_button);
        goBackButton = (Button) root.findViewById(R.id.login_cp_go_back_button);
    }

    private void initEvents() {
        changePasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.hideKeyboard(getView(), getActivity());
                String newPassword = newPasswordField.getText().toString();
                String currentPassword = currentPasswordField.getText().toString();
                if (arePasswordsOk(newPassword, currentPassword)) {
                    presenter.changePassword(newPassword, currentPassword);
                }
            }
        });

        goBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SessionManagerUtil.closeSession(getActivity());
            }
        });
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    private boolean arePasswordsOk(String newPassword, String currentPassword) {
        boolean ok = true;
        String repeatedPassword = repeatedPasswordField.getText().toString();
        if (newPassword.isEmpty() || currentPassword.isEmpty() || repeatedPassword.isEmpty()) {
            Toast.makeText(getActivity(), R.string.all_fields_are_mandatory, Toast.LENGTH_LONG).show();
            ok = false;
        } else if (newPassword.equals(currentPassword)) {
            Toast.makeText(getActivity(), R.string.current_and_new_password_are_equals, Toast.LENGTH_LONG).show();
            ok = false;
        } else if (!repeatedPassword.equals(newPassword)) {
            Toast.makeText(getActivity(), R.string.new_password_and_repeated_pass_not_equals, Toast.LENGTH_LONG).show();
            ok = false;
        } else if (!newPasswordContainsLowerChars(newPassword)) {
            Toast.makeText(getActivity(), R.string.new_password_doesnt_contains_lowercase_chars, Toast.LENGTH_LONG).show();
            ok = false;
        } else if (!newPasswordContainsEnoughChars(newPassword)) {
            Toast.makeText(getActivity(), R.string.new_password_doesnt_contains_enough_chars, Toast.LENGTH_LONG).show();
            ok = false;
        }

        return ok;
    }

    private boolean newPasswordContainsLowerChars(String newPassword) {
        boolean result = false;
        int lowerCase = 0;
        for (int k = 0; k < newPassword.length(); k++) {
            // Check for lowercase letters.
            if (Character.isLowerCase(newPassword.charAt(k))) lowerCase++;
        }
        if (lowerCase >= 4) {
            result = true;
        }
        return result;
    }

    private boolean newPasswordContainsEnoughChars(String newPassword) {
        return newPassword.length() >= 6;
    }

    @Override
    public void showProgress() {
        ViewUtils.hideKeyboard(getView(), getActivity());
        showProgressDialog();
    }

    private void showProgressDialog() {
        dialog.setMessage(getString(R.string.loading_dialog));
        dialog.show();
    }

    @Override
    public void hideProgress() {
        dialog.dismiss();
    }

    @Override
    public void onPasswordSuccessfullyChanged() {
        SessionManagerUtil.resetFingerprintPreferences(getActivity());
        Toast.makeText(getActivity(), R.string.profile_password_successfully_changed, Toast.LENGTH_LONG).show();
        navigateToSelectProfileView();
    }

    private void navigateToSelectProfileView() {
        final String eventDestination = "goToSelectFromChangePassword";
        NavigationUtil.goToNextScreen(ChangePasswordActivity.class.getName(), eventDestination);
    }

    @Override
    public void showRequestError(String error) {
        AlertDialogUtil.showAlertDialog(getActivity(), error);
    }

    @Override
    public void showServerError() {
        AlertDialogUtil.showAlertDialog(getActivity(), R.string.server_error);
    }

    @Override
    public void showOnExpiredTokenError() {
        AlertDialogUtil.showAlertDialogAndCloseSession(getActivity(), R.string.on_confluent_session_error);
    }

}
