package com.thinksmart.app4onesdk.engine.views;

import com.thinksmart.app4onesdk.core.model.bos.CategoriesBO;

/**
 * Created by roberto.demiguel on 17/10/2016.
 */
public interface ICategoriesView {

    /**
     * Shows progress dialog
     */
    void showProgress();

    /**
     * Hides progress dialog
     */
    void hideProgress();

    /**
     * Displays categories in view
     *
     * @param categories - to show
     */
    void showCategories(CategoriesBO categories);

    /**
     * Shows error toast
     */
    void showRequestError(String error);

    /**
     * Shows server error toast
     */
    void showServerError();

    /**
     * Shows on confluent session error toast
     */
    void showOnExpiredTokenError();
}
