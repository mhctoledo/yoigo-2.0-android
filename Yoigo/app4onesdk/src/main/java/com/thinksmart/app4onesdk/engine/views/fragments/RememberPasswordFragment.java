package com.thinksmart.app4onesdk.engine.views.fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.engine.application.utils.NavigationUtil;
import com.thinksmart.app4onesdk.engine.presenters.IRememberPasswordPresenter;
import com.thinksmart.app4onesdk.engine.presenters.imp.RememberPasswordPresenterImp;
import com.thinksmart.app4onesdk.engine.utils.AlertDialogUtil;
import com.thinksmart.app4onesdk.engine.utils.CustomEditTextView;
import com.thinksmart.app4onesdk.engine.views.IRememberPasswordView;
import com.thinksmart.app4onesdk.engine.views.activities.RememberPasswordActivity;
import com.thinksmart.app4onesdk.engine.views.utils.ViewUtils;

public class RememberPasswordFragment extends Fragment implements IRememberPasswordView {

    private ProgressDialog dialog;

    private IRememberPasswordPresenter presenter;

    private Button rememberPassButton;
    private CustomEditTextView userField;

    public RememberPasswordFragment() {
        // Required empty public constructor
    }

    public static RememberPasswordFragment newInstance() {
        return new RememberPasswordFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new RememberPasswordPresenterImp(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_remember_password, container, false);

        initUIReferences(root);
        initEvents();

        return root;
    }

    private void initUIReferences(View root) {
        dialog = new ProgressDialog(getActivity());
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        rememberPassButton = (Button) root.findViewById(R.id.remember_pass_button);
        userField = (CustomEditTextView) root.findViewById(R.id.remember_pass_user_value);
    }

    private void initEvents() {
        rememberPassButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user = userField.getText().toString();
                if (!user.isEmpty()) {
                    presenter.requestPasswordReset(user);
                } else {
                    Toast.makeText(getActivity(), R.string.remember_pass_no_user, Toast.LENGTH_LONG).show();
                }
            }
        });
    }


    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void showProgress() {
        ViewUtils.hideKeyboard(getView(), getActivity());
        showProgressDialog();
    }

    private void showProgressDialog() {
        dialog.setMessage(getString(R.string.loading_dialog));
        dialog.show();
    }

    @Override
    public void hideProgress() {
        dialog.dismiss();
    }

    @Override
    public void showPasswordResetSuccess() {
        Toast.makeText(getActivity(), R.string.remember_pass_successfully_requested, Toast.LENGTH_LONG).show();
        navigateToLoginView();
    }

    private void navigateToLoginView() {
        final String eventDestination = "goToLoginFromRememberPassword";
        NavigationUtil.goToNextScreen(RememberPasswordActivity.class.getName(), eventDestination);
    }

    @Override
    public void showRequestError(String error) {
        AlertDialogUtil.showAlertDialog(getActivity(), error);
    }

    @Override
    public void showServerError() {
        AlertDialogUtil.showAlertDialog(getActivity(), R.string.server_error);
    }


    @Override
    public void showNonExistingUserError() {
        AlertDialogUtil.showAlertDialog(getActivity(), R.string.non_existing_user_error);
    }


}
