package com.thinksmart.app4onesdk.engine.net;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thinksmart.app4onesdk.core.api.ApiResponse;
import com.thinksmart.app4onesdk.engine.interactors.IPresenterListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by roberto.demiguel on 18/10/2016.
 */
public class ResponseMapperUtil {

    public static <T> void parseResponse(JSONObject response, final IPresenterListener listener, final Class<T> objectClass) throws JSONException, IOException {
        if (response.has(ApiResponse.META)) {
            JSONObject meta = (JSONObject) response.get(ApiResponse.META);
            int code = meta.getInt(ApiResponse.CODE);
            if (code == ApiResponse.Code.SUCCESS) {
                T myObject = null;
                if (response.has(ApiResponse.DATA)) {
                    JSONObject dataJSON = (JSONObject) response.get(ApiResponse.DATA);
                    //ignore unknown json properties
                    ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                    myObject = mapper.readValue(dataJSON.toString(), objectClass);
                }
                listener.onSuccess(myObject);
            } else if (code == ApiResponse.Code.EXPIRED_TOKEN) {
                listener.onExpiredToken();
            } else {
                String error = meta.getString(ApiResponse.ERROR_DESCRIPTION);
                listener.onError(code, error);
            }
        } else {
            listener.onError(ApiResponse.Code.ERROR_NO_META_FOUND, ApiResponse.ERROR_NO_META_FOUND);
        }

    }
}
