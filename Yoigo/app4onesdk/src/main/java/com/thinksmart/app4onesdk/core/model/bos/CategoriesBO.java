package com.thinksmart.app4onesdk.core.model.bos;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by roberto.demiguel on 10/11/2016.
 */

public class CategoriesBO {

    @JsonProperty("Grupos")
    private List<GroupBO> groups;

    public CategoriesBO() {
    }

    public List<GroupBO> getGroups() {
        return groups;
    }

    public void setGroups(List<GroupBO> groups) {
        this.groups = groups;
    }

}