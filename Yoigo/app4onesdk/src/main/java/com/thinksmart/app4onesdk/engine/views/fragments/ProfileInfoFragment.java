package com.thinksmart.app4onesdk.engine.views.fragments;


import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.core.model.bos.UserBO;
import com.thinksmart.app4onesdk.engine.application.AppCore;
import com.thinksmart.app4onesdk.engine.application.utils.NavigationUtil;
import com.thinksmart.app4onesdk.engine.views.activities.ProfileInfoActivity;
import com.thinksmart.app4onesdk.engine.views.utils.StringUtils;
import com.thinksmart.app4onesdk.engine.views.utils.ViewUtils;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by roberto.demiguel on 02/11/2016.
 */
public class ProfileInfoFragment extends Fragment {

    //    private FloatingActionMenu menuButton;
//    private FloatingActionButton editProfileDataButton;
//    private FloatingActionButton changePasswordButton;
    private FloatingActionButton changePasswordButtonAux;
    private TextView headerName;
    private TextView headerPosition;
    private CircleImageView headerImage;
    private TextView nameField;
    private TextView surnameField;
    private TextView nifField;
    private TextView emailField;
    private TextView phoneField;

    public ProfileInfoFragment() {
        // Required empty public constructor
    }

    public static ProfileInfoFragment newInstance() {
        return new ProfileInfoFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile_info, container, false);

        initUIReferences(view);
        initEvents();
        populateInfo();

        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        populateInfo();
    }

    private void initUIReferences(View view) {
        headerName = (TextView) getActivity().findViewById(R.id.panel_profile_info_user_label);
        headerPosition = (TextView) getActivity().findViewById(R.id.panel_profile_info_dept_label);
        headerImage = (CircleImageView) getActivity().findViewById(R.id.panel_profile_info_user_image);

        nameField = (TextView) view.findViewById(R.id.profile_info_name_value);
        surnameField = (TextView) view.findViewById(R.id.profile_info_surname_value);
        nifField = (TextView) view.findViewById(R.id.profile_info_nif_value);
        emailField = (TextView) view.findViewById(R.id.profile_info_mail_value);
        phoneField = (TextView) view.findViewById(R.id.profile_info_phone_value);

//TODO not allow showing edit mode
//        menuButton = (FloatingActionMenu) getActivity().findViewById(R.id.menu_button);
//        menuButton.setClosedOnTouchOutside(true);
//        editProfileDataButton = (FloatingActionButton) getActivity().findViewById(R.id.edit_profile_button);
//        changePasswordButton = (FloatingActionButton) getActivity().findViewById(R.id.change_password_button);
        changePasswordButtonAux = (FloatingActionButton) getActivity().findViewById(R.id.change_password_button_aux);
    }

    private void initEvents() {
        changePasswordButtonAux.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewUtils.hideKeyboard(getView(), getActivity());

                final String eventDestination = "goToChangeProfilePasswordFromEditProfile";
                NavigationUtil.goToNextScreen(ProfileInfoActivity.class.getName(), eventDestination);
            }
        });

//TODO not allow showing edit mode
//        editProfileDataButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                ViewUtils.hideKeyboard(getView(), getActivity());
//                menuButton.close(true);
//
//                final String eventDestination = "goToEditProfileFromProfileInfo";
//                NavigationUtil.goToNextScreen(ProfileInfoActivity.class.getName(), eventDestination);
//            }
//        });
//
//        changePasswordButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                ViewUtils.hideKeyboard(getView(), getActivity());
//                menuButton.close(true);
//
//                final String eventDestination = "goToChangeProfilePasswordFromEditProfile";
//                NavigationUtil.goToNextScreen(ProfileInfoActivity.class.getName(), eventDestination);
//            }
//        });
    }

    private void populateInfo() {
        UserBO user = AppCore.getInstance().getSessionManager().getCurrentUser();
        headerName.setText(StringUtils.formatString(user.getName()));
        headerPosition.setText(StringUtils.formatString(user.getPosition()));
        //image
        if (user.getUrlAvatar() != null && !user.getUrlAvatar().isEmpty()) {
            Picasso.with(getActivity()).load(user.getUrlAvatar()).placeholder(getResources().getDrawable(R.drawable.ico_select_user)).fit().centerInside().into(headerImage);
        }

        nameField.setText(StringUtils.formatString(user.getNameShort()));
        surnameField.setText(StringUtils.formatString(user.getLastName()));
        nifField.setText(StringUtils.formatString(user.getNif()));
        emailField.setText(StringUtils.formatString(user.getEmail()));
        phoneField.setText(StringUtils.formatString(user.getMobile()));
    }


}
