package com.thinksmart.app4onesdk.engine.interactors.imp;

import android.support.annotation.NonNull;

import com.thinksmart.app4onesdk.core.api.ApiParameters;
import com.thinksmart.app4onesdk.core.api.ApiResponse;
import com.thinksmart.app4onesdk.core.api.ApiUrls;
import com.thinksmart.app4onesdk.core.model.bos.BasketBO;
import com.thinksmart.app4onesdk.engine.application.AppCore;
import com.thinksmart.app4onesdk.engine.interactors.IConfirmShipmentInteractor;
import com.thinksmart.app4onesdk.engine.interactors.IPresenterListener;
import com.thinksmart.app4onesdk.engine.net.IRequestCallback;
import com.thinksmart.app4onesdk.engine.net.ResponseMapperUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by roberto.demiguel on 04/11/2016.
 */
public class ConfirmShipmentInteractorImp implements IConfirmShipmentInteractor, IRequestCallback {

    private IPresenterListener presenterListener;

    public ConfirmShipmentInteractorImp(IPresenterListener presenterListener) {
        this.presenterListener = presenterListener;
    }

    @Override
    public void getShipment(String token, Integer campaignId, Integer participantId) {
        final JSONObject params = getJsonObject(token, campaignId, participantId);
        AppCore.getInstance().getNetworkManager().postRequest(params, ApiUrls.shipmentURL, this);
    }

    @Override
    public void onRequestSuccess(JSONObject response) {
        try {
            ResponseMapperUtil.parseResponse(response, this.presenterListener, BasketBO.class);
        } catch (JSONException | IOException e) {
            this.presenterListener.onError(ApiResponse.Code.ERROR_PARSING_RESPONSE, ApiResponse.ERROR_PARSING_RESPONSE);
        }
    }

    @Override
    public void onRequestError(String error) {
        presenterListener.onServerError(error);
    }

    @NonNull
    private JSONObject getJsonObject(String token, Integer campaignId, Integer participantId) {
        final JSONObject params = new JSONObject();
        try {
            params.put(ApiParameters.CAMPAIGN_ID, (campaignId != null) ? campaignId : "");
            params.put(ApiParameters.PARTICIPANT_ID, (participantId != null) ? participantId : "");
            params.put(ApiParameters.TOKEN, (token != null) ? token : "");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }
}
