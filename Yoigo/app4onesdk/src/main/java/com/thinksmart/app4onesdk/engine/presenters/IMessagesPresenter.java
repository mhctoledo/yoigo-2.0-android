package com.thinksmart.app4onesdk.engine.presenters;

/**
 * Created by roberto.demiguel on 14/11/2016.
 */
public interface IMessagesPresenter {

    /**
     * Call interector to get messages and display them in view.
     */
    void getMessages();

    /**
     * Destroy view
     */
    void onDestroy();
}
