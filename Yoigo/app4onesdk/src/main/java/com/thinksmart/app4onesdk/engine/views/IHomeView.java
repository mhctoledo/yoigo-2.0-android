package com.thinksmart.app4onesdk.engine.views;

import com.thinksmart.app4onesdk.core.model.bos.SliderBO;
import com.thinksmart.app4onesdk.core.vos.CatalogVO;

import java.util.List;

/**
 * Created by roberto.demiguel on 17/10/2016.
 */
public interface IHomeView {

    /**
     * Shows progress dialog
     */
    void showProgress();

    /**
     * Sets profile info in header and navigation drawer
     */
    void setProfileInfo();

    /**
     * Hides progress dialog
     */
    void hideProgress();

    /**
     * Displays banners in view
     *
     * @param sliderList - to show
     */
    void showBanners(List<SliderBO> sliderList);


    /**
     * Shows error toast
     */
    void showRequestError(String error);

    /**
     * Shows server error toast
     */
    void showServerError();

    /**
     * Shows on confluent session error toast
     */
    void showOnExpiredTokenError();

    void navigateToWebView(String search);

    void navigateToCatalog(CatalogVO catalogVO);

    void navigateToScreen(String destination);
}
