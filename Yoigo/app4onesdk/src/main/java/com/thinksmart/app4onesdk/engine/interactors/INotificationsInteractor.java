package com.thinksmart.app4onesdk.engine.interactors;

/**
 * Created by roberto.demiguel on 13/10/2016.
 */

public interface INotificationsInteractor {

    /**
     * This method creates request parameters, and sends a post request to get notifications. Listener will receive its result.
     */
    void getNotifications(String token, Integer participantId, Integer campaignId);
}
