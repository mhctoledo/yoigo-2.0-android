package com.thinksmart.app4onesdk.engine.views.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thinksmart.app4onesdk.R;
import com.thinksmart.app4onesdk.core.model.bos.PointBO;
import com.thinksmart.app4onesdk.engine.views.holders.PointsTreeNodeHolder;
import com.thinksmart.app4onesdk.engine.views.utils.StringUtils;
import com.unnamed.b.atv.model.TreeNode;
import com.unnamed.b.atv.view.AndroidTreeView;

/**
 * Created by roberto.demiguel on 19/10/2016.
 */
public class PointsDetailFragment extends Fragment {

    private static final String ARG_PARAM = "params";
    private static final String TREE_STATE = "tState";

    private PointBO points;
    private AndroidTreeView tView;
    private TreeNode root;


    public PointsDetailFragment() {
        // Required empty public constructor
    }

    public static PointsDetailFragment newInstance(PointBO points) {
        PointsDetailFragment pointsDetailFragment = new PointsDetailFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM, points);
        pointsDetailFragment.setArguments(args);
        return pointsDetailFragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            points = (PointBO) getArguments().getSerializable(ARG_PARAM);
        }
        root = TreeNode.root();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.fragment_points_detail, container, false);

        fillTree(root, points);

        initUIReferences(rootView);

        //restore state after rotation
        if (savedInstanceState != null) {
            String state = savedInstanceState.getString(TREE_STATE);
            if (!TextUtils.isEmpty(state)) {
                tView.restoreState(state);
            }
        }
        return rootView;
    }

    private void initUIReferences(View rootView) {
        ViewGroup containerView = (ViewGroup) rootView.findViewById(R.id.pd_container);
        tView = new AndroidTreeView(getActivity(), root);

        tView.setDefaultAnimation(true);
        tView.setDefaultContainerStyle(R.style.TreeNodeStyleCustom);
        containerView.addView(tView.getView());
//        tView.setUseAutoToggle(false);
//        tView.expandAll();
    }


    private void fillTree(TreeNode node, PointBO point) {
        if (point.getPoints() != null) {
            for (PointBO aPoint : point.getPoints()) {

                String text = aPoint.getName() + getString(R.string.tree_points_label) + StringUtils.formatPointsWithUnits(aPoint.getValue(), getContext());
                PointsTreeNodeHolder.PointsTreeNodeItem item = new PointsTreeNodeHolder.PointsTreeNodeItem(text);
                TreeNode file = new TreeNode(item).setViewHolder(new PointsTreeNodeHolder(getActivity()));
                node.addChild(file);
                fillTree(file, aPoint);
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(TREE_STATE, tView.getSaveState());
    }

}
